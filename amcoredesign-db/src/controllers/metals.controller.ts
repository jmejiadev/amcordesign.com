import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Metals} from '../models';
import {MetalsRepository} from '../repositories';

export class MetalsController {
  constructor(
    @repository(MetalsRepository)
    public metalsRepository : MetalsRepository,
  ) {}

  @post('/metals')
  @response(200, {
    description: 'Metals model instance',
    content: {'application/json': {schema: getModelSchemaRef(Metals)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Metals, {
            title: 'NewMetals',
            exclude: ['id'],
          }),
        },
      },
    })
    metals: Omit<Metals, 'id'>,
  ): Promise<Metals> {
    return this.metalsRepository.create(metals);
  }

  @get('/metals/count')
  @response(200, {
    description: 'Metals model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Metals) where?: Where<Metals>,
  ): Promise<Count> {
    return this.metalsRepository.count(where);
  }

  @get('/metals')
  @response(200, {
    description: 'Array of Metals model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Metals, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Metals) filter?: Filter<Metals>,
  ): Promise<Metals[]> {
    return this.metalsRepository.find(filter);
  }

  @patch('/metals')
  @response(200, {
    description: 'Metals PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Metals, {partial: true}),
        },
      },
    })
    metals: Metals,
    @param.where(Metals) where?: Where<Metals>,
  ): Promise<Count> {
    return this.metalsRepository.updateAll(metals, where);
  }

  @get('/metals/{id}')
  @response(200, {
    description: 'Metals model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Metals, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Metals, {exclude: 'where'}) filter?: FilterExcludingWhere<Metals>
  ): Promise<Metals> {
    return this.metalsRepository.findById(id, filter);
  }

  @patch('/metals/{id}')
  @response(204, {
    description: 'Metals PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Metals, {partial: true}),
        },
      },
    })
    metals: Metals,
  ): Promise<void> {
    await this.metalsRepository.updateById(id, metals);
  }

  @put('/metals/{id}')
  @response(204, {
    description: 'Metals PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() metals: Metals,
  ): Promise<void> {
    await this.metalsRepository.replaceById(id, metals);
  }

  @del('/metals/{id}')
  @response(204, {
    description: 'Metals DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.metalsRepository.deleteById(id);
  }
}
