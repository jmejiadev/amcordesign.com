// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteTypeSizesSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteTypeSizes {
    @get('/del-type-sizes/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: DeleteTypeSizesSchema } },
    })
    async function(@param.path.number('id') id: number) {
        var arr = Array();
        arr.push("size_type_size");
        arr.push("product_type_size");
        arr.push("order_products_sizes");

        let data = {};
        var extraCondition = "type_size_id = " + id;
        let sql = "";
        arr.forEach(element => {
            sql += 'DELETE FROM ' + element + ' WHERE ' + extraCondition + ";";
        });
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
