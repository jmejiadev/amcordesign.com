import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {StoneCenters} from '../models';
import {StoneCentersRepository} from '../repositories';

export class StoneCentersController {
  constructor(
    @repository(StoneCentersRepository)
    public stoneCentersRepository : StoneCentersRepository,
  ) {}

  @post('/stone-centers')
  @response(200, {
    description: 'StoneCenters model instance',
    content: {'application/json': {schema: getModelSchemaRef(StoneCenters)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneCenters, {
            title: 'NewStoneCenters',
            exclude: ['id'],
          }),
        },
      },
    })
    stoneCenters: Omit<StoneCenters, 'id'>,
  ): Promise<StoneCenters> {
    return this.stoneCentersRepository.create(stoneCenters);
  }

  @get('/stone-centers/count')
  @response(200, {
    description: 'StoneCenters model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StoneCenters) where?: Where<StoneCenters>,
  ): Promise<Count> {
    return this.stoneCentersRepository.count(where);
  }

  @get('/stone-centers')
  @response(200, {
    description: 'Array of StoneCenters model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StoneCenters, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StoneCenters) filter?: Filter<StoneCenters>,
  ): Promise<StoneCenters[]> {
    return this.stoneCentersRepository.find(filter);
  }

  @patch('/stone-centers')
  @response(200, {
    description: 'StoneCenters PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneCenters, {partial: true}),
        },
      },
    })
    stoneCenters: StoneCenters,
    @param.where(StoneCenters) where?: Where<StoneCenters>,
  ): Promise<Count> {
    return this.stoneCentersRepository.updateAll(stoneCenters, where);
  }

  @get('/stone-centers/{id}')
  @response(200, {
    description: 'StoneCenters model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StoneCenters, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StoneCenters, {exclude: 'where'}) filter?: FilterExcludingWhere<StoneCenters>
  ): Promise<StoneCenters> {
    return this.stoneCentersRepository.findById(id, filter);
  }

  @patch('/stone-centers/{id}')
  @response(204, {
    description: 'StoneCenters PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneCenters, {partial: true}),
        },
      },
    })
    stoneCenters: StoneCenters,
  ): Promise<void> {
    await this.stoneCentersRepository.updateById(id, stoneCenters);
  }

  @put('/stone-centers/{id}')
  @response(204, {
    description: 'StoneCenters PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() stoneCenters: StoneCenters,
  ): Promise<void> {
    await this.stoneCentersRepository.replaceById(id, stoneCenters);
  }

  @del('/stone-centers/{id}')
  @response(204, {
    description: 'StoneCenters DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.stoneCentersRepository.deleteById(id);
  }
}
