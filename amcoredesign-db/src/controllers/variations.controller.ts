import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Variations} from '../models';
import {VariationsRepository} from '../repositories';

export class VariationsController {
  constructor(
    @repository(VariationsRepository)
    public variationsRepository : VariationsRepository,
  ) {}

  @post('/variations')
  @response(200, {
    description: 'Variations model instance',
    content: {'application/json': {schema: getModelSchemaRef(Variations)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Variations, {
            title: 'NewVariations',
            exclude: ['id'],
          }),
        },
      },
    })
    variations: Omit<Variations, 'id'>,
  ): Promise<Variations> {
    return this.variationsRepository.create(variations);
  }

  @get('/variations/count')
  @response(200, {
    description: 'Variations model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Variations) where?: Where<Variations>,
  ): Promise<Count> {
    return this.variationsRepository.count(where);
  }

  @get('/variations')
  @response(200, {
    description: 'Array of Variations model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Variations, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Variations) filter?: Filter<Variations>,
  ): Promise<Variations[]> {
    return this.variationsRepository.find(filter);
  }

  @patch('/variations')
  @response(200, {
    description: 'Variations PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Variations, {partial: true}),
        },
      },
    })
    variations: Variations,
    @param.where(Variations) where?: Where<Variations>,
  ): Promise<Count> {
    return this.variationsRepository.updateAll(variations, where);
  }

  @get('/variations/{id}')
  @response(200, {
    description: 'Variations model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Variations, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Variations, {exclude: 'where'}) filter?: FilterExcludingWhere<Variations>
  ): Promise<Variations> {
    return this.variationsRepository.findById(id, filter);
  }

  @patch('/variations/{id}')
  @response(204, {
    description: 'Variations PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Variations, {partial: true}),
        },
      },
    })
    variations: Variations,
  ): Promise<void> {
    await this.variationsRepository.updateById(id, variations);
  }

  @put('/variations/{id}')
  @response(204, {
    description: 'Variations PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() variations: Variations,
  ): Promise<void> {
    await this.variationsRepository.replaceById(id, variations);
  }

  @del('/variations/{id}')
  @response(204, {
    description: 'Variations DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.variationsRepository.deleteById(id);
  }
}
