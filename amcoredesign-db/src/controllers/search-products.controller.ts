import { get, HttpErrors, param, response } from '@loopback/rest';
import { RowDataPacket } from 'mysql2';
import { ProductSimpleDto } from '../types/Dtos';
import { ProductWithImageResult } from '../types/models';
import { connectionPoolPromise, throwRequestError } from '../utils';
import { parseInlineImagesToArray } from '../utils/inlineImagesToArray';
import { selectProductWithThumbs } from '../utils/queries';

export declare const SearchProductsSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

interface IProductWithImageResult extends RowDataPacket, ProductWithImageResult {
}

export class SearchProductsController {

  @get('/search-products')
  @response(200, {
    description: 'Search products',
    content: { 'application/json': { schema: SearchProductsSchema } },
  })
  async function(
    @param.query.string('q') query: string = '',
  ) {
    if (query === '')
      return new HttpErrors.BadRequest('no search query');

    let sql = `
      ${selectProductWithThumbs}
      WHERE 
        MATCH ( 
          products.name, 
          products.local_sku, 
          products.supplier_sku, 
          products.finish, 
          products.setting 
          ) AGAINST ( :query )
      GROUP BY products.id

      LIMIT 50;
    `;

    const namedParams = {
      metal: 1,
      query
    };

    try {
      const [rows, fields] = await connectionPoolPromise.execute<IProductWithImageResult[]>(sql, namedParams);
      const mappedResult: ProductSimpleDto[] = rows.map<ProductSimpleDto>((result: IProductWithImageResult) => {
        return {
          id: result.id,
          name: result.name,
          slug: result.slug,
          sellingPrice: result.sellingPrice,
          discount: +result.discount,
          stoneColorCarat: result.stoneColorCarat,
          stoneColorId: result.stoneColorId,
          imagePreview: parseInlineImagesToArray(result),
        }
      })
      return mappedResult;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }
  }
}
