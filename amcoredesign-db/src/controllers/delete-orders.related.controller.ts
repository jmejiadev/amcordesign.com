// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteOrdersRelatedSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteOrdersRelated {
    @get('/del-orders-related/{id}')
    @response(200, {
        description: 'Orders Related delete ',
        content: { 'application/json': { schema: DeleteOrdersRelatedSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        try {
            let sql = "SELECT * FROM order_products where order_id =" + id;
            console.log(sql);
            let result = await connectionPoolPromise.query(sql);
            sql = "";
            var order_product_info = Array();
            order_product_info.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array = order_product_info[0];
            array.forEach(element => {
                var order_product_id = element.id;
                var arr = Array();
                arr.push("order_products_variants");
                arr.push("order_products_sizes");
                arr.forEach(element => {
                    sql += 'DELETE FROM ' + element + ' WHERE order_product_id = ' + order_product_id + ";";
                });
            });
            var extraCondition = " order_id = " + id;
            sql += 'DELETE FROM order_products WHERE ' + extraCondition ;
            console.log(sql);

            let result2 = connectionPoolPromise.query(sql);
            data = result2;
            return data;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
    }
}
