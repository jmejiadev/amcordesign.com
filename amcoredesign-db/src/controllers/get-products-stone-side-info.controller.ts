// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const ProductsStoneSideSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetProductsStoneSide {
    @get('/get-products-stone-sides/')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: ProductsStoneSideSchema } },
    })
    async function() {
        let data = {};
        var sql = 'SELECT * FROM `product_stone_side` pss INNER JOIN stone_sides ss ON pss.stone_side_id = ss.id;';
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
