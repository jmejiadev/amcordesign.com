// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { RowDataPacket } from 'mysql2';
import { GetGemstoneDto } from '../types/Dtos';
import { GemstoneStatusEnum } from '../types/enums/gemstoneStatusEnum';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const GemstonesSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

interface IGetGemstoneDto extends RowDataPacket, GetGemstoneDto {
}

export class GemstonesController {

  @get('/gemstones')
  @response(200, {
    description: 'Gemstones',
    content: { 'application/json': { schema: GemstonesSchema } },
  })
  async function(
    @param.query.string('stoneShape') stoneShape: string,
    @param.query.number('priceMin') priceMin: number,
    @param.query.number('priceMax') priceMax: number,
    @param.query.number('caratMin') caratMin: number,
    @param.query.number('caratMax') caratMax: number,
    @param.query.number('stoneColor') stoneColor: string,
    @param.array('color', 'query', { type: 'string' }) color: string[] = [],
    @param.array('cut', 'query', { type: 'string' }) cut: string[] = [],
  ) {
    const colorParams = color.map((s, i) => [`color${i}`, s])
    const cutParams = cut.map((s, i) => [`cut${i}`, s])

    let sql = `
      SELECT
        inventory.id,
        inventory.Stock_No as stock_No,
        inventory.Shape as shape,
        inventory.Weight as weight,
        inventory.Color as color,
        inventory.Cut_Grade as cut_Grade,
        inventory.Measurements as measurements,
        '' AS gem_specie,
        inventory.Buy_Price_Total AS price,
        '' AS stone_type,
        '' AS sku,
        inventory.ImageLink as imageLink,
        inventory.VideoLink videoLink
      FROM inventory
      WHERE inventory.status = ${GemstoneStatusEnum.DISPONIBLE}
      ${stoneColor ? ` AND inventory.stone_color_id = :stoneColor` : ''}
      ${stoneShape ? ` AND inventory.Shape = :stoneShape` : ''}
      ${priceMin ? ` AND inventory.Buy_Price_Total >= :priceMin` : ''}
      ${priceMax ? ` AND inventory.Buy_Price_Total <= :priceMax` : ''}
      ${caratMin ? ` AND inventory.Weight >= :caratMin` : ''}
      ${caratMax ? ` AND inventory.Weight <= :caratMax` : ''}
    `;

    //style params
    if (colorParams.length > 0) {
      sql = `
        ${sql}
        AND inventory.Color IN (${colorParams.map(([color]) => `:${color}`).join(',')})
      `
    }

    //setting params
    if (cutParams.length > 0) {
      sql = `
        ${sql}
        AND inventory.Cut_Grade IN (${cutParams.map(([cut]) => `:${cut}`).join(',')})
      `
    }

    const namedParams = {
      stoneColor,
      stoneShape,
      priceMin,
      priceMax,
      caratMin,
      caratMax,
      ...Object.fromEntries(colorParams),
      ...Object.fromEntries(cutParams),
    };

    try {
      const [rows, fields] = await connectionPoolPromise.execute<IGetGemstoneDto[]>(sql, namedParams);
      return rows;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }
  }
}
