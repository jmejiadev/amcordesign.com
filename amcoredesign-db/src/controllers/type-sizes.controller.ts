import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TypeSizes} from '../models';
import {TypeSizesRepository} from '../repositories';

export class TypeSizesController {
  constructor(
    @repository(TypeSizesRepository)
    public typeSizesRepository : TypeSizesRepository,
  ) {}

  @post('/type-sizes')
  @response(200, {
    description: 'TypeSizes model instance',
    content: {'application/json': {schema: getModelSchemaRef(TypeSizes)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TypeSizes, {
            title: 'NewTypeSizes',
            exclude: ['id'],
          }),
        },
      },
    })
    typeSizes: Omit<TypeSizes, 'id'>,
  ): Promise<TypeSizes> {
    return this.typeSizesRepository.create(typeSizes);
  }

  @get('/type-sizes/count')
  @response(200, {
    description: 'TypeSizes model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TypeSizes) where?: Where<TypeSizes>,
  ): Promise<Count> {
    return this.typeSizesRepository.count(where);
  }

  @get('/type-sizes')
  @response(200, {
    description: 'Array of TypeSizes model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TypeSizes, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TypeSizes) filter?: Filter<TypeSizes>,
  ): Promise<TypeSizes[]> {
    return this.typeSizesRepository.find(filter);
  }

  @patch('/type-sizes')
  @response(200, {
    description: 'TypeSizes PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TypeSizes, {partial: true}),
        },
      },
    })
    typeSizes: TypeSizes,
    @param.where(TypeSizes) where?: Where<TypeSizes>,
  ): Promise<Count> {
    return this.typeSizesRepository.updateAll(typeSizes, where);
  }

  @get('/type-sizes/{id}')
  @response(200, {
    description: 'TypeSizes model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TypeSizes, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(TypeSizes, {exclude: 'where'}) filter?: FilterExcludingWhere<TypeSizes>
  ): Promise<TypeSizes> {
    return this.typeSizesRepository.findById(id, filter);
  }

  @patch('/type-sizes/{id}')
  @response(204, {
    description: 'TypeSizes PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TypeSizes, {partial: true}),
        },
      },
    })
    typeSizes: TypeSizes,
  ): Promise<void> {
    await this.typeSizesRepository.updateById(id, typeSizes);
  }

  @put('/type-sizes/{id}')
  @response(204, {
    description: 'TypeSizes PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() typeSizes: TypeSizes,
  ): Promise<void> {
    await this.typeSizesRepository.replaceById(id, typeSizes);
  }

  @del('/type-sizes/{id}')
  @response(204, {
    description: 'TypeSizes DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.typeSizesRepository.deleteById(id);
  }
}
