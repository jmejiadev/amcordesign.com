import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductShoppingCart} from '../models';
import {ProductShoppingCartRepository} from '../repositories';

export class ProductShoppingCartController {
  constructor(
    @repository(ProductShoppingCartRepository)
    public productShoppingCartRepository : ProductShoppingCartRepository,
  ) {}

  @post('/product-shopping-carts')
  @response(200, {
    description: 'ProductShoppingCart model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductShoppingCart)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductShoppingCart, {
            title: 'NewProductShoppingCart',
            exclude: ['id'],
          }),
        },
      },
    })
    productShoppingCart: Omit<ProductShoppingCart, 'id'>,
  ): Promise<ProductShoppingCart> {
    return this.productShoppingCartRepository.create(productShoppingCart);
  }

  @get('/product-shopping-carts/count')
  @response(200, {
    description: 'ProductShoppingCart model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductShoppingCart) where?: Where<ProductShoppingCart>,
  ): Promise<Count> {
    return this.productShoppingCartRepository.count(where);
  }

  @get('/product-shopping-carts')
  @response(200, {
    description: 'Array of ProductShoppingCart model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductShoppingCart, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductShoppingCart) filter?: Filter<ProductShoppingCart>,
  ): Promise<ProductShoppingCart[]> {
    return this.productShoppingCartRepository.find(filter);
  }

  @patch('/product-shopping-carts')
  @response(200, {
    description: 'ProductShoppingCart PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductShoppingCart, {partial: true}),
        },
      },
    })
    productShoppingCart: ProductShoppingCart,
    @param.where(ProductShoppingCart) where?: Where<ProductShoppingCart>,
  ): Promise<Count> {
    return this.productShoppingCartRepository.updateAll(productShoppingCart, where);
  }

  @get('/product-shopping-carts/{id}')
  @response(200, {
    description: 'ProductShoppingCart model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductShoppingCart, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductShoppingCart, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductShoppingCart>
  ): Promise<ProductShoppingCart> {
    return this.productShoppingCartRepository.findById(id, filter);
  }

  @patch('/product-shopping-carts/{id}')
  @response(204, {
    description: 'ProductShoppingCart PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductShoppingCart, {partial: true}),
        },
      },
    })
    productShoppingCart: ProductShoppingCart,
  ): Promise<void> {
    await this.productShoppingCartRepository.updateById(id, productShoppingCart);
  }

  @put('/product-shopping-carts/{id}')
  @response(204, {
    description: 'ProductShoppingCart PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productShoppingCart: ProductShoppingCart,
  ): Promise<void> {
    await this.productShoppingCartRepository.replaceById(id, productShoppingCart);
  }

  @del('/product-shopping-carts/{id}')
  @response(204, {
    description: 'ProductShoppingCart DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productShoppingCartRepository.deleteById(id);
  }
}
