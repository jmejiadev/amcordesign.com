// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const ProductSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class ProductsSubDelete {
    @get('/productsDelSubC/{id}')
    @response(200, {
        description: 'Products deleting info ',
        content: { 'application/json': { schema: ProductSchema } },
    })
    async function(@param.path.string('id') id: string) {
        var condicion = "product_id = " + parseInt(id);
        var tables = [
            'millimeter_product',
            'product_stone_shape',
            'metal_product',
            'product_stone_ctw',
            'product_stone_center',
            'image_metal_products',
            'image_stone_side_products',
            'image_stone_shape_products',
            'image_stone_shape_color_products',
            'image_stone_ctw_products',
            'image_stone_color_products',
            'product_stone_color',
            'product_stone_side',
            'product_type_size',
            'product_variation',
            'category_product'
        ];
        var data = Array();
        var sql = '';
        tables.forEach((element, index) => {
            sql += 'DELETE FROM ' + element + ' WHERE ' + condicion + ";";
        });

        try {
            const [rows, fields] = await connectionPoolPromise.execute(sql);
            return [rows];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));
        }
    }
}
