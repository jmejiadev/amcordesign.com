import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ImageStoneShapeProducts} from '../models';
import {ImageStoneShapeProductsRepository} from '../repositories';

export class ImageStoneShapeProductsController {
  constructor(
    @repository(ImageStoneShapeProductsRepository)
    public imageStoneShapeProductsRepository : ImageStoneShapeProductsRepository,
  ) {}

  @post('/image-stone-shape-products')
  @response(200, {
    description: 'ImageStoneShapeProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(ImageStoneShapeProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneShapeProducts, {
            title: 'NewImageStoneShapeProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    imageStoneShapeProducts: Omit<ImageStoneShapeProducts, 'id'>,
  ): Promise<ImageStoneShapeProducts> {
    return this.imageStoneShapeProductsRepository.create(imageStoneShapeProducts);
  }

  @get('/image-stone-shape-products/count')
  @response(200, {
    description: 'ImageStoneShapeProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ImageStoneShapeProducts) where?: Where<ImageStoneShapeProducts>,
  ): Promise<Count> {
    return this.imageStoneShapeProductsRepository.count(where);
  }

  @get('/image-stone-shape-products')
  @response(200, {
    description: 'Array of ImageStoneShapeProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ImageStoneShapeProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ImageStoneShapeProducts) filter?: Filter<ImageStoneShapeProducts>,
  ): Promise<ImageStoneShapeProducts[]> {
    return this.imageStoneShapeProductsRepository.find(filter);
  }

  @patch('/image-stone-shape-products')
  @response(200, {
    description: 'ImageStoneShapeProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneShapeProducts, {partial: true}),
        },
      },
    })
    imageStoneShapeProducts: ImageStoneShapeProducts,
    @param.where(ImageStoneShapeProducts) where?: Where<ImageStoneShapeProducts>,
  ): Promise<Count> {
    return this.imageStoneShapeProductsRepository.updateAll(imageStoneShapeProducts, where);
  }

  @get('/image-stone-shape-products/{id}')
  @response(200, {
    description: 'ImageStoneShapeProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ImageStoneShapeProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ImageStoneShapeProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<ImageStoneShapeProducts>
  ): Promise<ImageStoneShapeProducts> {
    return this.imageStoneShapeProductsRepository.findById(id, filter);
  }

  @patch('/image-stone-shape-products/{id}')
  @response(204, {
    description: 'ImageStoneShapeProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneShapeProducts, {partial: true}),
        },
      },
    })
    imageStoneShapeProducts: ImageStoneShapeProducts,
  ): Promise<void> {
    await this.imageStoneShapeProductsRepository.updateById(id, imageStoneShapeProducts);
  }

  @put('/image-stone-shape-products/{id}')
  @response(204, {
    description: 'ImageStoneShapeProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() imageStoneShapeProducts: ImageStoneShapeProducts,
  ): Promise<void> {
    await this.imageStoneShapeProductsRepository.replaceById(id, imageStoneShapeProducts);
  }

  @del('/image-stone-shape-products/{id}')
  @response(204, {
    description: 'ImageStoneShapeProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.imageStoneShapeProductsRepository.deleteById(id);
  }
}
