import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ImageMetalProducts} from '../models';
import {ImageMetalProductsRepository} from '../repositories';

export class ImageMetalProductsController {
  constructor(
    @repository(ImageMetalProductsRepository)
    public imageMetalProductsRepository : ImageMetalProductsRepository,
  ) {}

  @post('/image-metal-products')
  @response(200, {
    description: 'ImageMetalProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(ImageMetalProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageMetalProducts, {
            title: 'NewImageMetalProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    imageMetalProducts: Omit<ImageMetalProducts, 'id'>,
  ): Promise<ImageMetalProducts> {
    return this.imageMetalProductsRepository.create(imageMetalProducts);
  }

  @get('/image-metal-products/count')
  @response(200, {
    description: 'ImageMetalProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ImageMetalProducts) where?: Where<ImageMetalProducts>,
  ): Promise<Count> {
    return this.imageMetalProductsRepository.count(where);
  }

  @get('/image-metal-products')
  @response(200, {
    description: 'Array of ImageMetalProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ImageMetalProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ImageMetalProducts) filter?: Filter<ImageMetalProducts>,
  ): Promise<ImageMetalProducts[]> {
    return this.imageMetalProductsRepository.find(filter);
  }

  @patch('/image-metal-products')
  @response(200, {
    description: 'ImageMetalProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageMetalProducts, {partial: true}),
        },
      },
    })
    imageMetalProducts: ImageMetalProducts,
    @param.where(ImageMetalProducts) where?: Where<ImageMetalProducts>,
  ): Promise<Count> {
    return this.imageMetalProductsRepository.updateAll(imageMetalProducts, where);
  }

  @get('/image-metal-products/{id}')
  @response(200, {
    description: 'ImageMetalProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ImageMetalProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ImageMetalProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<ImageMetalProducts>
  ): Promise<ImageMetalProducts> {
    return this.imageMetalProductsRepository.findById(id, filter);
  }

  @patch('/image-metal-products/{id}')
  @response(204, {
    description: 'ImageMetalProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageMetalProducts, {partial: true}),
        },
      },
    })
    imageMetalProducts: ImageMetalProducts,
  ): Promise<void> {
    await this.imageMetalProductsRepository.updateById(id, imageMetalProducts);
  }

  @put('/image-metal-products/{id}')
  @response(204, {
    description: 'ImageMetalProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() imageMetalProducts: ImageMetalProducts,
  ): Promise<void> {
    await this.imageMetalProductsRepository.replaceById(id, imageMetalProducts);
  }

  @del('/image-metal-products/{id}')
  @response(204, {
    description: 'ImageMetalProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.imageMetalProductsRepository.deleteById(id);
  }
}
