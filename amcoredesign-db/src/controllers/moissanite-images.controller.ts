import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {MoissaniteImages} from '../models';
import {MoissaniteImagesRepository} from '../repositories';

export class MoissaniteImagesController {
  constructor(
    @repository(MoissaniteImagesRepository)
    public moissaniteImagesRepository : MoissaniteImagesRepository,
  ) {}

  @post('/moissanite-images')
  @response(200, {
    description: 'MoissaniteImages model instance',
    content: {'application/json': {schema: getModelSchemaRef(MoissaniteImages)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MoissaniteImages, {
            title: 'NewMoissaniteImages',
            exclude: ['id'],
          }),
        },
      },
    })
    moissaniteImages: Omit<MoissaniteImages, 'id'>,
  ): Promise<MoissaniteImages> {
    return this.moissaniteImagesRepository.create(moissaniteImages);
  }

  @get('/moissanite-images/count')
  @response(200, {
    description: 'MoissaniteImages model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(MoissaniteImages) where?: Where<MoissaniteImages>,
  ): Promise<Count> {
    return this.moissaniteImagesRepository.count(where);
  }

  @get('/moissanite-images')
  @response(200, {
    description: 'Array of MoissaniteImages model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(MoissaniteImages, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(MoissaniteImages) filter?: Filter<MoissaniteImages>,
  ): Promise<MoissaniteImages[]> {
    return this.moissaniteImagesRepository.find(filter);
  }

  @patch('/moissanite-images')
  @response(200, {
    description: 'MoissaniteImages PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MoissaniteImages, {partial: true}),
        },
      },
    })
    moissaniteImages: MoissaniteImages,
    @param.where(MoissaniteImages) where?: Where<MoissaniteImages>,
  ): Promise<Count> {
    return this.moissaniteImagesRepository.updateAll(moissaniteImages, where);
  }

  @get('/moissanite-images/{id}')
  @response(200, {
    description: 'MoissaniteImages model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(MoissaniteImages, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(MoissaniteImages, {exclude: 'where'}) filter?: FilterExcludingWhere<MoissaniteImages>
  ): Promise<MoissaniteImages> {
    return this.moissaniteImagesRepository.findById(id, filter);
  }

  @patch('/moissanite-images/{id}')
  @response(204, {
    description: 'MoissaniteImages PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MoissaniteImages, {partial: true}),
        },
      },
    })
    moissaniteImages: MoissaniteImages,
  ): Promise<void> {
    await this.moissaniteImagesRepository.updateById(id, moissaniteImages);
  }

  @put('/moissanite-images/{id}')
  @response(204, {
    description: 'MoissaniteImages PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() moissaniteImages: MoissaniteImages,
  ): Promise<void> {
    await this.moissaniteImagesRepository.replaceById(id, moissaniteImages);
  }

  @del('/moissanite-images/{id}')
  @response(204, {
    description: 'MoissaniteImages DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.moissaniteImagesRepository.deleteById(id);
  }
}
