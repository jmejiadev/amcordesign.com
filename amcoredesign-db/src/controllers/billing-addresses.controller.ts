import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {BillingAddresses} from '../models';
import {BillingAddressesRepository} from '../repositories';

export class BillingAddressesController {
  constructor(
    @repository(BillingAddressesRepository)
    public billingAddressesRepository : BillingAddressesRepository,
  ) {}

  @post('/billing-addresses')
  @response(200, {
    description: 'BillingAddresses model instance',
    content: {'application/json': {schema: getModelSchemaRef(BillingAddresses)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BillingAddresses, {
            title: 'NewBillingAddresses',
            exclude: ['id'],
          }),
        },
      },
    })
    billingAddresses: Omit<BillingAddresses, 'id'>,
  ): Promise<BillingAddresses> {
    return this.billingAddressesRepository.create(billingAddresses);
  }

  @get('/billing-addresses/count')
  @response(200, {
    description: 'BillingAddresses model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(BillingAddresses) where?: Where<BillingAddresses>,
  ): Promise<Count> {
    return this.billingAddressesRepository.count(where);
  }

  @get('/billing-addresses')
  @response(200, {
    description: 'Array of BillingAddresses model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(BillingAddresses, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(BillingAddresses) filter?: Filter<BillingAddresses>,
  ): Promise<BillingAddresses[]> {
    return this.billingAddressesRepository.find(filter);
  }

  @patch('/billing-addresses')
  @response(200, {
    description: 'BillingAddresses PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BillingAddresses, {partial: true}),
        },
      },
    })
    billingAddresses: BillingAddresses,
    @param.where(BillingAddresses) where?: Where<BillingAddresses>,
  ): Promise<Count> {
    return this.billingAddressesRepository.updateAll(billingAddresses, where);
  }

  @get('/billing-addresses/{id}')
  @response(200, {
    description: 'BillingAddresses model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(BillingAddresses, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(BillingAddresses, {exclude: 'where'}) filter?: FilterExcludingWhere<BillingAddresses>
  ): Promise<BillingAddresses> {
    return this.billingAddressesRepository.findById(id, filter);
  }

  @patch('/billing-addresses/{id}')
  @response(204, {
    description: 'BillingAddresses PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BillingAddresses, {partial: true}),
        },
      },
    })
    billingAddresses: BillingAddresses,
  ): Promise<void> {
    await this.billingAddressesRepository.updateById(id, billingAddresses);
  }

  @put('/billing-addresses/{id}')
  @response(204, {
    description: 'BillingAddresses PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() billingAddresses: BillingAddresses,
  ): Promise<void> {
    await this.billingAddressesRepository.replaceById(id, billingAddresses);
  }

  @del('/billing-addresses/{id}')
  @response(204, {
    description: 'BillingAddresses DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.billingAddressesRepository.deleteById(id);
  }
}
