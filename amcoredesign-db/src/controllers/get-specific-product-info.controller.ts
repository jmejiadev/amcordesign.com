// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const GetProductEditSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetSpecificProduct {
    @get('/get-product-edit-info/{id}')
    @response(200, {
        description: 'Get product edit info',
        content: { 'application/json': { schema: GetProductEditSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        const condicion = " WHERE product_id = " + id + " ; ";
        //productInfo
        let sql = "SELECT * FROM `products` WHERE `id` = " + id + "; "
            // extrainfo
            + "SELECT * FROM `stone_centers` ; " //stoneCenters
            + "SELECT * FROM `stone_colors` ; " //stoneColors
            + "SELECT * FROM `stone_shapes` ; " //stoneShapes
            + "SELECT * FROM `stone_sides` ; " //stoneSides
            + "SELECT * FROM `categories` ; " //categories
            + "SELECT * FROM `metals` ; " //metals 
            + "SELECT * FROM `extra_variations` ; " //extraVariations
            + "SELECT * FROM `type_sizes` ; " //typeSizes
            + "SELECT * FROM `variations` ; " //variations
            //product related
            //json.sizes  cambiado por json.productTypeSizes
            + "SELECT * FROM `product_type_size` ptz INNER JOIN type_sizes tz ON ptz.type_size_id = tz.id " + condicion
            //CategoryProducts
            + "SELECT * FROM `category_product` " + condicion
            //metalProducts
            + "SELECT * FROM `metal_product` mp INNER JOIN metals m ON mp.metal_id = m.id " + condicion
            //productStoneCenters
            + "SELECT * FROM `product_stone_center` ps INNER JOIN stone_centers c ON ps.stone_center_id=c.id " + condicion
            //productStoneColors
            + "SELECT ps.*, s.name FROM `product_stone_color` ps INNER JOIN stone_colors s ON ps.stone_color_id = s.id  " + condicion
            //StoneShapes
            + "SELECT * FROM `product_stone_shape` ps INNER JOIN stone_shapes ss ON ss.id=ps.stone_shape_id  " + condicion
            //productStoneSides
            + "SELECT ps.*, s.name FROM `product_stone_side` ps INNER JOIN stone_sides s ON ps.stone_side_id = s.id " + condicion
            //productVariations
            + "SELECT pv.*, ev.name as extraVName FROM `product_variation` pv "
            + "INNER JOIN extra_variations ev ON pv.variation_id = ev.id " + condicion
            /* old query
            + "SELECT pv.*, v.*, ev.name as extraVName FROM `product_variation` pv "
            + "INNER JOIN variations v ON pv.variation_id=v.extra_variation_id "
            + "INNER JOIN extra_variations ev ON v.extra_variation_id = ev.id " + condicion
            */
            //images
            //imgStoneColorProducts
            + "SELECT * FROM `image_stone_color_products` " + condicion
            //imgStoneShapeColorProducts
            + "SELECT * FROM `image_stone_shape_color_products` " + condicion
            //imgStoneShapeProducts
            + "SELECT * FROM `image_stone_shape_products` " + condicion
            //imgStoneSideProducts
            + "SELECT * FROM `image_stone_side_products` " + condicion
            //imgMetalProducts
            + "SELECT * FROM `image_metal_products` " + condicion
            //Threesixties
            + "SELECT ts.path, ss.sku as stoneShapeSku, ss.name as stoneShapeName, mc.description FROM `three_sixties` ts "
            + "INNER JOIN products p on p.id = ts.product_id "
            + "INNER JOIN stone_shapes ss on ss.id = ts.stone_shape_id "
            + "INNER JOIN metal_colors mc on mc.id = ts.metal_color_id "
            + "WHERE ts.product_id = " + id + " ;"
            //Image distinct combinations
            //stoneColorCombination
            + "SELECT DISTINCT `metal_id` as metalId, `stone_color_id` as stoneColorId FROM `image_stone_color_products` " + condicion
            //combinationStoneShape
            + "SELECT DISTINCT `metal_id` as metalId, `stone_shape_id` as stoneShapeId FROM `image_stone_shape_products` " + condicion
            //combinationStoneShapeColor
            + "SELECT DISTINCT `metal_id` as metalId, `stone_shape_id` as stoneShapeId, `stone_color_id` as stoneColorId FROM `image_stone_shape_color_products` " + condicion
            //combinationStoneSide
            + "SELECT DISTINCT `metal_id` as metalId, `stone_side_id` as stoneSideId, `stone_color_id` as stoneColorId FROM `image_stone_side_products` " + condicion
            //combinationMetals
            + "SELECT DISTINCT `metal_id` as metalId  FROM `image_metal_products` " + condicion;
        //console.log(sql);
        try {
            let result = await connectionPoolPromise.query(sql);
            var response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array.push(response[0]);
            let ans = {
                productInfo: array[0][0][0],
                stoneCenters: array[0][1],
                stoneColors: array[0][2],
                stoneShapes: array[0][3],
                stoneSides: array[0][4],
                categories: array[0][5],
                metals: array[0][6],
                extraVariations: array[0][7],
                typeSizes: array[0][8],
                variations: array[0][9],

                productTypeSizes: array[0][10],
                categoryProducts: array[0][11],
                metalProducts: array[0][12],
                productStoneCenters: array[0][13],
                productStoneColors: array[0][14],
                productStoneShapes: array[0][15],
                productStoneSides: array[0][16],
                productVariations: array[0][17],

                imgStoneColorProducts: array[0][18],
                imgStoneShapeColorProducts: array[0][19],
                imgStoneShapeProducts: array[0][20],
                imgStoneSideProducts: array[0][21],
                imgMetalProducts: array[0][22],

                threeSixties: array[0][23],

                combinationImageStoneColor: array[0][24],
                combinationStoneShape: array[0][25],
                combinationStoneShapeColor: array[0][26],
                combinationStoneSide: array[0][27],
                combinationMetals: array[0][28],


            };
            data = ans;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
