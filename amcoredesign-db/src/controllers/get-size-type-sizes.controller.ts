// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const GetSizeTypeSizesSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetSizeTypeSizes {
    @get('/get-size-type-sizes')
    @response(200, {
        description: 'Size Type Sizes',
        content: { 'application/json': { schema: GetSizeTypeSizesSchema } },
    })
    async function() {
        let data = {};
        let sql = "SELECT * FROM `type_sizes`;";
        try {
            let result = await connectionPoolPromise.query(sql);
            var response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array = response[0];
            sql = "";
            array.forEach(element => {
                sql += "SELECT * FROM `size_type_size` stz INNER JOIN sizes s ON stz.size_id= s.id where type_size_id = " + element.id + ";";
            });
            let result1 = await connectionPoolPromise.query(sql);
            var response1 = Array();
            response1.push(Object.values(JSON.parse(JSON.stringify(result1[0]))));
            var array1 = Array();
            array1 = response1[0];
            array.forEach((element, index) => {
                array[index].sizeTypeSizes = array1[index];
            });
            data = array;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
