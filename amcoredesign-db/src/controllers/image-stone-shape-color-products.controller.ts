import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ImageStoneShapeColorProducts} from '../models';
import {ImageStoneShapeColorProductsRepository} from '../repositories';

export class ImageStoneShapeColorProductsController {
  constructor(
    @repository(ImageStoneShapeColorProductsRepository)
    public imageStoneShapeColorProductsRepository : ImageStoneShapeColorProductsRepository,
  ) {}

  @post('/image-stone-shape-color-products')
  @response(200, {
    description: 'ImageStoneShapeColorProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(ImageStoneShapeColorProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneShapeColorProducts, {
            title: 'NewImageStoneShapeColorProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    imageStoneShapeColorProducts: Omit<ImageStoneShapeColorProducts, 'id'>,
  ): Promise<ImageStoneShapeColorProducts> {
    return this.imageStoneShapeColorProductsRepository.create(imageStoneShapeColorProducts);
  }

  @get('/image-stone-shape-color-products/count')
  @response(200, {
    description: 'ImageStoneShapeColorProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ImageStoneShapeColorProducts) where?: Where<ImageStoneShapeColorProducts>,
  ): Promise<Count> {
    return this.imageStoneShapeColorProductsRepository.count(where);
  }

  @get('/image-stone-shape-color-products')
  @response(200, {
    description: 'Array of ImageStoneShapeColorProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ImageStoneShapeColorProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ImageStoneShapeColorProducts) filter?: Filter<ImageStoneShapeColorProducts>,
  ): Promise<ImageStoneShapeColorProducts[]> {
    return this.imageStoneShapeColorProductsRepository.find(filter);
  }

  @patch('/image-stone-shape-color-products')
  @response(200, {
    description: 'ImageStoneShapeColorProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneShapeColorProducts, {partial: true}),
        },
      },
    })
    imageStoneShapeColorProducts: ImageStoneShapeColorProducts,
    @param.where(ImageStoneShapeColorProducts) where?: Where<ImageStoneShapeColorProducts>,
  ): Promise<Count> {
    return this.imageStoneShapeColorProductsRepository.updateAll(imageStoneShapeColorProducts, where);
  }

  @get('/image-stone-shape-color-products/{id}')
  @response(200, {
    description: 'ImageStoneShapeColorProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ImageStoneShapeColorProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ImageStoneShapeColorProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<ImageStoneShapeColorProducts>
  ): Promise<ImageStoneShapeColorProducts> {
    return this.imageStoneShapeColorProductsRepository.findById(id, filter);
  }

  @patch('/image-stone-shape-color-products/{id}')
  @response(204, {
    description: 'ImageStoneShapeColorProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneShapeColorProducts, {partial: true}),
        },
      },
    })
    imageStoneShapeColorProducts: ImageStoneShapeColorProducts,
  ): Promise<void> {
    await this.imageStoneShapeColorProductsRepository.updateById(id, imageStoneShapeColorProducts);
  }

  @put('/image-stone-shape-color-products/{id}')
  @response(204, {
    description: 'ImageStoneShapeColorProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() imageStoneShapeColorProducts: ImageStoneShapeColorProducts,
  ): Promise<void> {
    await this.imageStoneShapeColorProductsRepository.replaceById(id, imageStoneShapeColorProducts);
  }

  @del('/image-stone-shape-color-products/{id}')
  @response(204, {
    description: 'ImageStoneShapeColorProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.imageStoneShapeColorProductsRepository.deleteById(id);
  }
}
