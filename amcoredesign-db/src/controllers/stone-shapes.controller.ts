import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {StoneShapes} from '../models';
import {StoneShapesRepository} from '../repositories';

export class StoneShapesController {
  constructor(
    @repository(StoneShapesRepository)
    public stoneShapesRepository : StoneShapesRepository,
  ) {}

  @post('/stone-shapes')
  @response(200, {
    description: 'StoneShapes model instance',
    content: {'application/json': {schema: getModelSchemaRef(StoneShapes)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneShapes, {
            title: 'NewStoneShapes',
            exclude: ['id'],
          }),
        },
      },
    })
    stoneShapes: Omit<StoneShapes, 'id'>,
  ): Promise<StoneShapes> {
    return this.stoneShapesRepository.create(stoneShapes);
  }

  @get('/stone-shapes/count')
  @response(200, {
    description: 'StoneShapes model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StoneShapes) where?: Where<StoneShapes>,
  ): Promise<Count> {
    return this.stoneShapesRepository.count(where);
  }

  @get('/stone-shapes')
  @response(200, {
    description: 'Array of StoneShapes model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StoneShapes, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StoneShapes) filter?: Filter<StoneShapes>,
  ): Promise<StoneShapes[]> {
    return this.stoneShapesRepository.find(filter);
  }

  @patch('/stone-shapes')
  @response(200, {
    description: 'StoneShapes PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneShapes, {partial: true}),
        },
      },
    })
    stoneShapes: StoneShapes,
    @param.where(StoneShapes) where?: Where<StoneShapes>,
  ): Promise<Count> {
    return this.stoneShapesRepository.updateAll(stoneShapes, where);
  }

  @get('/stone-shapes/{id}')
  @response(200, {
    description: 'StoneShapes model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StoneShapes, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StoneShapes, {exclude: 'where'}) filter?: FilterExcludingWhere<StoneShapes>
  ): Promise<StoneShapes> {
    return this.stoneShapesRepository.findById(id, filter);
  }

  @patch('/stone-shapes/{id}')
  @response(204, {
    description: 'StoneShapes PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneShapes, {partial: true}),
        },
      },
    })
    stoneShapes: StoneShapes,
  ): Promise<void> {
    await this.stoneShapesRepository.updateById(id, stoneShapes);
  }

  @put('/stone-shapes/{id}')
  @response(204, {
    description: 'StoneShapes PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() stoneShapes: StoneShapes,
  ): Promise<void> {
    await this.stoneShapesRepository.replaceById(id, stoneShapes);
  }

  @del('/stone-shapes/{id}')
  @response(204, {
    description: 'StoneShapes DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.stoneShapesRepository.deleteById(id);
  }
}
