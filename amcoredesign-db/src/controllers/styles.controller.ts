import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Styles} from '../models';
import {StylesRepository} from '../repositories';

export class StylesController {
  constructor(
    @repository(StylesRepository)
    public stylesRepository : StylesRepository,
  ) {}

  @post('/styles')
  @response(200, {
    description: 'Styles model instance',
    content: {'application/json': {schema: getModelSchemaRef(Styles)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Styles, {
            title: 'NewStyles',
            exclude: ['id'],
          }),
        },
      },
    })
    styles: Omit<Styles, 'id'>,
  ): Promise<Styles> {
    return this.stylesRepository.create(styles);
  }

  @get('/styles/count')
  @response(200, {
    description: 'Styles model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Styles) where?: Where<Styles>,
  ): Promise<Count> {
    return this.stylesRepository.count(where);
  }

  @get('/styles')
  @response(200, {
    description: 'Array of Styles model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Styles, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Styles) filter?: Filter<Styles>,
  ): Promise<Styles[]> {
    return this.stylesRepository.find(filter);
  }

  @patch('/styles')
  @response(200, {
    description: 'Styles PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Styles, {partial: true}),
        },
      },
    })
    styles: Styles,
    @param.where(Styles) where?: Where<Styles>,
  ): Promise<Count> {
    return this.stylesRepository.updateAll(styles, where);
  }

  @get('/styles/{id}')
  @response(200, {
    description: 'Styles model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Styles, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Styles, {exclude: 'where'}) filter?: FilterExcludingWhere<Styles>
  ): Promise<Styles> {
    return this.stylesRepository.findById(id, filter);
  }

  @patch('/styles/{id}')
  @response(204, {
    description: 'Styles PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Styles, {partial: true}),
        },
      },
    })
    styles: Styles,
  ): Promise<void> {
    await this.stylesRepository.updateById(id, styles);
  }

  @put('/styles/{id}')
  @response(204, {
    description: 'Styles PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() styles: Styles,
  ): Promise<void> {
    await this.stylesRepository.replaceById(id, styles);
  }

  @del('/styles/{id}')
  @response(204, {
    description: 'Styles DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.stylesRepository.deleteById(id);
  }
}
