import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Moissanites} from '../models';
import {MoissanitesRepository} from '../repositories';

export class MoissanitesController {
  constructor(
    @repository(MoissanitesRepository)
    public moissanitesRepository : MoissanitesRepository,
  ) {}

  @post('/moissanites')
  @response(200, {
    description: 'Moissanites model instance',
    content: {'application/json': {schema: getModelSchemaRef(Moissanites)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Moissanites, {
            title: 'NewMoissanites',
            exclude: ['id'],
          }),
        },
      },
    })
    moissanites: Omit<Moissanites, 'id'>,
  ): Promise<Moissanites> {
    return this.moissanitesRepository.create(moissanites);
  }

  @get('/moissanites/count')
  @response(200, {
    description: 'Moissanites model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Moissanites) where?: Where<Moissanites>,
  ): Promise<Count> {
    return this.moissanitesRepository.count(where);
  }

  @get('/moissanites')
  @response(200, {
    description: 'Array of Moissanites model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Moissanites, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Moissanites) filter?: Filter<Moissanites>,
  ): Promise<Moissanites[]> {
    return this.moissanitesRepository.find(filter);
  }

  @patch('/moissanites')
  @response(200, {
    description: 'Moissanites PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Moissanites, {partial: true}),
        },
      },
    })
    moissanites: Moissanites,
    @param.where(Moissanites) where?: Where<Moissanites>,
  ): Promise<Count> {
    return this.moissanitesRepository.updateAll(moissanites, where);
  }

  @get('/moissanites/{id}')
  @response(200, {
    description: 'Moissanites model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Moissanites, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Moissanites, {exclude: 'where'}) filter?: FilterExcludingWhere<Moissanites>
  ): Promise<Moissanites> {
    return this.moissanitesRepository.findById(id, filter);
  }

  @patch('/moissanites/{id}')
  @response(204, {
    description: 'Moissanites PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Moissanites, {partial: true}),
        },
      },
    })
    moissanites: Moissanites,
  ): Promise<void> {
    await this.moissanitesRepository.updateById(id, moissanites);
  }

  @put('/moissanites/{id}')
  @response(204, {
    description: 'Moissanites PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() moissanites: Moissanites,
  ): Promise<void> {
    await this.moissanitesRepository.replaceById(id, moissanites);
  }

  @del('/moissanites/{id}')
  @response(204, {
    description: 'Moissanites DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.moissanitesRepository.deleteById(id);
  }
}
