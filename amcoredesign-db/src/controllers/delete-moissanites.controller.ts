// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteMoissanitesSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteMoissanites {
    @get('/del-moissanites/{id}')
    @response(200, {
        description: 'Moissanites delete ',
        content: { 'application/json': { schema: DeleteMoissanitesSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        var condicion = "id = " + id;
        var condicionImages = "moissanite_id = " +id;
        let sql = 'DELETE FROM moissanite_images WHERE '+condicionImages+";";
        sql += 'DELETE FROM moissanites WHERE '+condicion;
        try { 
            const result = await connectionPoolPromise.query(sql);
            data = result;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
