import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {OrderProducts} from '../models';
import {OrderProductsRepository} from '../repositories';

export class OrderProductsController {
  constructor(
    @repository(OrderProductsRepository)
    public orderProductsRepository : OrderProductsRepository,
  ) {}

  @post('/order-products')
  @response(200, {
    description: 'OrderProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(OrderProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProducts, {
            title: 'NewOrderProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    orderProducts: Omit<OrderProducts, 'id'>,
  ): Promise<OrderProducts> {
    return this.orderProductsRepository.create(orderProducts);
  }

  @get('/order-products/count')
  @response(200, {
    description: 'OrderProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(OrderProducts) where?: Where<OrderProducts>,
  ): Promise<Count> {
    return this.orderProductsRepository.count(where);
  }

  @get('/order-products')
  @response(200, {
    description: 'Array of OrderProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(OrderProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(OrderProducts) filter?: Filter<OrderProducts>,
  ): Promise<OrderProducts[]> {
    return this.orderProductsRepository.find(filter);
  }

  @patch('/order-products')
  @response(200, {
    description: 'OrderProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProducts, {partial: true}),
        },
      },
    })
    orderProducts: OrderProducts,
    @param.where(OrderProducts) where?: Where<OrderProducts>,
  ): Promise<Count> {
    return this.orderProductsRepository.updateAll(orderProducts, where);
  }

  @get('/order-products/{id}')
  @response(200, {
    description: 'OrderProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(OrderProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(OrderProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<OrderProducts>
  ): Promise<OrderProducts> {
    return this.orderProductsRepository.findById(id, filter);
  }

  @patch('/order-products/{id}')
  @response(204, {
    description: 'OrderProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProducts, {partial: true}),
        },
      },
    })
    orderProducts: OrderProducts,
  ): Promise<void> {
    await this.orderProductsRepository.updateById(id, orderProducts);
  }

  @put('/order-products/{id}')
  @response(204, {
    description: 'OrderProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() orderProducts: OrderProducts,
  ): Promise<void> {
    await this.orderProductsRepository.replaceById(id, orderProducts);
  }

  @del('/order-products/{id}')
  @response(204, {
    description: 'OrderProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderProductsRepository.deleteById(id);
  }
}
