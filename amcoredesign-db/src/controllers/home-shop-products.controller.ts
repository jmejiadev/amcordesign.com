import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {HomeShopProducts} from '../models';
import {HomeShopProductsRepository} from '../repositories';

export class HomeShopProductsController {
  constructor(
    @repository(HomeShopProductsRepository)
    public homeShopProductsRepository : HomeShopProductsRepository,
  ) {}

  @post('/home-shop-products')
  @response(200, {
    description: 'HomeShopProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(HomeShopProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HomeShopProducts, {
            title: 'NewHomeShopProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    homeShopProducts: Omit<HomeShopProducts, 'id'>,
  ): Promise<HomeShopProducts> {
    return this.homeShopProductsRepository.create(homeShopProducts);
  }

  @get('/home-shop-products/count')
  @response(200, {
    description: 'HomeShopProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(HomeShopProducts) where?: Where<HomeShopProducts>,
  ): Promise<Count> {
    return this.homeShopProductsRepository.count(where);
  }

  @get('/home-shop-products')
  @response(200, {
    description: 'Array of HomeShopProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(HomeShopProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(HomeShopProducts) filter?: Filter<HomeShopProducts>,
  ): Promise<HomeShopProducts[]> {
    return this.homeShopProductsRepository.find(filter);
  }

  @patch('/home-shop-products')
  @response(200, {
    description: 'HomeShopProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HomeShopProducts, {partial: true}),
        },
      },
    })
    homeShopProducts: HomeShopProducts,
    @param.where(HomeShopProducts) where?: Where<HomeShopProducts>,
  ): Promise<Count> {
    return this.homeShopProductsRepository.updateAll(homeShopProducts, where);
  }

  @get('/home-shop-products/{id}')
  @response(200, {
    description: 'HomeShopProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(HomeShopProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(HomeShopProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<HomeShopProducts>
  ): Promise<HomeShopProducts> {
    return this.homeShopProductsRepository.findById(id, filter);
  }

  @patch('/home-shop-products/{id}')
  @response(204, {
    description: 'HomeShopProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HomeShopProducts, {partial: true}),
        },
      },
    })
    homeShopProducts: HomeShopProducts,
  ): Promise<void> {
    await this.homeShopProductsRepository.updateById(id, homeShopProducts);
  }

  @put('/home-shop-products/{id}')
  @response(204, {
    description: 'HomeShopProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() homeShopProducts: HomeShopProducts,
  ): Promise<void> {
    await this.homeShopProductsRepository.replaceById(id, homeShopProducts);
  }

  @del('/home-shop-products/{id}')
  @response(204, {
    description: 'HomeShopProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.homeShopProductsRepository.deleteById(id);
  }
}
