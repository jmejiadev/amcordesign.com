import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CenterShapeCtws} from '../models';
import {CenterShapeCtwsRepository} from '../repositories';

export class CenterShapeCtwsController {
  constructor(
    @repository(CenterShapeCtwsRepository)
    public centerShapeCtwsRepository : CenterShapeCtwsRepository,
  ) {}

  @post('/center-shape-ctws')
  @response(200, {
    description: 'CenterShapeCtws model instance',
    content: {'application/json': {schema: getModelSchemaRef(CenterShapeCtws)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtws, {
            title: 'NewCenterShapeCtws',
            exclude: ['id'],
          }),
        },
      },
    })
    centerShapeCtws: Omit<CenterShapeCtws, 'id'>,
  ): Promise<CenterShapeCtws> {
    return this.centerShapeCtwsRepository.create(centerShapeCtws);
  }

  @get('/center-shape-ctws/count')
  @response(200, {
    description: 'CenterShapeCtws model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CenterShapeCtws) where?: Where<CenterShapeCtws>,
  ): Promise<Count> {
    return this.centerShapeCtwsRepository.count(where);
  }

  @get('/center-shape-ctws')
  @response(200, {
    description: 'Array of CenterShapeCtws model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CenterShapeCtws, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CenterShapeCtws) filter?: Filter<CenterShapeCtws>,
  ): Promise<CenterShapeCtws[]> {
    return this.centerShapeCtwsRepository.find(filter);
  }

  @patch('/center-shape-ctws')
  @response(200, {
    description: 'CenterShapeCtws PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtws, {partial: true}),
        },
      },
    })
    centerShapeCtws: CenterShapeCtws,
    @param.where(CenterShapeCtws) where?: Where<CenterShapeCtws>,
  ): Promise<Count> {
    return this.centerShapeCtwsRepository.updateAll(centerShapeCtws, where);
  }

  @get('/center-shape-ctws/{id}')
  @response(200, {
    description: 'CenterShapeCtws model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CenterShapeCtws, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CenterShapeCtws, {exclude: 'where'}) filter?: FilterExcludingWhere<CenterShapeCtws>
  ): Promise<CenterShapeCtws> {
    return this.centerShapeCtwsRepository.findById(id, filter);
  }

  @patch('/center-shape-ctws/{id}')
  @response(204, {
    description: 'CenterShapeCtws PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtws, {partial: true}),
        },
      },
    })
    centerShapeCtws: CenterShapeCtws,
  ): Promise<void> {
    await this.centerShapeCtwsRepository.updateById(id, centerShapeCtws);
  }

  @put('/center-shape-ctws/{id}')
  @response(204, {
    description: 'CenterShapeCtws PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() centerShapeCtws: CenterShapeCtws,
  ): Promise<void> {
    await this.centerShapeCtwsRepository.replaceById(id, centerShapeCtws);
  }

  @del('/center-shape-ctws/{id}')
  @response(204, {
    description: 'CenterShapeCtws DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.centerShapeCtwsRepository.deleteById(id);
  }
}
