import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Sizes} from '../models';
import {SizesRepository} from '../repositories';

export class SizesController {
  constructor(
    @repository(SizesRepository)
    public sizesRepository : SizesRepository,
  ) {}

  @post('/sizes')
  @response(200, {
    description: 'Sizes model instance',
    content: {'application/json': {schema: getModelSchemaRef(Sizes)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Sizes, {
            title: 'NewSizes',
            exclude: ['id'],
          }),
        },
      },
    })
    sizes: Omit<Sizes, 'id'>,
  ): Promise<Sizes> {
    return this.sizesRepository.create(sizes);
  }

  @get('/sizes/count')
  @response(200, {
    description: 'Sizes model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Sizes) where?: Where<Sizes>,
  ): Promise<Count> {
    return this.sizesRepository.count(where);
  }

  @get('/sizes')
  @response(200, {
    description: 'Array of Sizes model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Sizes, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Sizes) filter?: Filter<Sizes>,
  ): Promise<Sizes[]> {
    return this.sizesRepository.find(filter);
  }

  @patch('/sizes')
  @response(200, {
    description: 'Sizes PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Sizes, {partial: true}),
        },
      },
    })
    sizes: Sizes,
    @param.where(Sizes) where?: Where<Sizes>,
  ): Promise<Count> {
    return this.sizesRepository.updateAll(sizes, where);
  }

  @get('/sizes/{id}')
  @response(200, {
    description: 'Sizes model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Sizes, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Sizes, {exclude: 'where'}) filter?: FilterExcludingWhere<Sizes>
  ): Promise<Sizes> {
    return this.sizesRepository.findById(id, filter);
  }

  @patch('/sizes/{id}')
  @response(204, {
    description: 'Sizes PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Sizes, {partial: true}),
        },
      },
    })
    sizes: Sizes,
  ): Promise<void> {
    await this.sizesRepository.updateById(id, sizes);
  }

  @put('/sizes/{id}')
  @response(204, {
    description: 'Sizes PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() sizes: Sizes,
  ): Promise<void> {
    await this.sizesRepository.replaceById(id, sizes);
  }

  @del('/sizes/{id}')
  @response(204, {
    description: 'Sizes DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.sizesRepository.deleteById(id);
  }
}
