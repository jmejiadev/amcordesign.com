import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CenterShapeCtwStoneCenter} from '../models';
import {CenterShapeCtwStoneCenterRepository} from '../repositories';

export class CenterShapeCtwStoneCenterController {
  constructor(
    @repository(CenterShapeCtwStoneCenterRepository)
    public centerShapeCtwStoneCenterRepository : CenterShapeCtwStoneCenterRepository,
  ) {}

  @post('/center-shape-ctw-stone-centers')
  @response(200, {
    description: 'CenterShapeCtwStoneCenter model instance',
    content: {'application/json': {schema: getModelSchemaRef(CenterShapeCtwStoneCenter)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtwStoneCenter, {
            title: 'NewCenterShapeCtwStoneCenter',
            exclude: ['id'],
          }),
        },
      },
    })
    centerShapeCtwStoneCenter: Omit<CenterShapeCtwStoneCenter, 'id'>,
  ): Promise<CenterShapeCtwStoneCenter> {
    return this.centerShapeCtwStoneCenterRepository.create(centerShapeCtwStoneCenter);
  }

  @get('/center-shape-ctw-stone-centers/count')
  @response(200, {
    description: 'CenterShapeCtwStoneCenter model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CenterShapeCtwStoneCenter) where?: Where<CenterShapeCtwStoneCenter>,
  ): Promise<Count> {
    return this.centerShapeCtwStoneCenterRepository.count(where);
  }

  @get('/center-shape-ctw-stone-centers')
  @response(200, {
    description: 'Array of CenterShapeCtwStoneCenter model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CenterShapeCtwStoneCenter, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CenterShapeCtwStoneCenter) filter?: Filter<CenterShapeCtwStoneCenter>,
  ): Promise<CenterShapeCtwStoneCenter[]> {
    return this.centerShapeCtwStoneCenterRepository.find(filter);
  }

  @patch('/center-shape-ctw-stone-centers')
  @response(200, {
    description: 'CenterShapeCtwStoneCenter PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtwStoneCenter, {partial: true}),
        },
      },
    })
    centerShapeCtwStoneCenter: CenterShapeCtwStoneCenter,
    @param.where(CenterShapeCtwStoneCenter) where?: Where<CenterShapeCtwStoneCenter>,
  ): Promise<Count> {
    return this.centerShapeCtwStoneCenterRepository.updateAll(centerShapeCtwStoneCenter, where);
  }

  @get('/center-shape-ctw-stone-centers/{id}')
  @response(200, {
    description: 'CenterShapeCtwStoneCenter model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CenterShapeCtwStoneCenter, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CenterShapeCtwStoneCenter, {exclude: 'where'}) filter?: FilterExcludingWhere<CenterShapeCtwStoneCenter>
  ): Promise<CenterShapeCtwStoneCenter> {
    return this.centerShapeCtwStoneCenterRepository.findById(id, filter);
  }

  @patch('/center-shape-ctw-stone-centers/{id}')
  @response(204, {
    description: 'CenterShapeCtwStoneCenter PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtwStoneCenter, {partial: true}),
        },
      },
    })
    centerShapeCtwStoneCenter: CenterShapeCtwStoneCenter,
  ): Promise<void> {
    await this.centerShapeCtwStoneCenterRepository.updateById(id, centerShapeCtwStoneCenter);
  }

  @put('/center-shape-ctw-stone-centers/{id}')
  @response(204, {
    description: 'CenterShapeCtwStoneCenter PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() centerShapeCtwStoneCenter: CenterShapeCtwStoneCenter,
  ): Promise<void> {
    await this.centerShapeCtwStoneCenterRepository.replaceById(id, centerShapeCtwStoneCenter);
  }

  @del('/center-shape-ctw-stone-centers/{id}')
  @response(204, {
    description: 'CenterShapeCtwStoneCenter DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.centerShapeCtwStoneCenterRepository.deleteById(id);
  }
}
