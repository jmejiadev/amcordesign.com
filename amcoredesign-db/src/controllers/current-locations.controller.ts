import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CurrentLocations} from '../models';
import {CurrentLocationsRepository} from '../repositories';

export class CurrentLocationsController {
  constructor(
    @repository(CurrentLocationsRepository)
    public currentLocationsRepository : CurrentLocationsRepository,
  ) {}

  @post('/current-locations')
  @response(200, {
    description: 'CurrentLocations model instance',
    content: {'application/json': {schema: getModelSchemaRef(CurrentLocations)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CurrentLocations, {
            title: 'NewCurrentLocations',
            exclude: ['id'],
          }),
        },
      },
    })
    currentLocations: Omit<CurrentLocations, 'id'>,
  ): Promise<CurrentLocations> {
    return this.currentLocationsRepository.create(currentLocations);
  }

  @get('/current-locations/count')
  @response(200, {
    description: 'CurrentLocations model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CurrentLocations) where?: Where<CurrentLocations>,
  ): Promise<Count> {
    return this.currentLocationsRepository.count(where);
  }

  @get('/current-locations')
  @response(200, {
    description: 'Array of CurrentLocations model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CurrentLocations, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CurrentLocations) filter?: Filter<CurrentLocations>,
  ): Promise<CurrentLocations[]> {
    return this.currentLocationsRepository.find(filter);
  }

  @patch('/current-locations')
  @response(200, {
    description: 'CurrentLocations PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CurrentLocations, {partial: true}),
        },
      },
    })
    currentLocations: CurrentLocations,
    @param.where(CurrentLocations) where?: Where<CurrentLocations>,
  ): Promise<Count> {
    return this.currentLocationsRepository.updateAll(currentLocations, where);
  }

  @get('/current-locations/{id}')
  @response(200, {
    description: 'CurrentLocations model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CurrentLocations, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CurrentLocations, {exclude: 'where'}) filter?: FilterExcludingWhere<CurrentLocations>
  ): Promise<CurrentLocations> {
    return this.currentLocationsRepository.findById(id, filter);
  }

  @patch('/current-locations/{id}')
  @response(204, {
    description: 'CurrentLocations PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CurrentLocations, {partial: true}),
        },
      },
    })
    currentLocations: CurrentLocations,
  ): Promise<void> {
    await this.currentLocationsRepository.updateById(id, currentLocations);
  }

  @put('/current-locations/{id}')
  @response(204, {
    description: 'CurrentLocations PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() currentLocations: CurrentLocations,
  ): Promise<void> {
    await this.currentLocationsRepository.replaceById(id, currentLocations);
  }

  @del('/current-locations/{id}')
  @response(204, {
    description: 'CurrentLocations DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.currentLocationsRepository.deleteById(id);
  }
}
