import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductStoneCenter} from '../models';
import {ProductStoneCenterRepository} from '../repositories';

export class ProductStoneCenterController {
  constructor(
    @repository(ProductStoneCenterRepository)
    public productStoneCenterRepository : ProductStoneCenterRepository,
  ) {}

  @post('/product-stone-centers')
  @response(200, {
    description: 'ProductStoneCenter model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductStoneCenter)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneCenter, {
            title: 'NewProductStoneCenter',
            exclude: ['id'],
          }),
        },
      },
    })
    productStoneCenter: Omit<ProductStoneCenter, 'id'>,
  ): Promise<ProductStoneCenter> {
    return this.productStoneCenterRepository.create(productStoneCenter);
  }

  @get('/product-stone-centers/count')
  @response(200, {
    description: 'ProductStoneCenter model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductStoneCenter) where?: Where<ProductStoneCenter>,
  ): Promise<Count> {
    return this.productStoneCenterRepository.count(where);
  }

  @get('/product-stone-centers')
  @response(200, {
    description: 'Array of ProductStoneCenter model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductStoneCenter, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductStoneCenter) filter?: Filter<ProductStoneCenter>,
  ): Promise<ProductStoneCenter[]> {
    return this.productStoneCenterRepository.find(filter);
  }

  @patch('/product-stone-centers')
  @response(200, {
    description: 'ProductStoneCenter PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneCenter, {partial: true}),
        },
      },
    })
    productStoneCenter: ProductStoneCenter,
    @param.where(ProductStoneCenter) where?: Where<ProductStoneCenter>,
  ): Promise<Count> {
    return this.productStoneCenterRepository.updateAll(productStoneCenter, where);
  }

  @get('/product-stone-centers/{id}')
  @response(200, {
    description: 'ProductStoneCenter model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductStoneCenter, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductStoneCenter, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductStoneCenter>
  ): Promise<ProductStoneCenter> {
    return this.productStoneCenterRepository.findById(id, filter);
  }

  @patch('/product-stone-centers/{id}')
  @response(204, {
    description: 'ProductStoneCenter PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneCenter, {partial: true}),
        },
      },
    })
    productStoneCenter: ProductStoneCenter,
  ): Promise<void> {
    await this.productStoneCenterRepository.updateById(id, productStoneCenter);
  }

  @put('/product-stone-centers/{id}')
  @response(204, {
    description: 'ProductStoneCenter PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productStoneCenter: ProductStoneCenter,
  ): Promise<void> {
    await this.productStoneCenterRepository.replaceById(id, productStoneCenter);
  }

  @del('/product-stone-centers/{id}')
  @response(204, {
    description: 'ProductStoneCenter DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productStoneCenterRepository.deleteById(id);
  }
}
