// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const GetDistinctCenterShapeImgsSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetDistinctCenterShapeImgs {
    @get('/get-distinct-center-shape-imgs/{id}')
    @response(200, {
        description: 'Size Type Sizes',
        content: { 'application/json': { schema: GetDistinctCenterShapeImgsSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        let sql = "SELECT DISTINCT `metal_id`,`stone_shape_id`,`stone_center_id` FROM `image_center_shape_ctws` WHERE `center_shape_ctw_id` = " + id + "; "
        try {
            let result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
