import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {GoogleCategories} from '../models';
import {GoogleCategoriesRepository} from '../repositories';

export class GoogleCategoriesController {
  constructor(
    @repository(GoogleCategoriesRepository)
    public googleCategoriesRepository : GoogleCategoriesRepository,
  ) {}

  @post('/google-categories')
  @response(200, {
    description: 'GoogleCategories model instance',
    content: {'application/json': {schema: getModelSchemaRef(GoogleCategories)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GoogleCategories, {
            title: 'NewGoogleCategories',
            exclude: ['id'],
          }),
        },
      },
    })
    googleCategories: Omit<GoogleCategories, 'id'>,
  ): Promise<GoogleCategories> {
    return this.googleCategoriesRepository.create(googleCategories);
  }

  @get('/google-categories/count')
  @response(200, {
    description: 'GoogleCategories model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(GoogleCategories) where?: Where<GoogleCategories>,
  ): Promise<Count> {
    return this.googleCategoriesRepository.count(where);
  }

  @get('/google-categories')
  @response(200, {
    description: 'Array of GoogleCategories model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(GoogleCategories, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(GoogleCategories) filter?: Filter<GoogleCategories>,
  ): Promise<GoogleCategories[]> {
    return this.googleCategoriesRepository.find(filter);
  }

  @patch('/google-categories')
  @response(200, {
    description: 'GoogleCategories PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GoogleCategories, {partial: true}),
        },
      },
    })
    googleCategories: GoogleCategories,
    @param.where(GoogleCategories) where?: Where<GoogleCategories>,
  ): Promise<Count> {
    return this.googleCategoriesRepository.updateAll(googleCategories, where);
  }

  @get('/google-categories/{id}')
  @response(200, {
    description: 'GoogleCategories model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(GoogleCategories, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(GoogleCategories, {exclude: 'where'}) filter?: FilterExcludingWhere<GoogleCategories>
  ): Promise<GoogleCategories> {
    return this.googleCategoriesRepository.findById(id, filter);
  }

  @patch('/google-categories/{id}')
  @response(204, {
    description: 'GoogleCategories PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GoogleCategories, {partial: true}),
        },
      },
    })
    googleCategories: GoogleCategories,
  ): Promise<void> {
    await this.googleCategoriesRepository.updateById(id, googleCategories);
  }

  @put('/google-categories/{id}')
  @response(204, {
    description: 'GoogleCategories PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() googleCategories: GoogleCategories,
  ): Promise<void> {
    await this.googleCategoriesRepository.replaceById(id, googleCategories);
  }

  @del('/google-categories/{id}')
  @response(204, {
    description: 'GoogleCategories DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.googleCategoriesRepository.deleteById(id);
  }
}
