import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {OrderPaypal} from '../models';
import {OrderPaypalRepository} from '../repositories';

export class OrderPaypalController {
  constructor(
    @repository(OrderPaypalRepository)
    public orderPaypalRepository : OrderPaypalRepository,
  ) {}

  @post('/order-paypals')
  @response(200, {
    description: 'OrderPaypal model instance',
    content: {'application/json': {schema: getModelSchemaRef(OrderPaypal)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderPaypal, {
            title: 'NewOrderPaypal',
            exclude: ['id'],
          }),
        },
      },
    })
    orderPaypal: Omit<OrderPaypal, 'id'>,
  ): Promise<OrderPaypal> {
    return this.orderPaypalRepository.create(orderPaypal);
  }

  @get('/order-paypals/count')
  @response(200, {
    description: 'OrderPaypal model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(OrderPaypal) where?: Where<OrderPaypal>,
  ): Promise<Count> {
    return this.orderPaypalRepository.count(where);
  }

  @get('/order-paypals')
  @response(200, {
    description: 'Array of OrderPaypal model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(OrderPaypal, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(OrderPaypal) filter?: Filter<OrderPaypal>,
  ): Promise<OrderPaypal[]> {
    return this.orderPaypalRepository.find(filter);
  }

  @patch('/order-paypals')
  @response(200, {
    description: 'OrderPaypal PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderPaypal, {partial: true}),
        },
      },
    })
    orderPaypal: OrderPaypal,
    @param.where(OrderPaypal) where?: Where<OrderPaypal>,
  ): Promise<Count> {
    return this.orderPaypalRepository.updateAll(orderPaypal, where);
  }

  @get('/order-paypals/{id}')
  @response(200, {
    description: 'OrderPaypal model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(OrderPaypal, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(OrderPaypal, {exclude: 'where'}) filter?: FilterExcludingWhere<OrderPaypal>
  ): Promise<OrderPaypal> {
    return this.orderPaypalRepository.findById(id, filter);
  }

  @patch('/order-paypals/{id}')
  @response(204, {
    description: 'OrderPaypal PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderPaypal, {partial: true}),
        },
      },
    })
    orderPaypal: OrderPaypal,
  ): Promise<void> {
    await this.orderPaypalRepository.updateById(id, orderPaypal);
  }

  @put('/order-paypals/{id}')
  @response(204, {
    description: 'OrderPaypal PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() orderPaypal: OrderPaypal,
  ): Promise<void> {
    await this.orderPaypalRepository.replaceById(id, orderPaypal);
  }

  @del('/order-paypals/{id}')
  @response(204, {
    description: 'OrderPaypal DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderPaypalRepository.deleteById(id);
  }
}
