// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const GetOrderSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetOrder {
    @get('/get-full-order-info/{id}')
    @response(200, {
        description: 'Size Type Sizes',
        content: { 'application/json': { schema: GetOrderSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        //order
        let sql = "SELECT * FROM `orders` o WHERE o.`id` = " + id + "; "
            //trackings
            + "SELECT t.* FROM `orders` o "
            + "INNER JOIN trackings t ON o.id = t.order_id WHERE o.`id` = " + id + "; "
            //order paypal
            + "SELECT opay.* FROM `orders` o "
            + "INNER JOIN order_paypal opay ON o.id = opay.order_id WHERE o.`id` = " + id + "; "
            //order products
            + "SELECT opro.*, p.manufacturer, psc.price as stone_color_price , i.Buy_Price_Total as gemstonePrice FROM `orders` o "
            + "LEFT  JOIN order_products opro ON o.id = opro.order_id "
            + "LEFT  JOIN products p on opro.product_id = p.id "
            + "LEFT  JOIN product_stone_color psc on opro.product_id = psc.product_id and opro.stone_color_id = psc.stone_color_id  "  
            + "LEFT  JOIN inventory i on opro.gemstone_id = i.id "
            + "WHERE o.`id` = " + id + "; "
            //order products sizes 
            + "SELECT tz.*, opro.order_id, oprs.* FROM `orders` o "
            + "INNER JOIN order_products opro ON o.id = opro.order_id "
            + "INNER JOIN order_products_sizes oprs ON  opro.id = oprs.order_product_id "
            + "INNER JOIN type_sizes tz ON tz.id = oprs.type_size_id  WHERE o.`id` = " + id + "; "
            //order products variants 
            + "SELECT opro.order_id, oprv.* FROM `orders` o "
            + "INNER JOIN order_products opro ON o.id = opro.order_id "
            + "INNER JOIN order_products_variants oprv ON  opro.id = oprv.order_product_id WHERE o.`id` = " + id + "; "
            //order inventory 
            + "SELECT inv.* FROM `orders` o "
            + "INNER JOIN order_products opro ON o.id = opro.order_id "
            + "INNER JOIN inventory inv ON inv.id = opro.gemstone_id WHERE o.`id` = " + id + "; ";
        //console.log(sql);
        try {
            let result = await connectionPoolPromise.query(sql);
            var response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array.push(response[0]);
            let ans = {
                order: array[0][0][0],
                trackings: array[0][1],
                orderPaypal: array[0][2],
                orderProducts: array[0][3],
                orderProductsSizes: array[0][4],
                orderProductsVariants: array[0][5],
                inventory: array[0][6],
            };
            data = ans;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
