// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteCouponsSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteCoupons {
    @get('/del-coupons/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: DeleteCouponsSchema } },
    })
    async function(@param.path.number('id') id: number) {
        var arr = Array();
        arr.push("coupon_product");

        let data = {};
        var extraCondition = "coupon_id = " + id;
        let sql = "";
        arr.forEach(element => {
            sql += 'DELETE FROM ' + element + ' WHERE ' + extraCondition + ";";
        });
        sql += 'DELETE FROM coupons WHERE id=' + id + ";";
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
