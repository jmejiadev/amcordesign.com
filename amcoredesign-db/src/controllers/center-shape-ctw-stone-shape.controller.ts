import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CenterShapeCtwStoneShape} from '../models';
import {CenterShapeCtwStoneShapeRepository} from '../repositories';

export class CenterShapeCtwStoneShapeController {
  constructor(
    @repository(CenterShapeCtwStoneShapeRepository)
    public centerShapeCtwStoneShapeRepository : CenterShapeCtwStoneShapeRepository,
  ) {}

  @post('/center-shape-ctw-stone-shapes')
  @response(200, {
    description: 'CenterShapeCtwStoneShape model instance',
    content: {'application/json': {schema: getModelSchemaRef(CenterShapeCtwStoneShape)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtwStoneShape, {
            title: 'NewCenterShapeCtwStoneShape',
            exclude: ['id'],
          }),
        },
      },
    })
    centerShapeCtwStoneShape: Omit<CenterShapeCtwStoneShape, 'id'>,
  ): Promise<CenterShapeCtwStoneShape> {
    return this.centerShapeCtwStoneShapeRepository.create(centerShapeCtwStoneShape);
  }

  @get('/center-shape-ctw-stone-shapes/count')
  @response(200, {
    description: 'CenterShapeCtwStoneShape model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CenterShapeCtwStoneShape) where?: Where<CenterShapeCtwStoneShape>,
  ): Promise<Count> {
    return this.centerShapeCtwStoneShapeRepository.count(where);
  }

  @get('/center-shape-ctw-stone-shapes')
  @response(200, {
    description: 'Array of CenterShapeCtwStoneShape model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CenterShapeCtwStoneShape, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CenterShapeCtwStoneShape) filter?: Filter<CenterShapeCtwStoneShape>,
  ): Promise<CenterShapeCtwStoneShape[]> {
    return this.centerShapeCtwStoneShapeRepository.find(filter);
  }

  @patch('/center-shape-ctw-stone-shapes')
  @response(200, {
    description: 'CenterShapeCtwStoneShape PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtwStoneShape, {partial: true}),
        },
      },
    })
    centerShapeCtwStoneShape: CenterShapeCtwStoneShape,
    @param.where(CenterShapeCtwStoneShape) where?: Where<CenterShapeCtwStoneShape>,
  ): Promise<Count> {
    return this.centerShapeCtwStoneShapeRepository.updateAll(centerShapeCtwStoneShape, where);
  }

  @get('/center-shape-ctw-stone-shapes/{id}')
  @response(200, {
    description: 'CenterShapeCtwStoneShape model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CenterShapeCtwStoneShape, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CenterShapeCtwStoneShape, {exclude: 'where'}) filter?: FilterExcludingWhere<CenterShapeCtwStoneShape>
  ): Promise<CenterShapeCtwStoneShape> {
    return this.centerShapeCtwStoneShapeRepository.findById(id, filter);
  }

  @patch('/center-shape-ctw-stone-shapes/{id}')
  @response(204, {
    description: 'CenterShapeCtwStoneShape PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CenterShapeCtwStoneShape, {partial: true}),
        },
      },
    })
    centerShapeCtwStoneShape: CenterShapeCtwStoneShape,
  ): Promise<void> {
    await this.centerShapeCtwStoneShapeRepository.updateById(id, centerShapeCtwStoneShape);
  }

  @put('/center-shape-ctw-stone-shapes/{id}')
  @response(204, {
    description: 'CenterShapeCtwStoneShape PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() centerShapeCtwStoneShape: CenterShapeCtwStoneShape,
  ): Promise<void> {
    await this.centerShapeCtwStoneShapeRepository.replaceById(id, centerShapeCtwStoneShape);
  }

  @del('/center-shape-ctw-stone-shapes/{id}')
  @response(204, {
    description: 'CenterShapeCtwStoneShape DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.centerShapeCtwStoneShapeRepository.deleteById(id);
  }
}
