// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const GetOrderIdSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetOrderId {
    @get('/get-order-id/')
    @response(200, {
        description: 'Get product edit info',
        content: { 'application/json': { schema: GetOrderIdSchema } },
    })
    async function() {
        let data = {};
        //productInfo
        let sql = "SELECT MAX(`order_number`)+1 as orderNumber from orders; ";
        try {
            let result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
