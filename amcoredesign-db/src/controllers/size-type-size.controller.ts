import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {SizeTypeSize} from '../models';
import {SizeTypeSizeRepository} from '../repositories';

export class SizeTypeSizeController {
  constructor(
    @repository(SizeTypeSizeRepository)
    public sizeTypeSizeRepository : SizeTypeSizeRepository,
  ) {}

  @post('/size-type-sizes')
  @response(200, {
    description: 'SizeTypeSize model instance',
    content: {'application/json': {schema: getModelSchemaRef(SizeTypeSize)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SizeTypeSize, {
            title: 'NewSizeTypeSize',
            exclude: ['id'],
          }),
        },
      },
    })
    sizeTypeSize: Omit<SizeTypeSize, 'id'>,
  ): Promise<SizeTypeSize> {
    return this.sizeTypeSizeRepository.create(sizeTypeSize);
  }

  @get('/size-type-sizes/count')
  @response(200, {
    description: 'SizeTypeSize model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(SizeTypeSize) where?: Where<SizeTypeSize>,
  ): Promise<Count> {
    return this.sizeTypeSizeRepository.count(where);
  }

  @get('/size-type-sizes')
  @response(200, {
    description: 'Array of SizeTypeSize model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(SizeTypeSize, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(SizeTypeSize) filter?: Filter<SizeTypeSize>,
  ): Promise<SizeTypeSize[]> {
    return this.sizeTypeSizeRepository.find(filter);
  }

  @patch('/size-type-sizes')
  @response(200, {
    description: 'SizeTypeSize PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SizeTypeSize, {partial: true}),
        },
      },
    })
    sizeTypeSize: SizeTypeSize,
    @param.where(SizeTypeSize) where?: Where<SizeTypeSize>,
  ): Promise<Count> {
    return this.sizeTypeSizeRepository.updateAll(sizeTypeSize, where);
  }

  @get('/size-type-sizes/{id}')
  @response(200, {
    description: 'SizeTypeSize model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(SizeTypeSize, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(SizeTypeSize, {exclude: 'where'}) filter?: FilterExcludingWhere<SizeTypeSize>
  ): Promise<SizeTypeSize> {
    return this.sizeTypeSizeRepository.findById(id, filter);
  }

  @patch('/size-type-sizes/{id}')
  @response(204, {
    description: 'SizeTypeSize PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SizeTypeSize, {partial: true}),
        },
      },
    })
    sizeTypeSize: SizeTypeSize,
  ): Promise<void> {
    await this.sizeTypeSizeRepository.updateById(id, sizeTypeSize);
  }

  @put('/size-type-sizes/{id}')
  @response(204, {
    description: 'SizeTypeSize PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() sizeTypeSize: SizeTypeSize,
  ): Promise<void> {
    await this.sizeTypeSizeRepository.replaceById(id, sizeTypeSize);
  }

  @del('/size-type-sizes/{id}')
  @response(204, {
    description: 'SizeTypeSize DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.sizeTypeSizeRepository.deleteById(id);
  }
}
