import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ImageStoneSideProducts} from '../models';
import {ImageStoneSideProductsRepository} from '../repositories';

export class ImageStoneSideProductsController {
  constructor(
    @repository(ImageStoneSideProductsRepository)
    public imageStoneSideProductsRepository : ImageStoneSideProductsRepository,
  ) {}

  @post('/image-stone-side-products')
  @response(200, {
    description: 'ImageStoneSideProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(ImageStoneSideProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneSideProducts, {
            title: 'NewImageStoneSideProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    imageStoneSideProducts: Omit<ImageStoneSideProducts, 'id'>,
  ): Promise<ImageStoneSideProducts> {
    return this.imageStoneSideProductsRepository.create(imageStoneSideProducts);
  }

  @get('/image-stone-side-products/count')
  @response(200, {
    description: 'ImageStoneSideProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ImageStoneSideProducts) where?: Where<ImageStoneSideProducts>,
  ): Promise<Count> {
    return this.imageStoneSideProductsRepository.count(where);
  }

  @get('/image-stone-side-products')
  @response(200, {
    description: 'Array of ImageStoneSideProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ImageStoneSideProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ImageStoneSideProducts) filter?: Filter<ImageStoneSideProducts>,
  ): Promise<ImageStoneSideProducts[]> {
    return this.imageStoneSideProductsRepository.find(filter);
  }

  @patch('/image-stone-side-products')
  @response(200, {
    description: 'ImageStoneSideProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneSideProducts, {partial: true}),
        },
      },
    })
    imageStoneSideProducts: ImageStoneSideProducts,
    @param.where(ImageStoneSideProducts) where?: Where<ImageStoneSideProducts>,
  ): Promise<Count> {
    return this.imageStoneSideProductsRepository.updateAll(imageStoneSideProducts, where);
  }

  @get('/image-stone-side-products/{id}')
  @response(200, {
    description: 'ImageStoneSideProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ImageStoneSideProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ImageStoneSideProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<ImageStoneSideProducts>
  ): Promise<ImageStoneSideProducts> {
    return this.imageStoneSideProductsRepository.findById(id, filter);
  }

  @patch('/image-stone-side-products/{id}')
  @response(204, {
    description: 'ImageStoneSideProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneSideProducts, {partial: true}),
        },
      },
    })
    imageStoneSideProducts: ImageStoneSideProducts,
  ): Promise<void> {
    await this.imageStoneSideProductsRepository.updateById(id, imageStoneSideProducts);
  }

  @put('/image-stone-side-products/{id}')
  @response(204, {
    description: 'ImageStoneSideProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() imageStoneSideProducts: ImageStoneSideProducts,
  ): Promise<void> {
    await this.imageStoneSideProductsRepository.replaceById(id, imageStoneSideProducts);
  }

  @del('/image-stone-side-products/{id}')
  @response(204, {
    description: 'ImageStoneSideProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.imageStoneSideProductsRepository.deleteById(id);
  }
}
