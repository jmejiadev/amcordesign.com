import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {StoneSides} from '../models';
import {StoneSidesRepository} from '../repositories';

export class StoneSidesController {
  constructor(
    @repository(StoneSidesRepository)
    public stoneSidesRepository : StoneSidesRepository,
  ) {}

  @post('/stone-sides')
  @response(200, {
    description: 'StoneSides model instance',
    content: {'application/json': {schema: getModelSchemaRef(StoneSides)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneSides, {
            title: 'NewStoneSides',
            exclude: ['id'],
          }),
        },
      },
    })
    stoneSides: Omit<StoneSides, 'id'>,
  ): Promise<StoneSides> {
    return this.stoneSidesRepository.create(stoneSides);
  }

  @get('/stone-sides/count')
  @response(200, {
    description: 'StoneSides model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StoneSides) where?: Where<StoneSides>,
  ): Promise<Count> {
    return this.stoneSidesRepository.count(where);
  }

  @get('/stone-sides')
  @response(200, {
    description: 'Array of StoneSides model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StoneSides, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StoneSides) filter?: Filter<StoneSides>,
  ): Promise<StoneSides[]> {
    return this.stoneSidesRepository.find(filter);
  }

  @patch('/stone-sides')
  @response(200, {
    description: 'StoneSides PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneSides, {partial: true}),
        },
      },
    })
    stoneSides: StoneSides,
    @param.where(StoneSides) where?: Where<StoneSides>,
  ): Promise<Count> {
    return this.stoneSidesRepository.updateAll(stoneSides, where);
  }

  @get('/stone-sides/{id}')
  @response(200, {
    description: 'StoneSides model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StoneSides, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StoneSides, {exclude: 'where'}) filter?: FilterExcludingWhere<StoneSides>
  ): Promise<StoneSides> {
    return this.stoneSidesRepository.findById(id, filter);
  }

  @patch('/stone-sides/{id}')
  @response(204, {
    description: 'StoneSides PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneSides, {partial: true}),
        },
      },
    })
    stoneSides: StoneSides,
  ): Promise<void> {
    await this.stoneSidesRepository.updateById(id, stoneSides);
  }

  @put('/stone-sides/{id}')
  @response(204, {
    description: 'StoneSides PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() stoneSides: StoneSides,
  ): Promise<void> {
    await this.stoneSidesRepository.replaceById(id, stoneSides);
  }

  @del('/stone-sides/{id}')
  @response(204, {
    description: 'StoneSides DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.stoneSidesRepository.deleteById(id);
  }
}
