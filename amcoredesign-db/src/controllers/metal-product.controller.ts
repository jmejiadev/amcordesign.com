import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {MetalProduct} from '../models';
import {MetalProductRepository} from '../repositories';

export class MetalProductController {
  constructor(
    @repository(MetalProductRepository)
    public metalProductRepository : MetalProductRepository,
  ) {}

  @post('/metal-products')
  @response(200, {
    description: 'MetalProduct model instance',
    content: {'application/json': {schema: getModelSchemaRef(MetalProduct)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetalProduct, {
            title: 'NewMetalProduct',
            exclude: ['id'],
          }),
        },
      },
    })
    metalProduct: Omit<MetalProduct, 'id'>,
  ): Promise<MetalProduct> {
    return this.metalProductRepository.create(metalProduct);
  }

  @get('/metal-products/count')
  @response(200, {
    description: 'MetalProduct model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(MetalProduct) where?: Where<MetalProduct>,
  ): Promise<Count> {
    return this.metalProductRepository.count(where);
  }

  @get('/metal-products')
  @response(200, {
    description: 'Array of MetalProduct model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(MetalProduct, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(MetalProduct) filter?: Filter<MetalProduct>,
  ): Promise<MetalProduct[]> {
    return this.metalProductRepository.find(filter);
  }

  @patch('/metal-products')
  @response(200, {
    description: 'MetalProduct PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetalProduct, {partial: true}),
        },
      },
    })
    metalProduct: MetalProduct,
    @param.where(MetalProduct) where?: Where<MetalProduct>,
  ): Promise<Count> {
    return this.metalProductRepository.updateAll(metalProduct, where);
  }

  @get('/metal-products/{id}')
  @response(200, {
    description: 'MetalProduct model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(MetalProduct, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(MetalProduct, {exclude: 'where'}) filter?: FilterExcludingWhere<MetalProduct>
  ): Promise<MetalProduct> {
    return this.metalProductRepository.findById(id, filter);
  }

  @patch('/metal-products/{id}')
  @response(204, {
    description: 'MetalProduct PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetalProduct, {partial: true}),
        },
      },
    })
    metalProduct: MetalProduct,
  ): Promise<void> {
    await this.metalProductRepository.updateById(id, metalProduct);
  }

  @put('/metal-products/{id}')
  @response(204, {
    description: 'MetalProduct PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() metalProduct: MetalProduct,
  ): Promise<void> {
    await this.metalProductRepository.replaceById(id, metalProduct);
  }

  @del('/metal-products/{id}')
  @response(204, {
    description: 'MetalProduct DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.metalProductRepository.deleteById(id);
  }
}
