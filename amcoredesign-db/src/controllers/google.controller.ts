// Uncomment these imports to begin using these cool features!

var google = require('googleapis');
import { get, param, response } from '@loopback/rest';
import { connectionPool } from '../utils';

export declare const googleSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GoogleController {
    @get('/sincronizar/{id}')
    @response(200, {
        description: 'Products insert google shopping',
        content: { 'application/json': { schema: googleSchema } },
    })
    function(@param.path.string('id') id: string) {
        console.dir(id)
        const auth = new google.google.auth.GoogleAuth({
            keyFile: './apis/google/content-api-key.json',
            scopes: ['https://www.googleapis.com/auth/content'],
        });
        //console.log(googleSchema);

        // @ts-ignore
        if (id == 'all') {
            var condicion = "AND (p.google_sync <> 'S' OR p.google_sync is NULL)";
        } else {
            var proId = parseInt(id);
            var condicion = "AND p.id = " + proId;
        }
        console.log(condicion);
        var content = google.google.content({ version: 'v2.1', auth: auth });
        //var sql = "select p.id, p.name, p.local_sku, p.description, p.slug, CASE WHEN gc.CategoryName IS NULL THEN 'Sin Categoria' ELSE gc.CategoryName END as CategoryName, p.selling_price, p.grams_weight,\n" +
        var sql = "select p.id, p.name, p.local_sku, p.description, p.slug, CASE WHEN gc.CategoryName IS NULL THEN 'Sin Categoria' ELSE gc.CategoryName END as CategoryName, p.grams_weight,\n" +
            "\tCASE WHEN imp.path IS NOT NULL \n" +
            "       THEN imp.path\n" +
            "       ELSE \n" +
            "       CASE WHEN iscp.path IS NOT NULL \n" +
            "       \t\tTHEN iscp.path\n" +
            "       \t\tELSE \n" +
            "\t       \t\tCASE WHEN isscp.path IS NOT NULL \n" +
            "       \t\t\t\tTHEN isscp.path\n" +
            "\t\t       \t\tELSE\n" +
            "\t\t       \t\tCASE WHEN issp.path IS NOT NULL \n" +
            "       \t\t\t\t\tTHEN issp.path\n" +
            "\t\t       \t\t\tELSE \n" +
            "\t\t       \t\t\tCASE WHEN isspd.path IS NOT NULL \n" +
            "\t\t\t\t\t       THEN isspd.path\n" +
            "       \t\t\t\t\t   ELSE 0\n" +
            "\t\t\t\t\t\tEND\n" +
            "\t\t\t\t\tEND \n" +
            "\t\t\tEND\n" +
            "\t\tEND\n" +
            "\tEND as img_link\n" +
            "FROM products p\n" +
            "INNER JOIN category_product cp on p.id = cp.product_id \n" +
            "INNER JOIN categories c on c.id = cp.category_id\n" +
            "LEFT JOIN google_categories gc on c.google_category_id = gc.id\n" +
            "LEFT JOIN image_metal_products imp on p.id = imp.product_id\n" +
            "LEFT JOIN image_stone_color_products iscp on p.id = iscp.product_id\n" +
            "LEFT JOIN image_stone_shape_color_products isscp on p.id = isscp.product_id\n" +
            "LEFT JOIN image_stone_shape_products issp on p.id = issp.product_id\n" +
            "LEFT JOIN image_stone_side_products isspd on p.id = isspd.product_id\n" +
            "WHERE p.google = true \n" +
            condicion +
            " GROUP BY 1\n" +
            "\n" +
            "\n" +
            "\n";
console.log( sql);
        connectionPool.query(sql, function (error: any, results: any[], fields: any) {
            if (error) throw error;
            results.forEach(async function (row, index) {
                //console.log(row);
                var pid = 'online:en:US:' + row.local_sku;
                content.products.get({ merchantId: process.env.MERCHANT_ID, productId: pid }, function (err: any, resp: any) {
                    //console.log(err);
                    //console.log(resp);

                    if (err) {
                        console.log("NO EXISTE");
                        var product = {
                            'offerId':
                                row.local_sku,
                            'title':
                                row.name,
                            'description':
                                row.description,
                            'link':
                                'https://amcordesign.com/product/' + row.slug,
                            'imageLink':
                                'https://api.amcordesign.com\\upload\\product\\' + row.img_link,
                            'contentLanguage':
                                'en',
                            'targetCountry':
                                'US',
                            'channel':
                                'online',
                            'availability':
                                'in stock',
                            'condition':
                                'new',
                            'googleProductCategory':
                                row.CategoryName,
                            'price': {
                                'value': row.selling_price,
                                'currency': 'USD'
                            },
                            'shipping': [{
                                'country': 'US',
                                'service': 'Standard shipping',
                                'price': {
                                    'value': '15',
                                    'currency': 'USD'
                                }
                            }],
                            'shippingWeight': {
                                'value': row.grams_weight,
                                'unit': 'grams'
                            }
                        };
                        content.products.insert(
                            {
                                merchantId: process.env.MERCHANT_ID,
                                resource: product
                            }, function (err: any, resp: any) {
                                // handle err and response
                                if (err) {
                                    console.log(err);
                                    var sql_u = "UPDATE products p SET p.google_sync = 'N' Where p.id=" + row.id;
                                } else {
                                    var sql_u = "UPDATE products p SET p.google_sync = 'S' Where p.id=" + row.id;
                                }
                                connectionPool.query(sql_u, function (error: any, results: any[], fields: any) {
                                    if (error) throw error;
                                    console.log("SYNC_I OK");
                                });
                            });
                        return true;
                    } else {
                        console.log("SI EXISTE");
                        var product_u = {
                            'title':
                                row.name,
                            'description':
                                row.description,
                            'link':
                                'https://amcordesign.com/product/' + row.slug,
                            'imageLink':
                                'https://api.amcordesign.com\\upload\\product\\' + row.img_link,
                            'availability':
                                'in stock',
                            'condition':
                                'new',
                            'googleProductCategory':
                                row.CategoryName,
                            'price': {
                                'value': row.selling_price,
                                'currency': 'USD'
                            },
                            'shipping': [{
                                'country': 'US',
                                'service': 'Standard shipping',
                                'price': {
                                    'value': '15',
                                    'currency': 'USD'
                                }
                            }],
                            'shippingWeight': {
                                'value': row.grams_weight,
                                'unit': 'grams'
                            }
                        };
                        content.products.update(
                            {
                                merchantId: process.env.MERCHANT_ID,
                                productId: pid,
                                resource: product_u
                            }, function (err: any, resp: any) {
                                // handle err and response
                                if (err) {
                                    console.log(err);
                                    var sql_u = "UPDATE products p SET p.google_sync = 'N' Where p.id=" + row.id;
                                } else {
                                    var sql_u = "UPDATE products p SET p.google_sync = 'S' Where p.id=" + row.id;
                                }
                                connectionPool.query(sql_u, function (error: any, results: any[], fields: any) {
                                    if (error) throw error;
                                    console.log("SYNC_U OK");
                                });
                            });
                        return true;
                    }
                });
            });
        });
        return [{ 'result': 'ok' }];
    };
}

