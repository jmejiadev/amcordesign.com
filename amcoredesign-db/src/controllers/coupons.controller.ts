import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Coupons} from '../models';
import {CouponsRepository} from '../repositories';

export class CouponsController {
  constructor(
    @repository(CouponsRepository)
    public couponsRepository : CouponsRepository,
  ) {}

  @post('/coupons')
  @response(200, {
    description: 'Coupons model instance',
    content: {'application/json': {schema: getModelSchemaRef(Coupons)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coupons, {
            title: 'NewCoupons',
            exclude: ['id'],
          }),
        },
      },
    })
    coupons: Omit<Coupons, 'id'>,
  ): Promise<Coupons> {
    return this.couponsRepository.create(coupons);
  }

  @get('/coupons/count')
  @response(200, {
    description: 'Coupons model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Coupons) where?: Where<Coupons>,
  ): Promise<Count> {
    return this.couponsRepository.count(where);
  }

  @get('/coupons')
  @response(200, {
    description: 'Array of Coupons model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Coupons, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Coupons) filter?: Filter<Coupons>,
  ): Promise<Coupons[]> {
    return this.couponsRepository.find(filter);
  }

  @patch('/coupons')
  @response(200, {
    description: 'Coupons PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coupons, {partial: true}),
        },
      },
    })
    coupons: Coupons,
    @param.where(Coupons) where?: Where<Coupons>,
  ): Promise<Count> {
    return this.couponsRepository.updateAll(coupons, where);
  }

  @get('/coupons/{id}')
  @response(200, {
    description: 'Coupons model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Coupons, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Coupons, {exclude: 'where'}) filter?: FilterExcludingWhere<Coupons>
  ): Promise<Coupons> {
    return this.couponsRepository.findById(id, filter);
  }

  @patch('/coupons/{id}')
  @response(204, {
    description: 'Coupons PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coupons, {partial: true}),
        },
      },
    })
    coupons: Coupons,
  ): Promise<void> {
    await this.couponsRepository.updateById(id, coupons);
  }

  @put('/coupons/{id}')
  @response(204, {
    description: 'Coupons PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() coupons: Coupons,
  ): Promise<void> {
    await this.couponsRepository.replaceById(id, coupons);
  }

  @del('/coupons/{id}')
  @response(204, {
    description: 'Coupons DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.couponsRepository.deleteById(id);
  }
}
