import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ExtraVariations} from '../models';
import {ExtraVariationsRepository} from '../repositories';

export class ExtraVariationsController {
  constructor(
    @repository(ExtraVariationsRepository)
    public extraVariationsRepository : ExtraVariationsRepository,
  ) {}

  @post('/extra-variations')
  @response(200, {
    description: 'ExtraVariations model instance',
    content: {'application/json': {schema: getModelSchemaRef(ExtraVariations)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExtraVariations, {
            title: 'NewExtraVariations',
            exclude: ['id'],
          }),
        },
      },
    })
    extraVariations: Omit<ExtraVariations, 'id'>,
  ): Promise<ExtraVariations> {
    return this.extraVariationsRepository.create(extraVariations);
  }

  @get('/extra-variations/count')
  @response(200, {
    description: 'ExtraVariations model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ExtraVariations) where?: Where<ExtraVariations>,
  ): Promise<Count> {
    return this.extraVariationsRepository.count(where);
  }

  @get('/extra-variations')
  @response(200, {
    description: 'Array of ExtraVariations model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ExtraVariations, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ExtraVariations) filter?: Filter<ExtraVariations>,
  ): Promise<ExtraVariations[]> {
    return this.extraVariationsRepository.find(filter);
  }

  @patch('/extra-variations')
  @response(200, {
    description: 'ExtraVariations PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExtraVariations, {partial: true}),
        },
      },
    })
    extraVariations: ExtraVariations,
    @param.where(ExtraVariations) where?: Where<ExtraVariations>,
  ): Promise<Count> {
    return this.extraVariationsRepository.updateAll(extraVariations, where);
  }

  @get('/extra-variations/{id}')
  @response(200, {
    description: 'ExtraVariations model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ExtraVariations, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ExtraVariations, {exclude: 'where'}) filter?: FilterExcludingWhere<ExtraVariations>
  ): Promise<ExtraVariations> {
    return this.extraVariationsRepository.findById(id, filter);
  }

  @patch('/extra-variations/{id}')
  @response(204, {
    description: 'ExtraVariations PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExtraVariations, {partial: true}),
        },
      },
    })
    extraVariations: ExtraVariations,
  ): Promise<void> {
    await this.extraVariationsRepository.updateById(id, extraVariations);
  }

  @put('/extra-variations/{id}')
  @response(204, {
    description: 'ExtraVariations PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() extraVariations: ExtraVariations,
  ): Promise<void> {
    await this.extraVariationsRepository.replaceById(id, extraVariations);
  }

  @del('/extra-variations/{id}')
  @response(204, {
    description: 'ExtraVariations DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.extraVariationsRepository.deleteById(id);
  }
}
