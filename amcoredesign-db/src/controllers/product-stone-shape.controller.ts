import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductStoneShape} from '../models';
import {ProductStoneShapeRepository} from '../repositories';

export class ProductStoneShapeController {
  constructor(
    @repository(ProductStoneShapeRepository)
    public productStoneShapeRepository : ProductStoneShapeRepository,
  ) {}

  @post('/product-stone-shapes')
  @response(200, {
    description: 'ProductStoneShape model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductStoneShape)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneShape, {
            title: 'NewProductStoneShape',
            exclude: ['id'],
          }),
        },
      },
    })
    productStoneShape: Omit<ProductStoneShape, 'id'>,
  ): Promise<ProductStoneShape> {
    return this.productStoneShapeRepository.create(productStoneShape);
  }

  @get('/product-stone-shapes/count')
  @response(200, {
    description: 'ProductStoneShape model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductStoneShape) where?: Where<ProductStoneShape>,
  ): Promise<Count> {
    return this.productStoneShapeRepository.count(where);
  }

  @get('/product-stone-shapes')
  @response(200, {
    description: 'Array of ProductStoneShape model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductStoneShape, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductStoneShape) filter?: Filter<ProductStoneShape>,
  ): Promise<ProductStoneShape[]> {
    return this.productStoneShapeRepository.find(filter);
  }

  @patch('/product-stone-shapes')
  @response(200, {
    description: 'ProductStoneShape PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneShape, {partial: true}),
        },
      },
    })
    productStoneShape: ProductStoneShape,
    @param.where(ProductStoneShape) where?: Where<ProductStoneShape>,
  ): Promise<Count> {
    return this.productStoneShapeRepository.updateAll(productStoneShape, where);
  }

  @get('/product-stone-shapes/{id}')
  @response(200, {
    description: 'ProductStoneShape model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductStoneShape, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductStoneShape, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductStoneShape>
  ): Promise<ProductStoneShape> {
    return this.productStoneShapeRepository.findById(id, filter);
  }

  @patch('/product-stone-shapes/{id}')
  @response(204, {
    description: 'ProductStoneShape PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneShape, {partial: true}),
        },
      },
    })
    productStoneShape: ProductStoneShape,
  ): Promise<void> {
    await this.productStoneShapeRepository.updateById(id, productStoneShape);
  }

  @put('/product-stone-shapes/{id}')
  @response(204, {
    description: 'ProductStoneShape PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productStoneShape: ProductStoneShape,
  ): Promise<void> {
    await this.productStoneShapeRepository.replaceById(id, productStoneShape);
  }

  @del('/product-stone-shapes/{id}')
  @response(204, {
    description: 'ProductStoneShape DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productStoneShapeRepository.deleteById(id);
  }
}
