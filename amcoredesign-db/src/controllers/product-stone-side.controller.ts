import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductStoneSide} from '../models';
import {ProductStoneSideRepository} from '../repositories';

export class ProductStoneSideController {
  constructor(
    @repository(ProductStoneSideRepository)
    public productStoneSideRepository : ProductStoneSideRepository,
  ) {}

  @post('/product-stone-sides')
  @response(200, {
    description: 'ProductStoneSide model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductStoneSide)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneSide, {
            title: 'NewProductStoneSide',
            exclude: ['id'],
          }),
        },
      },
    })
    productStoneSide: Omit<ProductStoneSide, 'id'>,
  ): Promise<ProductStoneSide> {
    return this.productStoneSideRepository.create(productStoneSide);
  }

  @get('/product-stone-sides/count')
  @response(200, {
    description: 'ProductStoneSide model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductStoneSide) where?: Where<ProductStoneSide>,
  ): Promise<Count> {
    return this.productStoneSideRepository.count(where);
  }

  @get('/product-stone-sides')
  @response(200, {
    description: 'Array of ProductStoneSide model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductStoneSide, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductStoneSide) filter?: Filter<ProductStoneSide>,
  ): Promise<ProductStoneSide[]> {
    return this.productStoneSideRepository.find(filter);
  }

  @patch('/product-stone-sides')
  @response(200, {
    description: 'ProductStoneSide PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneSide, {partial: true}),
        },
      },
    })
    productStoneSide: ProductStoneSide,
    @param.where(ProductStoneSide) where?: Where<ProductStoneSide>,
  ): Promise<Count> {
    return this.productStoneSideRepository.updateAll(productStoneSide, where);
  }

  @get('/product-stone-sides/{id}')
  @response(200, {
    description: 'ProductStoneSide model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductStoneSide, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductStoneSide, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductStoneSide>
  ): Promise<ProductStoneSide> {
    return this.productStoneSideRepository.findById(id, filter);
  }

  @patch('/product-stone-sides/{id}')
  @response(204, {
    description: 'ProductStoneSide PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneSide, {partial: true}),
        },
      },
    })
    productStoneSide: ProductStoneSide,
  ): Promise<void> {
    await this.productStoneSideRepository.updateById(id, productStoneSide);
  }

  @put('/product-stone-sides/{id}')
  @response(204, {
    description: 'ProductStoneSide PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productStoneSide: ProductStoneSide,
  ): Promise<void> {
    await this.productStoneSideRepository.replaceById(id, productStoneSide);
  }

  @del('/product-stone-sides/{id}')
  @response(204, {
    description: 'ProductStoneSide DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productStoneSideRepository.deleteById(id);
  }
}
