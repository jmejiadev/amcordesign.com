import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductWishList} from '../models';
import {ProductWishListRepository} from '../repositories';

export class ProductWishListController {
  constructor(
    @repository(ProductWishListRepository)
    public productWishListRepository : ProductWishListRepository,
  ) {}

  @post('/product-wish-lists')
  @response(200, {
    description: 'ProductWishList model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductWishList)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductWishList, {
            title: 'NewProductWishList',
            exclude: ['id'],
          }),
        },
      },
    })
    productWishList: Omit<ProductWishList, 'id'>,
  ): Promise<ProductWishList> {
    return this.productWishListRepository.create(productWishList);
  }

  @get('/product-wish-lists/count')
  @response(200, {
    description: 'ProductWishList model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductWishList) where?: Where<ProductWishList>,
  ): Promise<Count> {
    return this.productWishListRepository.count(where);
  }

  @get('/product-wish-lists')
  @response(200, {
    description: 'Array of ProductWishList model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductWishList, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductWishList) filter?: Filter<ProductWishList>,
  ): Promise<ProductWishList[]> {
    return this.productWishListRepository.find(filter);
  }

  @patch('/product-wish-lists')
  @response(200, {
    description: 'ProductWishList PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductWishList, {partial: true}),
        },
      },
    })
    productWishList: ProductWishList,
    @param.where(ProductWishList) where?: Where<ProductWishList>,
  ): Promise<Count> {
    return this.productWishListRepository.updateAll(productWishList, where);
  }

  @get('/product-wish-lists/{id}')
  @response(200, {
    description: 'ProductWishList model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductWishList, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductWishList, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductWishList>
  ): Promise<ProductWishList> {
    return this.productWishListRepository.findById(id, filter);
  }

  @patch('/product-wish-lists/{id}')
  @response(204, {
    description: 'ProductWishList PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductWishList, {partial: true}),
        },
      },
    })
    productWishList: ProductWishList,
  ): Promise<void> {
    await this.productWishListRepository.updateById(id, productWishList);
  }

  @put('/product-wish-lists/{id}')
  @response(204, {
    description: 'ProductWishList PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productWishList: ProductWishList,
  ): Promise<void> {
    await this.productWishListRepository.replaceById(id, productWishList);
  }

  @del('/product-wish-lists/{id}')
  @response(204, {
    description: 'ProductWishList DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productWishListRepository.deleteById(id);
  }
}
