// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const CopyOrderSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class CopyOrder {
    @get('/copy-order/{id}')
    @response(200, {
        description: 'Size Type Sizes',
        content: { 'application/json': { schema: CopyOrderSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        const now = new Date().toISOString();
        //order
        let sql = "SELECT @max := MAX( order_number ) FROM orders;"
            + "INSERT INTO orders (`order_number`, `first_name`, `last_name`, `email`, `phone`, `client_ip`, `total`, `notes`, `order_status`, `shipping_address_line1`, "
            + "`shipping_address_line2`, `shipping_zip_code`, `shipping_city`, `shipping_state`, `billing_address_line1`, `billing_address_line2`, "
            + "`billing_zip_code`, `billing_city`, `billing_state`, `payment_type`, `shipping_country`, `billing_country`, `tax`, `person_id`, `created_at`, `updated_at` ) "
            + " SELECT @max+1, `first_name`, `last_name`, `email`, `phone`, `client_ip`, `total`, `notes`, `order_status`, `shipping_address_line1`, `shipping_address_line2`,"
            + " `shipping_zip_code`, `shipping_city`, `shipping_state`, `billing_address_line1`, `billing_address_line2`, `billing_zip_code`, `billing_city`, `billing_state`, "
            + "`payment_type`, `shipping_country`, `billing_country`, `tax`, `person_id`,  now(),  now() from `orders` where `id` = " + id + "; ";
        try {
            
            let result = await connectionPoolPromise.query(sql);
            var response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array.push(response[0]);
            const idNewOrder = Number(array[0][2]);
            //trackings
            sql = "INSERT INTO `trackings`( `name`, `url`, `number`, `departureDate`, `departureTime`, `order_id`, `created_at`, `updated_at`) "
                + "SELECT  `name`, `url`, `number`, `departureDate`, `departureTime`, " + idNewOrder + ",  now(),  now() "
                + "FROM `trackings` where `order_id` = " + id + "; ";
            
            result = await connectionPoolPromise.query(sql);
            response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            array.push(response[0]);
            //order paypal
            sql = "INSERT INTO `order_paypal`(`order_id`, `paypal_id`, `create_time`, `update_time`, `detail`)"
                + "SELECT " + idNewOrder + ", `paypal_id`,  now(),  now(), `detail`"
                + "FROM `order_paypal` WHERE `order_id` = " + id + ";";
            
            result = await connectionPoolPromise.query(sql);
            response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            array.push(response[0]);
            //order products
            sql = "INSERT INTO `order_products`(`order_id`, `product_id`, `name`, `supplier_sku`, `grams`, `stone_number`, "
                + "`metal_id`, `metal_name`, `stone_center_id`, `stone_center_name`, "
                + "`stone_side_id`, `stone_side_name`, `selling_price`, `discount`, `personalization`, "
                + "`quantity`, `stone_shape_id`, `stone_shape_name`, `stone_color_id`, `stone_color_name`, "
                + "`subtotal`, `coupon_id`, `gemstone_id`, `created_at`, `updated_at`) "
                + "SELECT " + idNewOrder + ", `product_id`, `name`, `supplier_sku`, `grams`, `stone_number`, "
                + "`metal_id`, `metal_name`, `stone_center_id`, `stone_center_name`, "
                + "`stone_side_id`, `stone_side_name`, `selling_price`, `discount`, `personalization`, "
                + "`quantity`, `stone_shape_id`, `stone_shape_name`, `stone_color_id`, `stone_color_name`, "
                + "`subtotal`, `coupon_id`, `gemstone_id`,  now(),  now() FROM `order_products` WHERE `order_id` = " + id + ";";
            
            result = await connectionPoolPromise.query(sql);
            response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            array.push(response[0]);
            const idOrderProduct = Number(array[array.length - 1][2]);
            // order-products-sizes
            sql = "INSERT INTO `order_products_sizes`( `order_product_id`, `type_size_id`, `size_number`, `created_at`, `updated_at`) "
                + "SELECT " + idOrderProduct + ", `type_size_id`, `size_number`,  now(),  now()"
                + "FROM `order_products_sizes` WHERE  `order_product_id` = " + id + ";";
            
            result = await connectionPoolPromise.query(sql);
            response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            array.push(response[0]);
            // order-products-variants
            sql = "INSERT INTO `order_products_variants`(`order_product_id`, `extra_variation_id`, `extra_variation_name`, `variation_id`, `variation_name`, `created_at`, `updated_at`)"
                + "SELECT " + idOrderProduct + ", `extra_variation_id`, `extra_variation_name`, `variation_id`, `variation_name`,  now(),  now()"
                + "FROM `order_products_variants` WHERE `order_product_id` = " + id + ";";
            

            result = await connectionPoolPromise.query(sql);
            response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            array.push(response[0]);
            data = array;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
