import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {OrderProductsVariants} from '../models';
import {OrderProductsVariantsRepository} from '../repositories';

export class OrderProductsVariantsController {
  constructor(
    @repository(OrderProductsVariantsRepository)
    public orderProductsVariantsRepository : OrderProductsVariantsRepository,
  ) {}

  @post('/order-products-variants')
  @response(200, {
    description: 'OrderProductsVariants model instance',
    content: {'application/json': {schema: getModelSchemaRef(OrderProductsVariants)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProductsVariants, {
            title: 'NewOrderProductsVariants',
            exclude: ['id'],
          }),
        },
      },
    })
    orderProductsVariants: Omit<OrderProductsVariants, 'id'>,
  ): Promise<OrderProductsVariants> {
    return this.orderProductsVariantsRepository.create(orderProductsVariants);
  }

  @get('/order-products-variants/count')
  @response(200, {
    description: 'OrderProductsVariants model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(OrderProductsVariants) where?: Where<OrderProductsVariants>,
  ): Promise<Count> {
    return this.orderProductsVariantsRepository.count(where);
  }

  @get('/order-products-variants')
  @response(200, {
    description: 'Array of OrderProductsVariants model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(OrderProductsVariants, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(OrderProductsVariants) filter?: Filter<OrderProductsVariants>,
  ): Promise<OrderProductsVariants[]> {
    return this.orderProductsVariantsRepository.find(filter);
  }

  @patch('/order-products-variants')
  @response(200, {
    description: 'OrderProductsVariants PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProductsVariants, {partial: true}),
        },
      },
    })
    orderProductsVariants: OrderProductsVariants,
    @param.where(OrderProductsVariants) where?: Where<OrderProductsVariants>,
  ): Promise<Count> {
    return this.orderProductsVariantsRepository.updateAll(orderProductsVariants, where);
  }

  @get('/order-products-variants/{id}')
  @response(200, {
    description: 'OrderProductsVariants model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(OrderProductsVariants, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(OrderProductsVariants, {exclude: 'where'}) filter?: FilterExcludingWhere<OrderProductsVariants>
  ): Promise<OrderProductsVariants> {
    return this.orderProductsVariantsRepository.findById(id, filter);
  }

  @patch('/order-products-variants/{id}')
  @response(204, {
    description: 'OrderProductsVariants PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProductsVariants, {partial: true}),
        },
      },
    })
    orderProductsVariants: OrderProductsVariants,
  ): Promise<void> {
    await this.orderProductsVariantsRepository.updateById(id, orderProductsVariants);
  }

  @put('/order-products-variants/{id}')
  @response(204, {
    description: 'OrderProductsVariants PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() orderProductsVariants: OrderProductsVariants,
  ): Promise<void> {
    await this.orderProductsVariantsRepository.replaceById(id, orderProductsVariants);
  }

  @del('/order-products-variants/{id}')
  @response(204, {
    description: 'OrderProductsVariants DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderProductsVariantsRepository.deleteById(id);
  }
}
