// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const CopyProductSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class CopyProduct {
    @get('/copy-product/{id}')
    @response(200, {
        description: 'Size Type Sizes',
        content: { 'application/json': { schema: CopyProductSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        const now = new Date().toISOString();
        //order
        let sql = "INSERT INTO `products`( `name`, `short_description`, `description`, `local_sku`, `supplier_sku`, `manufacturer`, `slug`, `average_width`, "
            + "`finish`, `setting`, `discount`, `stone_number`, `grams_weight`, `publish`, `google`, `created_at`, `updated_at`, `google_sync`) "
            + " SELECT  CONCAT(`name`,'_clone_'), `short_description`, `description`,  CONCAT(`local_sku`,'_clone_'),  CONCAT(`supplier_sku`,'_clone_'), "
            + " `manufacturer`, CONCAT(`slug` , '_clone_'), `average_width`, `finish`, `setting`, `discount`, `stone_number`, `grams_weight`, `publish`, "
            + " `google`,  now(),  now(), `google_sync` FROM `products` WHERE `id` = " + id + "; ";
        try {
            let result = await connectionPoolPromise.query(sql);
            var response = Array();
            response.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array.push(response[0]);
            const idNewProduct = Number(array[0][2]);

            //product variation
            var table = '`product_variation`';
            sql = "INSERT INTO " + table + " (`variation_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT `variation_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`product_type_size`';
            sql = "INSERT INTO " + table + " (`type_size_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT `type_size_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`product_stone_side`';
            sql = "INSERT INTO " + table + " (`price`, `carat`, `stone_side_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT `price`, `carat`, `stone_side_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`product_stone_shape`';
            sql = "INSERT INTO " + table + " (`stone_shape_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT `stone_shape_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`product_stone_color`';
            sql = "INSERT INTO " + table + " (`price`, `carat`, `stone_color_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT `price`, `carat`, `stone_color_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`product_stone_center`';
            sql = "INSERT INTO " + table + " (`stone_center_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT  `stone_center_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`metal_product`';
            sql = "INSERT INTO " + table + " (`metal_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT  `metal_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`category_product`';
            sql = "INSERT INTO " + table + " (`category_id`, `product_id`, `created_at`, `updated_at`) "
                + "SELECT  `category_id`, " + idNewProduct + ", now(),  now() FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`image_stone_side_products`';
            sql = "INSERT INTO " + table + " (`path`, `product_id`, `metal_id`, `stone_side_id`, `stone_color_id`, `is_principal`, `created_at`, `updated_at`) "
                + "SELECT  `path`, " + idNewProduct + ", `metal_id`, `stone_side_id`, `stone_color_id`, `is_principal`, now(),  now() "
                + "FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`image_stone_shape_products`';
            sql = "INSERT INTO " + table + " (`path`, `product_id`, `metal_id`, `stone_shape_id`, `is_principal`, `created_at`, `updated_at`) "
                + "SELECT  `path`, " + idNewProduct + ", `metal_id`, `stone_shape_id`, `is_principal`, now(),  now() "
                + "FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`image_stone_shape_color_products`';
            sql = "INSERT INTO " + table + " (`path`, `product_id`, `metal_id`, `stone_shape_id`, `stone_color_id`, `is_principal`, `created_at`, `updated_at`) "
                + "SELECT  `path`, " + idNewProduct + ", `metal_id`, `stone_shape_id`, `stone_color_id`, `is_principal`, now(),  now() "
                + "FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`image_stone_color_products`';
            sql = "INSERT INTO " + table + " (`path`, `product_id`, `metal_id`, `stone_color_id`, `is_principal`, `created_at`, `updated_at`) "
                + "SELECT  `path`, " + idNewProduct + ", `metal_id`, `stone_color_id`, `is_principal`, now(),  now() "
                + "FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            table = '`image_metal_products`';
            sql = "INSERT INTO " + table + " (`path`, `product_id`, `metal_id`, `is_principal`, `created_at`, `updated_at`) "
                + "SELECT  `path`, " + idNewProduct + ", `metal_id`, `is_principal`, now(),  now() "
                + "FROM " + table + " WHERE `product_id` = " + id + "; ";;
            await execQuery(sql).then(function (results) {
                array.push(results);
            });

            data = array;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;


        async function execQuery(sql: string) {
            var response = Array();
            await connectionPoolPromise.query(sql).then(function (results) {
                response.push(Object.values(JSON.parse(JSON.stringify(results[0]))));
            });
            return response[0];

        }
    }

}
