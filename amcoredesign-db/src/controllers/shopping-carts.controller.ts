import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ShoppingCarts} from '../models';
import {ShoppingCartsRepository} from '../repositories';

export class ShoppingCartsController {
  constructor(
    @repository(ShoppingCartsRepository)
    public shoppingCartsRepository : ShoppingCartsRepository,
  ) {}

  @post('/shopping-carts')
  @response(200, {
    description: 'ShoppingCarts model instance',
    content: {'application/json': {schema: getModelSchemaRef(ShoppingCarts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShoppingCarts, {
            title: 'NewShoppingCarts',
            exclude: ['id'],
          }),
        },
      },
    })
    shoppingCarts: Omit<ShoppingCarts, 'id'>,
  ): Promise<ShoppingCarts> {
    return this.shoppingCartsRepository.create(shoppingCarts);
  }

  @get('/shopping-carts/count')
  @response(200, {
    description: 'ShoppingCarts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ShoppingCarts) where?: Where<ShoppingCarts>,
  ): Promise<Count> {
    return this.shoppingCartsRepository.count(where);
  }

  @get('/shopping-carts')
  @response(200, {
    description: 'Array of ShoppingCarts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ShoppingCarts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ShoppingCarts) filter?: Filter<ShoppingCarts>,
  ): Promise<ShoppingCarts[]> {
    return this.shoppingCartsRepository.find(filter);
  }

  @patch('/shopping-carts')
  @response(200, {
    description: 'ShoppingCarts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShoppingCarts, {partial: true}),
        },
      },
    })
    shoppingCarts: ShoppingCarts,
    @param.where(ShoppingCarts) where?: Where<ShoppingCarts>,
  ): Promise<Count> {
    return this.shoppingCartsRepository.updateAll(shoppingCarts, where);
  }

  @get('/shopping-carts/{id}')
  @response(200, {
    description: 'ShoppingCarts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ShoppingCarts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ShoppingCarts, {exclude: 'where'}) filter?: FilterExcludingWhere<ShoppingCarts>
  ): Promise<ShoppingCarts> {
    return this.shoppingCartsRepository.findById(id, filter);
  }

  @patch('/shopping-carts/{id}')
  @response(204, {
    description: 'ShoppingCarts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShoppingCarts, {partial: true}),
        },
      },
    })
    shoppingCarts: ShoppingCarts,
  ): Promise<void> {
    await this.shoppingCartsRepository.updateById(id, shoppingCarts);
  }

  @put('/shopping-carts/{id}')
  @response(204, {
    description: 'ShoppingCarts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() shoppingCarts: ShoppingCarts,
  ): Promise<void> {
    await this.shoppingCartsRepository.replaceById(id, shoppingCarts);
  }

  @del('/shopping-carts/{id}')
  @response(204, {
    description: 'ShoppingCarts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.shoppingCartsRepository.deleteById(id);
  }
}
