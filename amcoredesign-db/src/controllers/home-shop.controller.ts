import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {HomeShop} from '../models';
import {HomeShopRepository} from '../repositories';

export class HomeShopController {
  constructor(
    @repository(HomeShopRepository)
    public homeShopRepository : HomeShopRepository,
  ) {}

  @post('/home-shops')
  @response(200, {
    description: 'HomeShop model instance',
    content: {'application/json': {schema: getModelSchemaRef(HomeShop)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HomeShop, {
            title: 'NewHomeShop',
            exclude: ['id'],
          }),
        },
      },
    })
    homeShop: Omit<HomeShop, 'id'>,
  ): Promise<HomeShop> {
    return this.homeShopRepository.create(homeShop);
  }

  @get('/home-shops/count')
  @response(200, {
    description: 'HomeShop model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(HomeShop) where?: Where<HomeShop>,
  ): Promise<Count> {
    return this.homeShopRepository.count(where);
  }

  @get('/home-shops')
  @response(200, {
    description: 'Array of HomeShop model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(HomeShop, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(HomeShop) filter?: Filter<HomeShop>,
  ): Promise<HomeShop[]> {
    return this.homeShopRepository.find(filter);
  }

  @patch('/home-shops')
  @response(200, {
    description: 'HomeShop PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HomeShop, {partial: true}),
        },
      },
    })
    homeShop: HomeShop,
    @param.where(HomeShop) where?: Where<HomeShop>,
  ): Promise<Count> {
    return this.homeShopRepository.updateAll(homeShop, where);
  }

  @get('/home-shops/{id}')
  @response(200, {
    description: 'HomeShop model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(HomeShop, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(HomeShop, {exclude: 'where'}) filter?: FilterExcludingWhere<HomeShop>
  ): Promise<HomeShop> {
    return this.homeShopRepository.findById(id, filter);
  }

  @patch('/home-shops/{id}')
  @response(204, {
    description: 'HomeShop PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HomeShop, {partial: true}),
        },
      },
    })
    homeShop: HomeShop,
  ): Promise<void> {
    await this.homeShopRepository.updateById(id, homeShop);
  }

  @put('/home-shops/{id}')
  @response(204, {
    description: 'HomeShop PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() homeShop: HomeShop,
  ): Promise<void> {
    await this.homeShopRepository.replaceById(id, homeShop);
  }

  @del('/home-shops/{id}')
  @response(204, {
    description: 'HomeShop DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.homeShopRepository.deleteById(id);
  }
}
