// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const RelatedCouponsSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetRelatedCoupons {
    @get('/get-related-coupons/{id}')
    @response(200, {
        description: 'Coupons Related delete ',
        content: { 'application/json': { schema: RelatedCouponsSchema } },
    })
    async function(@param.path.number('id') id: number) {
        var arr = Array();
        arr.push("coupon_product");
        arr.push("order_products");

        let data = {};
        var extraCondition = "coupon_id = " + id;
        let sql = "";
        arr.forEach(element => {
            sql += 'SELECT * FROM ' + element + ' WHERE ' + extraCondition + ";";
        });
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
