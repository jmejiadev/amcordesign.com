import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {OrderProductsSizes} from '../models';
import {OrderProductsSizesRepository} from '../repositories';

export class OrderProductsSizesController {
  constructor(
    @repository(OrderProductsSizesRepository)
    public orderProductsSizesRepository : OrderProductsSizesRepository,
  ) {}

  @post('/order-products-sizes')
  @response(200, {
    description: 'OrderProductsSizes model instance',
    content: {'application/json': {schema: getModelSchemaRef(OrderProductsSizes)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProductsSizes, {
            title: 'NewOrderProductsSizes',
            exclude: ['id'],
          }),
        },
      },
    })
    orderProductsSizes: Omit<OrderProductsSizes, 'id'>,
  ): Promise<OrderProductsSizes> {
    return this.orderProductsSizesRepository.create(orderProductsSizes);
  }

  @get('/order-products-sizes/count')
  @response(200, {
    description: 'OrderProductsSizes model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(OrderProductsSizes) where?: Where<OrderProductsSizes>,
  ): Promise<Count> {
    return this.orderProductsSizesRepository.count(where);
  }

  @get('/order-products-sizes')
  @response(200, {
    description: 'Array of OrderProductsSizes model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(OrderProductsSizes, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(OrderProductsSizes) filter?: Filter<OrderProductsSizes>,
  ): Promise<OrderProductsSizes[]> {
    return this.orderProductsSizesRepository.find(filter);
  }

  @patch('/order-products-sizes')
  @response(200, {
    description: 'OrderProductsSizes PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProductsSizes, {partial: true}),
        },
      },
    })
    orderProductsSizes: OrderProductsSizes,
    @param.where(OrderProductsSizes) where?: Where<OrderProductsSizes>,
  ): Promise<Count> {
    return this.orderProductsSizesRepository.updateAll(orderProductsSizes, where);
  }

  @get('/order-products-sizes/{id}')
  @response(200, {
    description: 'OrderProductsSizes model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(OrderProductsSizes, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(OrderProductsSizes, {exclude: 'where'}) filter?: FilterExcludingWhere<OrderProductsSizes>
  ): Promise<OrderProductsSizes> {
    return this.orderProductsSizesRepository.findById(id, filter);
  }

  @patch('/order-products-sizes/{id}')
  @response(204, {
    description: 'OrderProductsSizes PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderProductsSizes, {partial: true}),
        },
      },
    })
    orderProductsSizes: OrderProductsSizes,
  ): Promise<void> {
    await this.orderProductsSizesRepository.updateById(id, orderProductsSizes);
  }

  @put('/order-products-sizes/{id}')
  @response(204, {
    description: 'OrderProductsSizes PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() orderProductsSizes: OrderProductsSizes,
  ): Promise<void> {
    await this.orderProductsSizesRepository.replaceById(id, orderProductsSizes);
  }

  @del('/order-products-sizes/{id}')
  @response(204, {
    description: 'OrderProductsSizes DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderProductsSizesRepository.deleteById(id);
  }
}
