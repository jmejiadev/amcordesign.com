import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ShippingAddresses} from '../models';
import {ShippingAddressesRepository} from '../repositories';

export class ShippingAddressesController {
  constructor(
    @repository(ShippingAddressesRepository)
    public shippingAddressesRepository : ShippingAddressesRepository,
  ) {}

  @post('/shipping-addresses')
  @response(200, {
    description: 'ShippingAddresses model instance',
    content: {'application/json': {schema: getModelSchemaRef(ShippingAddresses)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShippingAddresses, {
            title: 'NewShippingAddresses',
            exclude: ['id'],
          }),
        },
      },
    })
    shippingAddresses: Omit<ShippingAddresses, 'id'>,
  ): Promise<ShippingAddresses> {
    return this.shippingAddressesRepository.create(shippingAddresses);
  }

  @get('/shipping-addresses/count')
  @response(200, {
    description: 'ShippingAddresses model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ShippingAddresses) where?: Where<ShippingAddresses>,
  ): Promise<Count> {
    return this.shippingAddressesRepository.count(where);
  }

  @get('/shipping-addresses')
  @response(200, {
    description: 'Array of ShippingAddresses model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ShippingAddresses, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ShippingAddresses) filter?: Filter<ShippingAddresses>,
  ): Promise<ShippingAddresses[]> {
    return this.shippingAddressesRepository.find(filter);
  }

  @patch('/shipping-addresses')
  @response(200, {
    description: 'ShippingAddresses PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShippingAddresses, {partial: true}),
        },
      },
    })
    shippingAddresses: ShippingAddresses,
    @param.where(ShippingAddresses) where?: Where<ShippingAddresses>,
  ): Promise<Count> {
    return this.shippingAddressesRepository.updateAll(shippingAddresses, where);
  }

  @get('/shipping-addresses/{id}')
  @response(200, {
    description: 'ShippingAddresses model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ShippingAddresses, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ShippingAddresses, {exclude: 'where'}) filter?: FilterExcludingWhere<ShippingAddresses>
  ): Promise<ShippingAddresses> {
    return this.shippingAddressesRepository.findById(id, filter);
  }

  @patch('/shipping-addresses/{id}')
  @response(204, {
    description: 'ShippingAddresses PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShippingAddresses, {partial: true}),
        },
      },
    })
    shippingAddresses: ShippingAddresses,
  ): Promise<void> {
    await this.shippingAddressesRepository.updateById(id, shippingAddresses);
  }

  @put('/shipping-addresses/{id}')
  @response(204, {
    description: 'ShippingAddresses PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() shippingAddresses: ShippingAddresses,
  ): Promise<void> {
    await this.shippingAddressesRepository.replaceById(id, shippingAddresses);
  }

  @del('/shipping-addresses/{id}')
  @response(204, {
    description: 'ShippingAddresses DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.shippingAddressesRepository.deleteById(id);
  }
}
