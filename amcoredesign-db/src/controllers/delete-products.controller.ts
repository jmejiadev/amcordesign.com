// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteProductsSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteProducts {
    @get('/del-products/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: DeleteProductsSchema } },
    })
    async function(@param.path.number('id') id: number) {
        var arr = Array();
        arr.push("image_metal_products");
        arr.push("image_stone_color_products");
        arr.push("image_stone_shape_color_products");
        arr.push("image_stone_shape_products");
        arr.push("image_stone_side_products");
        
        arr.push("category_product");
        arr.push("metal_product");
        arr.push("product_stone_center");
        arr.push("product_stone_color");
        arr.push("product_stone_shape");
        arr.push("product_stone_side");
        arr.push("product_type_size");
        arr.push("product_variation");
        let data = {};
        var extraCondition = "product_id = " + id;
        let sql = "";
        arr.forEach(element => {
            sql += 'DELETE FROM ' + element + ' WHERE ' + extraCondition + ";";
        });
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
