import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ImageStoneColorProducts} from '../models';
import {ImageStoneColorProductsRepository} from '../repositories';

export class ImageStoneColorProductsController {
  constructor(
    @repository(ImageStoneColorProductsRepository)
    public imageStoneColorProductsRepository : ImageStoneColorProductsRepository,
  ) {}

  @post('/image-stone-color-products')
  @response(200, {
    description: 'ImageStoneColorProducts model instance',
    content: {'application/json': {schema: getModelSchemaRef(ImageStoneColorProducts)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneColorProducts, {
            title: 'NewImageStoneColorProducts',
            exclude: ['id'],
          }),
        },
      },
    })
    imageStoneColorProducts: Omit<ImageStoneColorProducts, 'id'>,
  ): Promise<ImageStoneColorProducts> {
    return this.imageStoneColorProductsRepository.create(imageStoneColorProducts);
  }

  @get('/image-stone-color-products/count')
  @response(200, {
    description: 'ImageStoneColorProducts model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ImageStoneColorProducts) where?: Where<ImageStoneColorProducts>,
  ): Promise<Count> {
    return this.imageStoneColorProductsRepository.count(where);
  }

  @get('/image-stone-color-products')
  @response(200, {
    description: 'Array of ImageStoneColorProducts model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ImageStoneColorProducts, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ImageStoneColorProducts) filter?: Filter<ImageStoneColorProducts>,
  ): Promise<ImageStoneColorProducts[]> {
    return this.imageStoneColorProductsRepository.find(filter);
  }

  @patch('/image-stone-color-products')
  @response(200, {
    description: 'ImageStoneColorProducts PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneColorProducts, {partial: true}),
        },
      },
    })
    imageStoneColorProducts: ImageStoneColorProducts,
    @param.where(ImageStoneColorProducts) where?: Where<ImageStoneColorProducts>,
  ): Promise<Count> {
    return this.imageStoneColorProductsRepository.updateAll(imageStoneColorProducts, where);
  }

  @get('/image-stone-color-products/{id}')
  @response(200, {
    description: 'ImageStoneColorProducts model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ImageStoneColorProducts, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ImageStoneColorProducts, {exclude: 'where'}) filter?: FilterExcludingWhere<ImageStoneColorProducts>
  ): Promise<ImageStoneColorProducts> {
    return this.imageStoneColorProductsRepository.findById(id, filter);
  }

  @patch('/image-stone-color-products/{id}')
  @response(204, {
    description: 'ImageStoneColorProducts PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageStoneColorProducts, {partial: true}),
        },
      },
    })
    imageStoneColorProducts: ImageStoneColorProducts,
  ): Promise<void> {
    await this.imageStoneColorProductsRepository.updateById(id, imageStoneColorProducts);
  }

  @put('/image-stone-color-products/{id}')
  @response(204, {
    description: 'ImageStoneColorProducts PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() imageStoneColorProducts: ImageStoneColorProducts,
  ): Promise<void> {
    await this.imageStoneColorProductsRepository.replaceById(id, imageStoneColorProducts);
  }

  @del('/image-stone-color-products/{id}')
  @response(204, {
    description: 'ImageStoneColorProducts DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.imageStoneColorProductsRepository.deleteById(id);
  }
}
