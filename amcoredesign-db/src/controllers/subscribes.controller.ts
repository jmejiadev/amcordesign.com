import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Subscribes} from '../models';
import {SubscribesRepository} from '../repositories';

export class SubscribesController {
  constructor(
    @repository(SubscribesRepository)
    public subscribesRepository : SubscribesRepository,
  ) {}

  @post('/subscribes')
  @response(200, {
    description: 'Subscribes model instance',
    content: {'application/json': {schema: getModelSchemaRef(Subscribes)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscribes, {
            title: 'NewSubscribes',
            exclude: ['id'],
          }),
        },
      },
    })
    subscribes: Omit<Subscribes, 'id'>,
  ): Promise<Subscribes> {
    return this.subscribesRepository.create(subscribes);
  }

  @get('/subscribes/count')
  @response(200, {
    description: 'Subscribes model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Subscribes) where?: Where<Subscribes>,
  ): Promise<Count> {
    return this.subscribesRepository.count(where);
  }

  @get('/subscribes')
  @response(200, {
    description: 'Array of Subscribes model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Subscribes, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Subscribes) filter?: Filter<Subscribes>,
  ): Promise<Subscribes[]> {
    return this.subscribesRepository.find(filter);
  }

  @patch('/subscribes')
  @response(200, {
    description: 'Subscribes PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscribes, {partial: true}),
        },
      },
    })
    subscribes: Subscribes,
    @param.where(Subscribes) where?: Where<Subscribes>,
  ): Promise<Count> {
    return this.subscribesRepository.updateAll(subscribes, where);
  }

  @get('/subscribes/{id}')
  @response(200, {
    description: 'Subscribes model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Subscribes, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Subscribes, {exclude: 'where'}) filter?: FilterExcludingWhere<Subscribes>
  ): Promise<Subscribes> {
    return this.subscribesRepository.findById(id, filter);
  }

  @patch('/subscribes/{id}')
  @response(204, {
    description: 'Subscribes PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscribes, {partial: true}),
        },
      },
    })
    subscribes: Subscribes,
  ): Promise<void> {
    await this.subscribesRepository.updateById(id, subscribes);
  }

  @put('/subscribes/{id}')
  @response(204, {
    description: 'Subscribes PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() subscribes: Subscribes,
  ): Promise<void> {
    await this.subscribesRepository.replaceById(id, subscribes);
  }

  @del('/subscribes/{id}')
  @response(204, {
    description: 'Subscribes DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.subscribesRepository.deleteById(id);
  }
}
