// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/core';
import {HttpErrors, post, Request, requestBody, RestBindings} from '@loopback/rest';
import {PostContactMailDto} from '../types/Dtos';
import Mailing from '../utils/Mailing';

const {
  CONTACT_SENDER_EMAIL_ADDRESS,
  CONTACT_TO_EMAIL_ADDRESS,
} = process.env;

export declare const ContactSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

export class ContactController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) { }

  @post('/contact')
  async Contact(
    @requestBody() contactInfo: PostContactMailDto,
  ) {
    const {
      firstName,
      lastName,
      email,
      phone,
      message,
    } = contactInfo;

    try {
      await Mailing.sendEmail({
        from: CONTACT_SENDER_EMAIL_ADDRESS,
        to: CONTACT_TO_EMAIL_ADDRESS,
        subject: 'Contact from website',
        text: `${firstName} ${lastName} wants to contact us: ${message} ${email} ${phone}`
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(e.message);
    }

    return;
  }
}
