// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const ProductsStoneColorSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetProductsStoneColor {
    @get('/get-products-stone-color/')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: ProductsStoneColorSchema } },
    })
    async function() {
        let data = {};
        var sql = 'SELECT psc.*, sc.name FROM `product_stone_color` psc inner join stone_colors sc on psc.stone_color_id = sc.id ;';
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
