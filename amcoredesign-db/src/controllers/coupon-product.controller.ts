import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CouponProduct} from '../models';
import {CouponProductRepository} from '../repositories';

export class CouponProductController {
  constructor(
    @repository(CouponProductRepository)
    public couponProductRepository : CouponProductRepository,
  ) {}

  @post('/coupon-products')
  @response(200, {
    description: 'CouponProduct model instance',
    content: {'application/json': {schema: getModelSchemaRef(CouponProduct)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CouponProduct, {
            title: 'NewCouponProduct',
            exclude: ['id'],
          }),
        },
      },
    })
    couponProduct: Omit<CouponProduct, 'id'>,
  ): Promise<CouponProduct> {
    return this.couponProductRepository.create(couponProduct);
  }

  @get('/coupon-products/count')
  @response(200, {
    description: 'CouponProduct model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CouponProduct) where?: Where<CouponProduct>,
  ): Promise<Count> {
    return this.couponProductRepository.count(where);
  }

  @get('/coupon-products')
  @response(200, {
    description: 'Array of CouponProduct model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CouponProduct, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CouponProduct) filter?: Filter<CouponProduct>,
  ): Promise<CouponProduct[]> {
    return this.couponProductRepository.find(filter);
  }

  @patch('/coupon-products')
  @response(200, {
    description: 'CouponProduct PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CouponProduct, {partial: true}),
        },
      },
    })
    couponProduct: CouponProduct,
    @param.where(CouponProduct) where?: Where<CouponProduct>,
  ): Promise<Count> {
    return this.couponProductRepository.updateAll(couponProduct, where);
  }

  @get('/coupon-products/{id}')
  @response(200, {
    description: 'CouponProduct model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CouponProduct, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CouponProduct, {exclude: 'where'}) filter?: FilterExcludingWhere<CouponProduct>
  ): Promise<CouponProduct> {
    return this.couponProductRepository.findById(id, filter);
  }

  @patch('/coupon-products/{id}')
  @response(204, {
    description: 'CouponProduct PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CouponProduct, {partial: true}),
        },
      },
    })
    couponProduct: CouponProduct,
  ): Promise<void> {
    await this.couponProductRepository.updateById(id, couponProduct);
  }

  @put('/coupon-products/{id}')
  @response(204, {
    description: 'CouponProduct PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() couponProduct: CouponProduct,
  ): Promise<void> {
    await this.couponProductRepository.replaceById(id, couponProduct);
  }

  @del('/coupon-products/{id}')
  @response(204, {
    description: 'CouponProduct DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.couponProductRepository.deleteById(id);
  }
}
