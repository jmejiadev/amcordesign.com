import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ThreeSixties} from '../models';
import {ThreeSixtiesRepository} from '../repositories';

export class ThreeSixtiesController {
  constructor(
    @repository(ThreeSixtiesRepository)
    public threeSixtiesRepository : ThreeSixtiesRepository,
  ) {}

  @post('/three-sixties')
  @response(200, {
    description: 'ThreeSixties model instance',
    content: {'application/json': {schema: getModelSchemaRef(ThreeSixties)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ThreeSixties, {
            title: 'NewThreeSixties',
            exclude: ['id'],
          }),
        },
      },
    })
    threeSixties: Omit<ThreeSixties, 'id'>,
  ): Promise<ThreeSixties> {
    return this.threeSixtiesRepository.create(threeSixties);
  }

  @get('/three-sixties/count')
  @response(200, {
    description: 'ThreeSixties model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ThreeSixties) where?: Where<ThreeSixties>,
  ): Promise<Count> {
    return this.threeSixtiesRepository.count(where);
  }

  @get('/three-sixties')
  @response(200, {
    description: 'Array of ThreeSixties model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ThreeSixties, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ThreeSixties) filter?: Filter<ThreeSixties>,
  ): Promise<ThreeSixties[]> {
    return this.threeSixtiesRepository.find(filter);
  }

  @patch('/three-sixties')
  @response(200, {
    description: 'ThreeSixties PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ThreeSixties, {partial: true}),
        },
      },
    })
    threeSixties: ThreeSixties,
    @param.where(ThreeSixties) where?: Where<ThreeSixties>,
  ): Promise<Count> {
    return this.threeSixtiesRepository.updateAll(threeSixties, where);
  }

  @get('/three-sixties/{id}')
  @response(200, {
    description: 'ThreeSixties model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ThreeSixties, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ThreeSixties, {exclude: 'where'}) filter?: FilterExcludingWhere<ThreeSixties>
  ): Promise<ThreeSixties> {
    return this.threeSixtiesRepository.findById(id, filter);
  }

  @patch('/three-sixties/{id}')
  @response(204, {
    description: 'ThreeSixties PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ThreeSixties, {partial: true}),
        },
      },
    })
    threeSixties: ThreeSixties,
  ): Promise<void> {
    await this.threeSixtiesRepository.updateById(id, threeSixties);
  }

  @put('/three-sixties/{id}')
  @response(204, {
    description: 'ThreeSixties PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() threeSixties: ThreeSixties,
  ): Promise<void> {
    await this.threeSixtiesRepository.replaceById(id, threeSixties);
  }

  @del('/three-sixties/{id}')
  @response(204, {
    description: 'ThreeSixties DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.threeSixtiesRepository.deleteById(id);
  }
}
