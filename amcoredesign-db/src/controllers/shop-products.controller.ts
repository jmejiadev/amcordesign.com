import { get, HttpErrors, response } from '@loopback/rest';
import { RowDataPacket } from 'mysql2';
import { ProductSimplePreviewDto, ShopCarouselDto, ShopCarouselProductData } from '../types/Dtos';
import { HomeShopProductResult, HomeShopResult } from '../types/models';
import { connectionPoolPromise, throwRequestError } from '../utils';
import { parseInlineImagesToArray } from '../utils/inlineImagesToArray';

export declare const ShopProductsSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

interface IHomeShopResult extends RowDataPacket, HomeShopResult {
}

interface IHomeShopProductResult extends RowDataPacket, HomeShopProductResult {
}

export class ShopProductsController {

  @get('/shop-products')
  @response(200, {
    description: 'Shop products',
    content: { 'application/json': { schema: ShopProductsSchema } },
  })
  async function() {
    let sql = `
      SELECT
        id,
        path 
      FROM home_shop
    `
    try {
      const [result, fields] = await connectionPoolPromise.query<IHomeShopResult[]>(sql);
      const shopResult: ShopCarouselDto[] = await Promise.all(
        result.map(async (homeShop): Promise<ShopCarouselDto> => {
          sql = `
            SELECT
              products.id,
              products.name,
              products.slug,
              (products.manufacturer + (metals.price * products.grams_weight) + IFNULL(stoneColor.price, 0)) AS sellingPrice,
              products.discount,
              stoneColor.carat AS stoneColorCarat,
              stoneColor.stone_color_id AS stoneColorId,
              
              home_shop_products.path AS shopProductPath,
              home_shop_products.metal_id AS shopProductMetalId,
              home_shop_products.stone_shape_id AS shopProductStoneShapeId,
              home_shop_products.stone_center_id AS shopProductStoneCenterId,
              home_shop_products.stone_side_id AS shopProductStoneSideId,
              home_shop_products.stone_color_id AS shopProductStoneColorId,
              
              ctwPreview.path AS ctwPreviewPath,
              ctwPreview.metal_id AS ctwPreviewMetalId,
              ctwPreview.stone_shape_id AS ctwPreviewStoneShapeId,
              ctwPreview.stone_center_id AS ctwPreviewStoneCenterId,
            
              stoneShapePreview.path AS stoneShapePreviewPath,
              stoneShapePreview.metal_id AS stoneShapePreviewMetalId,
              stoneShapePreview.stone_shape_id AS stoneShapePreviewStoneShapeId,
            
              stoneSidePreview.path AS stoneSidePreviewPath,
              stoneSidePreview.metal_id AS stoneSidePreviewMetalId,
              stoneSidePreview.stone_side_id AS stoneSidePreviewStoneSideId,
              stoneSidePreview.stone_color_id AS stoneSidePreviewStoneColorId,
                    
              home_shop_products.positionX,
              home_shop_products.positionY
              
            FROM home_shop_products
              INNER JOIN products ON home_shop_products.product_id = products.id
              INNER JOIN metals ON metals.id = home_shop_products.metal_id
              
              LEFT JOIN (
                SELECT
                  center_shape_ctws.product_id,
                  center_shape_ctws.sku,
                  image_center_shape_ctws.path,
                  image_center_shape_ctws.metal_id,
                  image_center_shape_ctws.stone_shape_id,
                  image_center_shape_ctws.stone_center_id 
                FROM products
                  INNER JOIN center_shape_ctws ON products.id = center_shape_ctws.product_id
                  INNER JOIN image_center_shape_ctws ON center_shape_ctws.id = image_center_shape_ctws.center_shape_ctw_id 
                ORDER BY
                  image_center_shape_ctws.metal_id,
                  image_center_shape_ctws.stone_shape_id,
                  image_center_shape_ctws.stone_center_id
              ) AS ctwPreview ON products.id = ctwPreview.product_id
                AND home_shop_products.metal_id = ctwPreview.metal_id
                AND IFNULL(home_shop_products.stone_shape_id, ctwPreview.stone_shape_id) = ctwPreview.stone_shape_id
                AND IFNULL(home_shop_products.stone_center_id, ctwPreview.stone_center_id) = ctwPreview.stone_center_id
                
              LEFT JOIN (
                SELECT
                  image_stone_shape_products.product_id,
                  image_stone_shape_products.path,
                  image_stone_shape_products.metal_id,
                  image_stone_shape_products.stone_shape_id 
                FROM products
                  INNER JOIN image_stone_shape_products ON products.id = image_stone_shape_products.product_id
                ORDER BY
                  image_stone_shape_products.metal_id,
                  image_stone_shape_products.stone_shape_id
              ) AS stoneShapePreview ON products.id = stoneShapePreview.product_id
                AND home_shop_products.metal_id = stoneShapePreview.metal_id
                AND IFNULL(home_shop_products.stone_shape_id, stoneShapePreview.stone_shape_id) = stoneShapePreview.stone_shape_id
                
              LEFT JOIN (
                SELECT
                  image_stone_side_products.product_id,
                  image_stone_side_products.path,
                  image_stone_side_products.metal_id,
                  image_stone_side_products.stone_side_id,
                  image_stone_side_products.stone_color_id 
                FROM products
                  INNER JOIN image_stone_side_products ON products.id = image_stone_side_products.product_id
                ORDER BY
                  image_stone_side_products.metal_id,
                  image_stone_side_products.stone_side_id,
                  image_stone_side_products.stone_color_id
              ) AS stoneSidePreview ON products.id = stoneSidePreview .product_id
                AND home_shop_products.metal_id = stoneSidePreview.metal_id
                AND IFNULL(home_shop_products.stone_side_id, stoneSidePreview.stone_side_id) = stoneSidePreview.stone_side_id
                AND IFNULL(home_shop_products.stone_color_id, stoneSidePreview.stone_color_id) = stoneSidePreview.stone_color_id
                
              LEFT JOIN (
                SELECT
                  product_stone_color.product_id,
                  product_stone_color.price, 
                  product_stone_color.carat, 
                  product_stone_color.stone_color_id
                FROM product_stone_color
                ORDER BY 
                  product_stone_color.stone_color_id
              ) AS stoneColor ON home_shop_products.product_id = stoneColor.product_id
                AND IFNULL(home_shop_products.stone_color_id, stoneColor.stone_color_id) = stoneColor.stone_color_id
            
            WHERE home_shop_products.home_shop_id = :homeShopId
            GROUP BY products.id
          `

          const namedParams = {
            homeShopId: homeShop.id,
          };

          const [homeShopProductResult, fields] = await connectionPoolPromise.execute<IHomeShopProductResult[]>(sql, namedParams);
          const products: ShopCarouselProductData[] = homeShopProductResult.map<ShopCarouselProductData>((homeShopProduct: IHomeShopProductResult) => {
            const shopPathInfo: ProductSimplePreviewDto = {
              path: homeShopProduct.shopProductPath,
              metalId: homeShopProduct.shopProductMetalId,
              stoneShapeId: homeShopProduct.shopProductStoneShapeId,
              stoneCenterId: homeShopProduct.shopProductStoneCenterId,
              stoneSideId: homeShopProduct.shopProductStoneSideId,
              stoneColorId: homeShopProduct.shopProductStoneColorId,
            }
            return {
              id: homeShopProduct.id,
              name: homeShopProduct.name,
              slug: homeShopProduct.slug,
              sellingPrice: homeShopProduct.sellingPrice,
              discount: +homeShopProduct.discount,
              stoneColorCarat: homeShopProduct.stoneColorCarat,
              stoneColorId: homeShopProduct.stoneColorId,
              imagePreview: [shopPathInfo, ...parseInlineImagesToArray(homeShopProduct)],
              positionX: homeShopProduct.positionX,
              positionY: homeShopProduct.positionY,
            }
          })

          return {
            imgSrc: homeShop.path,
            products,
          }
        })
      );
      return shopResult;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }
  }
}
