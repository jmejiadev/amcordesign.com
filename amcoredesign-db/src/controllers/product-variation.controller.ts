import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductVariation} from '../models';
import {ProductVariationRepository} from '../repositories';

export class ProductVariationController {
  constructor(
    @repository(ProductVariationRepository)
    public productVariationRepository : ProductVariationRepository,
  ) {}

  @post('/product-variations')
  @response(200, {
    description: 'ProductVariation model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductVariation)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductVariation, {
            title: 'NewProductVariation',
            exclude: ['id'],
          }),
        },
      },
    })
    productVariation: Omit<ProductVariation, 'id'>,
  ): Promise<ProductVariation> {
    return this.productVariationRepository.create(productVariation);
  }

  @get('/product-variations/count')
  @response(200, {
    description: 'ProductVariation model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductVariation) where?: Where<ProductVariation>,
  ): Promise<Count> {
    return this.productVariationRepository.count(where);
  }

  @get('/product-variations')
  @response(200, {
    description: 'Array of ProductVariation model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductVariation, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductVariation) filter?: Filter<ProductVariation>,
  ): Promise<ProductVariation[]> {
    return this.productVariationRepository.find(filter);
  }

  @patch('/product-variations')
  @response(200, {
    description: 'ProductVariation PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductVariation, {partial: true}),
        },
      },
    })
    productVariation: ProductVariation,
    @param.where(ProductVariation) where?: Where<ProductVariation>,
  ): Promise<Count> {
    return this.productVariationRepository.updateAll(productVariation, where);
  }

  @get('/product-variations/{id}')
  @response(200, {
    description: 'ProductVariation model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductVariation, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductVariation, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductVariation>
  ): Promise<ProductVariation> {
    return this.productVariationRepository.findById(id, filter);
  }

  @patch('/product-variations/{id}')
  @response(204, {
    description: 'ProductVariation PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductVariation, {partial: true}),
        },
      },
    })
    productVariation: ProductVariation,
  ): Promise<void> {
    await this.productVariationRepository.updateById(id, productVariation);
  }

  @put('/product-variations/{id}')
  @response(204, {
    description: 'ProductVariation PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productVariation: ProductVariation,
  ): Promise<void> {
    await this.productVariationRepository.replaceById(id, productVariation);
  }

  @del('/product-variations/{id}')
  @response(204, {
    description: 'ProductVariation DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productVariationRepository.deleteById(id);
  }
}
