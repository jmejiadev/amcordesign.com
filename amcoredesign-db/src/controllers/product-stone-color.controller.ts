import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductStoneColor} from '../models';
import {ProductStoneColorRepository} from '../repositories';

export class ProductStoneColorController {
  constructor(
    @repository(ProductStoneColorRepository)
    public productStoneColorRepository : ProductStoneColorRepository,
  ) {}

  @post('/product-stone-colors')
  @response(200, {
    description: 'ProductStoneColor model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductStoneColor)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneColor, {
            title: 'NewProductStoneColor',
            exclude: ['id'],
          }),
        },
      },
    })
    productStoneColor: Omit<ProductStoneColor, 'id'>,
  ): Promise<ProductStoneColor> {
    return this.productStoneColorRepository.create(productStoneColor);
  }

  @get('/product-stone-colors/count')
  @response(200, {
    description: 'ProductStoneColor model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductStoneColor) where?: Where<ProductStoneColor>,
  ): Promise<Count> {
    return this.productStoneColorRepository.count(where);
  }

  @get('/product-stone-colors')
  @response(200, {
    description: 'Array of ProductStoneColor model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductStoneColor, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductStoneColor) filter?: Filter<ProductStoneColor>,
  ): Promise<ProductStoneColor[]> {
    return this.productStoneColorRepository.find(filter);
  }

  @patch('/product-stone-colors')
  @response(200, {
    description: 'ProductStoneColor PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneColor, {partial: true}),
        },
      },
    })
    productStoneColor: ProductStoneColor,
    @param.where(ProductStoneColor) where?: Where<ProductStoneColor>,
  ): Promise<Count> {
    return this.productStoneColorRepository.updateAll(productStoneColor, where);
  }

  @get('/product-stone-colors/{id}')
  @response(200, {
    description: 'ProductStoneColor model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductStoneColor, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductStoneColor, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductStoneColor>
  ): Promise<ProductStoneColor> {
    return this.productStoneColorRepository.findById(id, filter);
  }

  @patch('/product-stone-colors/{id}')
  @response(204, {
    description: 'ProductStoneColor PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductStoneColor, {partial: true}),
        },
      },
    })
    productStoneColor: ProductStoneColor,
  ): Promise<void> {
    await this.productStoneColorRepository.updateById(id, productStoneColor);
  }

  @put('/product-stone-colors/{id}')
  @response(204, {
    description: 'ProductStoneColor PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productStoneColor: ProductStoneColor,
  ): Promise<void> {
    await this.productStoneColorRepository.replaceById(id, productStoneColor);
  }

  @del('/product-stone-colors/{id}')
  @response(204, {
    description: 'ProductStoneColor DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productStoneColorRepository.deleteById(id);
  }
}
