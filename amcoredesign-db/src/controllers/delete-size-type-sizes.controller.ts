// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteSizeTypeSizesSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteSizeTypeSizes {
    @get('/del-size-type-sizes/{id}')
    @response(200, {
        description: 'Delete Size Type Sizes',
        content: { 'application/json': { schema: DeleteSizeTypeSizesSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        let sql = "DELETE FROM `size_type_size` where type_size_id = "+id+";";
        try {
            let result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
