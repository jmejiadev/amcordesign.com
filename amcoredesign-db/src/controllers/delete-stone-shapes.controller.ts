// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteStoneShapesSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteStoneShapes {
    @get('/del-stone-shapes/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: DeleteStoneShapesSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        try {
            let sql = "SELECT * FROM order_products where stone_shape_id =" + id;
            let result = await connectionPoolPromise.query(sql);
            sql = "";
            var order_product_info = Array();
            order_product_info.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array = order_product_info[0];
            array.forEach(element => {
                var order_product_id = element.id;
                var arr = Array();
                arr.push("order_products_variants");
                arr.push("order_products_sizes");
                arr.forEach(element => {
                    sql += 'DELETE FROM ' + element + ' WHERE order_product_id = ' + order_product_id + ";";
                });
            });
            var arr = Array();
            arr.push("image_stone_shape_products");
            arr.push("image_center_shape_ctws");
            arr.push("image_stone_shape_color_products");
            arr.push("product_stone_shape");
            arr.push("order_products");
            arr.push("center_shape_ctw_stone_shape");
            arr.push("home_shop_products");
            arr.push("three_sixties");

            var extraCondition = "stone_shape_id = " + id;
            arr.forEach(element => {
                sql += 'DELETE FROM ' + element + ' WHERE ' + extraCondition + ";";
            });
            result = await connectionPoolPromise.query(sql);
            data = result;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
