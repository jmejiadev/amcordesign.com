import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CategoryStyle} from '../models';
import {CategoryStyleRepository} from '../repositories';

export class CategoryStyleController {
  constructor(
    @repository(CategoryStyleRepository)
    public categoryStyleRepository : CategoryStyleRepository,
  ) {}

  @post('/category-styles')
  @response(200, {
    description: 'CategoryStyle model instance',
    content: {'application/json': {schema: getModelSchemaRef(CategoryStyle)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CategoryStyle, {
            title: 'NewCategoryStyle',
            exclude: ['id'],
          }),
        },
      },
    })
    categoryStyle: Omit<CategoryStyle, 'id'>,
  ): Promise<CategoryStyle> {
    return this.categoryStyleRepository.create(categoryStyle);
  }

  @get('/category-styles/count')
  @response(200, {
    description: 'CategoryStyle model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CategoryStyle) where?: Where<CategoryStyle>,
  ): Promise<Count> {
    return this.categoryStyleRepository.count(where);
  }

  @get('/category-styles')
  @response(200, {
    description: 'Array of CategoryStyle model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CategoryStyle, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CategoryStyle) filter?: Filter<CategoryStyle>,
  ): Promise<CategoryStyle[]> {
    return this.categoryStyleRepository.find(filter);
  }

  @patch('/category-styles')
  @response(200, {
    description: 'CategoryStyle PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CategoryStyle, {partial: true}),
        },
      },
    })
    categoryStyle: CategoryStyle,
    @param.where(CategoryStyle) where?: Where<CategoryStyle>,
  ): Promise<Count> {
    return this.categoryStyleRepository.updateAll(categoryStyle, where);
  }

  @get('/category-styles/{id}')
  @response(200, {
    description: 'CategoryStyle model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CategoryStyle, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CategoryStyle, {exclude: 'where'}) filter?: FilterExcludingWhere<CategoryStyle>
  ): Promise<CategoryStyle> {
    return this.categoryStyleRepository.findById(id, filter);
  }

  @patch('/category-styles/{id}')
  @response(204, {
    description: 'CategoryStyle PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CategoryStyle, {partial: true}),
        },
      },
    })
    categoryStyle: CategoryStyle,
  ): Promise<void> {
    await this.categoryStyleRepository.updateById(id, categoryStyle);
  }

  @put('/category-styles/{id}')
  @response(204, {
    description: 'CategoryStyle PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() categoryStyle: CategoryStyle,
  ): Promise<void> {
    await this.categoryStyleRepository.replaceById(id, categoryStyle);
  }

  @del('/category-styles/{id}')
  @response(204, {
    description: 'CategoryStyle DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.categoryStyleRepository.deleteById(id);
  }
}
