import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ProductTypeSize} from '../models';
import {ProductTypeSizeRepository} from '../repositories';

export class ProductTypeSizeController {
  constructor(
    @repository(ProductTypeSizeRepository)
    public productTypeSizeRepository : ProductTypeSizeRepository,
  ) {}

  @post('/product-type-sizes')
  @response(200, {
    description: 'ProductTypeSize model instance',
    content: {'application/json': {schema: getModelSchemaRef(ProductTypeSize)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductTypeSize, {
            title: 'NewProductTypeSize',
            exclude: ['id'],
          }),
        },
      },
    })
    productTypeSize: Omit<ProductTypeSize, 'id'>,
  ): Promise<ProductTypeSize> {
    return this.productTypeSizeRepository.create(productTypeSize);
  }

  @get('/product-type-sizes/count')
  @response(200, {
    description: 'ProductTypeSize model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ProductTypeSize) where?: Where<ProductTypeSize>,
  ): Promise<Count> {
    return this.productTypeSizeRepository.count(where);
  }

  @get('/product-type-sizes')
  @response(200, {
    description: 'Array of ProductTypeSize model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ProductTypeSize, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ProductTypeSize) filter?: Filter<ProductTypeSize>,
  ): Promise<ProductTypeSize[]> {
    return this.productTypeSizeRepository.find(filter);
  }

  @patch('/product-type-sizes')
  @response(200, {
    description: 'ProductTypeSize PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductTypeSize, {partial: true}),
        },
      },
    })
    productTypeSize: ProductTypeSize,
    @param.where(ProductTypeSize) where?: Where<ProductTypeSize>,
  ): Promise<Count> {
    return this.productTypeSizeRepository.updateAll(productTypeSize, where);
  }

  @get('/product-type-sizes/{id}')
  @response(200, {
    description: 'ProductTypeSize model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ProductTypeSize, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProductTypeSize, {exclude: 'where'}) filter?: FilterExcludingWhere<ProductTypeSize>
  ): Promise<ProductTypeSize> {
    return this.productTypeSizeRepository.findById(id, filter);
  }

  @patch('/product-type-sizes/{id}')
  @response(204, {
    description: 'ProductTypeSize PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProductTypeSize, {partial: true}),
        },
      },
    })
    productTypeSize: ProductTypeSize,
  ): Promise<void> {
    await this.productTypeSizeRepository.updateById(id, productTypeSize);
  }

  @put('/product-type-sizes/{id}')
  @response(204, {
    description: 'ProductTypeSize PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() productTypeSize: ProductTypeSize,
  ): Promise<void> {
    await this.productTypeSizeRepository.replaceById(id, productTypeSize);
  }

  @del('/product-type-sizes/{id}')
  @response(204, {
    description: 'ProductTypeSize DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.productTypeSizeRepository.deleteById(id);
  }
}
