// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteSizesSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteSizes {
    @get('/del-sizes/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: DeleteSizesSchema } },
    })
    async function(@param.path.number('id') id: number) {
        var arr = Array();
        arr.push("size_type_size");

        let data = {};
        var extraCondition = "size_id = " + id;
        let sql = "";
        arr.forEach(element => {
            sql += 'DELETE FROM ' + element + ' WHERE ' + extraCondition + ";";
        });
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result;
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
