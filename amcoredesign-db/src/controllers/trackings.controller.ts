import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Trackings} from '../models';
import {TrackingsRepository} from '../repositories';

export class TrackingsController {
  constructor(
    @repository(TrackingsRepository)
    public trackingsRepository : TrackingsRepository,
  ) {}

  @post('/trackings')
  @response(200, {
    description: 'Trackings model instance',
    content: {'application/json': {schema: getModelSchemaRef(Trackings)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Trackings, {
            title: 'NewTrackings',
            exclude: ['id'],
          }),
        },
      },
    })
    trackings: Omit<Trackings, 'id'>,
  ): Promise<Trackings> {
    return this.trackingsRepository.create(trackings);
  }

  @get('/trackings/count')
  @response(200, {
    description: 'Trackings model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Trackings) where?: Where<Trackings>,
  ): Promise<Count> {
    return this.trackingsRepository.count(where);
  }

  @get('/trackings')
  @response(200, {
    description: 'Array of Trackings model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Trackings, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Trackings) filter?: Filter<Trackings>,
  ): Promise<Trackings[]> {
    return this.trackingsRepository.find(filter);
  }

  @patch('/trackings')
  @response(200, {
    description: 'Trackings PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Trackings, {partial: true}),
        },
      },
    })
    trackings: Trackings,
    @param.where(Trackings) where?: Where<Trackings>,
  ): Promise<Count> {
    return this.trackingsRepository.updateAll(trackings, where);
  }

  @get('/trackings/{id}')
  @response(200, {
    description: 'Trackings model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Trackings, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Trackings, {exclude: 'where'}) filter?: FilterExcludingWhere<Trackings>
  ): Promise<Trackings> {
    return this.trackingsRepository.findById(id, filter);
  }

  @patch('/trackings/{id}')
  @response(204, {
    description: 'Trackings PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Trackings, {partial: true}),
        },
      },
    })
    trackings: Trackings,
  ): Promise<void> {
    await this.trackingsRepository.updateById(id, trackings);
  }

  @put('/trackings/{id}')
  @response(204, {
    description: 'Trackings PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() trackings: Trackings,
  ): Promise<void> {
    await this.trackingsRepository.replaceById(id, trackings);
  }

  @del('/trackings/{id}')
  @response(204, {
    description: 'Trackings DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.trackingsRepository.deleteById(id);
  }
}
