// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, response } from '@loopback/rest';
import { RowDataPacket } from 'mysql2';
import { HomeReviewResult } from '../types/models';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const HomeReviewsSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

interface IHomeReviewResult extends RowDataPacket, HomeReviewResult {
}

export class HomeReviewsController {

  @get('/homeReviews')
  @response(200, {
    description: 'home reviews',
    content: { 'application/json': { schema: HomeReviewsSchema } },
  })
  async function() {

    let sql = `
        SELECT
          reviews.id,
          reviews.name as author,
          reviews.description,
          review_images.path 
        FROM
          reviews
          LEFT JOIN review_images ON reviews.id = review_images.review_id 
            AND review_images.is_principal = 1
        WHERE
          show_on_home = 1
    `;

    try {
      const [rows, fields] = await connectionPoolPromise.query<IHomeReviewResult[]>(sql);
      return rows;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }
  }
}
