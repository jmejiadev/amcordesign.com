import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Phones} from '../models';
import {PhonesRepository} from '../repositories';

export class PhonesController {
  constructor(
    @repository(PhonesRepository)
    public phonesRepository : PhonesRepository,
  ) {}

  @post('/phones')
  @response(200, {
    description: 'Phones model instance',
    content: {'application/json': {schema: getModelSchemaRef(Phones)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Phones, {
            title: 'NewPhones',
            exclude: ['id'],
          }),
        },
      },
    })
    phones: Omit<Phones, 'id'>,
  ): Promise<Phones> {
    return this.phonesRepository.create(phones);
  }

  @get('/phones/count')
  @response(200, {
    description: 'Phones model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Phones) where?: Where<Phones>,
  ): Promise<Count> {
    return this.phonesRepository.count(where);
  }

  @get('/phones')
  @response(200, {
    description: 'Array of Phones model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Phones, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Phones) filter?: Filter<Phones>,
  ): Promise<Phones[]> {
    return this.phonesRepository.find(filter);
  }

  @patch('/phones')
  @response(200, {
    description: 'Phones PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Phones, {partial: true}),
        },
      },
    })
    phones: Phones,
    @param.where(Phones) where?: Where<Phones>,
  ): Promise<Count> {
    return this.phonesRepository.updateAll(phones, where);
  }

  @get('/phones/{id}')
  @response(200, {
    description: 'Phones model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Phones, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Phones, {exclude: 'where'}) filter?: FilterExcludingWhere<Phones>
  ): Promise<Phones> {
    return this.phonesRepository.findById(id, filter);
  }

  @patch('/phones/{id}')
  @response(204, {
    description: 'Phones PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Phones, {partial: true}),
        },
      },
    })
    phones: Phones,
  ): Promise<void> {
    await this.phonesRepository.updateById(id, phones);
  }

  @put('/phones/{id}')
  @response(204, {
    description: 'Phones PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() phones: Phones,
  ): Promise<void> {
    await this.phonesRepository.replaceById(id, phones);
  }

  @del('/phones/{id}')
  @response(204, {
    description: 'Phones DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.phonesRepository.deleteById(id);
  }
}
