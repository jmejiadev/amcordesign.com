// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const RelatedProductsSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class GetRelatedProducts {
    @get('/get-related-products/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: RelatedProductsSchema } },
    })
    async function(@param.path.number('id') id: number) {
        var arr = Array();
        arr.push("coupon_product");
        arr.push("home_shop_products");
        arr.push("order_products");
        arr.push("product_shopping_cart");
        arr.push("product_wish_list");

        let data = {};
        var extraCondition = "product_id = " + id;
        let sql = "";
        arr.forEach(element => {
            sql += 'SELECT * FROM ' + element + ' WHERE ' + extraCondition + ";";
        });
        try {
            const result = await connectionPoolPromise.query(sql);
            data = result[0];
        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
