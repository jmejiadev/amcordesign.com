import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ReviewImages} from '../models';
import {ReviewImagesRepository} from '../repositories';

export class ReviewImagesController {
  constructor(
    @repository(ReviewImagesRepository)
    public reviewImagesRepository : ReviewImagesRepository,
  ) {}

  @post('/review-images')
  @response(200, {
    description: 'ReviewImages model instance',
    content: {'application/json': {schema: getModelSchemaRef(ReviewImages)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ReviewImages, {
            title: 'NewReviewImages',
            exclude: ['id'],
          }),
        },
      },
    })
    reviewImages: Omit<ReviewImages, 'id'>,
  ): Promise<ReviewImages> {
    return this.reviewImagesRepository.create(reviewImages);
  }

  @get('/review-images/count')
  @response(200, {
    description: 'ReviewImages model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ReviewImages) where?: Where<ReviewImages>,
  ): Promise<Count> {
    return this.reviewImagesRepository.count(where);
  }

  @get('/review-images')
  @response(200, {
    description: 'Array of ReviewImages model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ReviewImages, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ReviewImages) filter?: Filter<ReviewImages>,
  ): Promise<ReviewImages[]> {
    return this.reviewImagesRepository.find(filter);
  }

  @patch('/review-images')
  @response(200, {
    description: 'ReviewImages PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ReviewImages, {partial: true}),
        },
      },
    })
    reviewImages: ReviewImages,
    @param.where(ReviewImages) where?: Where<ReviewImages>,
  ): Promise<Count> {
    return this.reviewImagesRepository.updateAll(reviewImages, where);
  }

  @get('/review-images/{id}')
  @response(200, {
    description: 'ReviewImages model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ReviewImages, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ReviewImages, {exclude: 'where'}) filter?: FilterExcludingWhere<ReviewImages>
  ): Promise<ReviewImages> {
    return this.reviewImagesRepository.findById(id, filter);
  }

  @patch('/review-images/{id}')
  @response(204, {
    description: 'ReviewImages PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ReviewImages, {partial: true}),
        },
      },
    })
    reviewImages: ReviewImages,
  ): Promise<void> {
    await this.reviewImagesRepository.updateById(id, reviewImages);
  }

  @put('/review-images/{id}')
  @response(204, {
    description: 'ReviewImages PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() reviewImages: ReviewImages,
  ): Promise<void> {
    await this.reviewImagesRepository.replaceById(id, reviewImages);
  }

  @del('/review-images/{id}')
  @response(204, {
    description: 'ReviewImages DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.reviewImagesRepository.deleteById(id);
  }
}
