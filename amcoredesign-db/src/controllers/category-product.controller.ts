import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {CategoryProduct} from '../models';
import {CategoryProductRepository} from '../repositories';

export class CategoryProductController {
  constructor(
    @repository(CategoryProductRepository)
    public categoryProductRepository : CategoryProductRepository,
  ) {}

  @post('/category-products')
  @response(200, {
    description: 'CategoryProduct model instance',
    content: {'application/json': {schema: getModelSchemaRef(CategoryProduct)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CategoryProduct, {
            title: 'NewCategoryProduct',
            exclude: ['id'],
          }),
        },
      },
    })
    categoryProduct: Omit<CategoryProduct, 'id'>,
  ): Promise<CategoryProduct> {
    return this.categoryProductRepository.create(categoryProduct);
  }

  @get('/category-products/count')
  @response(200, {
    description: 'CategoryProduct model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CategoryProduct) where?: Where<CategoryProduct>,
  ): Promise<Count> {
    return this.categoryProductRepository.count(where);
  }

  @get('/category-products')
  @response(200, {
    description: 'Array of CategoryProduct model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CategoryProduct, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CategoryProduct) filter?: Filter<CategoryProduct>,
  ): Promise<CategoryProduct[]> {
    return this.categoryProductRepository.find(filter);
  }

  @patch('/category-products')
  @response(200, {
    description: 'CategoryProduct PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CategoryProduct, {partial: true}),
        },
      },
    })
    categoryProduct: CategoryProduct,
    @param.where(CategoryProduct) where?: Where<CategoryProduct>,
  ): Promise<Count> {
    return this.categoryProductRepository.updateAll(categoryProduct, where);
  }

  @get('/category-products/{id}')
  @response(200, {
    description: 'CategoryProduct model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CategoryProduct, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CategoryProduct, {exclude: 'where'}) filter?: FilterExcludingWhere<CategoryProduct>
  ): Promise<CategoryProduct> {
    return this.categoryProductRepository.findById(id, filter);
  }

  @patch('/category-products/{id}')
  @response(204, {
    description: 'CategoryProduct PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CategoryProduct, {partial: true}),
        },
      },
    })
    categoryProduct: CategoryProduct,
  ): Promise<void> {
    await this.categoryProductRepository.updateById(id, categoryProduct);
  }

  @put('/category-products/{id}')
  @response(204, {
    description: 'CategoryProduct PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() categoryProduct: CategoryProduct,
  ): Promise<void> {
    await this.categoryProductRepository.replaceById(id, categoryProduct);
  }

  @del('/category-products/{id}')
  @response(204, {
    description: 'CategoryProduct DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.categoryProductRepository.deleteById(id);
  }
}
