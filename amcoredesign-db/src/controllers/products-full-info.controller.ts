// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { GetProductCategoryDto, GetProductCenterShapeCtwDto, GetProductDto, GetProductExtraVariationDto, GetProductMetalsDto, GetProductStoneCenterDto, GetProductStoneColorsDto, GetProductStoneShapeDto, GetProductStoneShapeGalleryDto, GetProductStoneSideGalleryDto, GetProductThreeSixtyGalleryDto, GetProductVariationDto } from '../types/Dtos';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const fullProductSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

interface IGetProductDto extends RowDataPacket, GetProductDto {
}
interface IGetProductThreeSixtyGalleryDto extends RowDataPacket, GetProductThreeSixtyGalleryDto {
}
export class ProductsFullInfo {
  @get('/productFullInfo/{idOrSlug}')
  @response(200, {
    description: 'Products full info ',
    content: { 'application/json': { schema: fullProductSchema } },
  })
  async function(@param.path.string('idOrSlug') idOrSlug: string) {
    let data: GetProductDto;

    /*BASIC INFORMATION*/
    let sql = `
      SELECT
        products.id,
        products.name,
        products.short_description,
        products.description,
        products.local_sku,
        products.manufacturer,
        products.slug,
        products.stone_number,
        products.grams_weight,
        products.average_width,
        products.finish,
        products.setting,
        products.discount
      FROM products
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields] = await connectionPoolPromise.execute<IGetProductDto[]>(sql, { idOrSlug });
      if (rows.length > 0) {
        data = rows[0];
      } else {
        return new HttpErrors.NotFound();
      }
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT CATEGORIES*/
    sql = `
      SELECT
        categories.id,
        categories.name as title,
        categories.slug
      FROM products
        INNER JOIN category_product ON products.id = category_product.product_id
        INNER JOIN categories ON category_product.category_id = categories.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.categories = rows as GetProductCategoryDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT METALS*/
    sql = `
      SELECT
        metals.id,
        metals.name,
        metals.icon,
        metals.price
      FROM products
        INNER JOIN metal_product ON products.id = metal_product.product_id
        INNER JOIN metals ON metal_product.metal_id = metals.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.metals = rows as GetProductMetalsDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT STONE COLORS*/
    sql = `
      SELECT
        stone_colors.id,
        stone_colors.name,
        stone_colors.treatment,
        stone_colors.quality,
        stone_colors.cut,
        stone_colors.icon,
        product_stone_color.price,
        product_stone_color.carat
      FROM products
        INNER JOIN product_stone_color ON products.id = product_stone_color.product_id
        INNER JOIN stone_colors ON product_stone_color.stone_color_id = stone_colors.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.stoneColors = rows as GetProductStoneColorsDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT TYPE SIZES*/
    sql = `
      SELECT
        type_sizes.id AS typeSizeId,
        type_sizes.name AS typeSizeName
      FROM products
        INNER JOIN product_type_size ON products.id = product_type_size.product_id
        INNER JOIN type_sizes ON product_type_size.type_size_id = type_sizes.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.typeSizes = rows.map(r => ({
        id: r.typeSizeId,
        name: r.typeSizeName,
        sizes: []
      }));
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT SIZES*/
    sql = `
      SELECT
        product_type_size.type_size_id AS typeSizeId,
        sizes.id AS sizeId,
        sizes.number AS sizeNumber,
        sizes.price AS sizePrice
      FROM products
        INNER JOIN product_type_size ON products.id = product_type_size.product_id
        INNER JOIN size_type_size ON product_type_size.type_size_id = size_type_size.type_size_id
        INNER JOIN sizes ON size_type_size.size_id = sizes.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      rows.forEach(r => {
        const i = data.typeSizes.findIndex((x: any) => x.id === r.typeSizeId);
        data.typeSizes[i].sizes.push({
          id: r.sizeId,
          number: r.sizeNumber,
          price: r.sizePrice
        });
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT EXTRA VARIATIONS*/
    sql = `
      SELECT
        extra_variations.id,
        extra_variations.name,
        extra_variations.description
      FROM products
        INNER JOIN product_variation ON products.id = product_variation.product_id
        INNER JOIN variations ON product_variation.variation_id = variations.id
        INNER JOIN extra_variations ON variations.extra_variation_id = extra_variations.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
      GROUP BY extra_variations.id
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.extraVariations = rows as GetProductExtraVariationDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT VARIATIONS*/
    sql = `
      SELECT
        variations.id,
        variations.name,
        variations.description,
        variations.icon,
        variations.extra_variation_id
      FROM products
        INNER JOIN product_variation ON products.id = product_variation.product_id
        INNER JOIN variations ON product_variation.variation_id = variations.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.variations = rows as GetProductVariationDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT STONE SHAPES*/
    sql = `
      SELECT
        stone_shapes.id,
        stone_shapes.price
      FROM products
        INNER JOIN product_stone_shape ON products.id = product_stone_shape.product_id
        INNER JOIN stone_shapes ON product_stone_shape.stone_shape_id = stone_shapes.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.stone_shapes = rows as GetProductStoneShapeDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT STONE CENTERS*/
    sql = `
      SELECT
        stone_centers.id,
        stone_centers.name,
        stone_centers.size,
        stone_centers.price
      FROM products
        INNER JOIN product_stone_center ON products.id = product_stone_center.product_id
        INNER JOIN stone_centers ON product_stone_center.stone_center_id = stone_centers.id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.stone_centers = rows as GetProductStoneCenterDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT STONE SHAPE GALLERY*/
    sql = `
      SELECT
        image_stone_shape_products.id,
        image_stone_shape_products.path,
        image_stone_shape_products.metal_id,
        image_stone_shape_products.stone_shape_id
      FROM products
        INNER JOIN image_stone_shape_products ON products.id = image_stone_shape_products.product_id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.stoneShapeGallery = rows as GetProductStoneShapeGalleryDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT STONE SIDE GALLERY*/
    sql = `
      SELECT
        image_stone_side_products.id,
        image_stone_side_products.path,
        image_stone_side_products.metal_id,
        image_stone_side_products.stone_side_id,
        image_stone_side_products.stone_color_id
      FROM products
        INNER JOIN image_stone_side_products ON products.id = image_stone_side_products.product_id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      data.stoneSideGallery = rows as GetProductStoneSideGalleryDto[];
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    /*PRODUCT CENTER SHAPE CTW GALLERY*/
    sql = `
      SELECT
        center_shape_ctws.id AS centerShapeCtwId,
        image_center_shape_ctws.id,
        image_center_shape_ctws.path,
        image_center_shape_ctws.metal_id,
        image_center_shape_ctws.stone_shape_id,
        image_center_shape_ctws.stone_center_id
      FROM products
        INNER JOIN center_shape_ctws ON products.id = center_shape_ctws.product_id
        INNER JOIN image_center_shape_ctws ON center_shape_ctws.id = image_center_shape_ctws.center_shape_ctw_id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
      if (rows.length > 0) {
        const CenterShapeCtw: GetProductCenterShapeCtwDto = {
          id: +rows[0].centerShapeCtwId,
          stone_shapes: [],
          stone_centers: [],
          gallery: rows.map(r => ({
            id: r.id,
            path: r.path,
            metal_id: r.metal_id,
            stone_shape_id: r.stone_shape_id,
            stone_center_id: r.stone_center_id,
          }))
        };
        data.centerShapeCtw = CenterShapeCtw;
      }
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    if (data.centerShapeCtw) {
      /*STONE SHAPES FOR CTW*/
      sql = `
        SELECT
          stone_shapes.id,
          stone_shapes.price
        FROM products
          INNER JOIN center_shape_ctws ON products.id = center_shape_ctws.product_id
          INNER JOIN image_center_shape_ctws ON center_shape_ctws.id = image_center_shape_ctws.center_shape_ctw_id
          INNER JOIN stone_shapes ON image_center_shape_ctws.stone_shape_id = stone_shapes.id
        WHERE (products.id = :idOrSlug
          OR products.slug = :idOrSlug)
        GROUP BY stone_shapes.id
      `;
      try {
        const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
        data.centerShapeCtw.stone_shapes = rows as GetProductStoneShapeDto[];
      } catch (e) {
        throw new HttpErrors.BadRequest(throwRequestError(e));;
      }

      /*STONE CENTERS FOR CTW*/
      sql = `
        SELECT
          stone_centers.id,
          stone_centers.name,
          stone_centers.size,
          stone_centers.price
        FROM products
          INNER JOIN center_shape_ctws ON products.id = center_shape_ctws.product_id
          INNER JOIN image_center_shape_ctws ON center_shape_ctws.id = image_center_shape_ctws.center_shape_ctw_id
          INNER JOIN stone_centers ON image_center_shape_ctws.stone_center_id = stone_centers.id
        WHERE (products.id = :idOrSlug
          OR products.slug = :idOrSlug)
        GROUP BY stone_centers.id
      `;
      try {
        const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, { idOrSlug });
        data.centerShapeCtw.stone_centers = rows as GetProductStoneCenterDto[];
      } catch (e) {
        throw new HttpErrors.BadRequest(throwRequestError(e));;
      }
    }

    /*360 GALLERY*/
    sql = `
      SELECT
        three_sixties.product_id AS productId,
        three_sixties.path AS path,
        three_sixties.stone_shape_id AS stoneShapeId,
        three_sixties.metal_color_id AS metalColorId,
        three_sixties.image_count AS quantity 
      FROM products
        INNER JOIN three_sixties ON products.id = three_sixties.product_id
      WHERE (products.id = :idOrSlug
        OR products.slug = :idOrSlug)
      GROUP BY
        three_sixties.product_id,
        three_sixties.stone_shape_id,
        three_sixties.metal_color_id
    `;
    try {
      const [rows, fields] = await connectionPoolPromise.execute<IGetProductThreeSixtyGalleryDto[]>(sql, { idOrSlug });
      data.threeSixtyGallery = rows;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));;
    }

    return data;
  }
}
