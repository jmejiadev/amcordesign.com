import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {MetalColors} from '../models';
import {MetalColorsRepository} from '../repositories';

export class MetalColorsController {
  constructor(
    @repository(MetalColorsRepository)
    public metalColorsRepository : MetalColorsRepository,
  ) {}

  @post('/metal-colors')
  @response(200, {
    description: 'MetalColors model instance',
    content: {'application/json': {schema: getModelSchemaRef(MetalColors)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetalColors, {
            title: 'NewMetalColors',
            exclude: ['id'],
          }),
        },
      },
    })
    metalColors: Omit<MetalColors, 'id'>,
  ): Promise<MetalColors> {
    return this.metalColorsRepository.create(metalColors);
  }

  @get('/metal-colors/count')
  @response(200, {
    description: 'MetalColors model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(MetalColors) where?: Where<MetalColors>,
  ): Promise<Count> {
    return this.metalColorsRepository.count(where);
  }

  @get('/metal-colors')
  @response(200, {
    description: 'Array of MetalColors model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(MetalColors, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(MetalColors) filter?: Filter<MetalColors>,
  ): Promise<MetalColors[]> {
    return this.metalColorsRepository.find(filter);
  }

  @patch('/metal-colors')
  @response(200, {
    description: 'MetalColors PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetalColors, {partial: true}),
        },
      },
    })
    metalColors: MetalColors,
    @param.where(MetalColors) where?: Where<MetalColors>,
  ): Promise<Count> {
    return this.metalColorsRepository.updateAll(metalColors, where);
  }

  @get('/metal-colors/{id}')
  @response(200, {
    description: 'MetalColors model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(MetalColors, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(MetalColors, {exclude: 'where'}) filter?: FilterExcludingWhere<MetalColors>
  ): Promise<MetalColors> {
    return this.metalColorsRepository.findById(id, filter);
  }

  @patch('/metal-colors/{id}')
  @response(204, {
    description: 'MetalColors PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetalColors, {partial: true}),
        },
      },
    })
    metalColors: MetalColors,
  ): Promise<void> {
    await this.metalColorsRepository.updateById(id, metalColors);
  }

  @put('/metal-colors/{id}')
  @response(204, {
    description: 'MetalColors PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() metalColors: MetalColors,
  ): Promise<void> {
    await this.metalColorsRepository.replaceById(id, metalColors);
  }

  @del('/metal-colors/{id}')
  @response(204, {
    description: 'MetalColors DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.metalColorsRepository.deleteById(id);
  }
}
