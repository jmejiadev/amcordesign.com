import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ImageCenterShapeCtws} from '../models';
import {ImageCenterShapeCtwsRepository} from '../repositories';

export class ImageCenterShapeCtwsController {
  constructor(
    @repository(ImageCenterShapeCtwsRepository)
    public imageCenterShapeCtwsRepository : ImageCenterShapeCtwsRepository,
  ) {}

  @post('/image-center-shape-ctws')
  @response(200, {
    description: 'ImageCenterShapeCtws model instance',
    content: {'application/json': {schema: getModelSchemaRef(ImageCenterShapeCtws)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageCenterShapeCtws, {
            title: 'NewImageCenterShapeCtws',
            exclude: ['id'],
          }),
        },
      },
    })
    imageCenterShapeCtws: Omit<ImageCenterShapeCtws, 'id'>,
  ): Promise<ImageCenterShapeCtws> {
    return this.imageCenterShapeCtwsRepository.create(imageCenterShapeCtws);
  }

  @get('/image-center-shape-ctws/count')
  @response(200, {
    description: 'ImageCenterShapeCtws model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ImageCenterShapeCtws) where?: Where<ImageCenterShapeCtws>,
  ): Promise<Count> {
    return this.imageCenterShapeCtwsRepository.count(where);
  }

  @get('/image-center-shape-ctws')
  @response(200, {
    description: 'Array of ImageCenterShapeCtws model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ImageCenterShapeCtws, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ImageCenterShapeCtws) filter?: Filter<ImageCenterShapeCtws>,
  ): Promise<ImageCenterShapeCtws[]> {
    return this.imageCenterShapeCtwsRepository.find(filter);
  }

  @patch('/image-center-shape-ctws')
  @response(200, {
    description: 'ImageCenterShapeCtws PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageCenterShapeCtws, {partial: true}),
        },
      },
    })
    imageCenterShapeCtws: ImageCenterShapeCtws,
    @param.where(ImageCenterShapeCtws) where?: Where<ImageCenterShapeCtws>,
  ): Promise<Count> {
    return this.imageCenterShapeCtwsRepository.updateAll(imageCenterShapeCtws, where);
  }

  @get('/image-center-shape-ctws/{id}')
  @response(200, {
    description: 'ImageCenterShapeCtws model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ImageCenterShapeCtws, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ImageCenterShapeCtws, {exclude: 'where'}) filter?: FilterExcludingWhere<ImageCenterShapeCtws>
  ): Promise<ImageCenterShapeCtws> {
    return this.imageCenterShapeCtwsRepository.findById(id, filter);
  }

  @patch('/image-center-shape-ctws/{id}')
  @response(204, {
    description: 'ImageCenterShapeCtws PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ImageCenterShapeCtws, {partial: true}),
        },
      },
    })
    imageCenterShapeCtws: ImageCenterShapeCtws,
  ): Promise<void> {
    await this.imageCenterShapeCtwsRepository.updateById(id, imageCenterShapeCtws);
  }

  @put('/image-center-shape-ctws/{id}')
  @response(204, {
    description: 'ImageCenterShapeCtws PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() imageCenterShapeCtws: ImageCenterShapeCtws,
  ): Promise<void> {
    await this.imageCenterShapeCtwsRepository.replaceById(id, imageCenterShapeCtws);
  }

  @del('/image-center-shape-ctws/{id}')
  @response(204, {
    description: 'ImageCenterShapeCtws DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.imageCenterShapeCtwsRepository.deleteById(id);
  }
}
