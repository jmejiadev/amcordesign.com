import { get, HttpErrors, param, response } from '@loopback/rest';
import { RowDataPacket } from 'mysql2';
import { ProductSimpleDto, ProductSimplePreviewDto } from '../types/Dtos';
import { ProductWithImageResult } from '../types/models';
import { connectionPoolPromise, throwRequestError } from '../utils';
import { parseInlineImagesToArray } from '../utils/inlineImagesToArray';
import { selectProductWithThumbs } from '../utils/queries';

export declare const OfferProductsSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

interface IProductWithImageResult extends RowDataPacket, ProductWithImageResult {
}

export class OfferProductsController {

  @get('/offer-products')
  @response(200, {
    description: 'Products by category',
    content: { 'application/json': { schema: OfferProductsSchema } },
  })
  async function(
    @param.query.string('metal') metal: string = '1',
    @param.array('style', 'query', { type: 'string' }) style: string[] = [],
    @param.array('setting', 'query', { type: 'string' }) setting: string[] = [],
  ) {
    const styleParams = style.map((s, i) => [`style${i}`, `%${s}%`])
    const settingParams = setting.map((s, i) => [`setting${i}`, `%${s}%`])

    let sql = `
      ${selectProductWithThumbs}
      WHERE products.discount > 0
        AND (
          ctwPreview.path IS NOT NULL
          OR stoneShapePreview.path IS NOT NULL
          OR stoneSidePreview.path IS NOT NULL
        )
    `;

    //style params
    if (styleParams.length > 0) {
      sql = `
        ${sql}
        AND (${styleParams.map(([name]) => `products.short_description LIKE :${name}`).join(' OR ')})
      `
    }

    //setting params
    if (settingParams.length > 0) {
      sql = `
        ${sql}
        AND (${settingParams.map(([name]) => `products.setting LIKE :${name}`).join(' OR ')})
      `
    }

    //add group and order
    sql = `
      ${sql}
      GROUP BY products.id
      ORDER BY 
        products.discount DESC, 
        sellingPrice ASC
      LIMIT 10
    `

    const namedParams = {
      metal,
      ...Object.fromEntries(styleParams),
      ...Object.fromEntries(settingParams),
    };

    try {
      const [rows, fields] = await connectionPoolPromise.execute<IProductWithImageResult[]>(sql, namedParams);
      const mappedResult: ProductSimpleDto[] = rows.map<ProductSimpleDto>((result: IProductWithImageResult) => {
        return {
          id: result.id,
          name: result.name,
          slug: result.slug,
          sellingPrice: result.sellingPrice,
          discount: +result.discount,
          stoneColorCarat: result.stoneColorCarat,
          stoneColorId: result.stoneColorId,
          imagePreview: parseInlineImagesToArray(result),
        }
      })
      return mappedResult;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }
  }
}
