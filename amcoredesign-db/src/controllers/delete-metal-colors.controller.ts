// Uncomment these imports to begin using these cool features!

import { get, HttpErrors, param, response } from '@loopback/rest';
import { FieldPacket, RowDataPacket } from 'mysql2';
import { connectionPoolPromise, throwRequestError } from '../utils';

export declare const DeleteMetalColorsSchema: {
    type: "object";
    title: string;
    'x-typescript-type': string;
    properties: {
        status: {
            type: "string";
        };
        mesagge: {
            type: "string";
        };
    };
};

export class DeleteMetalColors {
    @get('/del-metal-colors/{id}')
    @response(200, {
        description: 'Products Related delete ',
        content: { 'application/json': { schema: DeleteMetalColorsSchema } },
    })
    async function(@param.path.number('id') id: number) {
        let data = {};
        try {
            let sql = "SELECT * FROM order_products where metal_id =" + id;
            let result = await connectionPoolPromise.query(sql);
            sql = "";
            var order_product_info = Array();
            order_product_info.push(Object.values(JSON.parse(JSON.stringify(result[0]))));
            var array = Array();
            array = order_product_info[0];
            array.forEach(element => {
                var order_product_id = element.id;
                var arr = Array();
                arr.push("order_products_variants");
                arr.push("order_products_sizes");
                arr.forEach(element => {
                    sql += 'DELETE FROM ' + element + ' WHERE order_product_id = ' + order_product_id + ";";
                });
            });
            var arr2 = Array();
            arr2.push("home_shop_products");
            arr2.push("image_center_shape_ctws");
            arr2.push("image_stone_color_products");
            arr2.push("image_stone_shape_color_products");
            arr2.push("image_stone_shape_products");
            arr2.push("image_stone_side_products");
            arr2.push("order_products");
            arr2.push("metal_product");
            arr2.push("image_metal_products");
            var extraCondition = "metal_id = " + id;
            arr2.forEach(element => {
                sql += 'DELETE FROM ' + element + ' WHERE ' + extraCondition + ";";
            });
            sql += 'DELETE FROM three_sixties WHERE metal_color_id = ' + id + ";";
            let result2 = connectionPoolPromise.query(sql);
            data = result2;


        } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));;
        }
        return data;
    }
}
