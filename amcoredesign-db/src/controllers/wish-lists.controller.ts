import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {WishLists} from '../models';
import {WishListsRepository} from '../repositories';

export class WishListsController {
  constructor(
    @repository(WishListsRepository)
    public wishListsRepository : WishListsRepository,
  ) {}

  @post('/wish-lists')
  @response(200, {
    description: 'WishLists model instance',
    content: {'application/json': {schema: getModelSchemaRef(WishLists)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WishLists, {
            title: 'NewWishLists',
            exclude: ['id'],
          }),
        },
      },
    })
    wishLists: Omit<WishLists, 'id'>,
  ): Promise<WishLists> {
    return this.wishListsRepository.create(wishLists);
  }

  @get('/wish-lists/count')
  @response(200, {
    description: 'WishLists model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(WishLists) where?: Where<WishLists>,
  ): Promise<Count> {
    return this.wishListsRepository.count(where);
  }

  @get('/wish-lists')
  @response(200, {
    description: 'Array of WishLists model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(WishLists, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(WishLists) filter?: Filter<WishLists>,
  ): Promise<WishLists[]> {
    return this.wishListsRepository.find(filter);
  }

  @patch('/wish-lists')
  @response(200, {
    description: 'WishLists PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WishLists, {partial: true}),
        },
      },
    })
    wishLists: WishLists,
    @param.where(WishLists) where?: Where<WishLists>,
  ): Promise<Count> {
    return this.wishListsRepository.updateAll(wishLists, where);
  }

  @get('/wish-lists/{id}')
  @response(200, {
    description: 'WishLists model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(WishLists, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(WishLists, {exclude: 'where'}) filter?: FilterExcludingWhere<WishLists>
  ): Promise<WishLists> {
    return this.wishListsRepository.findById(id, filter);
  }

  @patch('/wish-lists/{id}')
  @response(204, {
    description: 'WishLists PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WishLists, {partial: true}),
        },
      },
    })
    wishLists: WishLists,
  ): Promise<void> {
    await this.wishListsRepository.updateById(id, wishLists);
  }

  @put('/wish-lists/{id}')
  @response(204, {
    description: 'WishLists PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() wishLists: WishLists,
  ): Promise<void> {
    await this.wishListsRepository.replaceById(id, wishLists);
  }

  @del('/wish-lists/{id}')
  @response(204, {
    description: 'WishLists DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.wishListsRepository.deleteById(id);
  }
}
