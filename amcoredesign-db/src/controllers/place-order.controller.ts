// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/core';
import {HttpErrors, post, Request, requestBody, RestBindings} from '@loopback/rest';
import {FieldPacket, ResultSetHeader} from 'mysql2';
import {GetOrderDto, PaymentInfo, PostOrderDto} from '../types/Dtos';
import {Order, OrderProducts, OrderProductsSizes, OrderProductsVariants} from '../types/models';
import {connectionPoolPromise, throwRequestError} from '../utils';

export declare const PlaceOrderSchema: {
  type: "object";
  title: string;
  'x-typescript-type': string;
  properties: {
    status: {
      type: "string";
    };
    mesagge: {
      type: "string";
    };
  };
};

export class PlaceOrderController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) { }

  @post('/place-order')
  async placeOrder(
    @requestBody() orderToSave: PostOrderDto,
  ) {
    const {head, details, payment} = orderToSave;
    const savedOrder: GetOrderDto = new GetOrderDto();

    //GET ORDER NUMBER
    let orderNumber
    let sql = `
      SELECT
        MAX( order_number )+ 1 as orderNumber
      FROM orders;
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.query(sql);
      orderNumber = rows[0].orderNumber;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }

    let savedHeadId: number;

    //SAVE ORDER HEAD
    sql = `
      INSERT INTO orders
      VALUES
        (
          null, #id
          :orderNumber, #order_number,
          :firstName ,  #first_name,
          :lastName , #last_name,
          :email , #email,
          :phone , #phone,
          :clientIp , #client_ip,
          :total , #total,
          :notes , #notes,
          :orderStatus ,  #order_status,
          :shippingAddressLine1 ,  #shipping_address_line1,
          :shippingAddressLine2 ,  #shipping_address_line2,
          :shippingZipCode , #shipping_zip_code,
          :shippingCity , #shipping_city,
          :shippingState ,  #shipping_state,
          :billingAddressLine1 , #billing_address_line1,
          :billingAddressLine2 , #billing_address_line2,
          :billingZipCode ,  #billing_zip_code,
          :billingCity ,  #billing_city,
          :billingState , #billing_state,
          :paymentType ,  #payment_type,
          :shippingCountry ,  #shipping_country,
          :billingCountry , #billing_country,
          :tax , #tax,
          null, #person_id,
          NOW(),  #created_at,
          NOW() #updated_at
        );
    `;
    try {
      const [result, fields]: [ResultSetHeader, FieldPacket[]] = await connectionPoolPromise.execute(sql, {
        orderNumber,
        firstName: head.first_name,
        lastName: head.last_name,
        email: head.email,
        phone: head.phone,
        clientIp: (this.req.headers['x-forwarded-for'] as string)?.split(',').shift() || this.req.socket?.remoteAddress,
        total: head.total,
        notes: head.notes ?? null,
        orderStatus: head.order_status,
        shippingAddressLine1: head.shipping_address_line1,
        shippingAddressLine2: head.shipping_address_line2 ?? null,
        shippingZipCode: head.shipping_zip_code ?? null,
        shippingCity: head.shipping_city,
        shippingState: head.shipping_state,
        billingAddressLine1: head.billing_address_line1,
        billingAddressLine2: head.billing_address_line2 ?? null,
        billingZipCode: head.billing_zip_code ?? null,
        billingCity: head.billing_city,
        billingState: head.billing_state,
        paymentType: head.payment_type,
        shippingCountry: head.shipping_country,
        billingCountry: head.billing_country,
        tax: head.tax,
      });

      //SET SAVED ORDER ID
      savedHeadId = result.insertId;

    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }

    //GET SAVED ORDER
    sql = `
      SELECT
        id,
        order_number,
        first_name,
        last_name,
        email,
        phone,
        client_ip,
        total,
        notes,
        order_status,
        shipping_address_line1,
        shipping_address_line2,
        shipping_zip_code,
        shipping_city,
        shipping_state,
        billing_address_line1,
        billing_address_line2,
        billing_zip_code,
        billing_city,
        billing_state,
        payment_type,
        shipping_country,
        billing_country,
        tax,
        person_id,
        created_at,
        updated_at
      FROM orders
      WHERE id = :savedHeadId;
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, {savedHeadId});
      savedOrder.head = rows[0] as Order;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }

    //SAVE PAYPAL PAYMENT
    sql = `
    INSERT INTO order_paypal
    VALUES
      (
        null,	#id
        :savedHeadId,	#order_id
        :paypalId,	#paypal_id
        :createTime,	#create_time
        :updateTime,	#update_time
        :detail	#detail
      );
    `;
    try {
      const [result, fields]: [ResultSetHeader, FieldPacket[]] = await connectionPoolPromise.execute(sql, {
        savedHeadId,
        paypalId: payment.id,
        createTime: new Date(payment.createTime),
        updateTime: new Date(payment.updateTime),
        detail: payment.detail,
      });
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }

    //GET SAVED PAYPAL PAYMENT
    sql = `
      SELECT
        order_paypal.id,
        order_paypal.order_id,
        order_paypal.paypal_id,
        order_paypal.create_time,
        order_paypal.update_time,
        order_paypal.detail
      FROM order_paypal
      WHERE order_id = :savedHeadId;
    `;
    try {
      const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, {savedHeadId});
      savedOrder.payment = rows[0] as PaymentInfo;
    } catch (e) {
      throw new HttpErrors.BadRequest(throwRequestError(e));
    }

    //SAVE ORDER DETAIL
    savedOrder.details = await Promise.all(
      details.map(async (d): Promise<OrderProducts> => {
        let savedDetailId: number;
        let savedDetail: OrderProducts;

        sql = `
          INSERT INTO order_products
          VALUES
            (
              null, #id
              :savedHeadId,  #order_id
              :productid,  #product_id
              :name,  #name
              :supplierSku,  #supplier_sku
              :grams, #grams
              :stoneNumber,  #stone_number
              :metalid,  #metal_id
              :metalName,  #metal_name
              :stoneCenterid, #stone_center_id
              :stoneCenterName, #stone_center_name
              :stoneSideid, #stone_side_id
              :stoneSideName, #stone_side_name
              :sellingPrice, #selling_price
              :discount,  #discount
              :personalization, #personalization
              :quantity,  #quantity
              :stoneShapeid,  #stone_shape_id
              :stoneShapeName,  #stone_shape_name
              :stoneColorid,  #stone_color_id
              :stoneColorName,  #stone_color_name
              :subtotal,  #subtotal
              :couponid, #coupon_id
              :gemstoneid, #gemstone_id
              NOW(),  #created_at,
              NOW() #updated_at
            );
        `;
        try {
          const [result, fields]: [ResultSetHeader, FieldPacket[]] = await connectionPoolPromise.execute(sql, {
            savedHeadId,
            productid: d.product_id,
            name: d.name ?? null,
            supplierSku: d.supplier_sku ?? null,
            grams: d.grams ?? null,
            stoneNumber: d.stone_number ?? null,
            metalid: d.metal_id ?? null,
            metalName: d.metal_name ?? null,
            stoneCenterid: d.stone_center_id ?? null,
            stoneCenterName: d.stone_center_name ?? null,
            stoneSideid: d.stone_side_id ?? null,
            stoneSideName: d.stone_side_name ?? null,
            sellingPrice: d.selling_price,
            discount: d.discount ?? null,
            personalization: d.personalization ?? null,
            quantity: d.quantity ?? null,
            stoneShapeid: d.stone_shape_id ?? null,
            stoneShapeName: d.stone_shape_name ?? null,
            stoneColorid: d.stone_color_id ?? null,
            stoneColorName: d.stone_color_name ?? null,
            subtotal: d.subtotal ?? null,
            couponid: d.coupon_id ?? null,
            gemstoneid: d.gemstone_id ?? null,
          });

          //SET SAVED DETAIL ID
          savedDetailId = result.insertId;
        } catch (e) {
          throw new HttpErrors.BadRequest(throwRequestError(e));
        }

        //GET SAVED DETAIL
        sql = `
          SELECT
            id,
            order_id,
            product_id,
            name,
            supplier_sku,
            grams,
            stone_number,
            metal_id,
            metal_name,
            stone_center_id,
            stone_center_name,
            stone_side_id,
            stone_side_name,
            selling_price,
            discount,
            personalization,
            quantity,
            stone_shape_id,
            stone_shape_name,
            stone_color_id,
            stone_color_name,
            subtotal,
            coupon_id,
            gemstone_id,
            created_at,
            updated_at
          FROM order_products
          WHERE id = :savedDetailId;
        `;
        try {
          const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, {savedDetailId});
          savedDetail = rows[0] as OrderProducts;
        } catch (e) {
          throw new HttpErrors.BadRequest(throwRequestError(e));
        }

        //SAVE VARIANTS FOR THIS PRODUCT
        if (d.productVariants) {
          const savedIds = await Promise.all(
            d.productVariants.map(async (v) => {
              sql = `
                INSERT INTO order_products_variants
                VALUES
                  (
                    null, #id
                    :savedDetailId, #order_product_id
                    :extraVariationId, #extra_variation_id
                    :extraVariationName, #extra_variation_name
                    :variationId, #variation_id
                    :variationName, #variation_name
                    NOW(),  #created_at,
                    NOW() #updated_at
                  )
              `;
              try {
                const [result, fields]: [ResultSetHeader, FieldPacket[]] = await connectionPoolPromise.execute(sql, {
                  savedDetailId,
                  extraVariationId: v.extra_variation_id,
                  extraVariationName: v.extra_variation_name,
                  variationId: v.variation_id,
                  variationName: v.variation_name,
                });
              } catch (e) {
                throw new HttpErrors.BadRequest(throwRequestError(e));
              }
            })
          ).catch(e => {
            throw e;
          })

          //GET SAVED VARIANTS FOR THIS PRODUCT
          sql = `
            SELECT
              id,
              order_product_id,
              extra_variation_id,
              extra_variation_name,
              variation_id,
              variation_name,
              created_at,
              updated_at
            FROM order_products_variants
            WHERE order_product_id = :savedDetailId;
          `;
          try {
            const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, {savedDetailId});
            savedDetail.productVariants = rows as OrderProductsVariants[];
          } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));
          }
        }

        //SAVE SIZES FOR THIS PRODUCT
        if (d.productSizes) {
          const savedIds = await Promise.all(
            d.productSizes.map(async (s) => {
              sql = `
              INSERT INTO order_products_sizes
              VALUES
                (
                  null, #id
                  :savedDetailId, #order_product_id
                  :typeSizeId, #type_size_id
                  :sizeNumber, #size_number
                  NOW(),  #created_at,
                  NOW() #updated_at
                )
              `;
              try {
                const [result, fields]: [ResultSetHeader, FieldPacket[]] = await connectionPoolPromise.execute(sql, {
                  savedDetailId,
                  typeSizeId: s.type_size_id,
                  sizeNumber: s.size_number,
                });
                return result.insertId;
              } catch (e) {
                throw new HttpErrors.BadRequest(throwRequestError(e));
              }
            })
          ).catch(e => {
            throw e;
          })

          //GET SAVED SIZES FOR THIS PRODUCT
          sql = `
            SELECT
              id,
              order_product_id,
              type_size_id,
              size_number,
              created_at,
              updated_at
            FROM order_products_sizes
            WHERE order_product_id = :savedDetailId;
          `;
          try {
            const [rows, fields]: [any[], FieldPacket[]] = await connectionPoolPromise.execute(sql, {savedDetailId});
            savedDetail.productSizes = rows as OrderProductsSizes[];
          } catch (e) {
            throw new HttpErrors.BadRequest(throwRequestError(e));
          }
        }
        return savedDetail;
      })
    ).catch(e => {
      throw e;
    })


    return savedOrder;
  }
}
