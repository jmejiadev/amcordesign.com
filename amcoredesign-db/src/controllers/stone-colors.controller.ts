import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {StoneColors} from '../models';
import {StoneColorsRepository} from '../repositories';

export class StoneColorsController {
  constructor(
    @repository(StoneColorsRepository)
    public stoneColorsRepository : StoneColorsRepository,
  ) {}

  @post('/stone-colors')
  @response(200, {
    description: 'StoneColors model instance',
    content: {'application/json': {schema: getModelSchemaRef(StoneColors)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneColors, {
            title: 'NewStoneColors',
            exclude: ['id'],
          }),
        },
      },
    })
    stoneColors: Omit<StoneColors, 'id'>,
  ): Promise<StoneColors> {
    return this.stoneColorsRepository.create(stoneColors);
  }

  @get('/stone-colors/count')
  @response(200, {
    description: 'StoneColors model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StoneColors) where?: Where<StoneColors>,
  ): Promise<Count> {
    return this.stoneColorsRepository.count(where);
  }

  @get('/stone-colors')
  @response(200, {
    description: 'Array of StoneColors model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StoneColors, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StoneColors) filter?: Filter<StoneColors>,
  ): Promise<StoneColors[]> {
    return this.stoneColorsRepository.find(filter);
  }

  @patch('/stone-colors')
  @response(200, {
    description: 'StoneColors PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneColors, {partial: true}),
        },
      },
    })
    stoneColors: StoneColors,
    @param.where(StoneColors) where?: Where<StoneColors>,
  ): Promise<Count> {
    return this.stoneColorsRepository.updateAll(stoneColors, where);
  }

  @get('/stone-colors/{id}')
  @response(200, {
    description: 'StoneColors model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StoneColors, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StoneColors, {exclude: 'where'}) filter?: FilterExcludingWhere<StoneColors>
  ): Promise<StoneColors> {
    return this.stoneColorsRepository.findById(id, filter);
  }

  @patch('/stone-colors/{id}')
  @response(204, {
    description: 'StoneColors PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StoneColors, {partial: true}),
        },
      },
    })
    stoneColors: StoneColors,
  ): Promise<void> {
    await this.stoneColorsRepository.updateById(id, stoneColors);
  }

  @put('/stone-colors/{id}')
  @response(204, {
    description: 'StoneColors PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() stoneColors: StoneColors,
  ): Promise<void> {
    await this.stoneColorsRepository.replaceById(id, stoneColors);
  }

  @del('/stone-colors/{id}')
  @response(204, {
    description: 'StoneColors DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.stoneColorsRepository.deleteById(id);
  }
}
