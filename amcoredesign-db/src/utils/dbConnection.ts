import { ConnectionOptions, createPool } from 'mysql2';

const options: ConnectionOptions = {
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE_NAME,
  port: +(process.env.DB_PORT || 3306),
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  multipleStatements: true,
  namedPlaceholders: true,
  dateStrings:true,
};

// create the pool
export const connectionPool = createPool({
  connectionLimit: 10,
  ...options
});

// get a Promise wrapped instance of that pool
export const connectionPoolPromise = connectionPool.promise();