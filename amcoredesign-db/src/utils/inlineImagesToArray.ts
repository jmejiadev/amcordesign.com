import { ProductSimplePreviewDto } from "../types/Dtos";
import { ProductWithImageResult } from "../types/models";

export const parseInlineImagesToArray = (productData: ProductWithImageResult): ProductSimplePreviewDto[] => {
  let images: ProductSimplePreviewDto[] = [];
  if (productData.ctwPreviewPath) {
    images.push({
      path: productData.ctwPreviewPath,
      metalId: productData.ctwPreviewMetalId,
      stoneShapeId: productData.ctwPreviewStoneShapeId,
      stoneCenterId: productData.ctwPreviewStoneCenterId,
    });
  }
  if (productData.stoneShapePreviewPath) {
    images.push({
      path: productData.stoneShapePreviewPath,
      metalId: productData.stoneShapePreviewMetalId,
      stoneShapeId: productData.stoneShapePreviewStoneShapeId,
    });
  }
  if (productData.stoneSidePreviewPath) {
    images.push({
      path: productData.stoneSidePreviewPath,
      metalId: productData.stoneSidePreviewMetalId,
      stoneSideId: productData.stoneSidePreviewStoneSideId,
      stoneColorId: productData.stoneSidePreviewStoneColorId,
    });
  }
  return images;
}