export const throwRequestError = (e: any) => {
  const {code, errno} = e;
  return `${errno ?? 0} ${code ?? 'Ocurrió un problema al realizar la acción'}`;
}
