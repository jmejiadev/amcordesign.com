export const selectProductWithThumbs = `
  SELECT
    products.id,
    products.name,
    products.slug,
    (products.manufacturer + (metals.price * products.grams_weight) + IFNULL(stoneColor.price, 0)) AS sellingPrice,
    products.discount,
    stoneColor.carat AS stoneColorCarat,
    stoneColor.stone_color_id AS stoneColorId,
    
    ctwPreview.path AS ctwPreviewPath,
    ctwPreview.metal_id AS ctwPreviewMetalId,
    ctwPreview.stone_shape_id AS ctwPreviewStoneShapeId,
    ctwPreview.stone_center_id AS ctwPreviewStoneCenterId,

    stoneShapePreview.path AS stoneShapePreviewPath,
    stoneShapePreview.metal_id AS stoneShapePreviewMetalId,
    stoneShapePreview.stone_shape_id AS stoneShapePreviewStoneShapeId,

    stoneSidePreview.path AS stoneSidePreviewPath,
    stoneSidePreview.metal_id AS stoneSidePreviewMetalId,
    stoneSidePreview.stone_side_id AS stoneSidePreviewStoneSideId,
    stoneSidePreview.stone_color_id AS stoneSidePreviewStoneColorId 
  FROM products
    INNER JOIN category_product ON products.id = category_product.product_id
    INNER JOIN categories ON category_product.category_id = categories.id
    INNER JOIN metals ON metals.id = :metal
    LEFT JOIN (
      SELECT
        center_shape_ctws.product_id,
        center_shape_ctws.sku,
        image_center_shape_ctws.path,
        image_center_shape_ctws.metal_id,
        image_center_shape_ctws.stone_shape_id,
        image_center_shape_ctws.stone_center_id 
      FROM products
        INNER JOIN center_shape_ctws ON products.id = center_shape_ctws.product_id
        INNER JOIN image_center_shape_ctws ON center_shape_ctws.id = image_center_shape_ctws.center_shape_ctw_id 
      WHERE image_center_shape_ctws.metal_id = :metal
      GROUP BY products.id 
      ORDER BY
        image_center_shape_ctws.metal_id,
        image_center_shape_ctws.stone_shape_id,
        image_center_shape_ctws.stone_center_id
    ) AS ctwPreview ON products.id = ctwPreview.product_id
    LEFT JOIN (
      SELECT
        image_stone_shape_products.product_id,
        image_stone_shape_products.path,
        image_stone_shape_products.metal_id,
        image_stone_shape_products.stone_shape_id 
      FROM products
        INNER JOIN image_stone_shape_products ON products.id = image_stone_shape_products.product_id 
      WHERE image_stone_shape_products.metal_id = :metal
      GROUP BY products.id 
      ORDER BY
        image_stone_shape_products.metal_id,
        image_stone_shape_products.stone_shape_id
    ) AS stoneShapePreview ON products.id = stoneShapePreview.product_id
    LEFT JOIN (
      SELECT
        image_stone_side_products.product_id,
        image_stone_side_products.path,
        image_stone_side_products.metal_id,
        image_stone_side_products.stone_side_id,
        image_stone_side_products.stone_color_id 
      FROM products
        INNER JOIN image_stone_side_products ON products.id = image_stone_side_products.product_id 
      WHERE image_stone_side_products.metal_id = :metal
      GROUP BY products.id 
      ORDER BY
        image_stone_side_products.metal_id,
        image_stone_side_products.stone_side_id,
        image_stone_side_products.stone_color_id
    ) AS stoneSidePreview ON products.id = stoneSidePreview .product_id
    LEFT JOIN (
      SELECT
        product_stone_color.product_id,
        product_stone_color.price, 
        product_stone_color.carat, 
        product_stone_color.stone_color_id
      FROM product_stone_color
      GROUP BY product_stone_color.product_id
      ORDER BY 
        product_stone_color.stone_color_id
    ) AS stoneColor ON products.id = stoneColor.product_id
`;
