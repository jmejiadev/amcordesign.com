import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductWishList, ProductWishListRelations} from '../models';

export class ProductWishListRepository extends DefaultCrudRepository<
  ProductWishList,
  typeof ProductWishList.prototype.id,
  ProductWishListRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductWishList, dataSource);
  }
}
