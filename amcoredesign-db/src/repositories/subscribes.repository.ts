import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Subscribes, SubscribesRelations} from '../models';

export class SubscribesRepository extends DefaultCrudRepository<
  Subscribes,
  typeof Subscribes.prototype.id,
  SubscribesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Subscribes, dataSource);
  }
}
