import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CenterShapeCtwStoneShape, CenterShapeCtwStoneShapeRelations} from '../models';

export class CenterShapeCtwStoneShapeRepository extends DefaultCrudRepository<
  CenterShapeCtwStoneShape,
  typeof CenterShapeCtwStoneShape.prototype.id,
  CenterShapeCtwStoneShapeRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CenterShapeCtwStoneShape, dataSource);
  }
}
