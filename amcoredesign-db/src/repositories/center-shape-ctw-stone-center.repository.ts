import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CenterShapeCtwStoneCenter, CenterShapeCtwStoneCenterRelations} from '../models';

export class CenterShapeCtwStoneCenterRepository extends DefaultCrudRepository<
  CenterShapeCtwStoneCenter,
  typeof CenterShapeCtwStoneCenter.prototype.id,
  CenterShapeCtwStoneCenterRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CenterShapeCtwStoneCenter, dataSource);
  }
}
