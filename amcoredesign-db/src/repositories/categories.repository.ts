import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyThroughRepositoryFactory} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Categories, CategoriesRelations, Products, CategoryProduct} from '../models';
import {CategoryProductRepository} from './category-product.repository';
import {ProductsRepository} from './products.repository';

export class CategoriesRepository extends DefaultCrudRepository<
  Categories,
  typeof Categories.prototype.id,
  CategoriesRelations
> {

  public readonly products: HasManyThroughRepositoryFactory<Products, typeof Products.prototype.id,
          CategoryProduct,
          typeof Categories.prototype.id
        >;

  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource, @repository.getter('CategoryProductRepository') protected categoryProductRepositoryGetter: Getter<CategoryProductRepository>, @repository.getter('ProductsRepository') protected productsRepositoryGetter: Getter<ProductsRepository>,
  ) {
    super(Categories, dataSource);
    this.products = this.createHasManyThroughRepositoryFactoryFor('products', productsRepositoryGetter, categoryProductRepositoryGetter,);
    this.registerInclusionResolver('products', this.products.inclusionResolver);
  }
}
