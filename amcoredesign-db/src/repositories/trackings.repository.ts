import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Trackings, TrackingsRelations} from '../models';

export class TrackingsRepository extends DefaultCrudRepository<
  Trackings,
  typeof Trackings.prototype.id,
  TrackingsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Trackings, dataSource);
  }
}
