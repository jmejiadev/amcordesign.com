import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {GoogleCategories, GoogleCategoriesRelations} from '../models';

export class GoogleCategoriesRepository extends DefaultCrudRepository<
  GoogleCategories,
  typeof GoogleCategories.prototype.id,
  GoogleCategoriesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(GoogleCategories, dataSource);
  }
}
