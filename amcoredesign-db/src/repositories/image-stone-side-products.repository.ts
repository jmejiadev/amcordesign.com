import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ImageStoneSideProducts, ImageStoneSideProductsRelations} from '../models';

export class ImageStoneSideProductsRepository extends DefaultCrudRepository<
  ImageStoneSideProducts,
  typeof ImageStoneSideProducts.prototype.id,
  ImageStoneSideProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ImageStoneSideProducts, dataSource);
  }
}
