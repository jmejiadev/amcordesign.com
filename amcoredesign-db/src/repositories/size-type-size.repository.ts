import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {SizeTypeSize, SizeTypeSizeRelations} from '../models';

export class SizeTypeSizeRepository extends DefaultCrudRepository<
  SizeTypeSize,
  typeof SizeTypeSize.prototype.id,
  SizeTypeSizeRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(SizeTypeSize, dataSource);
  }
}
