import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {OrderProductsSizes, OrderProductsSizesRelations} from '../models';

export class OrderProductsSizesRepository extends DefaultCrudRepository<
  OrderProductsSizes,
  typeof OrderProductsSizes.prototype.id,
  OrderProductsSizesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(OrderProductsSizes, dataSource);
  }
}
