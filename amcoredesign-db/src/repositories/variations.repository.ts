import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Variations, VariationsRelations} from '../models';

export class VariationsRepository extends DefaultCrudRepository<
  Variations,
  typeof Variations.prototype.id,
  VariationsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Variations, dataSource);
  }
}
