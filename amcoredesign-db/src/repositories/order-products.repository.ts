import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {OrderProducts, OrderProductsRelations} from '../models';

export class OrderProductsRepository extends DefaultCrudRepository<
  OrderProducts,
  typeof OrderProducts.prototype.id,
  OrderProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(OrderProducts, dataSource);
  }
}
