import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {StoneCenters, StoneCentersRelations} from '../models';

export class StoneCentersRepository extends DefaultCrudRepository<
  StoneCenters,
  typeof StoneCenters.prototype.id,
  StoneCentersRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(StoneCenters, dataSource);
  }
}
