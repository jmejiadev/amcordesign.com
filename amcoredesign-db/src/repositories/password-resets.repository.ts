import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {PasswordResets, PasswordResetsRelations} from '../models';

export class PasswordResetsRepository extends DefaultCrudRepository<
  PasswordResets,
  typeof PasswordResets.prototype,
  PasswordResetsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(PasswordResets, dataSource);
  }
}
