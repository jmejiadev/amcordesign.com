import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CurrentLocations, CurrentLocationsRelations} from '../models';

export class CurrentLocationsRepository extends DefaultCrudRepository<
  CurrentLocations,
  typeof CurrentLocations.prototype.id,
  CurrentLocationsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CurrentLocations, dataSource);
  }
}
