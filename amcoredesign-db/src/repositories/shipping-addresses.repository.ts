import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ShippingAddresses, ShippingAddressesRelations} from '../models';

export class ShippingAddressesRepository extends DefaultCrudRepository<
  ShippingAddresses,
  typeof ShippingAddresses.prototype.id,
  ShippingAddressesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ShippingAddresses, dataSource);
  }
}
