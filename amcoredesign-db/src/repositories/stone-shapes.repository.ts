import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {StoneShapes, StoneShapesRelations} from '../models';

export class StoneShapesRepository extends DefaultCrudRepository<
  StoneShapes,
  typeof StoneShapes.prototype.id,
  StoneShapesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(StoneShapes, dataSource);
  }
}
