import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductShoppingCart, ProductShoppingCartRelations} from '../models';

export class ProductShoppingCartRepository extends DefaultCrudRepository<
  ProductShoppingCart,
  typeof ProductShoppingCart.prototype.id,
  ProductShoppingCartRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductShoppingCart, dataSource);
  }
}
