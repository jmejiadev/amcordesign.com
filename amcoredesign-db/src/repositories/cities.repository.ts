import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Cities, CitiesRelations} from '../models';

export class CitiesRepository extends DefaultCrudRepository<
  Cities,
  typeof Cities.prototype.id,
  CitiesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Cities, dataSource);
  }
}
