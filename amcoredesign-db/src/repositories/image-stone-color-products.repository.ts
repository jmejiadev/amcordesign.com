import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ImageStoneColorProducts, ImageStoneColorProductsRelations} from '../models';

export class ImageStoneColorProductsRepository extends DefaultCrudRepository<
  ImageStoneColorProducts,
  typeof ImageStoneColorProducts.prototype.id,
  ImageStoneColorProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ImageStoneColorProducts, dataSource);
  }
}
