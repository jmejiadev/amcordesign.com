import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {OrderProductsVariants, OrderProductsVariantsRelations} from '../models';

export class OrderProductsVariantsRepository extends DefaultCrudRepository<
  OrderProductsVariants,
  typeof OrderProductsVariants.prototype.id,
  OrderProductsVariantsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(OrderProductsVariants, dataSource);
  }
}
