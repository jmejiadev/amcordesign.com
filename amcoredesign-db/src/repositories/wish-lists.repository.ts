import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {WishLists, WishListsRelations} from '../models';

export class WishListsRepository extends DefaultCrudRepository<
  WishLists,
  typeof WishLists.prototype.id,
  WishListsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(WishLists, dataSource);
  }
}
