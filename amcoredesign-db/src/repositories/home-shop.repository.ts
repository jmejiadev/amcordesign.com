import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {HomeShop, HomeShopRelations} from '../models';

export class HomeShopRepository extends DefaultCrudRepository<
  HomeShop,
  typeof HomeShop.prototype.id,
  HomeShopRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(HomeShop, dataSource);
  }
}
