import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ImageCenterShapeCtws, ImageCenterShapeCtwsRelations} from '../models';

export class ImageCenterShapeCtwsRepository extends DefaultCrudRepository<
  ImageCenterShapeCtws,
  typeof ImageCenterShapeCtws.prototype.id,
  ImageCenterShapeCtwsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ImageCenterShapeCtws, dataSource);
  }
}
