import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {MoissaniteImages, MoissaniteImagesRelations} from '../models';

export class MoissaniteImagesRepository extends DefaultCrudRepository<
  MoissaniteImages,
  typeof MoissaniteImages.prototype.id,
  MoissaniteImagesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(MoissaniteImages, dataSource);
  }
}
