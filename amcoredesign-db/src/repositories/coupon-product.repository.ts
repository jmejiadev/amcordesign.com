import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CouponProduct, CouponProductRelations} from '../models';

export class CouponProductRepository extends DefaultCrudRepository<
  CouponProduct,
  typeof CouponProduct.prototype.id,
  CouponProductRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CouponProduct, dataSource);
  }
}
