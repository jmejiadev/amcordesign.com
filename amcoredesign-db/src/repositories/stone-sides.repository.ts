import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {StoneSides, StoneSidesRelations} from '../models';

export class StoneSidesRepository extends DefaultCrudRepository<
  StoneSides,
  typeof StoneSides.prototype.id,
  StoneSidesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(StoneSides, dataSource);
  }
}
