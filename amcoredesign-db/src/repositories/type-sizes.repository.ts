import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {TypeSizes, TypeSizesRelations} from '../models';

export class TypeSizesRepository extends DefaultCrudRepository<
  TypeSizes,
  typeof TypeSizes.prototype.id,
  TypeSizesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(TypeSizes, dataSource);
  }
}
