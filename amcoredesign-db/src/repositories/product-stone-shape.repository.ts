import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductStoneShape, ProductStoneShapeRelations} from '../models';

export class ProductStoneShapeRepository extends DefaultCrudRepository<
  ProductStoneShape,
  typeof ProductStoneShape.prototype.id,
  ProductStoneShapeRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductStoneShape, dataSource);
  }
}
