import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ThreeSixties, ThreeSixtiesRelations} from '../models';

export class ThreeSixtiesRepository extends DefaultCrudRepository<
  ThreeSixties,
  typeof ThreeSixties.prototype.id,
  ThreeSixtiesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ThreeSixties, dataSource);
  }
}
