import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {HomeShopProducts, HomeShopProductsRelations} from '../models';

export class HomeShopProductsRepository extends DefaultCrudRepository<
  HomeShopProducts,
  typeof HomeShopProducts.prototype.id,
  HomeShopProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(HomeShopProducts, dataSource);
  }
}
