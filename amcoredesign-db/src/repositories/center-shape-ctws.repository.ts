import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CenterShapeCtws, CenterShapeCtwsRelations} from '../models';

export class CenterShapeCtwsRepository extends DefaultCrudRepository<
  CenterShapeCtws,
  typeof CenterShapeCtws.prototype.id,
  CenterShapeCtwsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CenterShapeCtws, dataSource);
  }
}
