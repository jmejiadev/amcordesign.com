import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {OrderPaypal, OrderPaypalRelations} from '../models';

export class OrderPaypalRepository extends DefaultCrudRepository<
  OrderPaypal,
  typeof OrderPaypal.prototype.id,
  OrderPaypalRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(OrderPaypal, dataSource);
  }
}
