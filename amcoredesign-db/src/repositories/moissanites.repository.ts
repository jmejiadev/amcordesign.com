import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Moissanites, MoissanitesRelations} from '../models';

export class MoissanitesRepository extends DefaultCrudRepository<
  Moissanites,
  typeof Moissanites.prototype.id,
  MoissanitesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Moissanites, dataSource);
  }
}
