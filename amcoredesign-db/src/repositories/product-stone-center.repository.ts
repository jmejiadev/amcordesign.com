import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductStoneCenter, ProductStoneCenterRelations} from '../models';

export class ProductStoneCenterRepository extends DefaultCrudRepository<
  ProductStoneCenter,
  typeof ProductStoneCenter.prototype.id,
  ProductStoneCenterRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductStoneCenter, dataSource);
  }
}
