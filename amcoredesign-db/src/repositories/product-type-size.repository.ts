import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductTypeSize, ProductTypeSizeRelations} from '../models';

export class ProductTypeSizeRepository extends DefaultCrudRepository<
  ProductTypeSize,
  typeof ProductTypeSize.prototype.id,
  ProductTypeSizeRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductTypeSize, dataSource);
  }
}
