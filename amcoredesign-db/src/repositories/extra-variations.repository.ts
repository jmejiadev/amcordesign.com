import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ExtraVariations, ExtraVariationsRelations} from '../models';

export class ExtraVariationsRepository extends DefaultCrudRepository<
  ExtraVariations,
  typeof ExtraVariations.prototype.id,
  ExtraVariationsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ExtraVariations, dataSource);
  }
}
