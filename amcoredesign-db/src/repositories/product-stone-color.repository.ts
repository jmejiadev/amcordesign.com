import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductStoneColor, ProductStoneColorRelations} from '../models';

export class ProductStoneColorRepository extends DefaultCrudRepository<
  ProductStoneColor,
  typeof ProductStoneColor.prototype.id,
  ProductStoneColorRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductStoneColor, dataSource);
  }
}
