import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductVariation, ProductVariationRelations} from '../models';

export class ProductVariationRepository extends DefaultCrudRepository<
  ProductVariation,
  typeof ProductVariation.prototype.id,
  ProductVariationRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductVariation, dataSource);
  }
}
