import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CategoryStyle, CategoryStyleRelations} from '../models';

export class CategoryStyleRepository extends DefaultCrudRepository<
  CategoryStyle,
  typeof CategoryStyle.prototype.id,
  CategoryStyleRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CategoryStyle, dataSource);
  }
}
