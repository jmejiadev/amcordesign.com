import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ReviewImages, ReviewImagesRelations} from '../models';

export class ReviewImagesRepository extends DefaultCrudRepository<
  ReviewImages,
  typeof ReviewImages.prototype.id,
  ReviewImagesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ReviewImages, dataSource);
  }
}
