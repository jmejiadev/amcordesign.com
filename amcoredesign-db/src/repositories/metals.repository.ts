import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Metals, MetalsRelations} from '../models';

export class MetalsRepository extends DefaultCrudRepository<
  Metals,
  typeof Metals.prototype.id,
  MetalsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Metals, dataSource);
  }
}
