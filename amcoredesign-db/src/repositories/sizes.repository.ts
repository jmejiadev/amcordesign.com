import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Sizes, SizesRelations} from '../models';

export class SizesRepository extends DefaultCrudRepository<
  Sizes,
  typeof Sizes.prototype.id,
  SizesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Sizes, dataSource);
  }
}
