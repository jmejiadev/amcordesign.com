import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ProductStoneSide, ProductStoneSideRelations} from '../models';

export class ProductStoneSideRepository extends DefaultCrudRepository<
  ProductStoneSide,
  typeof ProductStoneSide.prototype.id,
  ProductStoneSideRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ProductStoneSide, dataSource);
  }
}
