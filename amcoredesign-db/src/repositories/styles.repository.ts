import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Styles, StylesRelations} from '../models';

export class StylesRepository extends DefaultCrudRepository<
  Styles,
  typeof Styles.prototype.id,
  StylesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Styles, dataSource);
  }
}
