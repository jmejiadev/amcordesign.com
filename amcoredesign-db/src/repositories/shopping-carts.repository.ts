import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ShoppingCarts, ShoppingCartsRelations} from '../models';

export class ShoppingCartsRepository extends DefaultCrudRepository<
  ShoppingCarts,
  typeof ShoppingCarts.prototype.id,
  ShoppingCartsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ShoppingCarts, dataSource);
  }
}
