import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {MetalColors, MetalColorsRelations} from '../models';

export class MetalColorsRepository extends DefaultCrudRepository<
  MetalColors,
  typeof MetalColors.prototype.id,
  MetalColorsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(MetalColors, dataSource);
  }
}
