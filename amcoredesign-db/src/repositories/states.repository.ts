import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {States, StatesRelations} from '../models';

export class StatesRepository extends DefaultCrudRepository<
  States,
  typeof States.prototype.id,
  StatesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(States, dataSource);
  }
}
