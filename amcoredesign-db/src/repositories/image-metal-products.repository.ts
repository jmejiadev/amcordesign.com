import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ImageMetalProducts, ImageMetalProductsRelations} from '../models';

export class ImageMetalProductsRepository extends DefaultCrudRepository<
  ImageMetalProducts,
  typeof ImageMetalProducts.prototype.id,
  ImageMetalProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ImageMetalProducts, dataSource);
  }
}
