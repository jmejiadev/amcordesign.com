import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {CategoryProduct, CategoryProductRelations} from '../models';

export class CategoryProductRepository extends DefaultCrudRepository<
  CategoryProduct,
  typeof CategoryProduct.prototype.id,
  CategoryProductRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(CategoryProduct, dataSource);
  }
}
