import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ImageStoneShapeProducts, ImageStoneShapeProductsRelations} from '../models';

export class ImageStoneShapeProductsRepository extends DefaultCrudRepository<
  ImageStoneShapeProducts,
  typeof ImageStoneShapeProducts.prototype.id,
  ImageStoneShapeProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ImageStoneShapeProducts, dataSource);
  }
}
