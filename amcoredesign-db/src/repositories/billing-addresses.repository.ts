import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {BillingAddresses, BillingAddressesRelations} from '../models';

export class BillingAddressesRepository extends DefaultCrudRepository<
  BillingAddresses,
  typeof BillingAddresses.prototype.id,
  BillingAddressesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(BillingAddresses, dataSource);
  }
}
