import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {MetalProduct, MetalProductRelations} from '../models';

export class MetalProductRepository extends DefaultCrudRepository<
  MetalProduct,
  typeof MetalProduct.prototype.id,
  MetalProductRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(MetalProduct, dataSource);
  }
}
