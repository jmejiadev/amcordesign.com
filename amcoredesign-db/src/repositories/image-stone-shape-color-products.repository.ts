import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {ImageStoneShapeColorProducts, ImageStoneShapeColorProductsRelations} from '../models';

export class ImageStoneShapeColorProductsRepository extends DefaultCrudRepository<
  ImageStoneShapeColorProducts,
  typeof ImageStoneShapeColorProducts.prototype.id,
  ImageStoneShapeColorProductsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(ImageStoneShapeColorProducts, dataSource);
  }
}
