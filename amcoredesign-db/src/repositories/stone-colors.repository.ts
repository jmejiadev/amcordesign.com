import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {StoneColors, StoneColorsRelations} from '../models';

export class StoneColorsRepository extends DefaultCrudRepository<
  StoneColors,
  typeof StoneColors.prototype.id,
  StoneColorsRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(StoneColors, dataSource);
  }
}
