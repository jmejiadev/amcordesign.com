import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {AmcoredesignDbDataSource} from '../datasources';
import {Phones, PhonesRelations} from '../models';

export class PhonesRepository extends DefaultCrudRepository<
  Phones,
  typeof Phones.prototype.id,
  PhonesRelations
> {
  constructor(
    @inject('datasources.amcoredesignDb') dataSource: AmcoredesignDbDataSource,
  ) {
    super(Phones, dataSource);
  }
}
