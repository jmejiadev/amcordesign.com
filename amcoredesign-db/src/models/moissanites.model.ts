import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'moissanites'}}
})
export class Moissanites extends Entity {
  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'color', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  color?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'creation_type', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  creationType?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'cut', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  cut?: string;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'description', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  description?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'gem_species', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  gemSpecies?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'hardness', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  hardness?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    precision: 9,
    scale: 2,
    mysql: {columnName: 'price', dataType: 'double', dataLength: null, dataPrecision: 9, dataScale: 2, nullable: 'Y'},
  })
  price?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'price_ha', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  priceHa?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'quality', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  quality?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'shape_cut', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  shapeCut?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'size_mm', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  sizeMm?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'sku', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  sku?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'stone_type', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  stoneType?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'three_sixty', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  threeSixty?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'title', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  title?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'treatment', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  treatment?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'unique_cut', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  uniqueCut?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'weight', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  weight?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Moissanites>) {
    super(data);
  }
}

export interface MoissanitesRelations {
  // describe navigational properties here
}

export type MoissanitesWithRelations = Moissanites & MoissanitesRelations;
