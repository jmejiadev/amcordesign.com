import {Entity, hasMany, model, property} from '@loopback/repository';
import {Products} from './products.model';
import {CategoryProduct} from './category-product.model';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'categories'}}
})
export class Categories extends Entity {
  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'banner_1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  banner_1?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'banner_2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  banner_2?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'banner_3', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  banner_3?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'banner_4', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  banner_4?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'description', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  description?: string;

  @property({
    type: 'number',
    precision: 8,
    scale: 2,
    mysql: {columnName: 'discount', dataType: 'decimal', dataLength: null, dataPrecision: 8, dataScale: 2, nullable: 'Y'},
  })
  discount?: number;

  @property({
    type: 'number',
    precision: 10,
    scale: 0,
    mysql: {columnName: 'google_category_id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'Y'},
  })
  googleCategoryId?: number;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  name: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'path', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  path?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'path_product', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  pathProduct?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'position_banner_1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  positionBanner_1?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'position_banner_2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  positionBanner_2?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'position_banner_4', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  positionBanner_4?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'slug', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  slug: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_banner_1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textBanner_1?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_banner_2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textBanner_2?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_banner_3', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textBanner_3?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_banner_4', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textBanner_4?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_description_banner_1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textDescriptionBanner_1?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_description_banner_2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textDescriptionBanner_2?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_description_banner_3', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textDescriptionBanner_3?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'text_description_banner_4', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  textDescriptionBanner_4?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'url_banner_1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  urlBanner_1?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'url_banner_2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  urlBanner_2?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'url_banner_3', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  urlBanner_3?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'url_banner_4', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  urlBanner_4?: string;
  
  @hasMany(() => Products, {through: {model: () => CategoryProduct, keyFrom: 'category_id', keyTo: 'product_id'}})
  products: Products[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Categories>) {
    super(data);
  }
}

export interface CategoriesRelations {
  // describe navigational properties here
}

export type CategoriesWithRelations = Categories & CategoriesRelations;
