import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'google_categories'}}
})
export class GoogleCategories extends Entity {
  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'CategoryName', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  categoryName: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'SubCategory_1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  subCategory_1?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'SubCategory_2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  subCategory_2?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'SubCategory_3', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  subCategory_3?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'SubCategory_4', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  subCategory_4?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'SubCategory_5', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  subCategory_5?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'SubCategory_6', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  subCategory_6?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<GoogleCategories>) {
    super(data);
  }
}

export interface GoogleCategoriesRelations {
  // describe navigational properties here
}

export type GoogleCategoriesWithRelations = GoogleCategories & GoogleCategoriesRelations;
