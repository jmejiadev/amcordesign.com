import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'order_products'}}
})
export class OrderProducts extends Entity {
  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'coupon_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  couponId?: number;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 4,
    mysql: {columnName: 'discount', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 4, nullable: 'Y'},
  })
  discount?: number;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'gemstone_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  gemstoneId?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'grams', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  grams?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'metal_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  metalId?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'metal_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  metalName?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  name?: string;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'order_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  orderId: number;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'personalization', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  personalization?: string;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'product_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  productId: number;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'quantity', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  quantity: number;

  @property({
    type: 'number',
    required: true,
    precision: 16,
    scale: 4,
    mysql: {columnName: 'selling_price', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 4, nullable: 'N'},
  })
  sellingPrice: number;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_center_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneCenterId?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'stone_center_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  stoneCenterName?: string;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_color_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneColorId?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'stone_color_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  stoneColorName?: string;

  @property({
    type: 'number',
    precision: 10,
    scale: 0,
    mysql: {columnName: 'stone_number', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'Y'},
  })
  stoneNumber?: number;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_shape_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneShapeId?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'stone_shape_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  stoneShapeName?: string;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_side_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneSideId?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'stone_side_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  stoneSideName?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 4,
    mysql: {columnName: 'subtotal', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 4, nullable: 'Y'},
  })
  subtotal?: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'supplier_sku', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  supplierSku?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<OrderProducts>) {
    super(data);
  }
}

export interface OrderProductsRelations {
  // describe navigational properties here
}

export type OrderProductsWithRelations = OrderProducts & OrderProductsRelations;
