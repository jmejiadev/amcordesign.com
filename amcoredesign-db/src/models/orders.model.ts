import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'orders'}}
})
export class Orders extends Entity {
  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'billing_address_line1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  billingAddressLine1: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'billing_address_line2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  billingAddressLine2?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'billing_city', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  billingCity: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'billing_country', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  billingCountry: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'billing_state', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  billingState: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'billing_zip_code', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  billingZipCode?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'client_ip', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  clientIp: string;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'email', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  email: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'first_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  firstName: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'last_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  lastName: string;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'notes', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  notes?: string;

  @property({
    type: 'number',
    required: true,
    precision: 19,
    scale: 0,
    mysql: {columnName: 'order_number', dataType: 'bigint', dataLength: null, dataPrecision: 19, dataScale: 0, nullable: 'N'},
  })
  orderNumber: number;

  @property({
    type: 'string',
    required: true,
    length: 14,
    mysql: {columnName: 'order_status', dataType: 'enum', dataLength: 14, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  orderStatus: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'payment_type', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  paymentType: string;

  @property({
    type: 'number',
    precision: 20,
    scale: 0,
    mysql: {columnName: 'person_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  personId?: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'phone', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  phone: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'shipping_address_line1', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  shippingAddressLine1: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'shipping_address_line2', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  shippingAddressLine2?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'shipping_city', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  shippingCity: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'shipping_country', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  shippingCountry: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'shipping_state', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  shippingState: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'shipping_zip_code', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  shippingZipCode?: string;

  @property({
    type: 'number',
    required: true,
    precision: 22,
    mysql: {columnName: 'tax', dataType: 'double', dataLength: null, dataPrecision: 22, dataScale: null, nullable: 'N'},
  })
  tax: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'total', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  total: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Orders>) {
    super(data);
  }
}

export interface OrdersRelations {
  // describe navigational properties here
}

export type OrdersWithRelations = Orders & OrdersRelations;
