import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'inventory'}}
})
export class Inventory extends Entity {
  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Allow_Raplink_Feed', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  allowRaplinkFeed?: string;

  //añadido por Cristian --nota no uso esta tabla si afecta comportamiento de otro proceso, no puedo hacerme responsable del mismo, Cristian debes evaluar el alcance del cambio
  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_color_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  stoneColorId: number;


  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Availability', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  availability?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'BGM', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  bgm?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Black_Inclusion', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  blackInclusion?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Brand', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  brand?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 2,
    mysql: {columnName: 'Buy_Price', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 2, nullable: 'Y'},
  })
  buyPrice?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Buy_Price_Discount_PER', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  buyPriceDiscountPer?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 2,
    mysql: {columnName: 'Buy_Price_Total', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 2, nullable: 'Y'},
  })
  buyPriceTotal?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Center_Inclusion', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  centerInclusion?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Cert_Comments', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  certComments?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Certificate', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  certificate?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'CertificateLink', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  certificateLink?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'City', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  city?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Clarity', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  clarity?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Clarity_Description', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  clarityDescription?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 2,
    mysql: {columnName: 'COD_Buy_Price', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 2, nullable: 'Y'},
  })
  codBuyPrice?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'COD_Buy_Price_Discount_PER', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  codBuyPriceDiscountPer?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 2,
    mysql: {columnName: 'COD_Buy_Price_Total', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 2, nullable: 'Y'},
  })
  codBuyPriceTotal?: number;

  @property({
    type: 'string',
    required: true,
    length: 65535,
    id: 2,
    mysql: {columnName: 'cod_proveedor', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  codProveedor: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Color', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  color?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Country', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  country?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Country_Of_Origin', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  countryOfOrigin?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Crown_Angle', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  crownAngle?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Crown_Height', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  crownHeight?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Culet_Condition', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  culetCondition?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Culet_Size', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  culetSize?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Cut_Grade', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  cutGrade?: string;

  @property({
    type: 'number',
    precision: 6,
    scale: 2,
    mysql: {columnName: 'DEPTH_PER', dataType: 'decimal', dataLength: null, dataPrecision: 6, dataScale: 2, nullable: 'Y'},
  })
  depthPer?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Eye_Clean', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  eyeClean?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Fancy_Color_Intensity', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  fancyColorIntensity?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'FancyColor', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  fancyColor?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'FancyColorOvertone', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  fancyColorOvertone?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Fluorescence_Color', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  fluorescenceColor?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Fluorescence_Intensity', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  fluorescenceIntensity?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Gemprint_ID', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  gemprintId?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Girdle_Condition', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  girdleCondition?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Girdle_Max', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  girdleMax?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Girdle_Min', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  girdleMin?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Girdle_Per', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  girdlePer?: string;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'ImageLink', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  imageLink?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Internal_Clarity_Desc_Code', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  internalClarityDescCode?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Key_To_Symbols', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  keyToSymbols?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Lab', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  lab?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Lab_Location', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  labLocation?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'LsMatchedPairSeparable', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  lsMatchedPairSeparable?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Measurements', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  measurements?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Member_Comments', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  memberComments?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Memo_Discount_PER', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  memoDiscountPer?: string;

  @property({
    type: 'number',
    precision: 16,
    scale: 2,
    mysql: {columnName: 'Memo_Price', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 2, nullable: 'Y'},
  })
  memoPrice?: number;

  @property({
    type: 'number',
    precision: 16,
    scale: 2,
    mysql: {columnName: 'Memo_Price_total', dataType: 'decimal', dataLength: null, dataPrecision: 16, dataScale: 2, nullable: 'Y'},
  })
  memoPriceTotal?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Milky', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  milky?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Modified_Rate', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  modifiedRate?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Pair_Stock', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  pairStock?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Parcel_Stones', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  parcelStones?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Pavilion_Angle', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  pavilionAngle?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Pavilion_Depth', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  pavilionDepth?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Polish', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  polish?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Report_Issue_Date', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  reportIssueDate?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Report_Type', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  reportType?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Sarine_Name', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  sarineName?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Shade', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  shade?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Shape', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  shape?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Star_Length', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  starLength?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'State', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  state?: string;

  @property({
    type: 'number',
    precision: 10,
    scale: 0,
    mysql: {columnName: 'status', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'Y'},
  })
  status?: number;

  @property({
    type: 'string',
    required: true,
    length: 65535,
    id: 1,
    mysql: {columnName: 'Stock_No', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  stockNo: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Symmetry', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  symmetry?: string;

  @property({
    type: 'number',
    precision: 6,
    scale: 2,
    mysql: {columnName: 'TABLE_PER', dataType: 'decimal', dataLength: null, dataPrecision: 6, dataScale: 2, nullable: 'Y'},
  })
  tablePer?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Time_to_Location', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  timeToLocation?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Treatment', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  treatment?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'Video_HTML', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  videoHtml?: string;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'VideoLink', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  videoLink?: string;

  @property({
    type: 'number',
    precision: 6,
    scale: 2,
    mysql: {columnName: 'Weight', dataType: 'decimal', dataLength: null, dataPrecision: 6, dataScale: 2, nullable: 'Y'},
  })
  weight?: number;

  @property({
    type: 'string',
    length: 65535,
    mysql: {columnName: 'wire_discount_price', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  wireDiscountPrice?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Inventory>) {
    super(data);
  }
}

export interface InventoryRelations {
  // describe navigational properties here
}

export type InventoryWithRelations = Inventory & InventoryRelations;
