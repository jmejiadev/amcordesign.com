import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'order_paypal'}}
})
export class OrderPaypal extends Entity {
  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'create_time', dataType: 'datetime', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  createTime: string;

  @property({
    type: 'string',
    required: true,
    length: 65535,
    mysql: {columnName: 'detail', dataType: 'text', dataLength: 65535, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  detail: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'order_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  orderId: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'paypal_id', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  paypalId: string;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'update_time', dataType: 'datetime', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  updateTime: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<OrderPaypal>) {
    super(data);
  }
}

export interface OrderPaypalRelations {
  // describe navigational properties here
}

export type OrderPaypalWithRelations = OrderPaypal & OrderPaypalRelations;
