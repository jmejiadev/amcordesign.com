import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    mysql: {schema: 'amcordesign', table: 'product_stone_side'}
  }
})
export class ProductStoneSide extends Entity {
  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'carat', dataType: 'char', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  carat?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    precision: 22,
    mysql: {columnName: 'price', dataType: 'double', dataLength: null, dataPrecision: 22, dataScale: null, nullable: 'Y'},
  })
  price?: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'product_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  productId: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_side_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  stoneSideId: number;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ProductStoneSide>) {
    super(data);
  }
}

export interface ProductStoneSideRelations {
  // describe navigational properties here
}

export type ProductStoneSideWithRelations = ProductStoneSide & ProductStoneSideRelations;
