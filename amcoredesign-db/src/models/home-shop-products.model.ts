import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    mysql: {schema: 'amcordesign', table: 'home_shop_products'}
  }
})
export class HomeShopProducts extends Entity {
  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'home_shop_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  homeShopId: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'product_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  productId: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'metal_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  metalId: number;

  @property({
    type: 'number',
    required: false,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_shape_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneShapeId?: number;

  @property({
    type: 'number',
    required: false,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_center_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneCenterId?: number;

  @property({
    type: 'number',
    required: false,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_side_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneSideId?: number;

  @property({
    type: 'number',
    required: false,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'stone_color_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'Y'},
  })
  stoneColorId?: number;

  @property({
    type: 'string',
    required: false,
    length: 4294967295,
    mysql: {columnName: 'path', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  path?: string;

  @property({
    type: 'number',
    required: true,
    precision: 5,
    scale: 2,
    mysql: {columnName: 'positionX', dataType: 'double', dataLength: null, dataPrecision: 5, dataScale: 2, nullable: 'N'},
  })
  positionX: number;

  @property({
    type: 'number',
    required: true,
    precision: 5,
    scale: 2,
    mysql: {columnName: 'positionY', dataType: 'double', dataLength: null, dataPrecision: 5, dataScale: 2, nullable: 'N'},
  })
  positionY: number;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<HomeShopProducts>) {
    super(data);
  }
}

export interface HomeShopProductsRelations {
  // describe navigational properties here
}

export type HomeShopProductsWithRelations = HomeShopProducts & HomeShopProductsRelations;
