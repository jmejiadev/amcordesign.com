import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    mysql: {schema: 'amcordesign', table: 'order_products_variants'}
  }
})
export class OrderProductsVariants extends Entity {
  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'extra_variation_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  extraVariationId: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'extra_variation_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  extraVariationName: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'order_product_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  orderProductId: number;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'variation_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  variationId: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'variation_name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  variationName?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<OrderProductsVariants>) {
    super(data);
  }
}

export interface OrderProductsVariantsRelations {
  // describe navigational properties here
}

export type OrderProductsVariantsWithRelations = OrderProductsVariants & OrderProductsVariantsRelations;
