import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'metals'}}
})
export class Metals extends Entity {
  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'carat', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  carat: number;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'icon', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  icon: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'metal_color_id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  metalColorId: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  name: string;

  @property({
    type: 'number',
    required: true,
    precision: 22,
    mysql: {columnName: 'price', dataType: 'double', dataLength: null, dataPrecision: 22, dataScale: null, nullable: 'N'},
  })
  price: number;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Metals>) {
    super(data);
  }
}

export interface MetalsRelations {
  // describe navigational properties here
}

export type MetalsWithRelations = Metals & MetalsRelations;
