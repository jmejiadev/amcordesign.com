import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'home_shop'}}
})
export class HomeShop extends Entity {
  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 4294967295,
    mysql: {columnName: 'path', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  path: string;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<HomeShop>) {
    super(data);
  }
}

export interface HomeShopRelations {
  // describe navigational properties here
}

export type HomeShopWithRelations = HomeShop & HomeShopRelations;
