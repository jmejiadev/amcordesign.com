import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'stone_centers'}}
})
export class StoneCenters extends Entity {
  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'description', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  description?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'icon', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  icon?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  name: string;

  @property({
    type: 'number',
    required: true,
    precision: 22,
    mysql: {columnName: 'price', dataType: 'double', dataLength: null, dataPrecision: 22, dataScale: null, nullable: 'N'},
  })
  price: number;

  @property({
    type: 'number',
    required: true,
    precision: 22,
    mysql: {columnName: 'size', dataType: 'double', dataLength: null, dataPrecision: 22, dataScale: null, nullable: 'N'},
  })
  size: number;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StoneCenters>) {
    super(data);
  }
}

export interface StoneCentersRelations {
  // describe navigational properties here
}

export type StoneCentersWithRelations = StoneCenters & StoneCentersRelations;
