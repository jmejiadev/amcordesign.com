import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'current_locations'}}
})
export class CurrentLocations extends Entity {
  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    length: 160,
    mysql: {columnName: 'latitude', dataType: 'char', dataLength: 160, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  latitude?: string;

  @property({
    type: 'string',
    length: 160,
    mysql: {columnName: 'longitude', dataType: 'char', dataLength: 160, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  longitude?: string;

  @property({
    type: 'number',
    required: true,
    precision: 20,
    scale: 0,
    mysql: {columnName: 'person_id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  personId: number;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CurrentLocations>) {
    super(data);
  }
}

export interface CurrentLocationsRelations {
  // describe navigational properties here
}

export type CurrentLocationsWithRelations = CurrentLocations & CurrentLocationsRelations;
