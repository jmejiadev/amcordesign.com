import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'products'}}
})
export class Products extends Entity {
  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'average_width', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  averageWidth?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'description', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  description?: string;

  @property({
    type: 'number',
    required: true,
    precision: 8,
    scale: 2,
    mysql: {columnName: 'discount', dataType: 'decimal', dataLength: null, dataPrecision: 8, dataScale: 2, nullable: 'N'},
  })
  discount: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'finish', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  finish?: string;

  @property({
    type: 'string',
    required: true,
    length: 5,
    mysql: {columnName: 'google', dataType: 'enum', dataLength: 5, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  google: string;

  @property({
    type: 'string',
    length: 1,
    mysql: {columnName: 'google_sync', dataType: 'char', dataLength: 1, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  googleSync?: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'grams_weight', dataType: 'decimal', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  gramsWeight: number;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'local_sku', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  localSku: string;

  @property({
    type: 'number',
    precision: 8,
    scale: 2,
    mysql: {columnName: 'manufacturer', dataType: 'decimal', dataLength: null, dataPrecision: 8, dataScale: 2, nullable: 'Y'},
  })
  manufacturer?: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    length: 5,
    mysql: {columnName: 'publish', dataType: 'enum', dataLength: 5, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  publish: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'setting', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  setting?: string;

  @property({
    type: 'string',
    length: 4294967295,
    mysql: {columnName: 'short_description', dataType: 'longtext', dataLength: 4294967295, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  shortDescription?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'slug', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  slug: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    mysql: {columnName: 'stone_number', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  stoneNumber: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'supplier_sku', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  supplierSku: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Products>) {
    super(data);
  }
}

export interface ProductsRelations {
  // describe navigational properties here
}

export type ProductsWithRelations = Products & ProductsRelations;
