import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'password_resets'}}
})
export class PasswordResets extends Entity {
  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'email', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  email: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'token', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  token: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<PasswordResets>) {
    super(data);
  }
}

export interface PasswordResetsRelations {
  // describe navigational properties here
}

export type PasswordResetsWithRelations = PasswordResets & PasswordResetsRelations;
