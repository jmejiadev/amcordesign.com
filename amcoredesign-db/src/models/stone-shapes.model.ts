import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'stone_shapes'}}
})
export class StoneShapes extends Entity {
  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'icon', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  icon?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'name', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  name: string;

  @property({
    type: 'number',
    required: true,
    precision: 8,
    scale: 2,
    mysql: {columnName: 'percent', dataType: 'decimal', dataLength: null, dataPrecision: 8, dataScale: 2, nullable: 'N'},
  })
  percent: number;

  @property({
    type: 'number',
    required: true,
    precision: 22,
    mysql: {columnName: 'price', dataType: 'double', dataLength: null, dataPrecision: 22, dataScale: null, nullable: 'N'},
  })
  price: number;

  @property({
    type: 'string',
    length: 255,
    mysql: {columnName: 'sku', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  sku?: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StoneShapes>) {
    super(data);
  }
}

export interface StoneShapesRelations {
  // describe navigational properties here
}

export type StoneShapesWithRelations = StoneShapes & StoneShapesRelations;
