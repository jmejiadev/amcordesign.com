import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, mysql: {schema: 'amcordesign', table: 'coupons'}}
})
export class Coupons extends Entity {
  @property({
    type: 'date',
    mysql: {columnName: 'created_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  createdAt?: string;

  @property({
    type: 'number',
    required: true,
    precision: 8,
    scale: 2,
    mysql: {columnName: 'discount', dataType: 'decimal', dataLength: null, dataPrecision: 8, dataScale: 2, nullable: 'N'},
  })
  discount: number;

  @property({
    type: 'string',
    mysql: {columnName: 'finish_date', dataType: 'date', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  finishDate?: string;

  @property({
    type: 'string',
    mysql: {columnName: 'finish_hour', dataType: 'time', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  finishHour?: string;

  @property({
    type: 'number',
    generated: true,
    required: false,
    precision: 20,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'bigint', dataLength: null, dataPrecision: 20, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    mysql: {columnName: 'start_date', dataType: 'date', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  startDate?: string;

  @property({
    type: 'string',
    mysql: {columnName: 'start_hour', dataType: 'time', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  startHour?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    mysql: {columnName: 'title', dataType: 'varchar', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  title: string;

  @property({
    type: 'date',
    mysql: {columnName: 'updated_at', dataType: 'timestamp', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'Y'},
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Coupons>) {
    super(data);
  }
}

export interface CouponsRelations {
  // describe navigational properties here
}

export type CouponsWithRelations = Coupons & CouponsRelations;
