import {OrderStatusEnum} from '../enums';
import {PaymentType} from '../literal-enums';

export type Order = {
  id: number;
  order_number: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  client_ip: string;
  total: number;
  notes?: string;
  order_status: OrderStatusEnum;
  shipping_address_line1: string;
  shipping_address_line2?: string;
  shipping_zip_code?: string;
  shipping_city: string;
  shipping_state: string;
  billing_address_line1: string;
  billing_address_line2?: string;
  billing_zip_code?: string;
  billing_city: string;
  billing_state: string;
  payment_type: PaymentType;
  shipping_country: string;
  billing_country: string;
  tax: number;
  person_id?: number;
  created_at?: Date;
  updated_at?: Date;
}
