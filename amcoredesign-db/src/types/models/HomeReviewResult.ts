export type HomeReviewResult = {
	id: number,
	author: string,
	description: string,
	path: string,
}