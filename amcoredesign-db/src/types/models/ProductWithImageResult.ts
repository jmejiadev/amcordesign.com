export type ProductWithImageResult = {
  id: number,
	name: string,
	slug: string,
	sellingPrice: number,
	discount: string,
	stoneColorCarat: string,
	stoneColorId: number,

	ctwPreviewPath: string,
	ctwPreviewMetalId: number,
	ctwPreviewStoneShapeId: number,
	ctwPreviewStoneCenterId: number,
  
  stoneShapePreviewPath: string,
	stoneShapePreviewMetalId: number,
	stoneShapePreviewStoneShapeId: number,
  
  stoneSidePreviewPath: string,
	stoneSidePreviewMetalId: number,
	stoneSidePreviewStoneSideId: number,
	stoneSidePreviewStoneColorId: number,
}