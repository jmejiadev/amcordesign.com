import { OrderProductsSizes, OrderProductsVariants } from ".";

export type OrderProducts = {
  id: number;
  order_id: number;
  product_id: number;
  name?: string;
  supplier_sku?: string;
  grams?: string;
  stone_number?: number;
  metal_id?: number;
  metal_name?: string;
  stone_center_id?: number;
  stone_center_name?: string;
  stone_side_id?: number;
  stone_side_name?: string;
  selling_price: number;
  discount?: number;
  personalization?: string;
  quantity?: number;
  stone_shape_id?: number;
  stone_shape_name?: string;
  stone_color_id?: number;
  stone_color_name?: string;
  subtotal?: number;
  coupon_id?: number;
  gemstone_id?: number;
  created_at?: Date;
  updated_at?: Date;

  // NAV PROPS
  productSizes?: OrderProductsSizes[];
  productVariants?: OrderProductsVariants[];
}
