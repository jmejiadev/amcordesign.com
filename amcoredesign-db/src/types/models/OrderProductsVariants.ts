
export type OrderProductsVariants = {
  id: number;
  order_product_id: number;
  extra_variation_id: number;
  extra_variation_name: string;
  variation_id: number;
  variation_name?: string;
  created_at?: Date;
  updated_at?: Date;
}
