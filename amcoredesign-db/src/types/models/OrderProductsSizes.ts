
export type OrderProductsSizes = {
  id: number;
  order_product_id: number;
  type_size_id: number;
  size_number: string;
  created_at?: Date;
  updated_at?: Date;
}
