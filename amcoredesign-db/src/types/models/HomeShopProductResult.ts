import { ProductWithImageResult } from "./ProductWithImageResult";

export type HomeShopProductResult = ProductWithImageResult &{
	shopProductPath: string,
	shopProductMetalId: number,
	shopProductStoneShapeId: number,
	shopProductStoneCenterId: number,
	shopProductStoneSideId: number,
	shopProductStoneColorId: number,

	positionX: number,
	positionY: number,
}