export enum GemstoneStatusEnum {
  NO_DISPONIBLE = 0,
  DISPONIBLE = 1,
  PEDIDO_REALIZADO = 2,
}