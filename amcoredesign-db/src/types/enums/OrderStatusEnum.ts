export enum OrderStatusEnum {
  CREATE = 1,
  SEND = 2,
  FINISH = 3,
  REFUND = 4,
  NEED_ATTENTION = 5,
}