export type MysqlErrorType = {
  message: string;
  code: string;
  errno: number;
  sqlState: string;
  sqlMessage: string;
}
