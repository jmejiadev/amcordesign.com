export type GetProductSimpleDto = {
  id: number;
  name: string;
  slug: string;
  selling_price: number;
  discount: number;   //percent
  imagePreview: string;
}

export type GetProductDto = {
  id: number;
  name: string;
  short_description: string;
  description: string;
  local_sku: string;
  manufacturer: number;
  slug: string;
  stone_number: number;
  grams_weight: number;
  average_width: string;
  finish: string;
  setting: string;
  categories: GetProductCategoryDto[];
  metals: GetProductMetalsDto[];
  stoneColors: GetProductStoneColorsDto[];
  typeSizes: GetProductTypeSizesDto[];
  discount: number;   //percent
  extraVariations: GetProductExtraVariationDto[];
  variations: GetProductVariationDto[];
  stone_shapes: GetProductStoneShapeDto[];
  stone_centers: GetProductStoneCenterDto[];
  stoneShapeGallery: GetProductStoneShapeGalleryDto[];
  stoneSideGallery: GetProductStoneSideGalleryDto[];
  centerShapeCtw: GetProductCenterShapeCtwDto;
  threeSixtyGallery: GetProductThreeSixtyGalleryDto[];
}

export type GetProductGalleryDto = {
  url: string;
}

export type GetProductCategoryDto = {
  id: number;
  title: string;
  slug: string;
}

export type GetProductMetalsDto = {
  id: number;
  name: string;
  icon: string;
  price: number;
}

export type GetProductStoneColorsDto = {
  id: number;
  name: string;
  treatment: string;
  quality: string;
  cut: string;
  icon: string;
  price: number;
  carat: string;
}

export type GetProductTypeSizesDto = {
  id: number;
  name: string;
  sizes: GetProductSizesDto[];
}

export type GetProductSizesDto = {
  id: number;
  number: string;
  price?: number;
}

export type GetProductStoneShapeDto = {
  id: number;
  price: number;
}

export type GetProductStoneCenterDto = {
  id: number;
  name: string;
  size: number;
  price: number;
}

export type GetProductStoneShapeGalleryDto = {
  id: number;
  path: string;
  metal_id: number;
  stone_shape_id: number;
}

export type GetProductStoneSideGalleryDto = {
  id: number;
  path: string;
  metal_id: number;
  stone_side_id: number;
  stone_color_id: number;
}

export type GetProductCenterShapeCtwDto = {
  id: number;
  stone_shapes: GetProductStoneShapeDto[];
  stone_centers: GetProductStoneCenterDto[];
  gallery: GetProductCenterShapeCtwGalleryDto[]; //image_center_shape_ctws
}

export type GetProductCenterShapeCtwGalleryDto = {
  id: number;
  path: string;
  metal_id: number;
  stone_shape_id: number;
  stone_center_id: number;
}

export type GetProductExtraVariationDto = {
  id: number;
  name: string;
  description: string;
}

export type GetProductVariationDto = {
  id: number;
  name: string;
  description: string;
  icon: string;
  extra_variation_id: number;
}

export type GetProductThreeSixtyGalleryDto = {
  productId: number;
  path: string;
  stoneShapeId: number;
  metalColorId: number;
  quantity: number;
}