export type ProductSimpleDto = {
  id: number;
	name: string;
	slug: string;
	sellingPrice: number;
	discount: number;
	stoneColorCarat?: string;
	stoneColorId?: number;
	imagePreview: ProductSimplePreviewDto[];
}

export type ProductSimplePreviewDto = {
	path: string;
	metalId: number;
	stoneShapeId?: number;
	stoneCenterId?: number;
	stoneSideId?: number;
	stoneColorId?: number;
}