import {Order, OrderProducts, OrderProductsSizes, OrderProductsVariants} from '../models';

export type PartialOrderHeadDto = Omit<
  Order,
  'id' | 'order_number' | 'client_ip' | 'created_at' | 'updated_at'
>;
export type PartialOrderProductsSizesDto = Omit<
  OrderProductsSizes,
  'id' | 'order_product_id' | 'created_at' | 'updated_at'
>;
export type PartialOrderProductsVariantsDto = Omit<
  OrderProductsVariants,
  'id' | 'order_product_id' | 'created_at' | 'updated_at'
>;
export type PartialOrderProductsDto = Omit<
  OrderProducts,
  'id' | 'order_id' | 'productSizes' | 'productVariants' | 'created_at' | 'updated_at'
> & {
  productSizes?: PartialOrderProductsSizesDto[];
  productVariants?: PartialOrderProductsVariantsDto[];
};

export type PaymentInfo = {
  id: string;
  createTime: Date;
  updateTime: Date;
  detail: string;
}

export type PostOrderDto = {
  head: PartialOrderHeadDto;
  details: PartialOrderProductsDto[];
  payment: PaymentInfo;
}

export interface IGetOrderDto {
  head: Order;
  details: OrderProducts[];
  payment: PaymentInfo;
}

export class GetOrderDto implements IGetOrderDto {
  head: Order;
  details: OrderProducts[];
  payment: PaymentInfo;
}
