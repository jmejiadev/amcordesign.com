import { ProductSimpleDto } from ".";

export type ShopCarouselDto = {
  imgSrc: string;
  products: ShopCarouselProductData[];
}

export type ShopCarouselProductData = PositionCoordinates & ProductSimpleDto;

export type PositionCoordinates = {
  positionX?: number;
  positionY?: number;
}