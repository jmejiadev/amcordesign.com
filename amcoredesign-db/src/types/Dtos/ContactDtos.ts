
export type PostContactMailDto = {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  message: string;
}
