export type GetGemstoneDto = {
  id: number;
  stock_No: string;
  shape: string;
  weight: number; // carat
  color: string;
  cut_Grade: string; //cut
  measurements: string; //Size mm
  gem_specie: string; //Gem Sp.
  price: number;
  stone_type: string;
  sku: string;
  imageLink: string;
  videoLink: string;
}
