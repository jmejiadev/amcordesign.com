export type HomeReviewDto = {
	id: number,
	author: string,
	description: string,
	path: string,
}