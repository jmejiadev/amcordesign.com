export type CouponDto = {
  id: number;
  title: string;
  image: string;
  code: string;
  discountInPercent: number;
  number_of_coupon: number;
  number_of_used_coupon: number;
  products: CouponProductDto[];
  status: "active" | "revoked";
  expiration_date: Date;
  creation_date: Date;
}

export type CouponProductDto = {
  id: number;
  url: string;
}
