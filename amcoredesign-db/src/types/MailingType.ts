import {Options} from 'nodemailer/lib/mailer';
'nodemailer/lib';

export type MailingType = {
  sendEmail: (mailInfo: Options) => void;
}
