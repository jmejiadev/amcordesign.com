
var google = require('googleapis');
var Request = require("request");
const fetch = require("node-fetch");
var mysql = require('mysql');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var en = require ('custom-env').env('apis','../');

var conexion= mysql.createConnection({
    host : process.env.DB_HOST,
    database : process.env.DATABASE,
    port: process.env.DB_PORT,
    user : process.env.DB_USER,
    password : process.env.DB_PASS,
});

conexion.connect(function(err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});

//console.log(process.env)

const auth = new google.google.auth.GoogleAuth({
    keyFile: './content-api-key.json',
    scopes: ['https://www.googleapis.com/auth/content'],
});

var content = google.google.content({ version: 'v2.1', auth: auth });

/*content.products.list({merchantId: process.env.MERCHANT_ID},function(err, resp) {
    // handle err and response
    if(err) return console.log(err);
    console.log(resp.data.resources);*/


//VALIDA SI AUN ESTA DISPONIBLE EL REGISTRO
var sql = "select p.id, p.name, p.local_sku, p.description, p.slug, CASE WHEN gc.CategoryName IS NULL THEN 'Sin Categoria' ELSE gc.CategoryName END as CategoryName, p.selling_price, p.grams_weight,\n" +
    "\tCASE WHEN imp.path IS NOT NULL \n" +
    "       THEN imp.path\n" +
    "       ELSE \n" +
    "       CASE WHEN iscp.path IS NOT NULL \n" +
    "       \t\tTHEN iscp.path\n" +
    "       \t\tELSE \n" +
    "       \t\tCASE WHEN iscpt.path IS NOT NULL \n" +
    "       \t\t\tTHEN iscpt.path\n" +
    "\t       \t\tELSE \n" +
    "\t       \t\tCASE WHEN isscp.path IS NOT NULL \n" +
    "       \t\t\t\tTHEN isscp.path\n" +
    "\t\t       \t\tELSE\n" +
    "\t\t       \t\tCASE WHEN issp.path IS NOT NULL \n" +
    "       \t\t\t\t\tTHEN issp.path\n" +
    "\t\t       \t\t\tELSE \n" +
    "\t\t       \t\t\tCASE WHEN isspd.path IS NOT NULL \n" +
    "\t\t\t\t\t       THEN isspd.path\n" +
    "       \t\t\t\t\t   ELSE 0\n" +
    "\t\t\t\t\t\tEND\n" +
    "\t\t\t\t\tEND \n" +
    "\t\t\t\tEND\n" +
    "\t\t\tEND\n" +
    "\t\tEND\n" +
    "\tEND as img_link\n" +
    "FROM products p\n" +
    "INNER JOIN category_product cp on p.id = cp.product_id \n" +
    "INNER JOIN categories c on c.id = cp.category_id\n" +
    "LEFT JOIN google_categories gc on c.google_category_id = gc.id\n" +
    "LEFT JOIN image_metal_products imp on p.id = imp.product_id\n" +
    "LEFT JOIN image_stone_color_products iscp on p.id = iscp.product_id\n" +
    "LEFT JOIN image_stone_ctw_products iscpt on p.id = iscpt.product_id\n" +
    "LEFT JOIN image_stone_shape_color_products isscp on p.id = isscp.product_id\n" +
    "LEFT JOIN image_stone_shape_products issp on p.id = issp.product_id\n" +
    "LEFT JOIN image_stone_side_products isspd on p.id = isspd.product_id\n" +
    "WHERE p.google = true\n" +
    "AND (p.google_sync <> 'S' OR p.google_sync is NULL)\n" +
    "GROUP BY 1\n" +
    "\n" +
    "\n" +
    "\n";

conexion.query(sql, function (error, results, fields) {
    if (error) throw error;
    results.forEach(async function (row, index) {
        //console.log(row);
        var pid = 'online:en:US:' + row.local_sku;
        content.products.get({merchantId: process.env.MERCHANT_ID, productId: pid},function(err, resp) {
            //console.log(err);
            //console.log(resp);

            if(err) {
                console.log("NO EXISTE");
                var product = {
                    'offerId':
                    row.local_sku,
                    'title':
                    row.name,
                    'description':
                    row.description,
                    'link':
                        'https://amcordesign.com/product/' + row.slug,
                    'imageLink':
                        'https://api.amcordesign.com\\upload\\product\\'+row.img_link,
                    'contentLanguage':
                        'en',
                    'targetCountry':
                        'US',
                    'channel':
                        'online',
                    'availability':
                        'in stock',
                    'condition':
                        'new',
                    'googleProductCategory':
                    row.CategoryName,
                    'price': {
                        'value': row.selling_price,
                        'currency': 'USD'
                    },
                    'shipping': [{
                        'country': 'US',
                        'service': 'Standard shipping',
                        'price': {
                            'value': '15',
                            'currency': 'USD'
                        }
                    }],
                    'shippingWeight': {
                        'value': row.grams_weight,
                        'unit': 'grams'
                    }
                };
                content.products.insert(
                    {
                        merchantId:process.env.MERCHANT_ID,
                        resource: product
                    },function(err, resp) {
                        // handle err and response
                        console.log(err);
                        console.log(resp);
                    });
                return true;
            }else{
                console.log("SI EXISTE");
                var product = {
                    'title':
                    row.name,
                    'description':
                    row.description,
                    'link':
                        'https://amcordesign.com/product/' + row.slug,
                    'imageLink':
                        'https://api.amcordesign.com\\upload\\product\\'+row.img_link,
                    'availability':
                        'in stock',
                    'condition':
                        'new',
                    'googleProductCategory':
                    row.CategoryName,
                    'price': {
                        'value': row.selling_price,
                        'currency': 'USD'
                    },
                    'shipping': [{
                        'country': 'US',
                        'service': 'Standard shipping',
                        'price': {
                            'value': '15',
                            'currency': 'USD'
                        }
                    }],
                    'shippingWeight': {
                        'value': row.grams_weight,
                        'unit': 'grams'
                    }
                };
                content.products.update(
                    {
                        merchantId:process.env.MERCHANT_ID,
                        productId: pid,
                        resource: product
                    },function(err, resp) {
                        // handle err and response
                        console.log(err);
                        console.log(resp);
                    });
                return true;
            }

        });
    });
});
//});

