var Client = require('ssh2').Client;
const fetch = require("node-fetch");
var mysql = require('mysql');
require ('custom-env').env('apis','../');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var conexion= mysql.createConnection({
    host : process.env.DB_HOST,
    database : process.env.DATABASE,
    port: process.env.DB_PORT,
    user : process.env.DB_USER,
    password : process.env.DB_PASS,
});

conexion.connect(function(err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});

//SFTP
var connSettings = {
    host: '13.72.97.254',
    port: 22, // Normal is 22 port
    username: 'userDiamondStart',
    password: 'password.Diamond_start'
    // You can use a key file too, read the ssh2 documentation
};
var remotePathToList = process.env.REMOTE_PATH_TO_LIST;

var conn = new Client();
conn.on('ready', function() {
    conn.sftp(function(err, sftp) {
        if (err) throw err;
        var results =[];
            sftp.readdir(remotePathToList, function(err, list) {
                if (err) throw err;
                // List the directory in the console
                list.forEach(function(item, index){

                    if(item.filename.search(/[.]/g) < 1){
                        console.log(item.filename);
                            sftp.readdir(remotePathToList+'/'+item.filename, function(err, list2) {
                                if (!err) {
                                    list2.forEach(function (file, index) {
                                        console.log("/"+item.filename+"/"+file.filename);
                                        var moveFrom = remotePathToList+"/"+item.filename+"/"+file.filename;
                                        var moveTo = "./"+item.filename+"/"+file.filename;

                                        const fs = require('fs');
                                        const path = require('path');

                                        fs.mkdir(path.join(__dirname, item.filename), (err) => {
                                            if (err) {
                                                return console.error(err);
                                            }
                                            console.log('Directory created successfully!');
                                        });

                                        sftp.fastGet(moveFrom, moveTo , {}, function(downloadError){
                                            if(downloadError) throw downloadError;

                                            console.log("Succesfully uploaded");
                                            const csvFilePath = moveTo;
                                            const csv=require('csvtojson')
                                            csv()
                                                .fromFile(csvFilePath)
                                                .then((jsonObj)=>{
                                                    //console.log(jsonObj);
                                                    //VALIDA SI AUN ESTA DISPONIBLE EL REGISTRO
                                                    var cadena = JSON.stringify(jsonObj);
                                                    let jsondata;
                                                    console.log(item.filename);
                                                    fetch(process.env.URL_SERVER + 'inventories?filter={"where":{"cod_proveedor":{"eq":"'+ item.filename +'"},"status":{"eq":"A"}}, "fields": {"id": true,"Stock_No": true,"cod_proveedor":true}}').then(
                                                        function(u){ return u.json();}
                                                    ).then(
                                                        function(json){
                                                            jsondata = json;
                                                            if(jsondata){
                                                                jsondata.forEach(async function (item2, index) {
                                                                    console.log(cadena.search(item2.Stock_No));
                                                                    if(cadena.search(item2.Stock_No) < 1){
                                                                        console.log("Ya no existe: " + item2.id);
                                                                        var sql = 'UPDATE inventory SET status = "I" WHERE id='+item2.id;
                                                                        console.log(sql);
                                                                        //CAMBIA EL ESTADO DEL REGISTRO QUE YA NO EXISTE A "I"
                                                                        conexion.query(sql, function (error, results, fields) {
                                                                            if (error)
                                                                                throw error;
                                                                            console.log(results);
                                                                        });


                                                                    }
                                                                });
                                                            }
                                                        }
                                                    )
                                                    //GUARDA LOS NUEVOS REGISTROS
                                                    jsonObj.forEach(function (row, index) {
                                                        row["cod_proveedor"] = item.filename;
                                                        row["status"] = 'A';
                                                        row["TABLE_PER"] = parseFloat(row["TABLE_PER"]);
                                                        row["DEPTH_PER"] = parseFloat(row["DEPTH_PER"]);
                                                        row["COD_Buy_Price_Total"] = parseFloat(row["COD_Buy_Price_Total"]);
                                                        row["Buy_Price_Total"] = parseFloat(row["Buy_Price_Total"]);
                                                        row["Memo_Price_total"] = parseFloat(row["Memo_Price_total"]);
                                                        row["Weight"] = parseFloat(row["Weight"]);
                                                        row["Buy_Price"] = parseFloat(row["Buy_Price"]);
                                                        row["COD_Buy_Price"] = parseFloat(row["COD_Buy_Price"]);
                                                        row["Memo_Price"] = parseFloat(row["Memo_Price"]);
                                                        console.log(row);
                                                        save(row);
                                                    });

                                                    async function save(row) {
                                                        await fetch(process.env.URL_SERVER + 'inventories', {
                                                            method: 'post',
                                                            headers: {
                                                                'Accept': 'application/json',
                                                                'Content-Type': 'application/json'
                                                            },
                                                            body: JSON.stringify(row).replace(/null/g,'""')
                                                        }).then(res => res.json())
                                                            .then(res => console.log(JSON.stringify(res)));
                                                    };

                                                })
                                        });
                                    });
                                }
                                //conn.end();
                            });
                    }

                });
                console.log(results);
                //conn.end();
            });
    });
}).connect(connSettings);



// Async / await usage
//const jsonArray= csv().fromFile(csvFilePath);
