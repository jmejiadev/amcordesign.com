var Request = require("request");
const fetch = require("node-fetch");
var mysql = require('mysql');
require ('custom-env').env('apis','../');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var conexion= mysql.createConnection({
    host : process.env.DB_HOST,
    database : process.env.DATABASE,
    port: process.env.DB_PORT,
    user : process.env.DB_USER,
    password : process.env.DB_PASS,
});

conexion.connect(function(err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + conexion.threadId);
});

Request.get("https://belgiumny.com/api/DeveloperAPI?stock=&APIKEY=" + process.env.API_KEY_BELGIUMNY, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    var response = JSON.parse(body);
    var data = response['Stock'];
    var cadena = JSON.stringify(data);

    var existentes;

    //VALIDA SI AUN ESTA DISPONIBLE EL REGISTRO
    let jsondata;
    fetch(process.env.URL_SERVER + 'inventories?filter={"where":{"cod_proveedor":{"eq":"belgiumnyApi"},"status":{"eq":"A"}}, "fields": {"id": true,"Stock_No": true,"cod_proveedor":true}}').then(
        function(u){ return u.json();}
    ).then(
        function(json){
            jsondata = json;
            if(jsondata){
                jsondata.forEach(async function (item, index) {
                    console.log(cadena.search(item.Stock_No));
                    if(cadena.search(item.Stock_No) < 1){
                        console.log("Ya no existe: " + item.id);
                        var sql = 'UPDATE inventory SET status = "I" WHERE id='+item.id;
                        console.log(sql);
                        //CAMBIA EL ESTADO DEL REGISTRO QUE YA NO EXISTE A "I"
                        conexion.query(sql, function (error, results, fields) {
                            if (error)
                                throw error;
                            console.log(results);
                        });


                    }
                });
            }
        }
    )

    //GUARDA LOS NUEVOS REGISTROS
    data.forEach(function (item, index) {
        item["cod_proveedor"] = "belgiumnyApi";
        item["status"] = 'A';
        save(item);
    });

    async function save(item) {
        await fetch(process.env.URL_SERVER + 'inventories', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item).replace(/null/g,'""')
        }).then(res => res.json())
            .then(res => console.log(res));
    };
    //connection.end();
});
