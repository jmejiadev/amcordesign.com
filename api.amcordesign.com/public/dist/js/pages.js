function updatePage(str) {
  var content = document.getElementById('main_content');
  var tittle = document.getElementById('dash-tittle');

  fetch('/'+str)
    .then(response => response.json())
    .then(data => showInfo(data));
  tittle.innerText = str;

}

function showInfo( data ) {
  console.log(data);
  content.innerHTML = JSON.stringify(data);
}

function loadXMLDoc(theURL)
{
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari, SeaMonkey
    xmlhttp=new XMLHttpRequest();
  }
  else
  {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      content.innerHTML = xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET", theURL, false);
  xmlhttp.send();
}
