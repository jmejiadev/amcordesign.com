
if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}
const dbUrl = process.env.DATABASE_URL;
const secretSession = process.env.SESSION_SECRET;

const express = require('express');
const session = require('express-session');
var bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const path = require('path');

app.set('view engine', 'ejs');
app.use(express.static('views'));

require('./routes/routes')(app);

app.use(express.static(path.join(__dirname, "public")));

TZ = "America/Guayaquil";
app.listen(8080);

