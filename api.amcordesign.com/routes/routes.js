const fetch = require('node-fetch');
const https = require('https');
const path = require('path');
const md5 = require('md5');
const dateFormat = require('dateformat');
const randomstring = require('randomstring');
var fs = require('fs');

var replaceall = require("replaceall");

var AdmZip = require('adm-zip');

let busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');


const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
});

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}
const dbUrl = process.env.DATABASE_URL;
const uploadUrl = process.env.CDN_URL + process.env.UPLOAD_URL;
const recoverUrl = process.env.RECOVER_URL;
const secretSession = process.env.SESSION_SECRET;
const emailSmtp = process.env.EMAIL_SMTP;
const emailPort = process.env.EMAIL_PORT;
const emailUser = process.env.EMAIL_USER;
const emailPw = process.env.EMAIL_PW;
const emailService = process.env.EMAIL_SERVICE;

const session = require('express-session');
const { get } = require('http');

const nodemailer = require('nodemailer');
const e = require('express');
//console.log(emailUser);
const transporter = nodemailer.createTransport({
    host: emailSmtp,
    port: emailPort,
    secure: false,
    logger: true,
    debug: true,
    ignoreTLS: false, // add this 
    service: emailService,

    // service: emailService,
    auth: {
        user: emailUser,
        pass: emailPw,
    },
});
//console.log(transporter);

module.exports = (app) => {

    app.use(busboy());
    var expiryDate = new Date(Date.now() + 60 * 60 * 1000); // 1 hour
    app.use(session({
        name: 'session',
        secret: secretSession,
        resave: true,
        saveUninitialized: true
    }));

    app.use(function (req, res, next) {
        res.locals.user = req.session.user;
        next();
    });

    app.get("/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let clients = 0;
        let orders = 0;


        let ans = fetch(dbUrl + '/clients/count', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                clients = data;
            });

        ans = fetch(dbUrl + '/orders/count', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                orders = data;
                req.session.ordersCount = orders.count;
            });

        const response = fetch(dbUrl + '/subscribes/count', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('index', {
                    title: "AMCOR DESIGN ADMIN | Dashboard",
                    option: "Dashboard",
                    constumersCount: clients.count,
                    ordersCount: orders.count,
                    subscriptionCount: data.count
                })
            });
    });

    //home shop
    app.get("/Home%20Shop/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const msg = req.params.msg;
        const response = fetch(dbUrl + '/home-shops', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('homeShop/index', {
                    title: "AMCOR DESIGN ADMIN | Home Shop",
                    option: "Home Shop",
                    msg: msg,
                    data: data,
                })
            })
    });
    app.post("/homeShop/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        let id = req.params.Id;
        json.updatedAt = now;
        json.path = uploadUrl + "home/" + json.path;
        sendReqNoRedirect('PUT', 'home-shops/', id, json);
        res.send({ response: "The image has been uploaded" });
    });

    app.post("/homeShop", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.updatedAt = now;
        json.createdAt = now;
        json.path = uploadUrl + "home/" + json.path;
        sendReqNoRedirect('POST', 'home-shops/', "", json);
        res.send({ response: "The image has been uploaded", url: "/Home%20Shop" });
    });
    app.get("/deleteHomePicture/:Id", (req, res) => {
        let id = req.params.Id;
        let response = fetch(dbUrl + '/home-shop-products?filter={"where": {"homeShopId": ' + req.params.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                const deleteMethod = {
                    method: 'DELETE', // Method itself
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                    },
                    agent: httpsAgent,
                };
                if (data.length == 0) {
                    fetch(dbUrl + '/home-shops/' + id, deleteMethod)
                        .then(response => response.json())
                        .then(data => {
                            res.redirect("/Home%20Shop/view");
                        })
                        .catch(err => {
                            res.redirect("/Home%20Shop/view");
                        })
                } else {
                    res.redirect("/Home%20Shop/error");
                }
            });
    });

    app.get("/Home%20Shop%20Products/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let json = {};
        let response = fetch(dbUrl + '/home-shop-products', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json.homeProducts = data;
                response = fetch(dbUrl + '/products', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.products = data;
                        response = fetch(dbUrl + '/metals', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                json.metals = data;
                                response = fetch(dbUrl + '/stone-shapes', {
                                    method: 'GET',
                                    agent: httpsAgent,
                                });
                                response.then(response => response.json())
                                    .then(data => {
                                        json.stoneShapes = data;
                                        response = fetch(dbUrl + '/stone-centers', {
                                            method: 'GET',
                                            agent: httpsAgent,
                                        });
                                        response.then(response => response.json())
                                            .then(data => {
                                                json.stoneCenters = data;
                                                response = fetch(dbUrl + '/stone-sides', {
                                                    method: 'GET',
                                                    agent: httpsAgent,
                                                });
                                                response.then(response => response.json())
                                                    .then(data => {
                                                        json.stoneSides = data;
                                                        response = fetch(dbUrl + '/stone-colors', {
                                                            method: 'GET',
                                                            agent: httpsAgent,
                                                        });
                                                        response.then(response => response.json())
                                                            .then(data => {
                                                                json.stoneColors = data;
                                                                response = fetch(dbUrl + '/home-shops', {
                                                                    method: 'GET',
                                                                    agent: httpsAgent,
                                                                });
                                                                response.then(response => response.json())
                                                                    .then(data => {
                                                                        json.homeShops = data;
                                                                        res.render('homeShopProducts/index', {
                                                                            title: "AMCOR DESIGN ADMIN | Home Shop Products",
                                                                            option: "Home Shop Products",
                                                                            data: json,
                                                                        })
                                                                    })
                                                            })
                                                    })
                                            })
                                    })
                            })
                    })
            })
    });
    app.get("/deleteHomeProductsPicture/:Id", (req, res) => {
        let id = req.params.Id;
        deleteElement("/home-shop-products", "Home%20Shop%20Products", id, res)
    });

    app.post("/homeShopProducts/:Id", (req, res) => {
        const now = new Date().toISOString();
        let id = req.params.Id;
        let json = req.body;
        json.updatedAt = now;
        const arr = json.homeShopId.split("---");
        json.homeShopId = Number(arr[0]);
        json.productId = Number(json.productId);
        json.metalId = Number(json.metalId);
        json.positionX = Number(json.positionX);
        json.positionY = Number(json.positionY);

        json.stoneShapeId == 'null' || json.stoneShapeId == '' ? delete json.stoneShapeId : json.stoneShapeId = Number(json.stoneShapeId);
        json.stoneCenterId == 'null' || json.stoneCenterId == '' ? delete json.stoneCenterId : json.stoneCenterId = Number(json.stoneCenterId);
        json.stoneSideId == 'null' || json.stoneSideId == '' ? delete json.stoneSideId : json.stoneSideId = Number(json.stoneSideId);
        json.stoneColorId == 'null' || json.stoneColorId == '' ? delete json.stoneColorId : json.stoneColorId = Number(json.stoneColorId);

        if (json.path != "noUpdateImg") {
            json.path ? json.path = uploadUrl + "homeProducts/" + json.path : delete json.path;
            sendReqNoRedirect('PUT', 'home-shop-products/', id, json);
            res.send({ response: "The image has been uploaded." });
        } else {
            const response = fetch(dbUrl + '/home-shop-products/' + id, {
                method: 'GET',
                agent: httpsAgent,
            });
            response.then(response => response.json())
                .then(data => {
                    json.path ? json.path = data.path : delete json.path;
                    sendReqNoRedirect('PUT', 'home-shop-products/', id, json);
                    res.send({ response: "The item has been updated" });
                })
        }
    });

    app.post("/homeShopProducts", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.updatedAt = now;
        json.createdAt = now;
        var arr = json.homeShopId.split("---");
        json.homeShopId = Number(arr[0]);
        json.productId = Number(json.productId);
        json.metalId = Number(json.metalId);
        json.positionX = Number(json.positionX);
        json.positionY = Number(json.positionY);

        json.stoneShapeId == 'null' || json.stoneShapeId == null ? delete json.stoneShapeId : json.stoneShapeId = Number(json.stoneShapeId);
        json.stoneCenterId == 'null' || json.stoneCenterId == null ? delete json.stoneCenterId : json.stoneCenterId = Number(json.stoneCenterId);
        json.stoneSideId == 'null' || json.stoneSideId == null ? delete json.stoneSideId : json.stoneSideId = Number(json.stoneSideId);
        json.stoneColorId == 'null' || json.stoneColorId == null ? delete json.stoneColorId : json.stoneColorId = Number(json.stoneColorId);
        json.path ? json.path = uploadUrl + "homeProducts/" + json.path : delete json.path;
        console.log(JSON.stringify(json));
        sendReq("POST", 'home-shop-products/', 'Home%20Shop%20Products', '', json, res);

    });

    //users
    app.get("/users/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/users', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('users/index', {
                    title: "AMCOR DESIGN ADMIN | Users",
                    option: "Users",
                    users: data,
                })
            })
    });
    app.get("/users/create", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/users', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('users/create', {
                    title: "AMCOR DESIGN ADMIN | Users",
                    option: "Create Users",
                    users: data
                })
            })
    });
    app.get("/editUser/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/users/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('users/edit', {
                    title: "AMCOR DESIGN ADMIN | Users",
                    option: "Edit Users",
                    view: "users",
                    subtittle: "Users",
                    data: data
                })
            })
    });
    app.post("/editUser/:Id", (req, res) => {
        const now = new Date().toISOString();
        var sha1 = require('sha1');
        let json = req.body;
        json.password = sha1(json.password);
        json.updatedAt = now;
        delete json.passwordConfirmation;
        sendReq("PUT", 'users/', 'users', req.params.Id, json, res);
    });
    app.post('/newUser', (req, res) => {
        let json = req.body;
        if (json.passwordConfirmation == json.password) {
            const now = new Date().toISOString();
            json.createdAt = now;
            var sha1 = require('sha1');
            json.password = sha1(json.password);
            delete json.passwordConfirmation;
            const response = fetch(dbUrl + '/users', {
                method: 'POST',
                agent: httpsAgent,
                headers: {
                    'Content-Type': 'application/json' // Indicates the content
                },
                body: JSON.stringify(json),
            });
            response.then(response => response.json())
                .then(data => {
                    res.redirect('/users');
                })
                .catch(err => {
                })
        } else {
            res.redirect('/users/create');
        }

    });
    app.get("/deleteUser/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        deleteElement("/users", "users", req.params.Id, res);
    });

    //clientes
    app.get("/customers/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/clients', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('customers/index', {
                    title: "AMCOR DESIGN ADMIN | Customers",
                    option: "Customers",
                    clients: data,
                })
            })
    });
    app.get("/copyCustomer/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const id = req.params.Id;
        const now = new Date().toISOString();
        const response = fetch(dbUrl + '/clients/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                const idNew = data.id;
                delete data.id;
                delete data.updatedAt;
                delete data.rememberToken;
                delete data.emailVerifiedAt
                data.createdAt = now;
                data.email = "CLONE_" + data.email;
                const response = fetch(dbUrl + "/clients", {
                    method: "POST",
                    agent: httpsAgent,
                    headers: {
                        'Content-Type': 'application/json' // Indicates the content
                    },
                    body: JSON.stringify(data),
                });
                response.then(response => response.json())
                    .then(data => {
                        res.redirect('/customers');
                    })
            })
    });
    app.get("/editCustomer/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/clients/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('customers/edit', {
                    title: "AMCOR DESIGN ADMIN | Customer",
                    option: "Edit Customer",
                    view: "customers",
                    subtittle: "Customer",
                    data: data
                })
            })
    });
    app.post("/editCustomer/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const id = req.params.Id;
        const now = new Date().toISOString();
        let json = req.body;
        json.updatedAt = now;
        // completar edit customer
    });
    app.get("/deleteCustomers/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        deleteElement("/clients", "customers", req.params.Id, res);
    });

    //countries
    app.get("/countries/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const msg = req.params.msg;
        if (msg == "create") {
            res.render('countries/create', {
                title: "AMCOR DESIGN ADMIN | Countries",
                option: "Country",
                view: "countries",
                subtittle: "Countries",
            })
        }
        const response = fetch(dbUrl + '/countries', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('countries/index', {
                    title: "AMCOR DESIGN ADMIN | Countries",
                    option: "Countries",
                    msg: msg,
                    countries: data,
                })
            })

    });

    app.get("/editCountries/:countryId", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/countries/', 'countries', req.params.countryId, 'Countries', 'Country', res);
    });

    app.post("/editCountries/:countryId", (req, res) => {
        const now = new Date().toISOString();
        const json = {
            name: req.body.name,
            currency: req.body.currency,
            createdAt: req.body.createdAt,
            updatedAt: now,
        };
        sendReqNoRedirect('PUT', 'countries/', req.params.countryId, json);
        res.redirect("/countries/all");
    });

    app.post('/newCountry', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        const response = fetch(dbUrl + '/countries', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                //console.log(JSON.stringify(json));
                //console.log(data);
                res.redirect('/countries/all');
            })
            .catch(err => {
                //console.log(err);
            }) // Do something with the error
    });
    app.get("/deleteCountry/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/states?filter={"where":{"countryId":' + req.params.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            }
        });
        response.then(response => response.json())
            .then(data => {
                console.log(data);
                if (data.length == 0) {
                    deleteElement("/countries", "countries/all", req.params.Id, res);
                    res.redirect('/countries/all');
                } else {
                    res.redirect('/countries/error');
                }
            })


    });
    //states
    app.get("/states/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let countries = [];
        const msg = req.params.msg;
        const ans = fetch(dbUrl + '/countries', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                countries = data;
            });
        const response = fetch(dbUrl + '/states', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                for (var i = 0; i < countries.length; i++) {
                    for (var j = 0; j < data.length; j++) {
                        if (countries[i].id == data[j].countryId) {
                            data[j].countryId = countries[i];
                            continue;
                        }
                    }
                }
                res.render('states/index', {
                    title: "AMCOR DESIGN ADMIN | States",
                    option: "States",
                    msg: msg,
                    data: data
                })
            })
    });
    app.get("/addStates", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const ans = fetch(dbUrl + '/countries', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                res.render('states/create', {
                    title: "AMCOR DESIGN ADMIN | States",
                    option: "Create States",
                    data: data,
                    view: "states",
                    subtittle: "States",
                })
            });
    });

    app.post('/newState', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        json.countryId = Number(json.countryId);
        const response = fetch(dbUrl + '/states', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                res.redirect('/states/all');
            })
    });

    app.get("/editState/:stateId", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let countries = [];
        const ans = fetch(dbUrl + '/countries', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                countries = data;
            });

        const response = fetch(dbUrl + '/states/' + req.params.stateId, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('states/edit', {
                    title: "AMCOR DESIGN ADMIN | States",
                    option: "Edit State",
                    data: data,
                    countries: countries,
                    view: "states",
                    subtittle: "States",
                })
            })
    });

    app.post("/editState/:stateId", (req, res) => {
        const now = new Date().toISOString();
        const json = {
            name: req.body.name,
            countryId: Number(req.body.countryId),
            createdAt: req.body.createdAt,
            updatedAt: now,
        };
        const response = fetch(dbUrl + '/states/' + req.params.stateId, {
            method: 'PUT',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                res.redirect('/states/all');
            }).catch(data => {
                res.redirect('/states/all');
            })

    });
    app.get("/deleteState/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/cities?filter={"where":{"stateId":' + req.params.stateId + '}}/', {
            method: 'GET',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            }
        });
        response.then(response => response.json())
            .then(data => {
                if (data.length == 0) {
                    deleteElement("/states", "states/all", req.params.Id, res);
                    res.redirect('/states/all');
                } else {
                    res.redirect('/states/error');
                }
            })
    });
    //Google
    app.get("/google-products/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/products?filter=%7B%22where%22%3A%20%7B%22google%22%3A%20%7B%22neq%22%3A%20%22false%22%7D%7D%7D', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('googleShopping/index', {
                    title: "AMCOR DESIGN ADMIN | Google Products",
                    option: "All Google Products",
                    data: data
                })
            })
    });
    app.get("/sendGoogle/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/sincronizar/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        let mensaje = req.params.Id == "all" ? "All the products have been sent to google shopping" : "Product has been sent to google shopping";
        response.then(response => response.json())
            .then(data => {
                res.redirect('/google-products?mensaje=' + mensaje);
            })
    });
    //CreateOrder
    app.get("/createOrder", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/products', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                let products = data;
                response = fetch(dbUrl + '/inventories?filter={"where":{"status":1}}', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        res.render('orders/create', {
                            title: "AMCOR DESIGN ADMIN | Create order",
                            option: "Create order",
                            products: products,
                            inventory: data
                        });
                    });
            });
    });

    app.post("/createOrder", (req, res) => {
        let body = req.body;
        const now = new Date().toISOString();
        body.clientIp = req.socket.remoteAddress;
        body.createdAt = now;
        body.updatedAt = now;
        body.orderStatus = "create";
        body.paymentType = "Credit Card";
        body.tax = Number(body.tax);
        body.total = body.grandTotal;
        let products = body.products;
        let productName = body.productName;
        let personalization = body.personalization;
        let grams = body.grams;
        let supplierSku = body.supplierSku;
        let stoneColor = body.stoneColor;
        let metal = body.metal;
        let gemstoneId = body.gemstoneId;
        let stoneSide = body.stoneSide;
        let typeSizes = body.typeSizes;
        let typeSizeValue = body.typeSizeValue;
        let variations = body.variations;
        let subtotal = body.subtotal;
        let quantity = body.quantity;
        let discount = body.discount;
        let trackingJson = {
            createdAt: now,
            departureDate: body.departureDate,
            departureTime: body.departureTime,
            name: body.name,
            number: body.number,
            orderId: 0, //This value is gonna change in the future
            updatedAt: now,
            url: body.url,
        };
        let paypalOrder = {
            createTime: now,
            detail: body.detail,
            orderId: 0, //This value is gonna change in the future
            paypalId: body.paypalId,
            updateTime: now
        };
        /*
        console.log(body);
        console.log(typeSizes[0]);
        console.log(typeSizeValue[0]);
return 0;
 */
        console.log(body);
        delete body.grandTotal;
        delete body.products;
        delete body.productName;
        delete body.personalization;
        delete body.name;
        delete body.grams;
        delete body.supplierSku;
        delete body.stoneColor;
        delete body.metal; //name ultima position
        delete body.gemstoneId; //arrat
        delete body.stoneSide; //name ultima position
        delete body.typeSizes;
        delete body.typeSizeValue;
        delete body.variations;
        delete body.subtotal;
        delete body.quantity;
        delete body.discount;
        delete body.departureDate;
        delete body.departureTime;
        delete body.name;
        delete body.number;
        delete body.url;

        delete body.detail;
        delete body.paypalId;

        let response = fetch(dbUrl + '/get-order-id/', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                body.orderNumber = data[0].orderNumber;
                console.log(JSON.stringify(body));
                response = fetch(dbUrl + '/orders', {
                    method: 'POST',
                    agent: httpsAgent,
                    headers: {
                        'Content-Type': 'application/json' // Indicates the content
                    },
                    body: JSON.stringify(body),
                });
                response.then(response => response.json())
                    .then(data => {
                        console.log("");
                        console.log(data);
                        var orderId = Number(data.id);
                        trackingJson.orderId = orderId;
                        sendReqNoRedirect('POST', 'trackings/', '', trackingJson);
                        paypalOrder.orderId = orderId;
                        if (paypalOrder.paypalId != undefined && paypalOrder.paypalId != "") {
                            sendReqNoRedirect('POST', 'order-paypals/', '', paypalOrder);
                        }
                        products.forEach((element, index) => {
                            let jsonProduct = {
                                discount: Number(discount[index]),
                                grams: grams[index],
                                name: productName[index],
                                orderId: orderId,
                                personalization: personalization[index],
                                productId: Number(element),
                                quantity: Number(quantity[index]),
                                subtotal: Number(subtotal[index]),
                                supplierSku: supplierSku[index],
                                createdAt: now,
                                updatedAt: now
                            }
                            if (metal != undefined) {
                                if (metal[index] != "") {
                                    //                                   console.log("metal index");
                                    //                                   console.log (metal[index]);
                                    var metalInfo = metal[index].split('-');
                                    jsonProduct.metalName = metalInfo[2];
                                    jsonProduct.sellingPrice = Number(metalInfo[1]);
                                    jsonProduct.metalId = Number(metalInfo[0]);
                                }
                            }
                            if (stoneColor != undefined) {
                                if (stoneColor[index] != "") {
                                    var stoneColorInfo = stoneColor[index].split('-');
                                    jsonProduct.stoneColorId = Number(stoneColorInfo[0]);
                                    jsonProduct.stoneColorName = stoneColorInfo[3] + " Carat: " + stoneColorInfo[2];
                                    jsonProduct.stoneNumber = Number(stoneColorInfo[1]);
                                }
                            }
                            if (stoneSide != undefined) {
                                if (stoneSide[index] != "") {
                                    var stoneSideInfo = stoneSide[index].split('-');
                                    jsonProduct.stoneSideId = Number(stoneSideInfo[0]);
                                    jsonProduct.stoneSideName = stoneSideInfo[2] + " Carat: " + stoneColorInfo[1];
                                }
                            }
                            if (gemstoneId != undefined) {
                                if (gemstoneId[index] != "") {
                                    jsonProduct.gemstoneId = Number(gemstoneId[index]);
                                }
                            }
                            console.log("");
                            console.log(jsonProduct);
                            response = fetch(dbUrl + '/order-products', {
                                method: 'POST',
                                agent: httpsAgent,
                                headers: {
                                    'Content-Type': 'application/json' // Indicates the content
                                },
                                body: JSON.stringify(jsonProduct),
                            });
                            response.then(response => response.json())
                                .then(data => {
                                    console.log(data);
                                    let orderProductId = Number(data.id);
                                    if (typeSizes != undefined) {
                                        typeSizes.forEach((elem, i) => {
                                            array = elem.split('-');
                                            if (Number(element) == Number(array[2])) {
                                                let jsonOrderProductSizes = {
                                                    createdAt: now,
                                                    orderProductId: orderProductId,
                                                    sizeNumber: typeSizeValue[i],
                                                    typeSizeId: Number(array[0]),
                                                    updatedAt: now
                                                }
                                                sendReqNoRedirect('POST', 'order-products-sizes/', '', jsonOrderProductSizes);
                                            }
                                        });
                                    }
                                    orderProductId = Number(data.id);
                                    console.log(JSON.stringify(orderProductId));
                                    if (variations != undefined) {
                                        variations.forEach(elem => {
                                            array = elem.split('-');
                                            if (Number(element) == Number(array[4])) {
                                                let jsonVariations = {
                                                    createdAt: now,
                                                    updatedAt: now,
                                                    extraVariationId: Number(array[0]),
                                                    extraVariationName: array[1],
                                                    orderProductId: orderProductId,
                                                    variationId: Number(array[2]),
                                                    variationName: array[3]
                                                };
                                                console.log("");
                                                console.log(JSON.stringify(jsonVariations));
                                                sendReqNoRedirect('POST', 'order-products-variants/', '', jsonVariations);
                                            }
                                        });
                                    }
                                });
                        });
                    })
            });

        res.redirect('/orders');
    });

    //Orders
    app.get("/orders", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/orders', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                for (var i = 0; i < data.length; i++) {
                    data[i].sent = calcDate(new Date(), new Date(data[i].updatedAt));
                    data[i].total = parseInt(data[i].total).toFixed(2);
                }
                res.render('orders/index', {
                    title: "AMCOR DESIGN ADMIN | Order Guests",
                    option: "All orders",
                    data: data,
                })
            });
    });
    app.get("/editOrders/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/get-full-order-info/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                var orderInfo = data;
                var grandSubtotal = 0;
                var grandTotal = 0;
                data.orderProducts.forEach(element => {
                    element.total = (parseFloat(element.subtotal).toFixed(2) * element.quantity) * (1 - (parseFloat(element.discount).toFixed(2) / 100));
                    element.total = element.total;
                    grandTotal += element.total;
                    grandSubtotal += (parseFloat(element.subtotal).toFixed(2) * element.quantity);
                    element.total = parseFloat(element.total).toFixed(2);
                });
                data.order.grandSubtotal = grandTotal;
                data.order.grandDiscount = (1 - ((grandTotal) / (grandSubtotal))) * 100;
                data.order.grandDiscount = parseFloat(data.order.grandDiscount).toFixed(2);
                response = fetch(dbUrl + '/products', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        let products = data;
                        response = fetch(dbUrl + '/inventories?filter={"where":{"status":1}}', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                let invetory = data;
                                response = fetch(dbUrl + '/metals', {
                                    method: 'GET',
                                    agent: httpsAgent,
                                });
                                response.then(response => response.json())
                                    .then(data => {
                                        let metals = data;
                                        response = fetch(dbUrl + '/get-products-stone-color', {
                                            method: 'GET',
                                            agent: httpsAgent,
                                        });
                                        response.then(response => response.json())
                                            .then(data => {
                                                let stoneColors = data;
                                                response = fetch(dbUrl + '/get-products-stone-sides', {
                                                    method: 'GET',
                                                    agent: httpsAgent,
                                                });
                                                response.then(response => response.json())
                                                    .then(data => {
                                                        let stoneSides = data;
                                                        //console.log(data);
                                                        res.render('orders/edit', {
                                                            title: "AMCOR DESIGN ADMIN | Orders",
                                                            option: "Orders",
                                                            products: products,
                                                            inventory: invetory,
                                                            metals: metals,
                                                            stoneColors: stoneColors,
                                                            stoneSides: stoneSides,
                                                            data: orderInfo
                                                        });
                                                    });
                                            });
                                    });
                            });
                    });
            })
    });
    app.post("/updateCustomerOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        const response = fetch(dbUrl + '/orders/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                data.updatedAt = now;
                delete data.id;
                delete data.personId;
                data.firstName = body.firstName;
                data.lastName = body.lastName;
                data.email = body.email;
                data.phone = body.phone;
                sendReqNoRedirect('PUT', '/orders/', id, data);
                res.send({
                    response: "The customer information has been updated."
                });
            });
    });
    app.post("/updateShippingAddressOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        const response = fetch(dbUrl + '/orders/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                data.updatedAt = now;
                delete data.id;
                delete data.personId;
                data.shippingCountry = body.shippingCountry;
                data.shippingState = body.shippingState;
                data.shippingCity = body.shippingCity;
                data.shippingAddressLine1 = body.shippingAddressLine1;
                data.shippingAddressLine2 = body.shippingAddressLine2;
                data.shippingZipCode = body.shippingZipCode;
                sendReqNoRedirect('PUT', '/orders/', id, data);
                res.send({
                    response: "The shipping information has been updated."
                });
            });
    });
    app.post("/updateBillingAddressOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        const response = fetch(dbUrl + '/orders/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                data.updatedAt = now;
                delete data.id;
                delete data.personId;
                data.billingCountry = body.billingCountry;
                data.billingState = body.billingState;
                data.billingCity = body.billingCity;
                data.billingAddressLine1 = body.billingAddressLine1;
                data.billingAddressLine2 = body.billingAddressLine2;
                data.billingZipCode = body.billingZipCode;
                sendReqNoRedirect('PUT', '/orders/', id, data);
                res.send({
                    response: "The billing information has been updated."
                });
            });
    });
    app.post("/updateNotesOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        const response = fetch(dbUrl + '/orders/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                data.updatedAt = now;
                delete data.id;
                delete data.personId;
                data.notes = body.notes;
                sendReqNoRedirect('PUT', '/orders/', id, data);
                res.send({
                    response: "The notes of the order has been updated."
                });
            });
    });

    app.post("/updateProductsOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const orderId = Number(req.params.Id);
        const body = req.body;
        const deleteMethod = {
            method: 'GET', // Method itself
            headers: {
                'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
            },
            agent: httpsAgent,
        };
        console.log(body);
        fetch(dbUrl + '/del-orders-related/' + orderId, deleteMethod)
            .then(response => response.json())
            .then(data => {
                body.products.forEach((element, index) => {
                    let jsonProduct = {
                        discount: Number(body.discount[index]),
                        grams: body.grams[index],
                        name: body.productName[index],
                        orderId: orderId,
                        personalization: body.personalization[index],
                        productId: Number(element),
                        quantity: Number(body.quantity[index]),
                        subtotal: Number(body.subtotal[index]),
                        supplierSku: body.supplierSku[index],
                        createdAt: now,
                        updatedAt: now
                    }
                    if (body.metal != undefined) {
                        if (body.metal[index] != "") {
                            var metalInfo = body.metal[index].split('-');
                            if (metalInfo[0] != "") {
                                jsonProduct.metalName = metalInfo[2];
                                jsonProduct.sellingPrice = Number(metalInfo[1]);
                                jsonProduct.metalId = Number(metalInfo[0]);
                            }
                        }
                    }
                    if (body.stoneColor != undefined) {
                        if (body.stoneColor[index] != "") {
                            var stoneColorInfo = body.stoneColor[index].split('-');
                            if (stoneColorInfo[0] != "") {
                                if (stoneColorInfo[1] != " ") {
                                    jsonProduct.stoneColorName = stoneColorInfo[3] + " Carat: " + stoneColorInfo[1];
                                } else {
                                    jsonProduct.stoneColorName = stoneColorInfo[3];
                                }
                                jsonProduct.stoneColorId = Number(stoneColorInfo[0]);
                                jsonProduct.stoneNumber = Number(stoneColorInfo[1]);
                            }
                        }
                    }
                    if (body.stoneSide != undefined) {
                        if (body.stoneSide[index] != "") {
                            var stoneSideInfo = body.stoneSide[index].split('-');
                            if (stoneSideInfo[0] != "") {
                                if (stoneSideInfo[2] != " ") {
                                    jsonProduct.stoneSideName = stoneSideInfo[2] + " Carat: " + stoneSideInfo[1];
                                } else {
                                    jsonProduct.stoneSideName = stoneSideInfo[2];
                                }
                                jsonProduct.stoneSideId = Number(stoneSideInfo[0]);
                            }
                        }
                    }
                    if (body.gemstoneId != undefined) {
                        if (body.gemstoneId[index] != "") {
                            jsonProduct.gemstoneId = Number(body.gemstoneId[index]);
                        }
                    }
                    let response = fetch(dbUrl + '/order-products', {
                        method: 'POST',
                        agent: httpsAgent,
                        headers: {
                            'Content-Type': 'application/json' // Indicates the content
                        },
                        body: JSON.stringify(jsonProduct),
                    });
                    response.then(response => response.json())
                        .then(data => {
                            let orderProductId = Number(data.id);
                            if (body.typeSizes != undefined) {
                                body.typeSizes.forEach((elem, i) => {
                                    array = elem.split('-');
                                    if (Number(element) == Number(array[2])) {
                                        let jsonOrderProductSizes = {
                                            createdAt: now,
                                            orderProductId: orderProductId,
                                            sizeNumber: body.typeSizeValue[i],
                                            typeSizeId: Number(array[0]),
                                            updatedAt: now
                                        }
                                        sendReqNoRedirect('POST', 'order-products-sizes/', '', jsonOrderProductSizes);
                                    }
                                });
                            }
                            orderProductId = Number(data.id);
                            if (body.variations != undefined) {
                                body.variations.forEach(elem => {
                                    array = elem.split('-');
                                    if (Number(element) == Number(array[4])) {
                                        let jsonVariations = {
                                            createdAt: now,
                                            updatedAt: now,
                                            extraVariationId: Number(array[0]),
                                            extraVariationName: array[1],
                                            orderProductId: orderProductId,
                                            variationId: Number(array[2]),
                                            variationName: array[3]
                                        };
                                        sendReqNoRedirect('POST', 'order-products-variants/', '', jsonVariations);
                                    }
                                });
                            }
                        });
                })

            });
        const response = fetch(dbUrl + '/orders/' + orderId, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                delete data.id;
                data.updatedAt = now;
                data.total = body.grandTotal;
                data.tax = Number(body.tax);
                delete data.personId;
                console.log(data);
                sendReqNoRedirect('PUT', '/orders/', orderId, data);
                res.send({
                    response: "The products of the order have been updated."
                });
            });


    });

    app.post("/updateTracking/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        const response = fetch(dbUrl + '/trackings/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                data.updatedAt = now;
                delete data.id;
                data.departureDate = body.departureDate;
                data.departureTime = body.departureTime;
                data.name = body.name;
                data.number = body.number;
                data.url = body.url;
                sendReqNoRedirect('PUT', '/trackings/', id, data);
                res.send({
                    response: "The tracking information has been updated."
                });
            });
    });
    app.post("/addTrackingOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        body.createdAt = now;
        body.updatedAt = now;
        body.orderId = Number(id);
        sendReqNoRedirect('POST', '/trackings', '', body);
        res.send({
            response: "The paypal information has been added.<br/> The page will reload."
        });
    });

    app.post("/updatePaypal/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        const response = fetch(dbUrl + '/order-paypals/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                data.updateTime = now;
                data.detail = body.detail;
                data.paypalId = body.paypalId;
                delete data.id;
                sendReqNoRedirect('PUT', '/order-paypals/', id, data);
                res.send({
                    response: "The paypal information has been updated."
                });
            });
    });
    app.post("/addPaypalOrder/:Id", (req, res) => {
        const now = new Date().toISOString();
        const id = req.params.Id;
        const body = req.body;
        body.updateTime = now;
        body.createTime = now;
        body.orderId = Number(id);
        sendReqNoRedirect('POST', '/order-paypals', '', body);
        res.send({
            response: "The paypal information has been added.<br/> The page will reload."
        });
    });

    app.post("/editOrders/:Id", (req, res) => {
        let json = req.body;
        let response = fetch(dbUrl + '/orders/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                let order = data;
                order.notes = json.notes;
                if (order.shippingAddressLine2 == null) {
                    delete order.shippingAddressLine2;
                }
                if (order.billingAddressLine2 == null) {
                    delete order.billingAddressLine2;
                }
                if (order.personId == null) {
                    delete order.personId;
                }
                sendReqNoRedirect('PUT', 'orders/', req.params.Id, order);
                response = fetch(dbUrl + '/trackings?filter={"where":{"orderId":' + req.params.Id + '}}', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        const now = new Date().toISOString();
                        let trackingJson = {
                            createdAt: json.createdAt ? json.createdAt : now,
                            updatedAt: now,
                            name: json.name,
                            number: json.number,
                            url: json.url,
                            departureDate: json.departureDate,
                            departureTime: json.departureTime,
                            orderId: Number(req.params.Id)
                        }
                        if (data.length > 0) {
                            sendReqNoRedirect('PUT', 'trackings/', data[0].id, trackingJson);
                        } else {
                            sendReqNoRedirect('POST', 'trackings/', "", trackingJson);
                        }
                    });
                res.redirect('/orders')
            });
    });
    app.get("/deleteOrders/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-orders/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/orders", "orders", req.params.Id, res);
            });
    });

    app.get("/copyOrder/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const id = req.params.Id;
        let response = fetch(dbUrl + '/copy-order/' + id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                //console.log(data);
                res.redirect("/orders");
            })
            .catch(err => {
                //console.log(err);
            })
    });

    app.get("/viewOrder/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/get-full-order-info/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data.orderPaypal[0] != undefined) {
                    //data.orderPaypal[0].detail = JSON.stringify(data.orderPaypal[0].detail)
                }
                var grandSubtotal = 0;
                var grandTotal = 0;
                data.orderProducts.forEach(element => {
                    element.total = (parseFloat(element.subtotal).toFixed(2) * element.quantity) * (1 - (parseFloat(element.discount).toFixed(2) / 100));
                    element.total = element.total;
                    grandTotal += element.total;
                    grandSubtotal += (parseFloat(element.subtotal).toFixed(2) * element.quantity);
                    element.total = parseFloat(element.total).toFixed(2);
                });
                data.order.grandSubtotal = grandTotal;
                data.order.grandDiscount = (1 - ((grandTotal) / (grandSubtotal))) * 100;
                data.order.grandDiscount = parseFloat(data.order.grandDiscount).toFixed(2);
                res.render('orders/view', {
                    title: "AMCOR DESIGN ADMIN | Orders",
                    option: "Orders",
                    data: data,
                })
            })
    });
    //images
    app.get("/images/:Option", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let option = req.params.Option;
        res.render('images/index', {
            title: "AMCOR DESIGN ADMIN | " + option.charAt(0).toUpperCase() + option.slice(1) + " Images",
            subtitle: "All " + option.charAt(0).toUpperCase() + option.slice(1) + " Images",
            option: option.charAt(0).toUpperCase() + option.slice(1),
        })
    });

    app.get("/360images/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        var msg = req.params.msg;
        let json = {};
        var response = fetch(dbUrl + '/products', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json.products = data;
                response = fetch(dbUrl + '/metal-colors', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.metalColor = data;
                        response = fetch(dbUrl + '/stone-shapes', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                json.stoneShapes = data;
                                res.render('360/index', {
                                    title: "AMCOR DESIGN ADMIN | 360 Images",
                                    subtitle: "Add 360 Images",
                                    option: "Add 360 Images",
                                    data: json,
                                    msg: msg,
                                    option: 360,
                                })
                            })
                    });
            });
    });

    app.get("/get360", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let json = {};
        var response = fetch(dbUrl + '/products', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json.products = data;
                response = fetch(dbUrl + '/metal-colors', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.metalColor = data;
                        response = fetch(dbUrl + '/stone-shapes', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                json.stoneShapes = data;
                                response = fetch(dbUrl + '/three-sixties', {
                                    method: 'GET',
                                    agent: httpsAgent,
                                });
                                response.then(response => response.json())
                                    .then(data => {
                                        json.threeGeneral = data;
                                        var values = {};
                                        var products = Array();
                                        var metals = Array();
                                        var shapes = Array();
                                        json.threeGeneral.forEach(elem => {
                                            var pos = json.products.findIndex(item => item.id === elem.productId);
                                            json.products[pos].available = "true";
                                            elem.localSku = json.products[pos]['localSku'];
                                            elem.name = json.products[pos]['name']
                                            pos = json.metalColor.findIndex(item => item.id === elem.metalColorId);
                                            elem.metalName = json.metalColor[pos]['description'];

                                            if (elem.stoneShapeId) {
                                                pos = json.stoneShapes.findIndex(item => item.id === elem.stoneShapeId);
                                                elem.stoneName = json.stoneShapes[pos]['name'];
                                            } else {
                                                elem.stoneName = 'N/A';
                                            }
                                        });
                                        values.products = products;
                                        values.metalColor = metals;
                                        values.stoneShapes = shapes;
                                        values.threeGeneral = json.threeGeneral;
                                        res.render('get360/index', {
                                            title: "AMCOR DESIGN ADMIN | 360 Images",
                                            subtitle: "Get 360 Images",
                                            data: values,
                                            option: "Get 360",
                                            db: dbUrl
                                        })
                                    });
                            });
                    });
            });
    });

    app.get("/delete360/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        var req = req.params.Id.split("-");
        const ids = {
            productId: req[0],
            metalColorId: req[1],
            stoneShapeId: req[2] ? Number(req[2]) : null
        }
        let productPath = ids.productId + "/";
        let metalPath = ids.metalColorId + "/";
        let stoneShapePath = ids.stoneShapeId ? ids.stoneShapeId + "/" : '';

        let ruta = "./public/upload/360/" + productPath + metalPath + stoneShapePath;
        //console.log(ruta);
        try {
            fs.readdir(ruta, (err, files) => {
                if (files.length > 0) {
                    files.forEach(file => {
                        if (file.includes('.')) {
                            fs.rmSync(ruta + file, { recursive: false }, () => console.log('done'));
                        }
                    });
                } else {
                    fs.rmdirSync(ruta, { recursive: false }, () => console.log('done'));
                    console.log('Folder removed');
                }
            });
        } catch (err) {
            console.error('Something wrong happened removing the file', err);
        }

        let response = fetch(dbUrl + '/three-sixties?filter={"where":{"and":[{"productId":' + ids.productId + '},{"stoneShapeId":' + ids.stoneShapeId + '},{"metalColorId":' + ids.metalColorId + '}]}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                const deleteMethod = {
                    method: 'DELETE', // Method itself
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                    },
                    agent: httpsAgent,
                };

                data.forEach(element => {
                    fetch(dbUrl + '/three-sixties/' + element.id, deleteMethod)
                        .then(response => response.json())
                        .then(data => {
                            res.redirect("/get360");
                        })
                        .catch(err => {
                            res.redirect("/get360");
                        })
                });
            });
    });

    app.post("/verify360", (req, res) => {
        const ids = req.body;
        var condicion = ids.stoneShapeId ?
            'filter={"where":{"and":[{"productId":' + ids.productId + '},{"stoneShapeId":' + ids.stoneShapeId + '},{"metalColorId":' + ids.metalColorId + '}]}}' :
            'filter={"where":{"and":[{"productId":' + ids.productId + '},{"metalColorId":' + ids.metalColorId + '}]}}';

        var url = dbUrl + '/three-sixties?' + condicion;
        let response = fetch(url, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data.length > 0) {
                    res.send({
                        response: "This combination has already 360 loaded.",
                        data: data,
                        color: "#e74c3c"
                    });
                } else {
                    res.send({
                        response: "This combination does not have 360 loaded.",
                        color: "#00bc8c"
                    });
                }
            });
    });

    app.post("/unzip", (req, res) => {
        let product = Number(req.body.productId);
        let stoneShape = Number(req.body.stoneShapeId);
        let metalColor = Number(req.body.metalColorId);

        var condicion = stoneShape ?
            'filter={"where":{"and":[{"productId":' + product + '},{"stoneShapeId":' + stoneShape + '},{"metalColorId":' + metalColor + '}]}}' :
            'filter={"where":{"and":[{"productId":' + product + '},{"metalColorId":' + metalColor + '}]}}';

        let productPath = product + "/";
        let metalPath = metalColor + "/";
        let stoneShapePath = stoneShape ? stoneShape + "/" : '';

        let ruta = "./public/upload/360/" + productPath + metalPath + stoneShapePath;
        let rutaZip = "./public/upload/360/" + productPath + metalPath + stoneShapePath + "file.zip";
        let response = fetch(dbUrl + '/three-sixties?' + condicion, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data.length > 0) {
                    fs.readdir(ruta, (err, files) => {
                        if (files.length > 0) {
                            files.forEach(file => {
                                if (file.includes('.zip')) {
                                    fs.rmSync(ruta + file, { recursive: false }, () => console.log('done'));
                                }
                            })
                        }
                    })
                    res.redirect("/360images/Error");
                } else {
                    if (!fs.existsSync(ruta)) {
                        fs.promises.mkdir(ruta, { recursive: true });
                    }
                    try {
                        var cont = 0;
                        var zip = new AdmZip(rutaZip);
                        var zipEntries = zip.getEntries()
                        zipEntries.forEach(function (zipEntry) {
                            cont++;
                        });
                        zip.extractAllToAsync(
                            ruta, true, () =>
                            fs.readdir(ruta, (err, files) => {
                                if (files.length > 0) {
                                    files.forEach(file => {
                                        if (file.includes('.zip')) {
                                            fs.rmSync(ruta + file, { recursive: false }, () => console.log('done'));
                                        }
                                    })
                                }
                            })
                        );
                        const now = new Date().toISOString();
                        let json = {
                            path: uploadUrl + "360/" + productPath + metalPath + stoneShapePath,
                            productId: product,
                            stoneShapeId: stoneShape,
                            metalColorId: metalColor,
                            imageCount: cont, //llenar correctamente 
                            createdAt: now,
                            updatedAt: now
                        };
                        if (!stoneShape)
                            delete json.stoneShapeId;

                        sendReqNoRedirect('POST', 'three-sixties/', "", json);

                        res.redirect("/360images/Ok");

                    } catch (err) {
                        //console.log(err);
                    }
                }
            });
    })

    //products
    app.get("/products/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        var msg = req.params.msg;
        var arra = msg.split("-");
        if (msg.includes("error")) {
            let ans = fetch(dbUrl + '/get-related-products/' + arra[1], {
                method: 'GET',
                agent: httpsAgent,
            });
            ans.then(response => response.json())
                .then(data => {
                    let info = {
                        coupon: data[0],
                        homeShop: data[1],
                        order: data[2],
                        shoppingCart: data[3],
                        wishList: data[4],
                    }
                    let error = Array();
                    if (info.coupon.length > 0) {
                        error.push("Product is related to a/some coupons");
                        is_ok = 0;
                    }
                    if (info.homeShop.length > 0) {
                        error.push("Product is related to a/some home shops");
                        is_ok = 0;
                    }
                    if (info.order.length > 0) {
                        error.push("Product is related to a/some orders");
                        is_ok = 0;
                    }

                    if (info.shoppingCart.length > 0) {
                        error.push("Product is related to a/some shopping carts");
                        is_ok = 0;
                    }
                    if (info.wishList.length > 0) {
                        error.push("Product is related to a/some wish lists");
                        is_ok = 0;
                    }
                    const response = fetch(dbUrl + '/products', {
                        method: 'GET',
                        agent: httpsAgent,
                    });
                    response.then(response => response.json())
                        .then(data => {
                            res.render('products/index', {
                                title: "AMCOR DESIGN ADMIN | Products",
                                option: "Products",
                                msg: arra[0],
                                error: error,
                                data: data
                            })
                        })
                })
        } else {
            const response = fetch(dbUrl + '/products', {
                method: 'GET',
                agent: httpsAgent,
            });
            response.then(response => response.json())
                .then(data => {
                    res.render('products/index', {
                        title: "AMCOR DESIGN ADMIN | Products",
                        option: "Products",
                        msg: arra[0],
                        data: data
                    })
                })
        }

    });

    app.post('/getProductInfo', function (req, res) {
        var response = fetch(dbUrl + '/get-product-edit-info/' + req.body.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json = data;
                res.send(json);
            });
    });

    app.post('/getVariations', function (req, res) {
        var response = fetch(dbUrl + '/variations?filter={"where":{"extraVariationId":' + req.body.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json = data;
                res.send(json);
            });
    });
    app.get("/editProducts/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        var json = {};
        var response = fetch(dbUrl + '/get-product-edit-info/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json = data;
                //productTypeSizes validating selected ones
                json.productTypeSizes.forEach(elem => {
                    var pos = json.typeSizes.findIndex(item => item.id === elem.type_size_id);
                    json.typeSizes[pos].selected = "selected";
                });
                //categories validating selected ones
                json.categoryProducts.forEach(elem => {
                    var pos = json.categories.findIndex(item => item.id === elem.category_id);
                    json.categories[pos].selected = "selected";
                });
                //metals validating selected ones
                json.metalProducts.forEach(elem => {
                    var pos = json.metals.findIndex(item => item.id === elem.metal_id);
                    json.metals[pos].selected = "selected";
                });
                //stoneCenters validating selected ones
                json.productStoneCenters.forEach(elem => {
                    var pos = json.stoneCenters.findIndex(item => item.id === elem.stone_center_id);
                    json.stoneCenters[pos].selected = "selected";
                });
                //stoneColors validating selected ones
                json.productStoneColors.forEach(elem => {
                    var pos = json.stoneColors.findIndex(item => item.id === elem.stone_color_id);
                    json.stoneColors[pos].selected = "selected";
                });
                //stoneShapes validating selected ones
                json.productStoneShapes.forEach(elem => {
                    var pos = json.stoneShapes.findIndex(item => item.id === elem.stone_shape_id);
                    json.stoneShapes[pos].selected = "selected";
                });
                //stoneSides validating selected ones
                json.productStoneSides.forEach(elem => {
                    var pos = json.stoneSides.findIndex(item => item.id === elem.stone_side_id);
                    json.stoneSides[pos].selected = "selected";
                });
                //variations validating selected ones
                json.productVariations.forEach(elem => {
                    var pos = json.variations.findIndex(item => item.id === elem.variation_id);
                    json.variations[pos].selected = "selected";
                });
                //images
                //imgStoneColorProducts
                json.imgStoneColorProducts.forEach(element => {
                    var pos = json.stoneColors.findIndex(item => item.id === element.stone_color_id);
                    element.nameStoneColor = json.stoneColors[pos].name;
                    pos = json.metals.findIndex(item => item.id === element.metal_id);
                    element.name = json.metals[pos].name;
                    var filename = element.path.split("/");
                    element.fileNameOnly = filename[filename.length - 1];
                });
                //imgStoneShapeColorProducts
                json.imgStoneShapeColorProducts.forEach(element => {
                    var pos = json.stoneShapes.findIndex(item => item.id === element.stone_shape_id);
                    element.stoneShapeName = json.stoneShapes[pos].name;
                    pos = json.stoneColors.findIndex(item => item.id === element.stone_color_id);
                    element.stoneColorName = json.stoneColors[pos].name;
                    pos = json.metals.findIndex(item => item.id === element.metal_id);
                    element.name = json.metals[pos].name;
                    var filename = element.path.split("/");
                    element.fileNameOnly = filename[filename.length - 1];
                });
                //imgStoneShapeProducts
                json.imgStoneShapeProducts.forEach(element => {
                    var pos = json.stoneShapes.findIndex(item => item.id === element.stone_shape_id);
                    element.nameStoneShape = json.stoneShapes[pos].name;
                    pos = json.metals.findIndex(item => item.id === element.metal_id);
                    element.name = json.metals[pos].name;
                    var filename = element.path.split("/");
                    element.fileNameOnly = filename[filename.length - 1];
                });
                //imgStoneSideProducts
                json.imgStoneSideProducts.forEach(element => {
                    var pos = json.stoneSides.findIndex(item => item.id === element.stone_side_id);
                    element.stoneSideName = json.stoneSides[pos].name;
                    pos = json.stoneColors.findIndex(item => item.id === element.stone_color_id);
                    element.stoneColorName = json.stoneColors[pos].name;
                    pos = json.metals.findIndex(item => item.id === element.metal_id);
                    element.name = json.metals[pos].name;
                    var filename = element.path.split("/");
                    element.fileNameOnly = filename[filename.length - 1];
                });
                //imgMetalProducts
                json.imgMetalProducts.forEach(element => {
                    var pos = json.metals.findIndex(item => item.id === element.metal_id);
                    element.name = json.metals[pos].name;
                    var filename = element.path.split("/");
                    element.fileNameOnly = filename[filename.length - 1];
                });

                res.render('products/edit', {
                    title: "AMCOR DESIGN ADMIN | Products",
                    option: "Products",
                    data: json,
                    view: "products",
                    subtittle: "Products",
                })
            })

    });

    app.post("/editProducts/:Id", (req, res) => {
        const now = new Date().toISOString();
        var productId = Number(req.params.Id);
        let json = req.body;
        const ans = fetch(dbUrl + '/del-products/' + productId, {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
            });

        // Static
        console.log(json);
        json.updatedAt = now;
        //format changes for products
        json.discount = Number(json.discount);
        json.publish = json.publish == 'on' ? "true" : "false";
        json.google = json.google == 'on' ? "true" : "false";
        json.gramsWeight = Number(json.gramsWeight);
        json.manufacturer = Number(json.manufacturer);
        json.stoneNumber = Number(json.stoneNumber);
        //extra information of product
        let stoneCenters = json.stoneCenters;
        let categories = json.categories;
        let metalColors = json.metalColors;
        let stoneColors = json.stoneColors;
        let stoneColorPrice = json.stoneColorPrice;
        let stoneColorCarat = json.stoneColorCarat;
        let stoneSides = json.stoneSides;
        let stoneSidesPrice = json.SidesPrice;
        let stoneSidesCarat = json.stoneSidesCarat;
        let stoneShapes = json.stoneShapes;
        let typeSizes = json.typeSizes;
        let extraVariations = json.extraVariations;
        let variations = json.variations;
        delete json.stoneCenters;
        delete json.categories;
        delete json.metalColors;
        delete json.stoneColors;
        delete json.stoneColorPrice;
        delete json.stoneColorCarat;
        delete json.stoneSides;
        delete json.SidesPrice;
        delete json.stoneSidesCarat;
        delete json.stoneShapes;
        delete json.typeSizes;
        delete json.extraVariations;
        delete json.variations;

        //info for images deleting from the main json
        let imagesIndex = json.imagesIndex;
        let metalIcon = json.metalIcon;
        let metalIconPrincipal = json.metalIconPrincipal;
        let imagesStoneColorIndex = json.imagesStoneColorIndex;
        let stoneColoricon = json.stoneColoricon;
        let stoneColoriconPrincipal = json.stoneColoriconPrincipal;
        let stoneShapeIndex = json.stoneShapeIndex;
        let stoneShapeIcon = json.stoneShapeIcon;
        let stoneShapeIconPrincipal = json.stoneShapeIconPrincipal;
        let stoneShapeCIndex = json.stoneShapeCIndex;
        let stoneShapeCicon = json.stoneShapeCicon;
        let stoneShapeCiconPrincipal = json.stoneShapeCiconPrincipal;
        let imagesStoneSideColorIndex = json.imagesStoneSideColorIndex;
        let stoneSideColoricon = json.stoneSideColoricon;
        let stoneSideColoriconPrincipal = json.stoneSideColoriconPrincipal;

        delete json.imagesIndex;
        delete json.metalIcon;
        delete json.metalIconPrincipal;
        delete json.imagesStoneColorIndex;
        delete json.stoneColoricon;
        delete json.stoneColoriconPrincipal;
        delete json.stoneShapeIndex;
        delete json.stoneShapeIcon;
        delete json.stoneShapeIconPrincipal;
        delete json.stoneShapeCIndex;
        delete json.stoneShapeCicon;
        delete json.stoneShapeCiconPrincipal;
        delete json.imagesStoneSideColorIndex;
        delete json.stoneSideColoricon;
        delete json.stoneSideColoriconPrincipal;
        //
        const response = fetch(dbUrl + '/products/' + productId, {
            method: 'PUT',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                console.log(data);
            });

        if (categories != undefined) {
            categories.forEach(element => {
                let jsonPath = {
                    categoryId: Number(element),
                    productId: productId,
                    createdAt: now,
                    updatedAt: now
                }
                sendReqNoRedirect('POST', 'category-products/', '', jsonPath);
            });
        }
        if (variations != undefined) {
            variations.forEach(element => {
                let jsonPath = {
                    variationId: Number(element),
                    productId: Number(productId),
                    createdAt: now,
                    updatedAt: now
                }
                sendReqNoRedirect('POST', 'product-variations/', '', jsonPath);
            });
        }
        if (typeSizes != undefined) {
            typeSizes.forEach(element => {
                let jsonPath = {
                    typeSizeId: Number(element),
                    productId: Number(productId),
                    createdAt: now,
                    updatedAt: now
                }
                sendReqNoRedirect('POST', 'product-type-sizes/', '', jsonPath);
            });
        }
        if (stoneSides != undefined) {
            stoneSides.forEach((element, index) => {
                let jsonPath;
                if (stoneSidesPrice[index] != '') {
                    jsonPath = {
                        price: Number(stoneSidesPrice[index]),
                        carat: stoneSidesCarat[index],
                        stoneSideId: Number(element),
                        productId: Number(productId),
                        createdAt: now,
                        updatedAt: now
                    }
                } else {
                    jsonPath = {
                        carat: stoneSidesCarat[index],
                        stoneSideId: Number(element),
                        productId: Number(productId),
                        createdAt: now,
                        updatedAt: now
                    }
                }
                sendReqNoRedirect('POST', 'product-stone-sides/', '', jsonPath);
            });
        }
        if (stoneShapes != undefined) {
            stoneShapes.forEach((element, index) => {
                let jsonPath = {
                    stoneShapeId: Number(element),
                    productId: Number(productId),
                    createdAt: now,
                    updatedAt: now
                }
                sendReqNoRedirect('POST', 'product-stone-shapes/', '', jsonPath);
            });
        }
        if (stoneColors != undefined) {
            stoneColors.forEach((element, index) => {
                let jsonPath;
                if (stoneColorPrice[index] != '') {
                    jsonPath = {
                        price: Number(stoneColorPrice[index]),
                        carat: stoneColorCarat[index],
                        stoneColorId: Number(element),
                        productId: Number(productId),
                        createdAt: now,
                        updatedAt: now
                    }
                } else {
                    jsonPath = {
                        carat: stoneColorCarat[index],
                        stoneColorId: Number(element),
                        productId: Number(productId),
                        createdAt: now,
                        updatedAt: now
                    }
                }
                sendReqNoRedirect('POST', 'product-stone-colors/', '', jsonPath);
            });
        }
        if (stoneCenters != undefined) {
            stoneCenters.forEach((element, index) => {
                let jsonPath = {
                    stoneCenterId: Number(element),
                    productId: Number(productId),
                    createdAt: now,
                    updatedAt: now
                }
                sendReqNoRedirect('POST', 'product-stone-centers/', '', jsonPath);
            });
        }
        if (metalColors != undefined) {
            metalColors.forEach((element, index) => {
                let jsonPath = {
                    metalId: Number(element),
                    productId: Number(productId),
                    createdAt: now,
                    updatedAt: now
                }
                sendReqNoRedirect('POST', 'metal-products/', '', jsonPath);
            });
        }
        //post of images

        if (metalIcon != undefined) {
            metalIcon.forEach((element, index) => {
                if (element != "") {
                    let images = element.split(",");
                    images.pop();
                    images.forEach(elem => {
                        let jsonPath = {
                            path: uploadUrl + "products/" + elem,
                            productId: Number(productId),
                            metalId: Number(imagesIndex[index]),
                            isPrincipal: elem == metalIconPrincipal[index] ? 1 : 0,
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'image-metal-products/', '', jsonPath);
                    });
                }

            });
        }
        if (stoneColoricon != undefined) {
            stoneColoricon.forEach((element, index) => {
                if (element != "") {
                    let array = imagesStoneColorIndex[index].split("-");
                    let images = element.split(",");
                    images.pop();
                    images.forEach(elem => {
                        let jsonPath = {
                            path: uploadUrl + "products/" + elem,
                            productId: Number(productId),
                            metalId: Number(array[0]),
                            stoneColorId: Number(array[1]),
                            isPrincipal: elem == stoneColoriconPrincipal[index] ? 1 : 0,
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'image-stone-color-products/', '', jsonPath);
                    });
                }
            });
        }
        if (stoneShapeIcon != undefined) {
            stoneShapeIcon.forEach((element, index) => {
                if (element != "") {
                    let array = stoneShapeIndex[index].split("-");
                    let images = element.split(",");
                    images.pop();
                    images.forEach(elem => {
                        let jsonPath = {
                            path: uploadUrl + "products/" + elem,
                            productId: Number(productId),
                            metalId: Number(array[1]),
                            stoneShapeId: Number(array[0]),
                            isPrincipal: elem == stoneShapeIconPrincipal[index] ? 1 : 0,
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'image-stone-shape-products/', '', jsonPath);
                    });
                }
            });
        }
        if (stoneShapeCicon != undefined) {
            stoneShapeCicon.forEach((element, index) => {
                if (element != "") {
                    //getPrincipal
                    stoneShapeCIndex.forEach((element, i) => {
                        let index = element.split("-");
                        let done = 0;
                        for (const [key, value] of Object.entries(stoneShapeCiconPrincipal)) {
                            let indice = index[0] + "" + index[1] + "" + index[2];
                            if (key == indice) {
                                stoneShapeCiconPrincipal[i] = value;
                                done++;
                            }
                            if (done == 0) {
                                stoneShapeCiconPrincipal[i] = "";
                            }
                        }
                    });
                    let array = stoneShapeCIndex[index].split("-");
                    let images = element.split(",");
                    images.pop();
                    images.forEach(elem => {
                        let jsonPath = {
                            path: uploadUrl + "products/" + elem,
                            productId: Number(productId),
                            metalId: Number(array[1]),
                            stoneShapeId: Number(array[0]),
                            stoneColorId: Number(array[2]),
                            isPrincipal: elem == stoneShapeCiconPrincipal[index] ? 1 : 0,
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'image-stone-shape-color-products/', '', jsonPath);
                    });
                }
            });
        }
        if (stoneSideColoricon != undefined) {
            stoneSideColoricon.forEach((element, index) => {
                if (element != "") {
                    //getPrincipal
                    imagesStoneSideColorIndex.forEach((element, i) => {
                        let index = element.split("-");
                        let done = 0;
                        for (const [key, value] of Object.entries(stoneSideColoriconPrincipal)) {
                            let indice = index[0] + "" + index[1] + "" + index[2];
                            if (key == indice) {
                                stoneSideColoriconPrincipal[i] = value;
                                done++;
                            }
                            if (done == 0) {
                                stoneSideColoriconPrincipal[i] = "";
                            }
                        }
                    });
                    let array = imagesStoneSideColorIndex[index].split("-");
                    let images = element.split(",");
                    images.pop();
                    images.forEach(elem => {
                        let jsonPath = {
                            path: uploadUrl + "products/" + elem,
                            productId: Number(productId),
                            metalId: Number(array[1]),
                            stoneSideId: Number(array[0]),
                            stoneColorId: Number(array[2]),
                            isPrincipal: elem == stoneSideColoriconPrincipal[index] ? 1 : 0,
                            createdAt: now,
                            updatedAt: now
                        }
                        console.log(jsonPath);
                        sendReqNoRedirect('POST', 'image-stone-side-products/', '', jsonPath);
                    });
                }

            });
        }
        res.redirect("/products/all")
    });

    app.get("/changeProductStatus/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/products/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                const now = new Date().toISOString();
                if (data.publish === "true") {
                    data.publish = "false";
                } else {
                    data.publish = "true";
                }
                data.updatedAt = now;
                if (data.googleSync == "S" || data.googleSync == "N") {
                    data.googleSync = data.googleSync;
                } else {
                    delete data.googleSync;
                }
                // console.log(JSON.stringify(data));
                sendReq('PUT', 'products/', 'products/all', req.params.Id, data, res);
            })
    });


    app.get("/addProducts/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let json = {};

        var response = fetch(dbUrl + '/stone-centers', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json.stoneCenters = data;
                response = fetch(dbUrl + '/categories', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.categories = data;

                        response = fetch(dbUrl + '/metals', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                json.metals = data;
                                response = fetch(dbUrl + '/stone-colors', {
                                    method: 'GET',
                                    agent: httpsAgent,
                                });
                                response.then(response => response.json())
                                    .then(data => {
                                        json.stoneColors = data;

                                        response = fetch(dbUrl + '/sizes', {
                                            method: 'GET',
                                            agent: httpsAgent,
                                        });
                                        response.then(response => response.json())
                                            .then(data => {
                                                json.sizes = data;
                                                response = fetch(dbUrl + '/extra-variations', {
                                                    method: 'GET',
                                                    agent: httpsAgent,
                                                });
                                                response.then(response => response.json())
                                                    .then(data => {
                                                        json.extraVariations = data;
                                                        response = fetch(dbUrl + '/stone-sides', {
                                                            method: 'GET',
                                                            agent: httpsAgent,
                                                        });
                                                        response.then(response => response.json())
                                                            .then(data => {
                                                                json.stoneSides = data;
                                                                response = fetch(dbUrl + '/stone-shapes', {
                                                                    method: 'GET',
                                                                    agent: httpsAgent,
                                                                });
                                                                response.then(response => response.json())
                                                                    .then(data => {
                                                                        json.stoneShapes = data;
                                                                        response = fetch(dbUrl + '/type-sizes', {
                                                                            method: 'GET',
                                                                            agent: httpsAgent,
                                                                        });
                                                                        response.then(response => response.json())
                                                                            .then(data => {
                                                                                json.typeSizes = data;
                                                                                response = fetch(dbUrl + '/variations', {
                                                                                    method: 'GET',
                                                                                    agent: httpsAgent,
                                                                                });
                                                                                response.then(response => response.json())
                                                                                    .then(data => {
                                                                                        json.variations = data;
                                                                                        res.render('products/create', {
                                                                                            title: "AMCOR DESIGN ADMIN | Products",
                                                                                            option: "Create Products",
                                                                                            data: json,
                                                                                            view: "products",
                                                                                            subtittle: "Products",
                                                                                        })
                                                                                    })
                                                                            })
                                                                    })
                                                            });
                                                    });
                                            })
                                    })
                            })
                    })
            })
    });
    app.get("/copyProduct/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let product;
        const ans = fetch(dbUrl + '/copy-product/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                product = data;
                res.redirect("/editProducts/" + data[0][2])
            })
    });
    app.post('/verifyProduct/:slug/:localSku/:supplierSku', (req, res) => {
        const slug = req.params.slug;
        const localSku = req.params.localSku;
        const supplierSku = req.params.supplierSku;
        const response = fetch(dbUrl + '/products?filter={"where":{"or":[{"slug":"' + slug + '"},{"localSku":"' + localSku + '"},{"supplierSku":"' + supplierSku + '"}]}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data.length > 0) {
                    res.send({
                        response: "The local Sku, slug or supplier Sku is already in use, <br/> Please remember, those fields have to be unique.",
                        data: data,
                        color: "#e74c3c"
                    });
                } else {
                    res.send({
                        response: "The product information will be loaded. <br/> This form is going to be sent.",
                        color: "#00bc8c"
                    });
                }
            });
    });
    app.post('/newProduct', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        //format changes for products
        json.discount = Number(json.discount);
        json.publish = json.publish == 'on' ? "true" : "false";
        json.google = json.google == 'on' ? "true" : "false";
        json.gramsWeight = Number(json.gramsWeight);
        json.manufacturer = Number(json.manufacturer);
        json.stoneNumber = Number(json.stoneNumber);
        //extra information of product
        let stoneCenters = json.stoneCenters;
        let categories = json.categories;
        let metalColors = json.metalColors;
        let stoneColors = json.stoneColors;
        let stoneColorPrice = json.stoneColorPrice;
        let stoneColorCarat = json.stoneColorCarat;
        let stoneSides = json.stoneSides;
        let stoneSidesPrice = json.SidesPrice;
        let stoneSidesCarat = json.stoneSidesCarat;
        let stoneShapes = json.stoneShapes;
        let typeSizes = json.typeSizes;
        let extraVariations = json.extraVariations;
        let variations = json.variations;
        delete json.stoneCenters;
        delete json.categories;
        delete json.metalColors;
        delete json.stoneColors;
        delete json.stoneColorPrice;
        delete json.stoneColorCarat;
        delete json.stoneSides;
        delete json.SidesPrice;
        delete json.stoneSidesCarat;
        delete json.stoneShapes;
        delete json.typeSizes;
        delete json.extraVariations;
        delete json.variations;

        //info for images deleting from the main json
        let imagesIndex = json.imagesIndex;
        let metalIcon = json.metalIcon;
        let metalIconPrincipal = json.metalIconPrincipal;
        let imagesStoneColorIndex = json.imagesStoneColorIndex;
        let stoneColoricon = json.stoneColoricon;
        let stoneColoriconPrincipal = json.stoneColoriconPrincipal;
        let stoneShapeIndex = json.stoneShapeIndex;
        let stoneShapeIcon = json.stoneShapeIcon;
        let stoneShapeIconPrincipal = json.stoneShapeIconPrincipal;
        let stoneShapeCIndex = json.stoneShapeCIndex;
        let stoneShapeCicon = json.stoneShapeCicon;
        let stoneShapeCiconPrincipal = json.stoneShapeCiconPrincipal;
        let imagesStoneSideColorIndex = json.imagesStoneSideColorIndex;
        let stoneSideColoricon = json.stoneSideColoricon;
        let stoneSideColoriconPrincipal = json.stoneSideColoriconPrincipal;

        delete json.imagesIndex;
        delete json.metalIcon;
        delete json.metalIconPrincipal;
        delete json.imagesStoneColorIndex;
        delete json.stoneColoricon;
        delete json.stoneColoriconPrincipal;
        delete json.stoneShapeIndex;
        delete json.stoneShapeIcon;
        delete json.stoneShapeIconPrincipal;
        delete json.stoneShapeCIndex;
        delete json.stoneShapeCicon;
        delete json.stoneShapeCiconPrincipal;
        delete json.imagesStoneSideColorIndex;
        delete json.stoneSideColoricon;
        delete json.stoneSideColoriconPrincipal;

        //begin the posts process
        const response = fetch(dbUrl + '/products/', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                var productId = Number(data.id);
                if (categories != undefined) {
                    categories.forEach(element => {
                        let jsonPath = {
                            categoryId: Number(element),
                            productId: productId,
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-products/', '', jsonPath);
                    });
                }
                if (variations != undefined) {
                    variations.forEach(element => {
                        let jsonPath = {
                            variationId: Number(element),
                            productId: Number(productId),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'product-variations/', '', jsonPath);
                    });
                }
                if (typeSizes != undefined) {
                    typeSizes.forEach(element => {
                        let jsonPath = {
                            typeSizeId: Number(element),
                            productId: Number(productId),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'product-type-sizes/', '', jsonPath);
                    });
                }
                if (stoneSides != undefined) {
                    stoneSides.forEach((element, index) => {
                        let jsonPath;
                        if (stoneSidesPrice[index] != '') {
                            jsonPath = {
                                price: Number(stoneSidesPrice[index]),
                                carat: stoneSidesCarat[index],
                                stoneSideId: Number(element),
                                productId: Number(productId),
                                createdAt: now,
                                updatedAt: now
                            }
                        } else {
                            jsonPath = {
                                carat: stoneSidesCarat[index],
                                stoneSideId: Number(element),
                                productId: Number(productId),
                                createdAt: now,
                                updatedAt: now
                            }
                        }
                        sendReqNoRedirect('POST', 'product-stone-sides/', '', jsonPath);
                    });
                }
                if (stoneShapes != undefined) {
                    stoneShapes.forEach((element, index) => {
                        let jsonPath = {
                            stoneShapeId: Number(element),
                            productId: Number(productId),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'product-stone-shapes/', '', jsonPath);
                    });
                }
                if (stoneColors != undefined) {
                    stoneColors.forEach((element, index) => {
                        let jsonPath;
                        if (stoneColorPrice[index] != '') {
                            jsonPath = {
                                price: Number(stoneColorPrice[index]),
                                carat: stoneColorCarat[index],
                                stoneColorId: Number(element),
                                productId: Number(productId),
                                createdAt: now,
                                updatedAt: now
                            }
                        } else {
                            jsonPath = {
                                carat: stoneColorCarat[index],
                                stoneColorId: Number(element),
                                productId: Number(productId),
                                createdAt: now,
                                updatedAt: now
                            }
                        }
                        sendReqNoRedirect('POST', 'product-stone-colors/', '', jsonPath);
                    });
                }
                if (stoneCenters != undefined) {
                    stoneCenters.forEach((element, index) => {
                        let jsonPath = {
                            stoneCenterId: Number(element),
                            productId: Number(productId),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'product-stone-centers/', '', jsonPath);
                    });
                }
                if (metalColors != undefined) {
                    metalColors.forEach((element, index) => {
                        let jsonPath = {
                            metalId: Number(element),
                            productId: Number(productId),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'metal-products/', '', jsonPath);
                    });
                }
                //post of images
                if (metalIcon != undefined) {
                    metalIcon.forEach((element, index) => {
                        if (element != "") {


                            let images = element.split(",");
                            images.pop();
                            images.forEach(elem => {
                                let jsonPath = {
                                    path: uploadUrl + "products/" + elem,
                                    productId: Number(productId),
                                    metalId: Number(imagesIndex[index]),
                                    isPrincipal: elem == metalIconPrincipal[index] ? 1 : 0,
                                    createdAt: now,
                                    updatedAt: now
                                }
                                sendReqNoRedirect('POST', 'image-metal-products/', '', jsonPath);
                            });
                        }

                    });
                }
                if (stoneColoricon != undefined) {
                    stoneColoricon.forEach((element, index) => {
                        if (element != "") {
                            let array = imagesStoneColorIndex[index].split("-");
                            let images = element.split(",");
                            images.pop();
                            images.forEach(elem => {
                                let jsonPath = {
                                    path: uploadUrl + "products/" + elem,
                                    productId: Number(productId),
                                    metalId: Number(array[0]),
                                    stoneColorId: Number(array[1]),
                                    isPrincipal: elem == stoneColoriconPrincipal[index] ? 1 : 0,
                                    createdAt: now,
                                    updatedAt: now
                                }
                                sendReqNoRedirect('POST', 'image-stone-color-products/', '', jsonPath);
                            });
                        }
                    });
                }
                if (stoneShapeIcon != undefined) {
                    stoneShapeIcon.forEach((element, index) => {
                        if (element != "") {
                            let array = stoneShapeIndex[index].split("-");
                            let images = element.split(",");
                            images.pop();
                            images.forEach(elem => {
                                let jsonPath = {
                                    path: uploadUrl + "products/" + elem,
                                    productId: Number(productId),
                                    metalId: Number(array[1]),
                                    stoneShapeId: Number(array[0]),
                                    isPrincipal: elem == stoneShapeIconPrincipal[index] ? 1 : 0,
                                    createdAt: now,
                                    updatedAt: now
                                }
                                sendReqNoRedirect('POST', 'image-stone-shape-products/', '', jsonPath);
                            });
                        }
                    });
                }
                if (stoneShapeCicon != undefined) {
                    stoneShapeCicon.forEach((element, index) => {
                        if (element != "") {
                            //getPrincipal
                            stoneShapeCIndex.forEach((element, i) => {
                                let index = element.split("-");
                                let done = 0;
                                for (const [key, value] of Object.entries(stoneShapeCiconPrincipal)) {
                                    let indice = index[0] + "" + index[1] + "" + index[2];
                                    if (key == indice) {
                                        stoneShapeCiconPrincipal[i] = value;
                                        done++;
                                    }
                                    if (done == 0) {
                                        stoneShapeCiconPrincipal[i] = "";
                                    }
                                }
                            });

                            let array = stoneShapeCIndex[index].split("-");
                            let images = element.split(",");
                            images.pop();
                            images.forEach(elem => {
                                let jsonPath = {
                                    path: uploadUrl + "products/" + elem,
                                    productId: Number(productId),
                                    metalId: Number(array[1]),
                                    stoneShapeId: Number(array[0]),
                                    stoneColorId: Number(array[2]),
                                    isPrincipal: elem == stoneShapeCiconPrincipal[index] ? 1 : 0,
                                    createdAt: now,
                                    updatedAt: now
                                }
                                sendReqNoRedirect('POST', 'image-stone-shape-color-products/', '', jsonPath);
                            });
                        }
                    });
                }

                if (stoneSideColoricon != undefined) {
                    stoneSideColoricon.forEach((element, index) => {
                        if (element != "") {
                            //getPrincipal
                            imagesStoneSideColorIndex.forEach((element, i) => {
                                let index = element.split("-");
                                let done = 0;
                                for (const [key, value] of Object.entries(stoneSideColoriconPrincipal)) {
                                    let indice = index[0] + "" + index[1] + "" + index[2];
                                    if (key == indice) {
                                        stoneSideColoriconPrincipal[i] = value;
                                        done++;
                                    }
                                    if (done == 0) {
                                        stoneSideColoriconPrincipal[i] = "";
                                    }
                                }
                            });

                            let array = imagesStoneSideColorIndex[index].split("-");
                            let images = element.split(",");
                            images.pop();
                            images.forEach(elem => {
                                let jsonPath = {
                                    path: uploadUrl + "products/" + elem,
                                    productId: Number(productId),
                                    metalId: Number(array[1]),
                                    stoneSideId: Number(array[0]),
                                    stoneColorId: Number(array[2]),
                                    isPrincipal: elem == stoneSideColoriconPrincipal[index] ? 1 : 0,
                                    createdAt: now,
                                    updatedAt: now
                                }
                                sendReqNoRedirect('POST', 'image-stone-side-products/', '', jsonPath);
                            });
                        }

                    });
                }
                res.redirect("/products/all")
            });
    });

    app.get("/deleteProducts/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let ans = fetch(dbUrl + '/get-related-products/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                let info = {
                    coupon: data[0],
                    homeShop: data[1],
                    order: data[2],
                    shoppingCart: data[3],
                    wishList: data[4],
                }
                let is_ok = 1;
                if (info.coupon.length > 0) {
                    is_ok = 0;
                }
                if (info.homeShop.length > 0) {
                    is_ok = 0;
                }
                if (info.order.length > 0) {
                    is_ok = 0;
                }
                if (info.shoppingCart.length > 0) {
                    is_ok = 0;
                }
                if (info.wishList.length > 0) {
                    is_ok = 0;
                }
                if (is_ok === 1) {
                    const response = fetch(dbUrl + '/del-products/' + req.params.Id, {
                        method: 'GET',
                        agent: httpsAgent,
                    });
                    response.then(response => response.json())
                        .then(data => {
                            const deleteMethod = {
                                method: 'DELETE', // Method itself
                                headers: {
                                    'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                                },
                                agent: httpsAgent,
                            };

                            fetch(dbUrl + '/products/' + Number(req.params.Id), deleteMethod)
                                .then(response => response.json())
                                .then(data => {
                                    res.redirect('/products/ok');
                                })
                                .catch(err => {
                                    res.redirect('/products/ok');
                                })

                        });
                } else {
                    res.redirect("/products/error-" + req.params.Id)
                }
            });
    });
    //centerShapes
    app.get("/centerShapes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/center-shape-ctws', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                for (var i = 0; i < data.length; i++) {
                    data[i].createdAt = calcDate(new Date(), new Date(data[i].createdAt));
                    data[i].updatedAt = calcDate(new Date(), new Date(data[i].updatedAt));
                }
                res.render('centerShapesImages/index', {
                    title: "AMCOR DESIGN ADMIN | Center Shape Images",
                    subtittle: 'All Center Shape Image',
                    option: "Center Shape Images",
                    data: data,
                })
            })
    });
    app.get("/deleteCenterShapes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-center-shape-images/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/center-shape-ctws", "centerShapes", req.params.Id, res);
            });
    });
    app.get("/addCenterShapes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let products = {};
        let metals = {};
        let stoneShapes = {};
        let stoneCenter = {};
        let response = fetch(dbUrl + '/products', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(
            response => response.json())
            .then(data => {
                products = data;
                response = fetch(dbUrl + '/metals', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(
                    response => response.json())
                    .then(data => {
                        metals = data;
                        response = fetch(dbUrl + '/stone-shapes', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(
                            response => response.json())
                            .then(data => {
                                stoneShapes = data;
                                response = fetch(dbUrl + '/stone-centers', {
                                    method: 'GET',
                                    agent: httpsAgent,
                                });
                                response.then(
                                    response => response.json())
                                    .then(data => {
                                        stoneCenter = data;
                                        let json = {
                                            products: products,
                                            metals: metals,
                                            stoneShapes: stoneShapes,
                                            stoneCenter: stoneCenter,
                                        };
                                        res.render('centerShapesImages/create', {
                                            title: "AMCOR DESIGN ADMIN | Center Shape Images",
                                            option: "Create Center Shape Images",
                                            data: json,
                                            view: "centerShapes",
                                            subtittle: "Center Shape Images",
                                        });
                                    })
                            })
                    })
            })
    });
    app.post("/newCenterShapes/", (req, res) => {
        const now = new Date().toISOString();
        let body = req.body;
        let myArr = body.productId.split("-");
        let json = {
            sku: myArr[1],
            productId: Number(myArr[0]),
            createdAt: now,
            updatedAt: now
        };
        let iconPrincipal = Array();

        body.index.forEach((element, i) => {
            let index = element.split("-");
            let done = 0;
            for (const [key, value] of Object.entries(body.iconPrincipal)) {
                let indice = index[0] + "" + index[1] + "" + index[2];
                if (key == indice) {
                    iconPrincipal[i] = value;
                    done++;
                }
                if (done == 0) {
                    iconPrincipal[i] = "";
                }
            }
        });

        const response = fetch(dbUrl + '/center-shape-ctws/', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                var centerShapeCtwsId = Number(data.id);
                body.icon.forEach((element, i) => {
                    let index = body.index[i].split("-");
                    let imgArray = element.split(",");
                    imgArray.pop();
                    imgArray.forEach(elem => {
                        let json = {
                            path: uploadUrl + "products/" + elem,
                            centerShapeCtwId: Number(centerShapeCtwsId),
                            metalId: Number(index[1]),
                            stoneShapeId: Number(index[0]),
                            stoneCenterId: Number(index[2]),
                            isPrincipal: elem == iconPrincipal[i] ? 1 : 0,
                            createdAt: now,
                            updatedAt: now,
                        }
                        sendReqNoRedirect('POST', 'image-center-shape-ctws/', '', json);
                    });

                });
            })
        res.redirect("/centerShapes");
    });

    app.get("/editCenterShapes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let products = {};
        let metals = {};
        let stoneShapes = {};
        let stoneCenter = {};
        let shapeInfo = {};
        let imagesInfo = {};
        let response = fetch(dbUrl + '/products', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(
            response => response.json())
            .then(data => {
                products = data;
                response = fetch(dbUrl + '/metals', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(
                    response => response.json())
                    .then(data => {
                        metals = data;
                        response = fetch(dbUrl + '/stone-shapes', {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(
                            response => response.json())
                            .then(data => {
                                stoneShapes = data;
                                response = fetch(dbUrl + '/stone-centers', {
                                    method: 'GET',
                                    agent: httpsAgent,
                                });
                                response.then(
                                    response => response.json())
                                    .then(data => {
                                        stoneCenter = data;

                                        response = fetch(dbUrl + '/center-shape-ctws/' + req.params.Id, {
                                            method: 'GET',
                                            agent: httpsAgent,
                                        });
                                        response.then(
                                            response => response.json())
                                            .then(data => {
                                                shapeInfo = data;
                                                response = fetch(dbUrl + '/image-center-shape-ctws?filter={"where":{"centerShapeCtwId":' + req.params.Id + '}}', {
                                                    method: 'GET',
                                                    agent: httpsAgent,
                                                });
                                                response.then(
                                                    response => response.json())
                                                    .then(data => {
                                                        imagesInfo = data;
                                                        for (var i = 0; i < products.length; i++) {
                                                            var pos = products.findIndex(item => item.id === shapeInfo.productId);
                                                            products[pos].selected = 'selected';
                                                        }

                                                        metals.forEach(item => {
                                                            imagesInfo.forEach(element => {
                                                                if (Number(item.id) == Number(element.metalId)) {
                                                                    item.selected = "selected";
                                                                }
                                                            });
                                                        });
                                                        stoneShapes.forEach(item => {
                                                            imagesInfo.forEach(element => {
                                                                if (Number(item.id) == Number(element.stoneShapeId)) {
                                                                    item.selected = "selected";
                                                                }
                                                            });
                                                        });
                                                        stoneCenter.forEach(item => {
                                                            imagesInfo.forEach(element => {
                                                                if (Number(item.id) == Number(element.stoneCenterId)) {
                                                                    item.selected = "selected";
                                                                }
                                                            });
                                                        });
                                                        imagesInfo.forEach(element => {
                                                            var filename = element.path.split("/");
                                                            element.fileNameOnly = filename[filename.length - 1];
                                                        });


                                                        response = fetch(dbUrl + '/get-distinct-center-shape-imgs/' + req.params.Id, {
                                                            method: 'GET',
                                                            agent: httpsAgent,
                                                        });
                                                        response.then(
                                                            response => response.json())
                                                            .then(data => {
                                                                let combinations = Array();
                                                                data.forEach(item => {
                                                                    combinations.push("combination" + item.stone_shape_id + "" + item.metal_id + "" + item.stone_center_id);
                                                                })
                                                                let json = {
                                                                    products: products,
                                                                    metals: metals,
                                                                    stoneShapes: stoneShapes,
                                                                    stoneCenter: stoneCenter,
                                                                    shapeInfo: shapeInfo,
                                                                    imagesInfo: imagesInfo,
                                                                    combinations: combinations
                                                                };
                                                                res.render('centerShapesImages/edit', {
                                                                    title: "AMCOR DESIGN ADMIN | Center Shape Images",
                                                                    option: "Center Shape Images",
                                                                    data: json,
                                                                    view: "centerShapes",
                                                                    subtittle: "Center Shape Images",
                                                                });
                                                            })
                                                    });
                                            })
                                    })
                            })
                    })
            })
    });

    app.post("/editCenterShapes/:Id", (req, res) => {
        const now = new Date().toISOString();
        let body = req.body;
        body.now = now;
        var centerShapeCtwsId = Number(req.params.Id);
        let myArr = body.productId.split("-");
        let json = {
            sku: myArr[1],
            productId: Number(myArr[0]),
            createdAt: body.createdAt,
            updatedAt: now
        };

        sendReqNoRedirect('PUT', 'center-shape-ctws/', centerShapeCtwsId, json);
        response = fetch(dbUrl + '/image-center-shape-ctws?filter={"where":{"centerShapeCtwId":' + centerShapeCtwsId + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(
            response => response.json())
            .then(data => {
                data.forEach(element => {
                    const deleteMethod = {
                        method: 'DELETE', // Method itself
                        headers: {
                            'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                        },
                        agent: httpsAgent,
                    };

                    fetch(dbUrl + '/image-center-shape-ctws/' + Number(element.id), deleteMethod)
                        .then(response => response.json())
                        .then(data => {
                            //console.log(data);
                        })
                        .catch(err => {
                            //console.log(err);
                        })
                });
            });



        if (body.icon != undefined) {
            let iconPrincipal = Array();

            body.index.forEach((element, i) => {
                let index = element.split("-");
                let done = 0;
                for (const [key, value] of Object.entries(body.iconPrincipal)) {
                    let indice = index[0] + "" + index[1] + "" + index[2];
                    if (key == indice) {
                        iconPrincipal[i] = value;
                        done++;
                    }
                    if (done == 0) {
                        iconPrincipal[i] = "";
                    }
                }
            });
            body.icon.forEach((element, i) => {
                let index = body.index[i].split("-");
                let imgArray = element.split(",");
                imgArray.pop();
                imgArray.forEach(elem => {
                    let json = {
                        path: uploadUrl + "products/" + elem,
                        centerShapeCtwId: centerShapeCtwsId,
                        metalId: Number(index[1]),
                        stoneShapeId: Number(index[0]),
                        stoneCenterId: Number(index[2]),
                        isPrincipal: elem == iconPrincipal[i] ? 1 : 0,
                        createdAt: now,
                        updatedAt: now,
                    }
                    sendReqNoRedirect('POST', 'image-center-shape-ctws/', '', json);
                });
            });
        }
        res.redirect("/centerShapes");
    });

    //categories
    app.get("/categories/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/categories', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('categories/index', {
                    title: "AMCOR DESIGN ADMIN | Categories",
                    option: "Categories",
                    data: data,
                })
            })
    });
    app.get("/addcategories/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/styles', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('categories/create', {
                    title: "AMCOR DESIGN ADMIN | Categories",
                    option: "Create Categories",
                    data: data,
                    view: "categories",
                    subtittle: "Categories",
                })
            })
    });
    app.post('/newcategories', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        let categoryStyle = json.styles;
        delete json.styles;
        json.path = uploadUrl + "categories/" + json.path;
        json.pathProduct = uploadUrl + "categories/" + json.pathProduct;
        json.banner_1 = uploadUrl + "categories/" + json.banner_1;
        json.banner_2 = uploadUrl + "categories/" + json.banner_2;
        json.banner_3 = uploadUrl + "categories/" + json.banner_3;
        json.banner_4 = uploadUrl + "categories/" + json.banner_4;
        json.discount = Number(json.discount);
        json.createdAt = now;

        const response = fetch(dbUrl + '/categories', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                var categoyId = data.id;
                if (!Array.isArray(categoryStyle)) {
                    if (categoryStyle != "") {
                        let jsonPath = {
                            categoryId: Number(categoyId),
                            styleId: Number(categoryStyle),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-styles/', '', jsonPath);
                    }
                } else {
                    categoryStyle.forEach(element => {
                        let jsonPath = {
                            styleId: Number(element),
                            categoryId: Number(categoyId),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-styles/', '', jsonPath);
                    });
                }
                res.redirect('/categories/');
            })
    });
    app.get("/deleteCategories/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-categories/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/categories", "categories", req.params.Id, res);
            });
    });

    app.get("/editCategories/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let json = {};
        let styles = {};
        let response = fetch(dbUrl + "/styles/", {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                styles = data;
                response = fetch(dbUrl + "/categories/" + req.params.Id, {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.categories = data;
                        json.styles = styles;
                        response = fetch(dbUrl + "/category-styles?filter=%7B%22where%22%3A%7B%22categoryId%22%3A" + req.params.Id + "%7D%7D", {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                for (var i = 0; i < data.length; i++) {
                                    var pos = json.styles.findIndex(item => item.id === data[i].styleId);
                                    json.styles[pos].selected = 'selected';
                                }
                                res.render('categories/edit', {
                                    title: "AMCOR DESIGN ADMIN | Categories",
                                    option: "Edit Category",
                                    data: json,
                                    view: "categories",
                                    subtittle: "Categories",
                                })
                            })
                    })

            });
    });
    app.post("/editCategories/:Id", (req, res) => {
        const now = new Date().toISOString();
        const ans = fetch(dbUrl + '/category-styles?filter={"where":{"categoryId":' + req.params.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                var categoryStyles = data;
                //console.log(categoryStyles);
                const deleteMethod = {
                    method: 'DELETE', // Method itself
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                    },
                    agent: httpsAgent,
                };
                categoryStyles.forEach(data => {
                    fetch(dbUrl + '/category-styles/' + data.id, deleteMethod)
                        .then(response => response.json())
                        .then(data => {
                            //console.log(data);
                        })
                        .catch(err => {
                            //console.log(err);
                        })
                });
            });
        let json = req.body;
        let categoryStyle = json.styles;
        delete json.styles;

        json.path = json.path != "" ? uploadUrl + "categories/" + json.path : json.pathOld;
        json.pathProduct = json.pathProduct != "" ? uploadUrl + "categories/" + json.pathProduct : json.pathProductOld;
        json.banner_1 = json.banner_1 != "" ? uploadUrl + "categories/" + json.banner_1 : json.banner_1Old;
        json.banner_2 = json.banner_3 != "" ? uploadUrl + "categories/" + json.banner_2 : json.banner_2Old;
        json.banner_3 = json.banner_3 != "" ? uploadUrl + "categories/" + json.banner_3 : json.banner_3Old;
        json.banner_4 = json.banner_4 != "" ? uploadUrl + "categories/" + json.banner_4 : json.banner_4Old;

        json.textDescriptionBanner_1 = json.textDescriptionBanner_1.trimStart();
        json.textDescriptionBanner_2 = json.textDescriptionBanner_2.trimStart();
        json.textDescriptionBanner_3 = json.textDescriptionBanner_3.trimStart();
        json.textDescriptionBanner_4 = json.textDescriptionBanner_4.trimStart();

        json.textDescriptionBanner_1 = json.textDescriptionBanner_1.trimEnd();
        json.textDescriptionBanner_2 = json.textDescriptionBanner_2.trimEnd();
        json.textDescriptionBanner_3 = json.textDescriptionBanner_3.trimEnd();
        json.textDescriptionBanner_4 = json.textDescriptionBanner_4.trimEnd();

        delete json.pathOld;
        delete json.pathProductOld;
        delete json.banner_1Old;
        delete json.banner_2Old;
        delete json.banner_3Old;
        delete json.banner_4Old;
        json.discount = Number(json.discount);
        json.updatedAt = now;

        const response = fetch(dbUrl + '/categories/' + req.params.Id, {
            method: 'PUT',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                //console.log(data);
                if (!Array.isArray(categoryStyle)) {
                    if (categoryStyle != "") {
                        let jsonPath = {
                            categoryId: Number(req.params.Id),
                            styleId: Number(categoryStyle),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-styles/', '', jsonPath);
                    }
                } else {
                    categoryStyle.forEach(element => {
                        let jsonPath = {
                            styleId: Number(element),
                            categoryId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-styles/', '', jsonPath);
                    });
                }
                res.redirect('/categories/');
            })
            .catch(err => {
                if (!Array.isArray(categoryStyle)) {
                    if (categoryStyle != "") {
                        let jsonPath = {
                            categoryId: Number(req.params.Id),
                            styleId: Number(categoryStyle),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-styles/', '', jsonPath);
                    }
                } else {
                    categoryStyle.forEach(element => {
                        let jsonPath = {
                            styleId: Number(element),
                            categoryId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'category-styles/', '', jsonPath);
                    });
                }
                res.redirect('/categories/');
            });
    });

    //coupons
    app.get("/coupons/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }

        var msg = req.params.msg;
        var arra = msg.split("-");
        if (msg.includes("error")) {
            let ans = fetch(dbUrl + '/get-related-coupons/' + arra[1], {
                method: 'GET',
                agent: httpsAgent,
            });
            ans.then(response => response.json())
                .then(data => {
                    let info = {
                        couponProducts: data[0],
                        orderProducts: data[1],
                    }
                    let error = Array();
                    if (info.couponProducts.length > 0) {
                        error.push("Coupon is related to one or more products.");
                        is_ok = 0;
                    }
                    if (info.orderProducts.length > 0) {
                        error.push("Coupon it has been used in an order.");
                        is_ok = 0;
                    }
                    const response = fetch(dbUrl + '/coupons', {
                        method: 'GET',
                        agent: httpsAgent,
                    });
                    response.then(response => response.json())
                        .then(data => {
                            data.forEach(element => {
                                let date = element.startDate.split(" ");
                                element.startDate = date[0] + " " + date[1] + " " + date[2] + " " + date[3] + " T " + element.startHour;
                                date = element.finishDate.split(" ");
                                element.finishDate = date[0] + " " + date[1] + " " + date[2] + " " + date[3] + " T " + element.finishHour;
                            });
                            res.render('coupons/index', {
                                title: "AMCOR DESIGN ADMIN | Coupons",
                                subtittle: 'All Coupons',
                                option: "Coupons",
                                msg: arra[0],
                                error: error,
                                data: data
                            })
                        })
                })
        } else {
            const response = fetch(dbUrl + '/coupons', {
                method: 'GET',
                agent: httpsAgent,
            });
            response.then(response => response.json())
                .then(data => {
                    data.forEach(element => {
                        let date = element.startDate.split(" ");
                        element.startDate = date[0] + " " + date[1] + " " + date[2] + " " + date[3] + " T " + element.startHour;
                        date = element.finishDate.split(" ");
                        element.finishDate = date[0] + " " + date[1] + " " + date[2] + " " + date[3] + " T " + element.finishHour;
                    });
                    res.render('coupons/index', {
                        title: "AMCOR DESIGN ADMIN | Coupons",
                        subtittle: 'All Coupons',
                        option: "Coupons",
                        msg: arra[0],
                        data: data
                    })
                })
        }
    });
    app.get("/addCoupons", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const ans = fetch(dbUrl + '/products', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                res.render('coupons/create', {
                    title: "AMCOR DESIGN ADMIN | Coupons",
                    option: "Create Coupon",
                    data: data,
                    view: "coupons",
                    subtittle: "Coupons",
                })
            });
    });

    app.post('/newCoupon', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        json.discount = Number(json.discount);
        let startDate = json.startDate.split(" ");
        let finishDate = json.finishDate.split(" ");
        let fecha = startDate[0].split("-");
        json.startDate = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
        json.startHour = startDate[1];
        fecha = finishDate[0].split("-");
        json.finishDate = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
        json.finishHour = finishDate[1];

        let products = Array();
        if (Array.isArray(json.productId)) {
            products = json.productId;
        } else {
            products.push(json.productId);
        }
        delete json.productId;
        const response = fetch(dbUrl + '/coupons/', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                products.forEach(element => {
                    let jsonPath = {
                        productId: Number(element),
                        couponId: Number(data.id),
                        createdAt: now,
                        updatedAt: now
                    }
                    sendReqNoRedirect('POST', 'coupon-products/', '', jsonPath);
                });
                res.redirect('/coupons/all');
            });
    });

    app.get("/editCoupons/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let json = {};
        let products = {};
        let response = fetch(dbUrl + "/products/", {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                products = data;
                response = fetch(dbUrl + "/coupons/" + req.params.Id, {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.coupons = data;
                        json.coupons.startDate = dateFormat(new Date(json.coupons.startDate), "dd-mm-yyyy");
                        json.coupons.finishDate = dateFormat(new Date(json.coupons.finishDate), "dd-mm-yyyy");
                        json.products = products;
                        response = fetch(dbUrl + "/coupon-products?filter=%7B%22where%22%3A%7B%22couponId%22%3A" + req.params.Id + "%7D%7D", {
                            method: 'GET',
                            agent: httpsAgent,
                        });
                        response.then(response => response.json())
                            .then(data => {
                                if (Array.isArray(data)) {
                                    for (var i = 0; i < data.length; i++) {
                                        var pos = json.products.findIndex(item => item.id === data[i].productId);
                                        json.products[pos].selected = 'selected';
                                    }
                                } else {
                                    var pos = json.products.findIndex(item => item.id === data.productId);
                                    json.products.selected = 'selected';
                                }
                                res.render('coupons/edit', {
                                    title: "AMCOR DESIGN ADMIN | Coupons",
                                    option: "Edit Coupon",
                                    data: json,
                                    view: "coupons",
                                    subtittle: "Coupons",
                                })
                            })
                    })


            });
    });

    app.post("/editCoupons/:Id", (req, res) => {
        const now = new Date().toISOString();

        const ans = fetch(dbUrl + '/coupon-products?filter={"where":{"couponId":' + req.params.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                var products = data;
                const deleteMethod = {
                    method: 'DELETE', // Method itself
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                    },
                    agent: httpsAgent,
                };
                products.forEach(data => {
                    fetch(dbUrl + '/coupon-products/' + data.id, deleteMethod)
                        .then(response => response.json())
                        .then(data => { })
                        .catch(err => { })
                });
            });

        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        json.discount = Number(json.discount);
        let products = json.productId;
        let startDate = json.startDate.split(" ");
        let finishDate = json.finishDate.split(" ");
        let fecha = startDate[0].split("-");
        json.startDate = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
        json.startHour = startDate[1];
        fecha = finishDate[0].split("-");
        json.finishDate = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
        json.finishHour = finishDate[1];
        delete json.productId;

        const response = fetch(dbUrl + '/coupons/' + req.params.Id, {
            method: 'PUT',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                console.log(data);
                if (!Array.isArray(products)) {
                    if (products != "") {
                        let jsonPath = {
                            productId: Number(products),
                            couponId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'coupon-products/', '', jsonPath);
                    }
                } else {
                    products.forEach(element => {
                        let jsonPath = {
                            productId: Number(element),
                            couponId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        }
                        //console.log(jsonPath);
                        sendReqNoRedirect('POST', 'coupon-products/', '', jsonPath);
                    });
                }
                res.redirect('/coupons/');
            })
            .catch(err => {
                if (!Array.isArray(products)) {
                    if (products != "") {
                        let jsonPath = {
                            productId: Number(products),
                            couponId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'coupon-products/', '', jsonPath);
                    }
                } else {
                    products.forEach(element => {
                        let jsonPath = {
                            productId: Number(element),
                            couponId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect('POST', 'coupon-products/', '', jsonPath);
                    });
                }
                res.redirect('/coupons/all');
            }) // Do something with the error

    });
    app.get("/deleteCoupons/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let ans = fetch(dbUrl + '/get-related-coupons/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                let info = {
                    couponProducts: data[0],
                    orderProducts: data[1],
                }
                let is_ok = 1;
                if (info.orderProducts.length > 0) {
                    is_ok = 0;
                }

                if (is_ok === 1) {
                    const response = fetch(dbUrl + '/del-coupons/' + req.params.Id, {
                        method: 'GET',
                        agent: httpsAgent,
                    });
                    response.then(response => response.json())
                        .then(data => {
                            console.log(data);
                            res.redirect('/coupons/ok');
                        });
                } else {
                    res.redirect("/coupons/error-" + req.params.Id)
                }
            });
    });
    app.get("/sendCoupons", (req, res) => {
        let json = {};
        let response = fetch(dbUrl + '/coupons', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                json.coupons = data;
                response = fetch(dbUrl + '/clients', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        json.clients = data;
                        res.render('coupons/send', {
                            title: "AMCOR DESIGN ADMIN | Coupons",
                            subtittle: 'Send Coupons',
                            option: "Coupons",
                            data: json,
                            view: "coupons",
                            subtittle: "Coupons",
                        })
                    })
            })
    });
    //metals
    app.get("/metals/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/metals', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('metals/index', {
                    title: "AMCOR DESIGN ADMIN | Metals",
                    subtittle: 'All Metals',
                    option: "Metals",
                    data: data,
                })
            })
    });
    app.get("/deleteMetals/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-metal-colors/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/metals", "metals", req.params.Id, res);
            });
    });
    app.get("/addMetals/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const ans = fetch(dbUrl + '/metal-colors', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                res.render('metals/create', {
                    title: "AMCOR DESIGN ADMIN | Metals",
                    subtittle: 'Create Metals',
                    option: "Create Metals",
                    data: data,
                    view: "metals",
                    subtittle: "Metals",
                })
            });

    });
    app.post('/newMetals', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.icon = uploadUrl + "metals/" + json.icon;
        json.carat = Number(json.carat);
        json.price = Number(json.price);
        json.metalColorId = Number(json.metalColorId);
        json.createdAt = now;
        sendReq('POST', 'metals/', 'metals/', "", json, res);
    });
    app.get("/editMetals/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let metalcolors;
        const ans = fetch(dbUrl + '/metal-colors', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                metalcolors = data
                const response = fetch(dbUrl + '/metals/' + req.params.Id, {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        res.render('metals/edit', {
                            title: "AMCOR DESIGN ADMIN | Metals",
                            option: "Edit Metals",
                            data: data,
                            metalcolors: metalcolors,
                            view: "metals",
                            subtittle: "Metals",
                        })
                    })
            });
    });
    app.post("/editMetals/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        if (json.icon.length > 0)
            json.icon = uploadUrl + "metals/" + json.icon;
        else
            json.icon = json.oldIcon;
        delete json.oldIcon;
        json.carat = Number(json.carat);
        json.price = Number(json.price);
        json.metalColorId = Number(json.metalColorId);
        json.updatedAt = now;
        sendReq('PUT', 'metals/', 'metals/', req.params.Id, json, res);

    });

    //styles
    app.get("/styles/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/styles', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('styles/index', {
                    title: "AMCOR DESIGN ADMIN | Styles",
                    subtittle: 'All Styles',
                    option: "Styles",
                    data: data,
                })
            })
    });
    app.get("/deleteStyles/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-styles/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/styles", "styles", req.params.Id, res);
            });
    });
    app.get("/addStyles/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('styles/create', {
            title: "AMCOR DESIGN ADMIN | Styles",
            subtittle: 'Create Styles',
            option: "Create Styles",
            view: "styles",
            subtittle: "Styles",
        })

    });
    app.post('/newStyles', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.icon = uploadUrl + "styles/" + json.icon;
        json.createdAt = now;
        sendReq('POST', 'styles/', 'styles/', "", json, res);
    });
    app.get("/editStyles/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/styles/', 'styles', req.params.Id, 'Styles', 'Styles', res);
    });
    app.post("/editStyles/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        if (json.icon.length > 0)
            json.icon = uploadUrl + "styles/" + json.icon;
        else
            json.icon = json.oldIcon;
        delete json.oldIcon;
        json.updatedAt = now;
        sendReq('PUT', 'styles/', 'styles/', req.params.Id, json, res);

    });

    //stoneShapes
    app.get("/stoneShapes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/stone-shapes', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('stoneShapes/index', {
                    title: "AMCOR DESIGN ADMIN | Stone Shapes",
                    subtittle: 'All Stone Shapes',
                    option: "Stone Shapes",
                    data: data,
                })
            })
    });
    app.get("/deleteStoneShapes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-stone-shapes/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/stone-shapes", "stoneShapes", req.params.Id, res);
            });
    });
    app.get("/addStoneShapes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('stoneShapes/create', {
            title: "AMCOR DESIGN ADMIN | Stone Shapes",
            subtittle: 'Create Stone Shapes',
            option: "Create Stone Shapes",
            view: "stoneShapes",
            subtittle: "Stone Shapes",
        })

    });
    app.post('/newStoneShapes', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.icon = uploadUrl + "stone-shapes/" + json.icon;
        json.price = Number(json.price);
        json.percent = Number(json.percent);
        json.createdAt = now;
        sendReq('POST', 'stone-shapes/', 'stoneShapes/', "", json, res);
    });
    app.get("/editStoneShapes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/stone-shapes/', 'stoneShapes', req.params.Id, 'Stone Shapes', 'Stone Shapes', res);
    });
    app.post("/editStoneShapes/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.price = Number(json.price);
        json.percent = Number(json.percent);

        if (json.icon.length > 0)
            json.icon = uploadUrl + "stone-shapes/" + json.icon;
        else
            json.icon = json.oldIcon;
        delete json.oldIcon;

        json.updatedAt = now;
        sendReq('PUT', 'stone-shapes/', 'stoneShapes/', req.params.Id, json, res);

    });
    //stoneColors
    app.get("/stoneColors/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/stone-colors', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('stoneColors/index', {
                    title: "AMCOR DESIGN ADMIN | Stone Colors",
                    subtittle: 'All Stone Colors',
                    option: "Stone Colors",
                    data: data,
                })
            })
    });
    app.get("/deleteStoneColors/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-stone-colors/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/stone-colors", "stoneColors", req.params.Id, res);
            });
    });
    app.get("/addStoneColors/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('stoneColors/create', {
            title: "AMCOR DESIGN ADMIN | Stone Colors",
            subtittle: 'Create Stone Colors',
            option: "Create Stone Colors",
            view: "stoneColors",
            subtittle: "Stone Colors",
        })

    });
    app.post('/newStoneColors', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.icon = uploadUrl + "stone-colors/" + json.icon;
        json.price = Number(json.price);
        json.createdAt = now;
        json.updatedAt = now;
        sendReq('POST', 'stone-colors/', 'stoneColors/', "", json, res);
    });
    app.get("/editStoneColors/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/stone-colors/', 'stoneColors', req.params.Id, 'Stone Colors', 'Stone Colors', res);
    });
    app.post("/editStoneColors/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        if (json.icon.length > 0)
            json.icon = uploadUrl + "stone-colors/" + json.icon;
        else
            json.icon = json.oldIcon;
        delete json.oldIcon;
        json.updatedAt = now;
        json.price = Number(json.price);
        sendReq('PUT', 'stone-colors/', 'stoneColors/', req.params.Id, json, res);

    });
    //stoneSides
    app.get("/stoneSides/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/stone-sides', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('stoneSides/index', {
                    title: "AMCOR DESIGN ADMIN | Stone Sides",
                    subtittle: 'All Stone Sides',
                    option: "Stone Sides",
                    data: data,
                })
            })
    });
    app.get("/deleteStoneSides/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }

        let response = fetch(dbUrl + '/del-stone-sides/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/stone-sides", "stoneSides", req.params.Id, res);
            });
    });

    app.get("/addStoneSides/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('stoneSides/create', {
            title: "AMCOR DESIGN ADMIN | Stone Sides",
            subtittle: 'Create Stone Sides',
            option: "Create Stone Sides",
            view: "stoneSides",
            subtittle: "Stone Sides",
        })

    });
    app.post('/newStoneSides', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.icon = uploadUrl + "stone-sides/" + json.icon;
        json.createdAt = now;
        json.updatedAt = now;
        sendReq('POST', 'stone-sides/', 'stoneSides/', "", json, res);
    });
    app.get("/editStoneSides/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/stone-sides/', 'stoneSides', req.params.Id, 'Stone Sides', 'Stone Sides', res);
    });
    app.post("/editStoneSides/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        if (json.icon.length > 0)
            json.icon = uploadUrl + "stone-sides/" + json.icon;
        else
            json.icon = json.oldIcon;
        delete json.oldIcon;
        json.updatedAt = now;
        sendReq('PUT', 'stone-sides/', 'stoneSides/', req.params.Id, json, res);

    });

    //stoneCenters
    app.get("/stoneCenters/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/stone-centers', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('stoneCenters/index', {
                    title: "AMCOR DESIGN ADMIN | Stone Center",
                    subtittle: 'All Stone Center',
                    option: "Stone Center",
                    data: data,
                })
            })
    });
    app.get("/addStoneCenters", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('stoneCenters/create', {
            title: "AMCOR DESIGN ADMIN | Stone Centers",
            subtittle: 'Create Stone Centers',
            option: "Create Stone Centers",
            view: "stoneCenters",
            subtittle: "Stone Centers",
        })

    });
    app.post('/newStoneCenters', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.icon = uploadUrl + "stone-centers/" + json.icon;
        json.size = Number(json.size);
        json.price = Number(json.price);
        json.createdAt = now;
        json.updatedAt = now;
        sendReq('POST', 'stone-centers/', 'stoneCenters/', "", json, res);
    });
    app.get("/deleteStoneCenters/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-stone-centers/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/stone-centers", "stoneCenters", req.params.Id, res);
            });
    });
    app.get("/editStoneCenters/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/stone-centers/', 'stoneCenters', req.params.Id, 'Stone Centers', 'Stone Centers', res);
    });
    app.post("/editStoneCenters/:Id", (req, res) => {
        let json = req.body;
        const now = new Date().toISOString();
        if (json.icon.length > 0)
            json.icon = uploadUrl + "stone-centers/" + json.icon;
        else
            json.icon = json.oldIcon;
        json.price = Number(json.price);
        json.size = Number(json.size);
        delete json.oldIcon;
        json.updatedAt = now;
        sendReq('PUT', 'stone-centers/', 'stoneCenters/', req.params.Id, json, res);
    });

    //typeSizes
    app.get("/typeSizes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/type-sizes', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                for (var i = 0; i < data.length; i++) {
                    data[i].createdAt = calcDate(new Date(), new Date(data[i].createdAt));
                    data[i].updatedAt = calcDate(new Date(), new Date(data[i].updatedAt));
                }
                res.render('typeSizes/index', {
                    title: "AMCOR DESIGN ADMIN | Type Sizes",
                    subtittle: 'All Type Sizes',
                    option: "Type Sizes",
                    data: data,
                })
            })
    });
    app.get("/addTypeSizes", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('typeSizes/create', {
            title: "AMCOR DESIGN ADMIN | Type Sizes",
            option: "Create Type Sizes",
            view: "typeSizes",
            subtittle: "Type Sizes",
        })

    });

    app.post('/newTypeSizes', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;

        sendReq('POST', 'type-sizes/', 'typeSizes/', "", json, res);
    });
    app.get("/editTypeSizes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/type-sizes/', 'typeSizes', req.params.Id, 'Type Sizes', 'Type sizes', res);
    });
    app.post("/editTypeSizes/:Id", (req, res) => {
        const now = new Date().toISOString();
        const json = {
            id: Number(req.body.id),
            name: req.body.name,
            createdAt: req.body.createdAt,
            updatedAt: now,
        };
        sendReq('PUT', 'type-sizes/', 'typeSizes/', req.params.Id, json, res);
    });
    app.get("/deleteTypeSizes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-type-sizes/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/type-sizes", "typeSizes", req.params.Id, res);
            });
    });
    //sizes
    app.get("/sizes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }

        const response = fetch(dbUrl + '/sizes', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                for (var i = 0; i < data.length; i++) {
                    data[i].createdAt = calcDate(new Date(), new Date(data[i].createdAt));
                    data[i].updatedAt = calcDate(new Date(), new Date(data[i].updatedAt));
                }
                res.render('sizes/index', {
                    title: "AMCOR DESIGN ADMIN | Sizes",
                    subtittle: 'All Sizes',
                    option: "Sizes",
                    data: data,
                })
            })
    });
    app.get("/addSizes", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/type-sizes', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('sizes/create', {
                    title: "AMCOR DESIGN ADMIN | Sizes",
                    option: "Create Sizes",
                    data: data,
                    view: "sizes",
                    subtittle: "Sizes",
                })
            })
    });

    app.post('/newSizes', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        json.price = Number(json.price);
        sendReq('POST', 'sizes/', 'sizes/', "", json, res);
    });
    app.get("/editSizes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/sizes/', 'sizes', req.params.Id, 'Sizes', 'Sizes', res);
    });
    app.post("/editSizes/:Id", (req, res) => {
        const now = new Date().toISOString();
        const json = {
            id: Number(req.body.id),
            number: req.body.number,
            price: Number(req.body.price),
            createdAt: req.body.createdAt,
            updatedAt: now,
        };
        sendReq('PUT', 'sizes/', 'sizes/', req.params.Id, json, res);
    });
    app.get("/deleteSizes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let response = fetch(dbUrl + '/del-sizes/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/sizes", "sizes", req.params.Id, res);
            });

    });
    //size_type_sizes
    app.get("/sizeTypeSizes", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/get-size-type-sizes', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('sizeTypeSizes/index', {
                    title: "AMCOR DESIGN ADMIN | Size Type Sizes",
                    subtittle: 'All Size Type Sizes',
                    option: "Size Type Sizes",
                    view: "sizeTypeSizes",
                    data: data,
                })
            })
    });
    app.get("/editSizeTypeSizes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const id = req.params.Id;
        let info = {};
        let response = fetch(dbUrl + '/size-type-sizes?filter={"where": {"typeSizeId": ' + id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                info.sizeTypeSizes = data;
                response = fetch(dbUrl + '/sizes', {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        info.sizes = data;
                        for (var i = 0; i < info.sizeTypeSizes.length; i++) {
                            var pos = info.sizes.findIndex(item => item.id === info.sizeTypeSizes[i].sizeId);
                            if (pos != -1) {
                                info.sizes[pos].selected = 'selected';
                            }
                        }
                        res.render('sizeTypeSizes/edit', {
                            title: "AMCOR DESIGN ADMIN | Edit Size Type Sizes",
                            subtittle: 'Edit Size Type Sizes',
                            option: "Size Type Sizes",
                            view: "sizeTypeSizes",
                            data: info,
                        })
                    })
            })
    });
    app.post('/editSizeTypeSizes/:Id', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        const response = fetch(dbUrl + '/del-size-type-sizes/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (Array.isArray(json.sizeId)) {
                    json.sizeId.forEach(element => {
                        let data = {
                            typeSizeId: Number(req.params.Id),
                            sizeId: Number(element),
                            createdAt: now,
                            updatedAt: now
                        }
                        sendReqNoRedirect("POST", "size-type-sizes", "", data);
                    });
                } else {
                    json.typeSizeId = Number(req.params.Id);
                    json.sizeId = Number(json.sizeId);
                    json.createdAt = now;
                    json.updatedAt = now;
                    sendReqNoRedirect("POST", "size-type-sizes", "", json);
                }
                setTimeout(() => {
                    res.redirect("/sizeTypeSizes");
                }, 2000);
            })

    });
    app.get("/deleteSizeTypeSizes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/del-size-type-sizes/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.redirect("/sizeTypeSizes");
            })

    });

    //extraVariations
    app.get("/extraVariations/:msg", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const msg = req.params.msg;
        var arra = msg.split("-");
        if (msg.includes("error")) {
            let ans = fetch(dbUrl + '/get-related-extra-variations/' + arra[1], {
                method: 'GET',
                agent: httpsAgent,
            });
            ans.then(response => response.json())
                .then(data => {
                    let info = {
                        variations: data[0],
                        orderProductVariants: data[1],
                    }
                    let error = Array();
                    if (info.variations.length > 0) {
                        error.push("No se puede eliminar el registro ya que tiene variations ligadas, elimine la relación primero en la pantalla de variations.");
                    }
                    if (info.orderProductVariants.length > 0) {
                        error.push("Order products variants are using the extra variation id you are trying to delete.");
                    }
                    const response = fetch(dbUrl + '/extra-variations', {
                        method: 'GET',
                        agent: httpsAgent,
                    });
                    response.then(response => response.json())
                        .then(data => {
                            res.render('extraVariations/index', {
                                title: "AMCOR DESIGN ADMIN | Extra Variations",
                                subtittle: 'All Extra Variations',
                                option: "Extra Variations",
                                msg: arra[0],
                                error: error,
                                data: data
                            })
                        })
                })
        } else {
            const response = fetch(dbUrl + '/extra-variations', {
                method: 'GET',
                agent: httpsAgent,
            });
            response.then(response => response.json())
                .then(data => {
                    res.render('extraVariations/index', {
                        title: "AMCOR DESIGN ADMIN | Extra Variations",
                        subtittle: 'All Extra Variations',
                        option: "Extra Variations",
                        msg: arra[0],
                        data: data
                    })
                })
        }
    });
    app.get("/deleteExtraVariations/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }

        let ans = fetch(dbUrl + '/get-related-extra-variations/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                let info = {
                    variations: data[0],
                    orderProductVariants: data[1],
                }
                let is_ok = 1;
                if (info.variations.length > 0) {
                    is_ok = 0;
                }
                if (info.orderProductVariants.length > 0) {
                    is_ok = 0;
                }
                if (is_ok === 1) {
                    const deleteMethod = {
                        method: 'DELETE', // Method itself
                        headers: {
                            'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                        },
                        agent: httpsAgent,
                    };

                    fetch(dbUrl + '/extra-variations/' + Number(req.params.Id), deleteMethod)
                        .then(response => response.json())
                        .then(data => {
                            res.redirect('/extraVariations/ok');
                        })
                        .catch(err => {
                            res.redirect('/extraVariations/ok');
                        });
                } else {
                    res.redirect("/extraVariations/error-" + req.params.Id)
                }
            });
    });
    app.get("/addExtraVariations", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('extraVariations/create', {
            title: "AMCOR DESIGN ADMIN | Extra Variations",
            option: "Create Extra Variations",
            view: "extraVariations",
            subtittle: "Extra Variations",
        })
    });
    app.post('/newExtraVariations', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        sendReq('POST', 'extra-variations/', 'extraVariations/all', "", json, res);
    });
    app.get("/editExtraVariations/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/extra-variations/', 'extraVariations', req.params.Id, 'Extra Variations', 'Extra Variations', res);
    });
    app.post("/editExtraVariations/:Id", (req, res) => {
        const now = new Date().toISOString();
        const json = req.body;
        json.updatedAt = now;
        sendReq('PUT', 'extra-variations/', 'extraVariations/all', req.params.Id, json, res);
    });
    //Variations
    app.get("/variations/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/variations', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('variations/index', {
                    title: "AMCOR DESIGN ADMIN | Variations",
                    subtittle: 'All Variations',
                    option: "Variations",
                    data: data,
                })
            })
    });
    app.get("/deleteVariations/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        deleteElement("/variations", "variations", req.params.Id, res);
    });
    app.get("/addVariations", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }

        const response = fetch(dbUrl + '/extra-variations', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('variations/create', {
                    title: "AMCOR DESIGN ADMIN | Variations",
                    option: "Create Variations",
                    data: data,
                    view: "variations",
                    subtittle: "Variations",
                })
            })
    });
    app.post('/newVariations', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.createdAt = now;
        json.updatedAt = now;
        json.price = Number(json.price);
        json.extraVariationId = Number(json.extraVariationId);
        json.icon = uploadUrl + "extra-variations/" + json.icon;
        json.zoom = json.exampleRadios + "";
        delete json.exampleRadios;
        sendReq('POST', 'variations/', 'variations/', "", json, res);
    });
    app.get("/editVariations/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let extraV;
        const ans = fetch(dbUrl + '/extra-variations', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                extraV = data;
                const response = fetch(dbUrl + '/variations/' + req.params.Id, {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        res.render('variations/edit', {
                            title: "AMCOR DESIGN ADMIN | Variations",
                            subtittle: 'Edit Variations',
                            option: "Variations",
                            data: data,
                            extra: extraV,
                            view: "variations",
                            subtittle: "Variations",
                        })
                    })
            })
    });
    app.post("/editVariations/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.updatedAt = now;
        json.price = Number(json.price);
        json.extraVariationId = Number(json.extraVariationId);
        if (json.icon.length > 0)
            json.icon = uploadUrl + "extra-variations/" + json.icon;
        else
            json.icon = json.oldIcon;
        delete json.oldIcon;
        json.zoom = json.exampleRadios + "";
        delete json.exampleRadios;
        sendReq('PUT', 'variations/', 'variations/', req.params.Id, json, res);
    });
    //moissanites
    app.get("/moissanites/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/moissanites', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('moissanites/index', {
                    title: "AMCOR DESIGN ADMIN | Moissanites",
                    subtittle: 'All Moissanites',
                    option: "Moissanites",
                    data: data,
                })
            })
    });
    app.get("/deleteMoissanites/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/del-moissanites/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.redirect("/moissanites");
            })
    });
    app.get("/addMoissanites/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('moissanites/create', {
            title: "AMCOR DESIGN ADMIN | Moissanites",
            subtittle: 'Create Moissanites',
            option: "Create Moissanites",
            view: "moissanites",
            subtittle: "Moissanites",
        })

    });
    app.post('/newMoissanites', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.price = Number(json.price);
        var sha1 = require('sha1');
        json.priceHa = sha1(json.price);
        json.createdAt = now;
        json.updatedAt = now;
        if (!json.sizeMm.includes('mm')) {
            json.sizeMm += " mm";
        }
        let pathArray = json.path;
        delete json.path;
        const response = fetch(dbUrl + '/moissanites/', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                pathArray.forEach(element => {
                    let jsonPath = {
                        path: element,
                        moissaniteId: Number(data.id),
                        createdAt: now,
                        updatedAt: now
                    }
                    sendReqNoRedirect('POST', 'moissanite-images/', '', jsonPath);
                });
                res.redirect('/moissanites/');
            })
    });
    app.get("/editMoissanites/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let imgs;
        const ans = fetch(dbUrl + '/moissanite-images?filter=%7B%0A%20%20%22where%22%3A%20%7B%0A%20%20%20%20%22moissaniteId%22%3A%20' + req.params.Id + '%0A%20%20%7D%0A%7D', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                imgs = data;
                const response = fetch(dbUrl + '/moissanites/' + req.params.Id, {
                    method: 'GET',
                    agent: httpsAgent,
                });
                response.then(response => response.json())
                    .then(data => {
                        //console.log(data);
                        res.render('moissanites/edit', {
                            title: "AMCOR DESIGN ADMIN | " + 'Moissanites',
                            option: "Edit " + 'Moissanites',
                            data: data,
                            img: imgs,
                            view: "moissanites",
                            subtittle: "Moissanites",
                        })
                    })
            });
    });
    app.post("/editMoissanites/:Id", (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.price = Number(json.price);
        var sha1 = require('sha1');
        json.priceHa = sha1(json.price);
        json.updatedAt = now;
        if (!json.sizeMm.includes('mm')) {
            json.sizeMm += " mm";
        }
        let idMoi = req.params.Id;
        let pathArray = json.path;
        let imgArray = json.imgId;
        delete json.path;
        delete json.imgId;
        const response = fetch(dbUrl + '/moissanites/' + idMoi, {
            method: 'PUT',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                for (let i = 0; i < pathArray.length; i++) {
                    let jsonPath = {
                        path: pathArray[i],
                        moissaniteId: Number(idMoi),
                        createdAt: now,
                        updatedAt: now
                    }
                    sendReqNoRedirect('PUT', 'moissanite-images/', imgArray[i], jsonPath);
                }
                res.redirect('/moissanites/');
            })
            .catch(err => {
                for (let i = 0; i < pathArray.length; i++) {
                    let jsonPath = {
                        path: pathArray[i],
                        moissaniteId: Number(idMoi),
                        createdAt: now,
                        updatedAt: now
                    }
                    sendReqNoRedirect('PUT', 'moissanite-images/', imgArray[i], jsonPath);
                }
                res.redirect('/moissanites/');
            }) // Do something with the error

    });
    //reviews
    app.get("/reviews/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/reviews', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                res.render('reviews/index', {
                    title: "AMCOR DESIGN ADMIN | Reviews",
                    subtittle: 'All Reviews',
                    option: "Reviews",
                    data: data,
                })
            })
    });
    app.get("/deleteReviews/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/del-reviews/' + req.params.Id, {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                deleteElement("/reviews", "reviews", req.params.Id, res);
            })
    });

    app.get("/addReviews/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        res.render('reviews/create', {
            title: "AMCOR DESIGN ADMIN | Reviews",
            option: "Create Reviews",
            view: "reviews",
            subtittle: "Reviews",
        })
    });
    app.post('/newReviews', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        json.stars = Number(json.stars);
        json.icon = uploadUrl + "reviews/" + json.icon;
        json.publish = json.publish == 'on' ? "true" : "false";
        json.showOnHome = Number(json.showOnHome);
        json.createdAt = now;
        var multipleImages = json.multipleImages;
        delete json.multipleImages;
        var isPrincipal = json.isPrincipal;
        delete json.isPrincipal;
        const response = fetch(dbUrl + '/reviews/', {
            method: 'POST',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                if (!Array.isArray(multipleImages)) {
                    if (multipleImages != "") {
                        let jsonImage = {
                            path: uploadUrl + "reviews/" + multipleImages,
                            reviewId: Number(data.id),
                            isPrincipal: isPrincipal == multipleImages ? 1 : 0,
                            createdAt: now,
                            updatedAt: now
                        };
                        sendReqNoRedirect('POST', 'review-images/', "", jsonImage);
                    }
                } else {
                    for (var i = 0; i < multipleImages.length; i++) {
                        let jsonImage = {
                            path: uploadUrl + "reviews/" + multipleImages[i],
                            isPrincipal: isPrincipal == multipleImages[i] ? 1 : 0,
                            reviewId: Number(data.id),
                            createdAt: now,
                            updatedAt: now
                        };
                        sendReqNoRedirect('POST', 'review-images/', "", jsonImage);
                    }
                }
                res.redirect('/reviews/');
            })
            .catch(err => {
                //console.log(err);
                res.redirect('/' + view);
            }) // Do something with the error
    });

    app.get("/editReviews/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        let images;
        const ans = fetch(dbUrl + '/review-images?filter={"where": {"reviewId": ' + req.params.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                images = data;
                const response = fetch(dbUrl + '/reviews/' + req.params.Id, {
                    method: 'GET',
                    agent: httpsAgent,
                });
                let principal = Array();
                images.forEach((element, i) => {
                    console.log(element);
                    console.log(i);
                    var arr = element.path.split("/");
                    let posicion = arr.length;
                    let selected = images[i].isPrincipal == 1 ? 'selected' : '';
                    principal.push({ name: arr[posicion - 1], selected: selected })
                });
                response.then(response => response.json())
                    .then(data => {
                        res.render('reviews/edit', {
                            title: "AMCOR DESIGN ADMIN | Reviews",
                            option: "Edit Reviews",
                            data: data,
                            images: images,
                            principal,
                            principal,
                            view: "reviews",
                            subtittle: "Reviews",
                        })
                    })
            });
    });
    app.post('/editReviews/:Id', (req, res) => {
        const now = new Date().toISOString();
        let json = req.body;
        var isPrincipal = json.isPrincipal;
        delete json.isPrincipal;

        const ans = fetch(dbUrl + '/review-images?filter={"where": {"reviewId": ' + req.params.Id + '}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        ans.then(response => response.json())
            .then(data => {
                var images = data;
                const deleteMethod = {
                    method: 'DELETE', // Method itself
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8', // Indicates the content
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
                    },
                    agent: httpsAgent,
                };
                images.forEach(data => {
                    fetch(dbUrl + '/review-images/' + data.id, deleteMethod)
                        .then(response => response.json())
                        .then(data => {
                            //console.log(data);
                        })
                        .catch(err => {
                            //console.log(err);
                        })
                });
            });
        json.stars = Number(json.stars);
        json.showOnHome = Number(json.showOnHome);
        if (!json.createdAt)
            json.createdAt = now
        json.updatedAt = now;
        if (json.icon == '') {
            json.icon = json.iconOld;
        } else {
            json.icon = uploadUrl + "reviews/" + json.icon;
        }

        delete json.iconOld;
        json.publish = json.publish == 'on' ? "true" : "false";
        json.updatedAt = now;
        var multipleImages = (json.multipleImages == '' || json.multipleImages == null) ? json.oldMultipleImages : json.multipleImages;
        delete json.multipleImages;
        delete json.oldMultipleImages;

        const response = fetch(dbUrl + '/reviews/' + req.params.Id, {
            method: 'PUT',
            agent: httpsAgent,
            headers: {
                'Content-Type': 'application/json' // Indicates the content
            },
            body: JSON.stringify(json),
        });
        response.then(response => response.json())
            .then(data => {
                if (!Array.isArray(multipleImages)) {
                    var arr = multipleImages.split("/");
                    var cont = arr.length;
                    if (cont == 1) {
                        multipleImages = uploadUrl + "reviews/" + multipleImages;
                    }
                    if (multipleImages != "") {
                        let jsonImage = {
                            path: multipleImages,
                            isPrincipal: isPrincipal == arr[cont - 1] ? 1 : 0,
                            reviewId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        };
                        sendReqNoRedirect('POST', 'review-images/', "", jsonImage);
                    }
                } else {
                    for (var i = 0; i < multipleImages.length; i++) {
                        var arr = multipleImages[i].split("/");
                        var cont = arr.length;
                        if (cont == 1) {
                            multipleImages[i] = uploadUrl + "reviews/" + multipleImages[i];
                        }
                        let jsonImage = {
                            path: multipleImages[i],
                            isPrincipal: isPrincipal == arr[cont - 1] ? 1 : 0,
                            reviewId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        };
                        sendReqNoRedirect('POST', 'review-images/', "", jsonImage);
                    }
                }
                res.redirect('/reviews/');
            })
            .catch(err => {
                console.log(multipleImages);
                if (!Array.isArray(multipleImages)) {
                    if (multipleImages != "" && multipleImages != null) {
                        var arr = multipleImages.split("/");
                        var cont = arr.length;
                        if (cont == 1) {
                            multipleImages = uploadUrl + "reviews/" + multipleImages;
                        }
                        let jsonImage = {
                            path: multipleImages,
                            isPrincipal: isPrincipal == arr[cont - 1] ? 1 : 0,
                            reviewId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        };
                        sendReqNoRedirect('POST', 'review-images/', "", jsonImage);
                    }
                } else {
                    for (var i = 0; i < multipleImages.length; i++) {
                        var arr = multipleImages[i].split("/");
                        var cont = arr.length;
                        if (cont == 1) {
                            multipleImages[i] = uploadUrl + "reviews/" + multipleImages[i];
                        }
                        let jsonImage = {
                            path: multipleImages[i],
                            isPrincipal: isPrincipal == arr[cont - 1] ? 1 : 0,
                            reviewId: Number(req.params.Id),
                            createdAt: now,
                            updatedAt: now
                        };
                        sendReqNoRedirect('POST', 'review-images/', "", jsonImage);
                    }
                }
                res.redirect('/reviews/');

            }) // Do something with the error
    });

    //subscribes
    app.get("/subscribes/", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        const response = fetch(dbUrl + '/subscribes', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                for (var i = 0; i < data.length; i++) {
                    data[i].createdAt = calcDate(new Date(), new Date(data[i].createdAt));
                    data[i].updatedAt = calcDate(new Date(), new Date(data[i].updatedAt));
                }
                res.render('subscribes/index', {
                    title: "AMCOR DESIGN ADMIN | Subscribes",
                    option: "Subscribed",
                    data: data,
                })
            })
    });
    app.get("/editSubscribes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        editPage('/subscribes/', 'subscribes', req.params.Id, 'Subscrbies', 'Subscribe', res);
    });

    app.post("/editSubscribes/:Id", (req, res) => {
        const now = new Date().toISOString();
        //console.log(JSON.stringify(req.body));
        const json = {
            id: Number(req.body.id),
            email: req.body.email,
            createdAt: req.body.createdAt,
            updatedAt: now,
        };
        sendReq('PUT', 'subscribes/', 'subscribes/', req.params.Id, json, res);
    });

    app.get("/deleteSubscribes/:Id", (req, res) => {
        if (!req.session.user) {
            res.redirect('/login')
        }
        deleteElement("/subscribes", "subscribes", req.params.Id, res);
    });

    app.post("/uploadFile/:Id", (req, res) => {
        var type = req.params.Id + "";
        if (type.includes('--')) {
            type = replaceall("--", '/', type);
        }
        if (!fs.existsSync("./public/upload/" + type)) {
            fs.mkdirSync("./public/upload/" + type, { recursive: true });
        }
        var response = saveImg("./public/upload/" + type, req);
        res.json(response);
    });

    app.get("/login", (req, res) => {
        res.render('login/index', {
            title: "AMCOR DESIGN ADMIN | Login",
            option: "login",
            msg: ''
        })
    });

    app.get("/recoverAccount", (req, res) => {
        res.render('recoverAccount/index', {
            title: "AMCOR DESIGN ADMIN | Recover Account",
            option: "login",
            msg: ''
        })
    });

    app.post("/recoverAccount", (req, res) => {
        let email = req.body.email;
        const response = fetch(dbUrl + '/users?filter={"where":{"and":[{"email":"' + email + '"}]}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data[0]) {
                    let id = data[0].id;
                    delete data[0].id;
                    delete data[0].emailVerifiedAt;
                    delete data[0].stripeId;
                    delete data[0].cardLastFour;
                    delete data[0].trialEndsAt;
                    delete data[0].cardBrand;
                    let json = data[0];
                    json.rememberToken = md5(randomstring.generate(75))

                    sendReqNoRedirect("PUT", "users/", id, json)
                    let html = "<h2>You recently requested a password reset with us.</h2>";
                    html += "<p>Please click <a href='" + recoverUrl + json.rememberToken + '/' + email + "'>here</a> to be redirected to a page to reset your password. Thanks!</p>";
                    sendEmail(
                        email,
                        html,
                        "Recover your account ✔");
                }
            })
        res.render('recoverAccount/index', {
            title: "AMCOR DESIGN ADMIN |  Recover Account",
            option: "login",
            msg: "If the email is right and if you have an account, we have sent you an email in order to recover your account."
        })
    });

    app.get("/recover_acc/:token/:email", (req, res) => {
        res.render('recoverAcc/index', {
            title: "AMCOR DESIGN ADMIN | Recover Account",
            option: "recover account",
            email: req.params.email
        })
    });

    app.post("/recover_acc/:token/:email", (req, res) => {
        let json = req.body;
        delete json.passwordConfirmation;
        const response = fetch(dbUrl + '/users?filter={"where":{"and":[{"email":"' + req.params.email + '"},{"rememberToken":"' + req.params.token + '"}]}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data[0]) {
                    var sha1 = require('sha1');
                    let id = data[0].id;
                    delete data[0].id;
                    delete data[0].emailVerifiedAt;
                    delete data[0].stripeId;
                    delete data[0].cardLastFour;
                    delete data[0].trialEndsAt;
                    delete data[0].cardBrand;
                    delete data[0].rememberToken;
                    let body = data[0];
                    body.password = sha1(json.password);
                    const now = new Date().toISOString();
                    body.updatedAt = now;
                    sendReqNoRedirect("PUT", "users/", id, body)
                }
            })
        res.redirect("/login");
    });


    app.post("/login", (req, res) => {
        let json = req.body;
        var sha1 = require('sha1');
        json.passwordSha1 = sha1(json.password);
        const response = fetch(dbUrl + '/users?filter={"where":{"and":[{"email":"' + json.email + '"},{"password":"' + json.passwordSha1 + '"}]}}', {
            method: 'GET',
            agent: httpsAgent,
        });
        response.then(response => response.json())
            .then(data => {
                if (data[0]) {
                    req.session.user = data[0];
                    // //console.log(req.session.user);
                    req.session.save(function (err) {
                        //console.log(err);
                        res.redirect('/')
                    })
                } else {
                    res.render('login/index', {
                        title: "AMCOR DESIGN ADMIN | Login",
                        option: "login",
                        msg: "Invalid credentials, try again."
                    })
                }
            })
    });
    app.get("/logout", (req, res) => {
        req.session.destroy();
        res.render('login/index', {
            title: "AMCOR DESIGN ADMIN | Login",
            option: "login",
            msg: "You have been logged out."
        })
    });
}

function calcDate(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);
    var diff = Math.floor(date1.getTime() - date2.getTime());
    var day = 1000 * 60 * 60 * 24;

    var days = parseInt(Math.floor(diff / day));
    var weeks = parseInt(Math.floor(days / 7));
    var months = parseInt(Math.floor(days / 30));
    var years = parseInt(Math.floor(months / 12));
    var message = "";

    if (days < 7) {
        message = days + " days ";
    }
    if (days > 6 && weeks < 4) {
        message = weeks + " weeks ";
    }
    if (weeks > 3 && months < 12) {
        message = months + " months ";
    }
    if (years >= 1) {
        message = years + " years ";
    }
    return message
}

function deleteElement(serviceName, view, id, res) {
    const deleteMethod = {
        method: 'DELETE', // Method itself
        headers: {
            'Content-type': 'application/json; charset=UTF-8', // Indicates the content
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
        },
        agent: httpsAgent,
    };
    fetch(dbUrl + serviceName + '/' + id, deleteMethod)
        .then(response => response.json())
        .then(data => {
            //console.log(data);
            res.redirect('/' + view + '/');
        })
        .catch(err => {
            //console.log(err);
            res.redirect('/' + view + '/');
        })
}

function editPage(serviceName, view, Id, tittle, option, res) {
    const response = fetch(dbUrl + serviceName + Id, {
        method: 'GET',
        agent: httpsAgent,
    });
    response.then(response => response.json())
        .then(data => {
            //console.log(data);
            res.render(view + '/edit', {
                title: "AMCOR DESIGN ADMIN | " + tittle,
                option: "Edit " + option,
                view: view.toLowerCase(),
                subtittle: tittle,
                data: data
            })
        })
}

function sendReq(method, serviceName, view, id, json, res) {
    //console.log(JSON.stringify(json));
    const response = fetch(dbUrl + "/" + serviceName + id, {
        method: method,
        agent: httpsAgent,
        headers: {
            'Content-Type': 'application/json' // Indicates the content
        },
        body: JSON.stringify(json),
    });
    response.then(response => response.json())
        .then(data => {
            //console.log(data);
            res.redirect('/' + view);
        })
        .catch(err => {
            //console.log(err);
            res.redirect('/' + view);
        }) // Do something with the error
}

function sendReqNoRedirect(method, serviceName, id, json) {
    //console.log(serviceName);
    console.log(JSON.stringify(json));
    const response = fetch(dbUrl + "/" + serviceName + id, {
        method: method,
        agent: httpsAgent,
        headers: {
            'Content-Type': 'application/json' // Indicates the content
        },
        body: JSON.stringify(json),
    });
    response.then(response => response.json())
        .then(data => {
            console.log(serviceName + " el json es: " + JSON.stringify(json));
            //console.log(data);
        })
        .catch(err => {
            // console.log(err);
        }) // Do something with the error
}

const sendEmail = async function (receiver, html, subject) {
    try {
        let info = await transporter.sendMail({
            from: '"Amcoredesign" <' + emailUser + '>', // sender address
            to: receiver, // list of receivers
            subject: subject, // Subject line
            html: html, // html body
        });
        console.log(info);

        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    } catch (error) {
        console.log(error);
    }


}

function saveImg(ruta, req) {
    var res = [];
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        if (filename.includes('.zip')) {
            filename = "file.zip";
        }
        var fstream = fs.createWriteStream(ruta + "/" + filename);
        res.push(filename);
        file.pipe(fstream);
        fstream.on('close', function () {
            res = [
                '200',
                'Carga exitosa'
            ];
            return res;
        });
    });
    return res;

}